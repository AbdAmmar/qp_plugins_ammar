program ij_Sdiag_kl 

  implicit none

  read_wf = .True.
  TOUCH read_wf

  PROVIDE N_int

  call S_svd_to_det()

end

! ---

subroutine S_svd_to_det()

  implicit none
  integer                       :: i, j, k, l, a, b, n_svd
  double precision, allocatable :: U(:,:), V(:,:)
  double precision, allocatable :: Sdiag_svd(:,:)
  double precision, allocatable :: tmp(:,:,:), Sdet(:,:,:,:)


  allocate( U(n_det_alpha_unique,n_det_alpha_unique) )
  allocate( V(n_det_beta_unique,n_det_beta_unique) )

  open(unit=11, form="unformatted", file='U_unf', action="read")
    do i = 1, n_det_alpha_unique
      read(11) U(:,i)
    enddo
  close(11)
  open(unit=11, form="unformatted", file='V_unf', action="read")
    do i = 1, n_det_beta_unique
      read(11) V(:,i)
    enddo
  close(11)

  !n_svd = min(n_det_alpha_unique, n_det_beta_unique)
  call ezfio_get_spindeterminants_n_svd_coefs(n_svd)

  ! read overlap matrix
  allocate( Sdiag_svd(n_svd,n_svd) )

  Sdiag_svd(:,:) = 0.d0
  open(unit=11, file="Ci_osvd_diag", action="read")
    do l = 1, n_svd
      do k = 1, n_svd
        read(11, *) Sdiag_svd(k,l)
      enddo
    enddo
  close(11)

  allocate( tmp(n_det_alpha_unique,n_det_beta_unique,n_svd) )

  tmp(:,:,:) = 0.d0
  do b = 1, n_svd
    do j = 1, n_det_beta_unique
      do i = 1, n_det_alpha_unique

        tmp(i,j,b) = 0.d0
        do a = 1, n_svd
          tmp(i,j,b) += U(i,a) * V(j,a) * Sdiag_svd(a,b)
        enddo

      enddo
    enddo
  enddo

  deallocate( Sdiag_svd )

  allocate( Sdet(n_det_alpha_unique,n_det_beta_unique,n_det_alpha_unique,n_det_beta_unique) )

  Sdet(:,:,:,:) = 0.d0
  do i = 1, n_det_alpha_unique
    do j = 1, n_det_beta_unique
      do k = 1, n_det_alpha_unique
        do l = 1, n_det_beta_unique

          Sdet(i,j,k,l) = 0.d0
          do b = 1, n_svd
            Sdet(i,j,k,l) += tmp(i,j,b) * U(k,b) * V(l,b)
          enddo

          !write(111, *) Sdet(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  
  deallocate( tmp )
  deallocate( U, V )

  open(unit=11, file="Ci_odet_diag", action="write")
    do a = 1, N_det
      i = psi_bilinear_matrix_rows   (a)
      j = psi_bilinear_matrix_columns(a)
      do b = 1, N_det
        k = psi_bilinear_matrix_rows   (b)
        l = psi_bilinear_matrix_columns(b)

        write(11, *) Sdet(i,j,k,l)
      enddo
    enddo
  close(11)

  deallocate( Sdet )

end subroutine S_svd_to_det 
