program ij_vec 

  implicit none

  read_wf = .True.
  TOUCH read_wf

  PROVIDE N_int

  call run()

end

! ---

subroutine run()

  implicit none
  integer                       :: i, j
  integer                       :: a, b
  integer                       :: n_svd_a, n_svd_b
  character(len = 20)           :: file_det, file_svd
  double precision, allocatable :: U(:,:), V(:,:)
  double precision, allocatable :: vec_svd(:,:), vec_det(:,:)

  read *, file_svd, file_det

  call ezfio_get_spindeterminants_n_svd_alpha(n_svd_a)
  call ezfio_get_spindeterminants_n_svd_beta (n_svd_b)

  allocate( U(n_det_alpha_unique,n_svd_a) )
  allocate( V(n_det_beta_unique ,n_svd_b) )
  open(unit=11, form="unformatted", file='U_unf', action="read")
    do i = 1, n_svd_a
      read(11) U(:,i)
    enddo
  close(11)
  open(unit=11, form="unformatted", file='V_unf', action="read")
    do i = 1, n_svd_b
      read(11) V(:,i)
    enddo
  close(11)


  allocate( vec_svd(n_svd_a,n_svd_b) )
  vec_svd(:,:) = 0.d0
  open(unit=11, file=file_svd, action="read")
    do a = 1, n_svd_a
      do b = 1, n_svd_b
        read(11, *) vec_svd(a,b)
      enddo
    enddo
  close(11)


  allocate( vec_det(n_det_alpha_unique,n_det_beta_unique) )
  vec_det(:,:) = 0.d0
  do i = 1, n_det_alpha_unique
    do j = 1, n_det_beta_unique
      do a = 1, n_svd_a
        do b = 1, n_svd_b
          vec_det(i,j) += vec_svd(a,b) * U(i,a) * V(j,b)
        enddo
      enddo
    enddo
  enddo

  deallocate( vec_svd )
  deallocate( U, V )

  open(unit=11, file=file_det, action="write")
    do a = 1, N_det
      i = psi_bilinear_matrix_rows   (a)
      j = psi_bilinear_matrix_columns(a)

      write(11, *) vec_det(i,j)
    enddo
  close(11)

  deallocate( vec_det )

end subroutine run
