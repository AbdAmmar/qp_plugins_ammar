program ij_S_kl 

  implicit none

  read_wf = .True.
  TOUCH read_wf

  PROVIDE N_int

  call run()

end

! ---

subroutine run()

  implicit none
  integer                       :: i, j, k, l
  integer                       :: a, b, c, d
  integer                       :: n_svd_a, n_svd_b
  double precision, allocatable :: U(:,:), V(:,:)
  double precision, allocatable :: S_svd(:,:,:,:)
  double precision, allocatable :: tmp(:,:,:,:), Sdet(:,:,:,:)

  !n_svd_a = n_svd_alpha !n_det_alpha_unique !10 
  !n_svd_b = n_svd_beta  !n_det_beta_unique  !10 

  call ezfio_get_spindeterminants_n_svd_alpha(n_svd_a)
  call ezfio_get_spindeterminants_n_svd_beta (n_svd_b)

  allocate( U(n_det_alpha_unique,n_svd_a) )
  allocate( V(n_det_beta_unique ,n_svd_b) )

  open(unit=11, form="unformatted", file='U_unf', action="read")
    do i = 1, n_svd_a
      read(11) U(:,i)
    enddo
  close(11)
  open(unit=11, form="unformatted", file='V_unf', action="read")
    do i = 1, n_svd_b
      read(11) V(:,i)
    enddo
  close(11)

  ! read overlap matrix

  allocate( S_svd(n_svd_a,n_svd_b,n_svd_a,n_svd_b) )

  S_svd(:,:,:,:) = 0.d0
  open(unit=11, file="Ci_osvd", action="read")
    do a = 1, n_svd_a
      do b = 1, n_svd_b
        do c = 1, n_svd_a
          do d = 1, n_svd_b
            read(11, *) S_svd(a,b,c,d)
          enddo
        enddo
      enddo
    enddo
  close(11)

  allocate( tmp(n_det_alpha_unique,n_det_beta_unique,n_svd_a,n_svd_b) )

  tmp(:,:,:,:) = 0.d0
  do a = 1, n_svd_a
    do b = 1, n_svd_b
      do k = 1, n_det_alpha_unique
        do l = 1, n_det_beta_unique

          tmp(k,l,a,b) = 0.d0
          do c = 1, n_svd_a
            do d = 1, n_svd_b
              tmp(k,l,a,b) += S_svd(a,b,c,d) * U(k,c) * V(l,d)
            enddo
          enddo

        enddo
      enddo
    enddo
  enddo

  deallocate( S_svd )

  allocate( Sdet(n_det_alpha_unique,n_det_beta_unique,n_det_alpha_unique,n_det_beta_unique) )

  Sdet(:,:,:,:) = 0.d0
  do i = 1, n_det_alpha_unique
    do j = 1, n_det_beta_unique
      do k = 1, n_det_alpha_unique
        do l = 1, n_det_beta_unique

          Sdet(i,j,k,l) = 0.d0
          do a = 1, n_svd_a
            do b = 1, n_svd_b
              Sdet(i,j,k,l) += tmp(k,l,a,b) * U(i,a) * V(j,b)
            enddo
          enddo

          !write(111, *) Sdet(i,j,k,l)
        enddo
      enddo
    enddo
  enddo
  
  deallocate( tmp )
  deallocate( U, V )

  open(unit=11, file="Ci_odet", action="write")
    do a = 1, N_det
      i = psi_bilinear_matrix_rows   (a)
      j = psi_bilinear_matrix_columns(a)
      do b = 1, N_det
        k = psi_bilinear_matrix_rows   (b)
        l = psi_bilinear_matrix_columns(b)

        write(11, *) Sdet(i,j,k,l)
      enddo
    enddo
  close(11)

  deallocate( Sdet )

end subroutine run
