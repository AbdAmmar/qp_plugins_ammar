program err_estim 

  implicit none

  read_wf = .True.
  TOUCH read_wf

  PROVIDE N_int

  call S_svd_to_det()

end

! ---

subroutine S_svd_to_det()

  implicit none
  integer                       :: i, j, k, l
  integer                       :: a, b, c, d
  integer                       :: n_svd, n_svd_a, n_svd_b
  character(len = 30)           :: name_file
  character(len = 5 )           :: charI
  double precision              :: v_full, v_app, err_as, err_rl
  double precision, allocatable :: U(:,:), V(:,:)
  double precision, allocatable :: S_svd(:,:,:,:)
  double precision, allocatable :: tmp3(:,:,:), tmp4(:,:,:,:)
  double precision, allocatable :: Sdet_diag(:,:,:,:), Sdet_trunc(:,:,:,:), Sdet_full(:,:,:,:)


  ! ---

  allocate( U(n_det_alpha_unique,n_det_alpha_unique) )
  allocate( V(n_det_beta_unique,n_det_beta_unique) )

  U(:,:) = 0.d0
  V(:,:) = 0.d0

  open(unit=11, form="unformatted", file='U_unf', action="read")
    do i = 1, n_det_alpha_unique
      read(11) U(:,i)
    enddo
  close(11)
  open(unit=11, form="unformatted", file='V_unf', action="read")
    do i = 1, n_det_beta_unique
      read(11) V(:,i)
    enddo
  close(11)

  ! ---

  allocate( S_svd(n_det_alpha_unique,n_det_beta_unique,n_det_alpha_unique,n_det_beta_unique) )

  S_svd(:,:,:,:) = 0.d0
  open(unit=11, file="Ci_osvd", action="read")
    do a = 1, n_det_alpha_unique 
      do b = 1, n_det_beta_unique
        do c = 1, n_det_alpha_unique
          do d = 1, n_det_beta_unique
            read(11, *) S_svd(a,b,c,d)
          enddo
        enddo
      enddo
    enddo
  close(11)

  ! ---

  allocate( tmp4(n_det_alpha_unique,n_det_beta_unique,n_det_alpha_unique,n_det_beta_unique) )

  tmp4(:,:,:,:) = 0.d0
  do a = 1, n_det_alpha_unique
    do b = 1, n_det_beta_unique
      do k = 1, n_det_alpha_unique
        do l = 1, n_det_beta_unique
          do c = 1, n_det_alpha_unique
            do d = 1, n_det_beta_unique
              tmp4(k,l,a,b) += S_svd(a,b,c,d) * U(k,c) * V(l,d)
            enddo
          enddo
        enddo
      enddo
    enddo
  enddo

  allocate( Sdet_full(n_det_alpha_unique,n_det_beta_unique,n_det_alpha_unique,n_det_beta_unique) )

  Sdet_full(:,:,:,:) = 0.d0
  do i = 1, n_det_alpha_unique
    do j = 1, n_det_beta_unique
      do k = 1, n_det_alpha_unique
        do l = 1, n_det_beta_unique
          do a = 1, n_det_alpha_unique
            do b = 1, n_det_beta_unique
              Sdet_full(i,j,k,l) += tmp4(k,l,a,b) * U(i,a) * V(j,b)
            enddo
          enddo
        enddo
      enddo
    enddo
  enddo

  deallocate( tmp4 )

  open(unit=11, file="Sdet_full", action="write")
   do a = 1, N_det
     i = psi_bilinear_matrix_rows   (a)
     j = psi_bilinear_matrix_columns(a)
     do b = 1, N_det
       k = psi_bilinear_matrix_rows   (b)
       l = psi_bilinear_matrix_columns(b)

       v_full = Sdet_full(i,j,k,l)
       write(11, *) v_full
     enddo
   enddo
  close(11)

  ! ---

  do n_svd_a = 1, n_det_alpha_unique, 2

    n_svd_b = n_svd_a

    allocate( tmp4(n_det_alpha_unique,n_det_beta_unique,n_svd_a,n_svd_b) )
    tmp4(:,:,:,:) = 0.d0
    do a = 1, n_svd_a
      do b = 1, n_svd_b
        do k = 1, n_det_alpha_unique
          do l = 1, n_det_beta_unique
            do c = 1, n_svd_a
              do d = 1, n_svd_b
                tmp4(k,l,a,b) += S_svd(a,b,c,d) * U(k,c) * V(l,d)
              enddo
            enddo
          enddo
        enddo
      enddo
    enddo

    allocate( Sdet_trunc(n_det_alpha_unique,n_det_beta_unique,n_det_alpha_unique,n_det_beta_unique) )
    Sdet_trunc(:,:,:,:) = 0.d0
    do i = 1, n_det_alpha_unique
      do j = 1, n_det_beta_unique
        do k = 1, n_det_alpha_unique
          do l = 1, n_det_beta_unique
            do a = 1, n_svd_a
              do b = 1, n_svd_b
                Sdet_trunc(i,j,k,l) += tmp4(k,l,a,b) * U(i,a) * V(j,b)
              enddo
            enddo
          enddo
        enddo
      enddo
    enddo
    deallocate( tmp4 )

    write(charI, '(I5)') n_svd_a
    write(name_file, '("Sdet_trunc_", A, ".out")') trim(adjustl(charI))
    open(unit=11, file=name_file, action="write")
      do a = 1, N_det
        i = psi_bilinear_matrix_rows   (a)
        j = psi_bilinear_matrix_columns(a)
        do b = 1, N_det
          k = psi_bilinear_matrix_rows   (b)
          l = psi_bilinear_matrix_columns(b)

          v_full = Sdet_full (i,j,k,l)
          v_app  = Sdet_trunc(i,j,k,l)
          err_as = dabs(v_full - v_app) 
          err_rl = 100.d0 * err_as / dabs(v_full)
          write(11, *) v_app, err_as, err_rl
        enddo
      enddo
    close(11)

    deallocate( Sdet_trunc )

  enddo

  ! ---

  n_svd = min(n_det_alpha_unique, n_det_beta_unique)

  allocate( tmp3(n_det_alpha_unique,n_det_beta_unique,n_svd) )

  tmp3(:,:,:) = 0.d0
  do b = 1, n_svd
    do j = 1, n_det_beta_unique
      do i = 1, n_det_alpha_unique
        do a = 1, n_svd
          tmp3(i,j,b) += U(i,a) * V(j,a) * S_svd(a,a,b,b)
        enddo
      enddo
    enddo
  enddo

  deallocate( S_svd )

  allocate( Sdet_diag(n_det_alpha_unique,n_det_beta_unique,n_det_alpha_unique,n_det_beta_unique) )
  Sdet_diag(:,:,:,:) = 0.d0
  do i = 1, n_det_alpha_unique
    do j = 1, n_det_beta_unique
      do k = 1, n_det_alpha_unique
        do l = 1, n_det_beta_unique
          do b = 1, n_svd
            Sdet_diag(i,j,k,l) += tmp3(i,j,b) * U(k,b) * V(l,b)
          enddo
        enddo
      enddo
    enddo
  enddo
  deallocate( tmp3 )

  deallocate( U, V )

  open(unit=11, file="Sdet_diag", action="write")
    do a = 1, N_det
      i = psi_bilinear_matrix_rows   (a)
      j = psi_bilinear_matrix_columns(a)
      do b = 1, N_det
        k = psi_bilinear_matrix_rows   (b)
        l = psi_bilinear_matrix_columns(b)

        v_full = Sdet_full(i,j,k,l)
        v_app  = Sdet_diag(i,j,k,l)
        err_as = dabs(v_full - v_app) 
        err_rl = 100.d0 * err_as / dabs(v_full)
        write(11, *) v_app, err_as, err_rl
      enddo
    enddo
  close(11)

  deallocate( Sdet_diag )

  ! ---

  deallocate( Sdet_full )

end subroutine S_svd_to_det
