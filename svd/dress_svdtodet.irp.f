program dress_svdtodet 

  implicit none

  read_wf = .True.
  touch read_wf

  !call delta_svdtodet()
  call delta_svdtodet_openmp()

end 

! ---

subroutine delta_svdtodet_openmp()

  implicit none

  integer                       :: i, j, ii, jj, a, b
  integer                       :: n_svd_a, n_svd_b
  integer                       :: la, lb, ld, lla, llb, lld, n_dress_svd
  character(len = 35)           :: file_det, file_svd
  double precision, allocatable :: U(:,:), V(:,:)
  double precision, allocatable :: delta_svd(:), delta_det(:)


  read *, file_svd, file_det


  call ezfio_get_spindeterminants_n_svd_alpha(n_svd_a)
  call ezfio_get_spindeterminants_n_svd_beta (n_svd_b)

  allocate( U(n_det_alpha_unique,n_svd_a) )
  allocate( V(n_det_beta_unique ,n_svd_b) )
  open(unit=11, form="unformatted", file='U_unf', action="read")
    do i = 1, n_svd_a
      read(11) U(:,i)
    enddo
  close(11)
  open(unit=11, form="unformatted", file='V_unf', action="read")
    do i = 1, n_svd_b
      read(11) V(:,i)
    enddo
  close(11)


  call ezfio_get_dmc_dress_la(la)
  call ezfio_get_dmc_dress_lb(lb)
  call ezfio_get_dmc_dress_ld(ld)
  call ezfio_get_dmc_dress_lla(lla)
  call ezfio_get_dmc_dress_llb(llb)
  call ezfio_get_dmc_dress_lld(lld)
  n_dress_svd = ( ld * ld       &
                + lld - ld      &
                + (lla-ld) * lb &
                + (llb-ld) * la )

  allocate( delta_svd(n_dress_svd) )
  delta_svd(:) = 0.d0
  open(unit=11, file=file_svd, action="read")
    do jj = 1, n_dress_svd
      read(11, *) delta_svd(jj)
    enddo
  close(11)


  allocate( delta_det(N_det) )
  delta_det(:) = 0.d0

 !$OMP PARALLEL DO DEFAULT(NONE) SCHEDULE(dynamic,8)                 &
 !$OMP SHARED( N_det, la, lb, ld, lla, llb, lld                      &
 !$OMP       , psi_bilinear_matrix_rows, psi_bilinear_matrix_columns &
 !$OMP       , delta_det, delta_svd, U, V)                           &
 !$OMP PRIVATE(ii, i, j, jj, a, b)
  do ii = 1, N_det

    i  = psi_bilinear_matrix_rows   (ii)
    j  = psi_bilinear_matrix_columns(ii)
    jj = 0

    do a = 1, ld
      do b = 1, ld
        jj = jj + 1
        delta_det(ii) = delta_det(ii) + U(i,a) * V(j,b) * delta_svd(jj)
      enddo
    enddo

    do a = ld+1, lld
      b  = a
      jj = jj + 1
      delta_det(ii) = delta_det(ii) + U(i,a) * V(j,b) * delta_svd(jj)
    enddo

    do a = ld+1, lla
      do b = 1, lb
        jj = jj + 1
        delta_det(ii) = delta_det(ii) + U(i,a) * V(j,b) * delta_svd(jj)
      enddo
    enddo

    do a = 1, la
      do b = ld+1, llb
        jj = jj + 1
        delta_det(ii) = delta_det(ii) + U(i,a) * V(j,b) * delta_svd(jj)
      enddo
    enddo

  enddo
 !$OMP END PARALLEL DO

  deallocate( U, V )
  deallocate( delta_svd )

  open(unit=11, file=file_det, action="write")
    do ii = 1, N_det
      write(11, *) delta_det(ii)
    enddo
  close(11)

end subroutine delta_svdtodet_openmp

! ---

subroutine delta_svdtodet()

  implicit none

  integer                       :: i, j, ii, jj, a, b
  integer                       :: n_svd_a, n_svd_b
  integer                       :: la, lb, ld, lla, llb, lld, n_dress_svd
  character(len = 35)           :: file_det, file_svd
  double precision, allocatable :: U(:,:), V(:,:)
  double precision, allocatable :: delta_svd(:), delta_det(:)


  read *, file_svd, file_det


  call ezfio_get_spindeterminants_n_svd_alpha(n_svd_a)
  call ezfio_get_spindeterminants_n_svd_beta (n_svd_b)

  allocate( U(n_det_alpha_unique,n_svd_a) )
  allocate( V(n_det_beta_unique ,n_svd_b) )
  open(unit=11, form="unformatted", file='U_unf', action="read")
    do i = 1, n_svd_a
      read(11) U(:,i)
    enddo
  close(11)
  open(unit=11, form="unformatted", file='V_unf', action="read")
    do i = 1, n_svd_b
      read(11) V(:,i)
    enddo
  close(11)


  call ezfio_get_dmc_dress_la(la)
  call ezfio_get_dmc_dress_lb(lb)
  call ezfio_get_dmc_dress_ld(ld)
  call ezfio_get_dmc_dress_lla(lla)
  call ezfio_get_dmc_dress_llb(llb)
  call ezfio_get_dmc_dress_lld(lld)
  n_dress_svd = ( ld * ld       &
                + lld - ld      &
                + (lla-ld) * lb &
                + (llb-ld) * la )

  allocate( delta_svd(n_dress_svd) )
  delta_svd(:) = 0.d0
  open(unit=11, file=file_svd, action="read")
    do jj = 1, n_dress_svd
      read(11, *) delta_svd(jj)
    enddo
  close(11)


  allocate( delta_det(N_det) )
  delta_det(:) = 0.d0
  do ii = 1, N_det

    i  = psi_bilinear_matrix_rows   (ii)
    j  = psi_bilinear_matrix_columns(ii)
    jj = 0

    do a = 1, ld
      do b = 1, ld
        jj = jj + 1
        delta_det(ii) = delta_det(ii) + U(i,a) * V(j,b) * delta_svd(jj)
      enddo
    enddo

    do a = ld+1, lld
      b  = a
      jj = jj + 1
      delta_det(ii) = delta_det(ii) + U(i,a) * V(j,b) * delta_svd(jj)
    enddo

    do a = ld+1, lla
      do b = 1, lb
        jj = jj + 1
        delta_det(ii) = delta_det(ii) + U(i,a) * V(j,b) * delta_svd(jj)
      enddo
    enddo

    do a = 1, la
      do b = ld+1, llb
        jj = jj + 1
        delta_det(ii) = delta_det(ii) + U(i,a) * V(j,b) * delta_svd(jj)
      enddo
    enddo

  enddo

  deallocate( U, V )
  deallocate( delta_svd )

  open(unit=11, file=file_det, action="write")
    do ii = 1, N_det
      write(11, *) delta_det(ii)
    enddo
  close(11)

end subroutine delta_svdtodet


