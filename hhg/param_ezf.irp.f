
BEGIN_PROVIDER [double precision, pol_pump, (3)]

  BEGIN_DOC
  !
  ! polarization of the laser
  !
  END_DOC

  implicit none
  logical :: exists
  integer :: i_nucl, p
  integer :: ierr

  PROVIDE ezfio_filename

  if(mpi_master) then
    call ezfio_has_hhg_pol_pump(exists)
  endif

  IRP_IF MPI_DEBUG
    print *,  irp_here, mpi_rank
    call MPI_BARRIER(MPI_COMM_WORLD, ierr)
  IRP_ENDIF

  IRP_IF MPI
    include 'mpif.h'
    call MPI_BCAST(pol_pump, (3), MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    if(ierr /= MPI_SUCCESS) then
      stop 'Unable to read pol_pump with MPI'
    endif
  IRP_ENDIF

  if(exists) then
    if(mpi_master) then
      write(6,'(A)') '.. >>>>> [ IO READ: pol_pump ] <<<<< ..'
      call ezfio_get_hhg_pol_pump(pol_pump)
      IRP_IF MPI
        call MPI_BCAST(pol_pump, (3), MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
        if(ierr /= MPI_SUCCESS) then
          stop 'Unable to read pol_pump with MPI'
        endif
      IRP_ENDIF
    endif
  else
    pol_pump = (/0.d0, 0.d0, 1.d0/)
    call ezfio_set_hhg_pol_pump(pol_pump)
  endif

END_PROVIDER

! ---

BEGIN_PROVIDER [double precision, pol_pump_b, (3)]

  BEGIN_DOC
  !
  ! polarization of ??
  !
  END_DOC

  implicit none
  logical :: exists
  integer :: i_nucl, p
  integer :: ierr

  PROVIDE ezfio_filename

  if(mpi_master) then
    call ezfio_has_hhg_pol_pump_b(exists)
  endif

  IRP_IF MPI_DEBUG
    print *,  irp_here, mpi_rank
    call MPI_BARRIER(MPI_COMM_WORLD, ierr)
  IRP_ENDIF

  IRP_IF MPI
    include 'mpif.h'
    call MPI_BCAST(pol_pump_b, (3), MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    if(ierr /= MPI_SUCCESS) then
      stop 'Unable to read pol_pump_b with MPI'
    endif
  IRP_ENDIF

  if(exists) then
    if(mpi_master) then
      write(6,'(A)') '.. >>>>> [ IO READ: pol_pump_b ] <<<<< ..'
      call ezfio_get_hhg_pol_pump_b(pol_pump_b)
      IRP_IF MPI
        call MPI_BCAST(pol_pump_b, (3), MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
        if(ierr /= MPI_SUCCESS) then
          stop 'Unable to read pol_pump_b with MPI'
        endif
      IRP_ENDIF
    endif
  else
    pol_pump_b = (/0.d0, 0.d0, 1.d0/)
    call ezfio_set_hhg_pol_pump_b(pol_pump_b)
  endif

END_PROVIDER

! ---

