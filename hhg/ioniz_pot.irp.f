
BEGIN_PROVIDER [double precision, Ion_pot]

  BEGIN_DOC
  !
  ! ionization potential
  !
  END_DOC

  implicit none
  integer          :: i, ii
  logical          :: exists
  double precision :: smallest_positive

  call ezfio_has_hhg_ion_pot(exists)
  if(exists) then

    write(*,*) 'Read Ion_pot'
    call ezfio_get_hhg_ion_pot(Ion_pot)

  else

    PROVIDE mo_num
    PROVIDE fock_matrix_diag_mo
    PROVIDE ionized_MO

    ii = ionized_MO
    if(ionized_MO == 0) then
      ii = elec_alpha_num
      call ezfio_set_hhg_ionized_MO(ii)
    endif
    write(*,*) 'Ionized MO:', ii
    Ion_pot = - fock_matrix_diag_mo(ii)

  endif

  print*, 'Ion_pot = ',  Ion_pot

END_PROVIDER
