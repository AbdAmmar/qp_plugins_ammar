

! ---

subroutine propagation_laser_cossquare(NRoots)

  implicit none

  include 'constants.include.F'

  integer, intent(in)           :: NRoots

  integer                       :: idet, i, j, k, w, g, nf, kw, a, b
  integer                       :: iter, write_prob, wf_iter, nfft
  integer                       :: num_tukey, num_tukey_one, num_tukey_two, num_hann
  integer                       :: NV_a, NO_a, NO_b, NV_b
  integer                       :: degree, exc(0:2,2,2)
  double precision              :: phase
  double precision              :: iter_dt, Up, alpha, E_tot
  double precision              :: tau0_pump
  double precision              :: momx, momy, momz
  double precision              :: norm, dum, vecpx, vecpy, vecpz
  double precision              :: potx, poty, potz, mpotx, mpoty, mpotz
  double precision              :: sum_norm, sum_ion
  double precision              :: shhg, dtmp, env
  double precision              :: P(NRoots+1), TP(NRoots+1)
  complex*16                    :: cmomx, cmomy, cmomz
  double precision, allocatable :: mom_X(:), mom_Y(:), mom_Z(:)
  double precision, allocatable :: energies(:), amplitudessq_cis(:), virtualene_a(:), virtualene_b(:), amps_a(:,:,:), amps_b(:,:,:)
  double precision, allocatable :: dip_mom_x(:,:), dip_mom_y(:,:), dip_mom_z(:,:) 
  double precision, allocatable :: dip_eigenval_x(:), dip_eigenval_y(:), dip_eigenval_z(:)
  double precision, allocatable :: lifetimes(:), rfit(:), WF(:)
  double precision, allocatable :: CHH_S(:), HHGmom_Z(:), gmom_Z(:), HHGmom_noWF(:), HHGmom_b(:)
  double precision, allocatable :: XXHHGmom(:), XAHHGmom(:), XBHHGmom(:), XCHHGmom(:), XNHHGmom(:), AAHHGmom(:), ABHHGmom(:), ACHHGmom(:)
  double precision, allocatable :: ANHHGmom(:), BBHHGmom(:), BCHHGmom(:), BNHHGmom(:), CCHHGmom(:), CNHHGmom(:), NNHHGmom(:)
  complex*16,       allocatable :: energies_exp(:,:), zeigenvec_x(:,:), zeigenvec_y(:,:), zeigenvec_z(:,:)
  complex*16,       allocatable :: C_t(:,:), C_t_dt(:,:), C_t_tmp(:,:) 
  complex*16,       allocatable :: C_t_dt_one(:,:), C_t_dt_two(:,:), C_t_dt_three(:,:), C_t_dt_four(:,:), C_t_dt_five(:,:)
  complex*16,       allocatable :: C_t_dt_six(:,:), C_t_dt_seven(:,:), C_t_dt_eight(:,:), C_t_dt_nine(:,:)
  complex*16,       allocatable :: lasermu_x(:,:), lasermu_y(:,:), lasermu_z(:,:)
  complex*16,       allocatable :: mo1tmp(:), mo2tmp(:), mo3tmp(:), mo4tmp(:), motmp(:), Cmom_Z(:), Cmom_b(:)
  complex*16,       allocatable :: CXXHHGmom(:), CXAHHGmom(:), CXBHHGmom(:), CXCHHGmom(:), CXNHHGmom(:), CAAHHGmom(:), CABHHGmom(:), CACHHGmom(:)
  complex*16,       allocatable :: CANHHGmom(:), CBBHHGmom(:), CBCHHGmom(:), CBNHHGmom(:), CCCHHGmom(:), CCNHHGmom(:), CNNHHGmom(:)
  complex*16,       allocatable :: CHH_Z(:), CHH_B(:), CHH_axis_X(:), CHH_axis_Y(:), CHH_axis_Z(:)
  complex*16,       allocatable :: CXXHH(:), CXAHH(:), CXBHH(:), CXCHH(:), CXNHH(:), CAAHH(:), CABHH(:), CACHH(:)
  complex*16,       allocatable :: CANHH(:), CBBHH(:), CBCHH(:), CBNHH(:), CCCHH(:), CCNHH(:), CNNHH(:)
  complex*16,       allocatable :: HH1(:), HH2(:), HH3(:), HH4(:), HHt(:)

  integer, external             :: good_fft_order_1dz

  !Start definition for CISGBC
  double precision              :: momGGx, momGGy, momGGz
  double precision              :: momGBBGx, momGBBGy, momGBBGz
  double precision              :: momGCCGx, momGCCGy, momGCCGz
  double precision              :: momBBx, momBBy, momBBz
  double precision              :: momBCCBx, momBCCBy, momBCCBz
  double precision              :: momCCx, momCCy, momCCz
  double precision, allocatable :: momtotal_X(:), momtotal_Y(:), momtotal_Z(:)
  double precision, allocatable :: momGG_X(:), momGG_Y(:), momGG_Z(:)
  double precision, allocatable :: momGBBG_X(:), momGBBG_Y(:), momGBBG_Z(:)
  double precision, allocatable :: momGCCG_X(:), momGCCG_Y(:), momGCCG_Z(:)
  double precision, allocatable :: momBB_X(:), momBB_Y(:), momBB_Z(:)
  double precision, allocatable :: momBCCB_X(:), momBCCB_Y(:), momBCCB_Z(:)
  double precision, allocatable :: momCC_X(:), momCC_Y(:), momCC_Z(:)
  double precision, allocatable :: momtotal(:), momGG(:), momGBBG(:), momGCCG(:)
  double precision, allocatable :: momBB(:), momBCCB(:), momCC(:)
  complex*16,       allocatable :: Cmomtotal(:), CmomGG(:), CmomGBBG(:), CmomGCCG(:)
  complex*16,       allocatable :: CmomBB(:), CmomBCCB(:), CmomCC(:)
  complex*16,       allocatable :: totalHHG(:), GGHHG(:), GBBGHHG(:), GCCGHHG(:)
  complex*16,       allocatable :: BBHHG(:), BCCBHHG(:), CCHHG(:)
  !End definition for CISGBC

  !Start definition for POSTCIS
  double precision              :: oneonemomx, oneonemomy, oneonemomz
  double precision              :: XXmomx, XXmomy, XXmomz
  double precision              :: XAmomx, XAmomy, XAmomz
  double precision              :: XBmomx, XBmomy, XBmomz
  double precision              :: XCmomx, XCmomy, XCmomz
  double precision              :: XNmomx, XNmomy, XNmomz
  double precision              :: AAmomx, AAmomy, AAmomz
  double precision              :: ABmomx, ABmomy, ABmomz
  double precision              :: ACmomx, ACmomy, ACmomz
  double precision              :: ANmomx, ANmomy, ANmomz
  double precision              :: BBmomx, BBmomy, BBmomz
  double precision              :: BCmomx, BCmomy, BCmomz
  double precision              :: BNmomx, BNmomy, BNmomz
  double precision              :: CCmomx, CCmomy, CCmomz
  double precision              :: CNmomx, CNmomy, CNmomz
  double precision              :: NNmomx, NNmomy, NNmomz
  double precision, allocatable :: XXmom_X(:), XXmom_Y(:), XXmom_Z(:)
  double precision, allocatable :: XAmom_X(:), XAmom_Y(:), XAmom_Z(:)
  double precision, allocatable :: XBmom_X(:), XBmom_Y(:), XBmom_Z(:)
  double precision, allocatable :: XCmom_X(:), XCmom_Y(:), XCmom_Z(:)
  double precision, allocatable :: XNmom_X(:), XNmom_Y(:), XNmom_Z(:)
  double precision, allocatable :: AAmom_X(:), AAmom_Y(:), AAmom_Z(:)
  double precision, allocatable :: ABmom_X(:), ABmom_Y(:), ABmom_Z(:)
  double precision, allocatable :: ACmom_X(:), ACmom_Y(:), ACmom_Z(:)
  double precision, allocatable :: ANmom_X(:), ANmom_Y(:), ANmom_Z(:)
  double precision, allocatable :: BBmom_X(:), BBmom_Y(:), BBmom_Z(:)
  double precision, allocatable :: BCmom_X(:), BCmom_Y(:), BCmom_Z(:)
  double precision, allocatable :: BNmom_X(:), BNmom_Y(:), BNmom_Z(:)
  double precision, allocatable :: CCmom_X(:), CCmom_Y(:), CCmom_Z(:)
  double precision, allocatable :: CNmom_X(:), CNmom_Y(:), CNmom_Z(:)
  double precision, allocatable :: NNmom_X(:), NNmom_Y(:), NNmom_Z(:)
  !End definition for POSTCIS

  !Start definition for POSTCISMO
  double precision              :: zeromomcismozmoocc
  double precision              :: onemomcismoz, onemomcismozmoocc, onemomcismozmoocc_local
  double precision              :: twomomcismoz, twomomcismozmoocc, twomomcismozmoocc_local
  double precision              :: threemomcismoz, threemomcismozmoocc, threemomcismozmoocc_local
  double precision              :: fourmomcismoz, fourmomcismozmoocc, fourmomcismozmoocc_local
  double precision, allocatable :: momcismoz(:)
  double precision, allocatable :: mooccmomcismoz(:)
  double precision, allocatable :: moocconemomcismoz(:), moocctwomomcismoz(:), mooccthreemomcismoz(:), mooccfourmomcismoz(:)
  double precision, allocatable :: dipxMO(:,:), dipyMO(:,:), dipzMO(:,:)
  !End definition for POSTCISMO

  ! ---

  PROVIDE elec_alpha_num elec_beta_num elec_num
  PROVIDE mo_num
  PROVIDE fock_matrix_diag_mo
  PROVIDE psi_coef
  PROVIDE psi_det
  PROVIDE N_int
  PROVIDe N_det

  PROVIDE propa
  PROVIDE in_state
  PROVIDE CISGBC POSTCIS POSTCISMO
  PROVIDE LINOPT
  PROVIDE Ion_pot
  PROVIDE field_type a0_pump phi_pump pol_pump pol_pump_b a1_pump_b omega0_pump omega1_pump delta_phi_pump
  PROVIDE td_max_iter dt
  PROVIDE WF_REC WF_HANN WF_TUKEY
  PROVIDE doubled dist_escape dist1_escape
  PROVIDE nsigma

  NO_a = elec_alpha_num
  NO_b = elec_beta_num
  NV_a = mo_num - NO_a
  NV_b = mo_num - NO_b

  tau0_pump = dble(oc) * PI / omega0_pump

  Up = 9.33d-14 * a0_pump * a0_pump * Wcm2 * (1.240d0 / (omega0_pump * au_to_eV))**2

  write(*,*) ''
  write(*,*) '***************************************'
  write(*,*) '***  PARAMETERS OF THE PROPAGATION  ***'
  write(*,*) '***************************************'
  write(*,*) ''
  write(*,*) 'Time step a.u.', dt
  write(*,*) 'omega0_pump a.u.', omega0_pump
  write(*,*) 'omega0_pump eV', omega0_pump * au_to_eV
  write(*,*) 'lamda (microm)', 1.240d0 / (omega0_pump * au_to_eV)
  write(*,*) 'a0_pump', a0_pump
  write(*,*) 'tau0_pump', tau0_pump
  write(*,*) 'PHASE of the LASER: ', -omega0_pump*tau0_pump
  write(*,*) 'ONE OPTICAL CYCLE:  ', 2.d0 * PI / omega0_pump, 'a.u.'
  write(*,*) 'ONE OPTICAL CYCLE:  ', 2.d0 * PI / omega0_pump * au_to_fsec, 'fs'
  write(*,*) 'MAXIMUM INTENSITY I: ', a0_pump * a0_pump, 'a.u.' 
  write(*,*) 'MAXIMUM INTENSITY I: ', a0_pump * a0_pump * Wcm2, 'W/cm^2' 
  write(*,*) 'IONIZATION POTENTIAL Ion_pot: ', Ion_pot, 'a.u.'
  write(*,*) 'IONIZATION POTENTIAL Ion_pot: ', Ion_pot*au_to_eV, 'eV'
  write(*,*) 'PONDEROMOTIVE ENERGY Up: ', Up , 'eV'
  write(*,*) 'PONDEROMOTIVE ENERGY Up: ', Up/au_to_eV, 'a.u.'
  write(*,*) 'N_cutoff = (Ion_pot + 3.17 Up)/omega0 ', (Ion_pot + 3.17d0 * Up/au_to_eV) / omega0_pump
  write(*,*) 'Molecule N_cutoff = (1.32 Ion_pot + 3.17 Up)/omega0 ', (1.32d0 * Ion_pot + 3.17d0 * Up/au_to_eV) / omega0_pump
  write(*,*) 'Keldish parameter ',  dsqrt(Ion_pot / (2.d0 * Up/au_to_eV))


  allocate(energies(NRoots+1), virtualene_a(NV_a), amps_a(2:NRoots+1,NO_a,NV_a), virtualene_b(NV_b), amps_b(2:NRoots+1,NO_b,NV_b))
  allocate(amplitudessq_cis(NRoots+1), energies_exp(NRoots+1,NRoots+1), lifetimes(NRoots+1), rfit(NRoots+1), WF(td_max_iter))
  allocate(dip_mom_x(NRoots+1,NRoots+1), dip_mom_y(NRoots+1,NRoots+1), dip_mom_z(NRoots+1,NRoots+1))
  allocate(zeigenvec_x(NRoots+1,NRoots+1), zeigenvec_y(NRoots+1,NRoots+1), zeigenvec_z(NRoots+1,NRoots+1))
  allocate(dip_eigenval_x(NRoots+1), dip_eigenval_y(NRoots+1), dip_eigenval_z(NRoots+1))
  allocate(lasermu_x(NRoots+1,NRoots+1), lasermu_y(NRoots+1,NRoots+1), lasermu_z(NRoots+1,NRoots+1))
  allocate( C_t(NRoots+1,1), C_t_tmp(NRoots+1,1), C_t_dt(NRoots+1,1)                 &
          , C_t_dt_one(NRoots+1,1), C_t_dt_two(NRoots+1,1), C_t_dt_three(NRoots+1,1) &
          , C_t_dt_four(NRoots+1,1), C_t_dt_five(NRoots+1,1), C_t_dt_six(NRoots+1,1) &
          , C_t_dt_seven(NRoots+1,1), C_t_dt_eight(NRoots+1,1), C_t_dt_nine(NRoots+1,1) )
  allocate(mom_X(td_max_iter), mom_Y(td_max_iter), mom_Z(td_max_iter), HHGmom_Z(td_max_iter), HHGmom_b(td_max_iter), HHGmom_noWF(td_max_iter))
  allocate(CHH_Z(td_max_iter), CHH_b(td_max_iter), CHH_S(td_max_iter), Cmom_Z(td_max_iter), Cmom_b(td_max_iter))

  call ezfio_get_cis_energy(energies)

  do i = elec_alpha_num+1, mo_num
    virtualene_a(i-elec_alpha_num) = fock_matrix_diag_mo(i)
  enddo
  do i = elec_beta_num+1, mo_num
    virtualene_b(i-elec_beta_num) = fock_matrix_diag_mo(i)
  enddo
  open(unit=50, file='virtual.dat', action='write')
    write(50,'(i5)') NV_a
    write(50,'(f7.3)')  (virtualene_a(i),i=1,NV_a)
  close(50)
  

  amps_a = 0.d0
  amps_b = 0.d0
  do idet = 2, N_det
    call get_excitation(psi_det(1,1,1), psi_det(1,1,idet), exc, degree, phase, N_int)
    if(degree .ne. 1) then
      print*, ' problem in CIS WF'
      print*, ' degree = ', degree
      stop
    endif
    if (exc(0,1,1) == 1) then
      i = exc(1,1,1)
      a = exc(1,2,1) - elec_alpha_num
      do k = 2, NRoots+1
        amps_a(k,i,a) = psi_coef(idet,k)
      enddo
    else
      i = exc(1,1,2)
      a = exc(1,2,2) - elec_beta_num
      do k = 2, NRoots+1
        amps_b(k,i,a) = psi_coef(idet,k)
      enddo
    endif
  enddo

  open(unit=91, file='amplitudes.dat', action="write")
    write(91,*) NO_a
    do k = 2, NRoots+1
      do i = 1, NO_a
        do j = 1, NV_a
          write(91,'(f7.4)') amps_a(k,i,j)
        enddo
      enddo
    enddo
  close(91)

  amplitudessq_cis = 0.d0
  if (.not.doubled) then
  
    ! Original heuristic model (only one escape length value)
    do k = 2, NRoots+1
      do i = 1, NO_a
        do j = 1, NV_a
          if (virtualene_a(j) .ge. 0.d0) then
            amplitudessq_cis(k) = amplitudessq_cis(k) + amps_a(k,i,j) * amps_a(k,i,j) * dsqrt(2.d0*virtualene_a(j)) / dist_escape
          endif   
        enddo
      enddo
      !write(*,*) ' AMPLITUDE SQUARE FOR CIS ROOTS', k, amplitudessq_cis(k)
    enddo
  
  else
  
    ! Large value of d (dist1_escape) for the low-energy continuum states => reducing the ionization rate 
    ! Small value of d (dist_escape) for the high-energy continuum states
    ! The energy threshold is given by (Ion_pot + 3.17*Up)
    do k = 2, NRoots+1
      if (energies(k) .le. (1.32d0*Ion_pot+3.17d0*Up/au_to_eV+energies(1))) then
        do i = 1, NO_a
          do j = 1, NV_a
            if (virtualene_a(j) .ge. 0.d0) then
              amplitudessq_cis(k) = amplitudessq_cis(k) + amps_a(k,i,j) * amps_a(k,i,j) * dsqrt(2.d0*virtualene_a(j)) / dist1_escape
            endif
          enddo
        enddo
        !write(*,*) 'AMPLITUDE SQUARE FOR CIS ROOTS', k,amplitudessq_cis(k)
      else
        do i = 1, NO_a
          do j = 1, NV_a
            if (virtualene_a(j) .ge. 0.d0) then
              !to remove high-energy states and noise
              if (dist_escape .eq. 0.d0) then
                amplitudessq_cis(k) = 1.d8
              else
                amplitudessq_cis(k) = amplitudessq_cis(k) + amps_a(k,i,j) * amps_a(k,i,j) * dsqrt(2.d0*virtualene_a(j)) / dist_escape
              endif 
            endif   
          enddo
        enddo
        !write(*,*) ' AMPLITUDE SQUARE FOR CIS ROOTS', k, amplitudessq_cis(k)
      endif
    enddo
  
  endif ! doubled

  if (POSTCISMO) then

    PROVIDE OCCMO mo1

    allocate(dipxMO(mo_num,mo_num), mo_dipole_y(mo_num,mo_num), mo_dipole_z(mo_num,mo_num))
    allocate(momcismoz(td_max_iter), mooccmomcismoz(td_max_iter))
    allocate(moocconemomcismoz(td_max_iter), moocctwomomcismoz(td_max_iter), mooccthreemomcismoz(td_max_iter), mooccfourmomcismoz(td_max_iter))
    allocate(HH1(td_max_iter), HH2(td_max_iter), HH3(td_max_iter), HH4(td_max_iter), HHt(td_max_iter))
    allocate(mo1tmp(td_max_iter), mo2tmp(td_max_iter), mo3tmp(td_max_iter), mo4tmp(td_max_iter), motmp(td_max_iter))

    ! TODO check
    PROVIDE mo_dipole_x mo_dipole_y mo_dipole_z
    dipxMO = mo_dipole_x
    dipyMO = mo_dipole_y
    dipzMO = mo_dipole_z
    FREE mo_dipole_x mo_dipole_y mo_dipole_z

  endif

  PROVIDE mo_dipole_x mo_dipole_y mo_dipole_z
  open(unit=33, file="dip_mom.dat", action="write")
    do i = 1, mo_num
      write(33,*) mo_dipole_x(i,1), mo_dipole_y(i,1), mo_dipole_z(i,1)
    enddo
  close(33)
  open(unit=33, file="dip_mom_T.dat", action="write")
    do i = 1, mo_num
      write(33,*) mo_dipole_x(1,i), mo_dipole_y(1,i), mo_dipole_z(1,i)
    enddo
  close(33)

  PROVIDE multi_s_x_dipole_moment multi_s_y_dipole_moment multi_s_z_dipole_moment
  PROVIDE multi_s_x_dipole_moment_eigenvec multi_s_x_dipole_moment_eigenval
  PROVIDE multi_s_y_dipole_moment_eigenvec multi_s_y_dipole_moment_eigenval
  PROVIDE multi_s_z_dipole_moment_eigenvec multi_s_z_dipole_moment_eigenval

  dip_mom_x = multi_s_x_dipole_moment
  dip_mom_y = multi_s_y_dipole_moment
  dip_mom_z = multi_s_z_dipole_moment

  open(unit=33, file="dip_mom_GStoGS.dat", action="write")
    write(33,*) dip_mom_x(1,1), dip_mom_y(1,1), dip_mom_z(1,1)
  close(33)
  open(unit=33, file="dip_mom_GStoEX.dat", action="write")
    do i = 1, NRoots
      write(33,*) dip_mom_x(1,i+1), dip_mom_y(1,i+1), dip_mom_z(1,i+1)
    enddo
  close(33)
  open(unit=33, file="dip_mom_GStoEX_T.dat", action="write")
    do i = 1, NRoots
      write(33,*) dip_mom_x(i+1,1), dip_mom_y(i+1,1), dip_mom_z(i+1,1)
    enddo
  close(33)
  open(unit=33, file="dip_mom_ExtoEX_diag.dat", action="write")
    do i = 1, NRoots
      write(33,*) dip_mom_x(i+1,i+1), dip_mom_y(i+1,i+1), dip_mom_z(i+1,i+1)
    enddo
  close(33)
  open(unit=33, file="dip_mom_ExtoEX.dat", action="write")
    do j = 1, NRoots
      do i = j+1, NRoots
        write(33,*) dip_mom_x(j+1,i+1), dip_mom_y(j+1,i+1), dip_mom_z(j+1,i+1)
      enddo
    enddo
  close(33)

  ! complex static dipoles
  zeigenvec_x = multi_s_x_dipole_moment_eigenvec * (1.d0, 0.d0)
  zeigenvec_y = multi_s_y_dipole_moment_eigenvec * (1.d0, 0.d0)
  zeigenvec_z = multi_s_z_dipole_moment_eigenvec * (1.d0, 0.d0)
  dip_eigenval_x = multi_s_x_dipole_moment_eigenval
  dip_eigenval_y = multi_s_y_dipole_moment_eigenval
  dip_eigenval_z = multi_s_z_dipole_moment_eigenval

  if (LINOPT) then
    write(*,*) ' Linear Optics calculation'
    call linear_optics(NRoots, energies, dip_mom_x, dip_mom_y, dip_mom_z)
    stop
  endif

  !Lifetimes from the heuristic model
  lifetimes = 0.d0
  open(301, file='ion_rate.dat')
  do i = 1, NRoots+1
    if (energies(i) .gt. (Ion_pot + energies(1))) then
      lifetimes(i) = amplitudessq_cis(i)
    endif
    write(301,'(i4,2x,f29.15,2x,f29.15)') i, energies(i), lifetimes(i)
  enddo
  close(301)

  ! exp cmplx energies for propagator
  ! e^{- i E_{i} dt }
  energies_exp = (0.d0,0.d0)
  do i = 1, NRoots+1
    energies_exp(i,i) = dcmplx(dcos(energies(i)*dt), -dsin(energies(i)*dt)) * dexp(-0.5d0*lifetimes(i)*dt)
  enddo

  do i = 1, NRoots+1
    C_t(i,1) = (0.d0, 0.d0)
  enddo
  C_t(in_state,1) = (1.d0, 0.d0)

  E_tot = 0.d0
  do i = 1, NRoots+1
    E_tot = E_tot + real(dconjg(C_t(i,1)) * C_t(i,1)) * energies(i)
  enddo
  write(*,"(/,6x, ' Total Energy = ',f10.6)") E_tot

  ! ---

  if (CISGBC) then

    PROVIDE nb_CISGBC

    allocate(momtotal_X(td_max_iter), momtotal_Y(td_max_iter), momtotal_Z(td_max_iter))
    allocate(momGG_X(td_max_iter), momGG_Y(td_max_iter), momGG_Z(td_max_iter))
    allocate(momGBBG_X(td_max_iter), momGBBG_Y(td_max_iter), momGBBG_Z(td_max_iter))
    allocate(momGCCG_X(td_max_iter), momGCCG_Y(td_max_iter), momGCCG_Z(td_max_iter))
    allocate(momBB_X(td_max_iter), momBB_Y(td_max_iter), momBB_Z(td_max_iter))
    allocate(momBCCB_X(td_max_iter), momBCCB_Y(td_max_iter), momBCCB_Z(td_max_iter))
    allocate(momCC_X(td_max_iter), momCC_Y(td_max_iter), momCC_Z(td_max_iter))
    allocate(momGG(td_max_iter), momGBBG(td_max_iter), momGCCG(td_max_iter), momBB(td_max_iter), momBCCB(td_max_iter), momCC(td_max_iter))
    allocate(momtotal(td_max_iter), Cmomtotal(td_max_iter), totalHHG(td_max_iter))
    allocate(CmomGG(td_max_iter), CmomGBBG(td_max_iter), CmomGCCG(td_max_iter), CmomBB(td_max_iter), CmomBCCB(td_max_iter), CmomCC(td_max_iter))
    allocate(GGHHG(td_max_iter), GBBGHHG(td_max_iter), GCCGHHG(td_max_iter), BBHHG(td_max_iter), BCCBHHG(td_max_iter), CCHHG(td_max_iter))
  endif

  if (POSTCIS) then

    PROVIDE X_POSTCIS A_POSTCIS B_POSTCIS C_POSTCIS

    allocate(XXmom_X(td_max_iter), XXmom_Y(td_max_iter), XXmom_Z(td_max_iter), XAmom_X(td_max_iter), XAmom_Y(td_max_iter), XAmom_Z(td_max_iter))
    allocate(XBmom_x(td_max_iter), XBmom_Y(td_max_iter), XBmom_Z(td_max_iter), XCmom_X(td_max_iter), XCmom_Y(td_max_iter), XCmom_Z(td_max_iter))
    allocate(XNmom_X(td_max_iter), XNmom_Y(td_max_iter), XNmom_Z(td_max_iter), AAmom_X(td_max_iter), AAmom_Y(td_max_iter), AAmom_Z(td_max_iter))
    allocate(ABmom_X(td_max_iter), ABmom_Y(td_max_iter), ABmom_Z(td_max_iter), ACmom_X(td_max_iter), ACmom_Y(td_max_iter), ACmom_Z(td_max_iter))
    allocate(ANmom_X(td_max_iter), ANmom_Y(td_max_iter), ANmom_Z(td_max_iter), BBmom_X(td_max_iter), BBmom_Y(td_max_iter), BBmom_Z(td_max_iter))
    allocate(BCmom_X(td_max_iter), BCmom_Y(td_max_iter), BCmom_Z(td_max_iter), BNmom_X(td_max_iter), BNmom_Y(td_max_iter), BNmom_Z(td_max_iter))
    allocate(CCmom_X(td_max_iter), CCmom_Y(td_max_iter), CCmom_Z(td_max_iter), CNmom_X(td_max_iter), CNmom_Y(td_max_iter), CNmom_Z(td_max_iter))
    allocate(NNmom_X(td_max_iter), NNmom_Y(td_max_iter), NNmom_Z(td_max_iter))
    allocate(XXHHGmom(td_max_iter), XAHHGmom(td_max_iter), XBHHGmom(td_max_iter), XCHHGmom(td_max_iter), XNHHGmom(td_max_iter))
    allocate(AAHHGmom(td_max_iter), ABHHGmom(td_max_iter), ACHHGmom(td_max_iter), ANHHGmom(td_max_iter))
    allocate(BBHHGmom(td_max_iter), BCHHGmom(td_max_iter), BNHHGmom(td_max_iter), CCHHGmom(td_max_iter), CNHHGmom(td_max_iter), NNHHGmom(td_max_iter))
    allocate(CXXHHGmom(td_max_iter), CXAHHGmom(td_max_iter), CXBHHGmom(td_max_iter), CXCHHGmom(td_max_iter))
    allocate(CXNHHGmom(td_max_iter), CAAHHGmom(td_max_iter), CABHHGmom(td_max_iter), CACHHGmom(td_max_iter))
    allocate(CANHHGmom(td_max_iter), CBBHHGmom(td_max_iter), CBCHHGmom(td_max_iter), CBNHHGmom(td_max_iter))
    allocate(CCCHHGmom(td_max_iter), CCNHHGmom(td_max_iter), CNNHHGmom(td_max_iter))
    allocate(CXXHH(td_max_iter), CXAHH(td_max_iter), CXBHH(td_max_iter), CXCHH(td_max_iter), CXNHH(td_max_iter))
    allocate(CAAHH(td_max_iter), CABHH(td_max_iter), CACHH(td_max_iter), CANHH(td_max_iter), CBBHH(td_max_iter))
    allocate(CBCHH(td_max_iter), CBNHH(td_max_iter), CCCHH(td_max_iter), CCNHH(td_max_iter), CNNHH(td_max_iter))
  endif

  ! ---

  !Start : open files and input parameters 
  open(unit=51, file='laser_no_enveloppe.dat', status='unknown', form='formatted')
  if (field_type == 'bcr') then
    open(unit=52, file='bcr_laser_pulse.dat', status='unknown', form='formatted')
  else
    open(unit=52, file='enveloppe_x.dat', status='unknown', form='formatted')
  endif
  open(unit=53 , file='enveloppe_y.dat'   , status='unknown', form='formatted')
  open(unit=54 , file='enveloppe_z.dat'   , status='unknown', form='formatted')
  open(unit=55 , file='laser_x.dat'       , status='unknown', form='formatted')
  open(unit=56 , file='laser_y.dat'       , status='unknown', form='formatted')
  open(unit=57 , file='laser_z.dat'       , status='unknown', form='formatted')
  open(unit=58 , file='momt_x.dat'        , status='unknown', form='formatted')
  open(unit=59 , file='momt_y.dat'        , status='unknown', form='formatted')
  open(unit=60 , file='momt_z.dat'        , status='unknown', form='formatted')
  open(unit=62 , file='momt.dat'          , status='unknown', form='formatted')
  open(unit=63 , file='momt_noWF.dat'     , status='unknown', form='formatted')
  open(unit=90 , file='bcr_amp.dat'       , status='unknown', form='formatted')
  open(unit=100, file='WindowFunction.dat', status='unknown', form='formatted')
  open(unit=108, file='MOMZ_WF_X.dat'     , status='unknown', form='formatted')
  open(unit=109, file='MOMZ_WF_Y.dat'     , status='unknown', form='formatted')
  open(unit=110, file='MOMZ_WF_Z.dat'     , status='unknown', form='formatted')
  open(unit=140, file='norm.dat')
  open(unit=141, file='pop_time.dat') 
  open(unit=142, file='ion_prop.dat')
  open(unit=143, file='ion_prop0.dat')

  if(WF_REC) then
    write(*,*) 'Rectangular Window Function'
    WF(:) = 0.d0
  else if(WF_TUKEY) then
    write(*,*) 'Tukey Window Function'
    WF(:) = 0.d0
    num_tukey = int(2.d0 * tau0_pump / dt)
    write(*,*) 'num_tukey', num_tukey
    alpha = 0.5d0
    num_tukey_one = int(alpha*num_tukey*0.5d0)
    write(*,*) 'num_tukey_one', num_tukey_one
    num_tukey_two = int(num_tukey*(1.d0 - alpha*0.5d0))
    write(*,*) 'num_tukey_two', num_tukey_two
  else if(WF_HANN) then
    WF(:) = 0.d0
    num_hann = int(2.d0 * tau0_pump / dt)
    write(*,*) 'num_hann for the Window Function', num_hann 
  else
    write(*,*) 'No Window Function'
    WF(:) = 1.d0
  endif

  write_prob = 0
  wf_iter = 0
  
  !Start : TIME PROPAGATION
  do iter = 1, td_max_iter
  
    iter_dt = dble(iter-1)*dt
  
    if(iter_dt <= 2.d0*tau0_pump) then
  
      call field_def(iter_dt, tau0_pump, potx, poty, potz)

      write(55,'(f19.7,2x,f19.12)') iter_dt, potx
      write(56,'(f19.7,2x,f19.12)') iter_dt, poty
      write(57,'(f19.7,2x,f19.12)') iter_dt, potz
  
      lasermu_x = (0.d0, 0.d0)
      lasermu_y = (0.d0, 0.d0)
      lasermu_z = (0.d0, 0.d0)

      ! The LG Hamiltonian is: H = K_0 + V_0 + r*F (-d*F),
      ! where K_0 and V_0 refer to the isolated system, 
      ! F is the electric field and d is the dipole     
      ! Length gauge
      do i = 1, NRoots + 1
        if (propa .eq. 0) then 
          ! dt^2
          lasermu_x(i,i) = dcmplx(dcos(potx*dip_eigenval_x(i)*dt), -dsin(potx*dip_eigenval_x(i)*dt))
          lasermu_y(i,i) = dcmplx(dcos(poty*dip_eigenval_y(i)*dt), -dsin(poty*dip_eigenval_y(i)*dt))
          lasermu_z(i,i) = dcmplx(dcos(potz*dip_eigenval_z(i)*dt), -dsin(potz*dip_eigenval_z(i)*dt))
        elseif (propa.eq.1) then
          ! dt^3
          lasermu_x(i,i) = dcmplx(dcos(0.5d0*potx*dip_eigenval_x(i)*dt), -dsin(0.5d0*potx*dip_eigenval_x(i)*dt))
          lasermu_y(i,i) = dcmplx(dcos(0.5d0*poty*dip_eigenval_y(i)*dt), -dsin(0.5d0*poty*dip_eigenval_y(i)*dt))
          lasermu_z(i,i) = dcmplx(dcos(0.5d0*potz*dip_eigenval_z(i)*dt), -dsin(0.5d0*potz*dip_eigenval_z(i)*dt))
        endif
      enddo
  
      wf_iter = wf_iter + 1
  
      ! window function
      if(WF_TUKEY) then
        if(wf_iter.le.num_tukey_one) then
          WF(iter) = 0.5d0 * (1.d0 + dcos(PI*(2.d0*iter/alpha/num_tukey - 1.d0)))
        else if((wf_iter.gt.num_tukey_one).and.(wf_iter.lt.num_tukey_two)) then
          WF(iter) = 1.d0
        else
          WF(iter) = 0.5d0 * (1.d0 + dcos(PI*(2.d0*iter/alpha/num_tukey - 2.d0/alpha + 1.d0)))
        endif
      else if(WF_REC) then
        WF(iter) = 1.d0
      else if(WF_HANN) then
        WF(iter) = 0.5d0 * (1.d0 - dcos(2.d0*PI*iter/num_hann))
      endif
 
    else ! iter_dt > 2.d0*tau0_pump
  
      write(51,'(f19.7,2x,f19.12)') iter_dt, dsin(omega0_pump*(iter_dt+phi_pump))
      write(52,'(f19.7,2x,f19.12)') iter_dt, pol_pump(1)*a0_pump*(dcos(PI*(iter_dt-tau0_pump)/(2.d0*tau0_pump)))**2
      write(53,'(f19.7,2x,f19.12)') iter_dt, pol_pump(2)*a0_pump*(dcos(PI*(iter_dt-tau0_pump)/(2.d0*tau0_pump)))**2
      write(54,'(f19.7,2x,f19.12)') iter_dt, pol_pump(3)*a0_pump*(dcos(PI*(iter_dt-tau0_pump)/(2.d0*tau0_pump)))**2
      write(55,'(f19.7,2x,f19.12)') iter_dt, 0.d0
      write(56,'(f19.7,2x,f19.12)') iter_dt, 0.d0
      write(57,'(f19.7,2x,f19.12)') iter_dt, 0.d0

      lasermu_x = (0.d0, 0.d0)
      lasermu_y = (0.d0, 0.d0)
      lasermu_z = (0.d0, 0.d0)

      do i = 1, NRoots+1
        lasermu_x(i,i) = (1.d0, 0.d0)
        lasermu_y(i,i) = (1.d0, 0.d0)
        lasermu_z(i,i) = (1.d0, 0.d0)
      enddo
      write_prob = write_prob + 1
      if(write_prob==1) then
        open(unit=61, file='probability.dat', status='unknown', form='formatted')
        open(unit=50, file='coefficients.dat', status='unknown', form='formatted')
        do i = 1, NRoots+1
          P(i) = real(dconjg(C_t(i,1)) * C_t(i,1))
          write(61,'(i5,2x,f19.7)') i, P(i) 
          write(50,'(i5,2x,2e19.7)') i, C_t(i,1) 
        enddo
        close(61)
        close(50)
      endif
  
    endif  
    !End : tau0_pump in TIME PROPAGATION 
  
    !Error prop to dt^2
    if(propa .eq. 0) then

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                     &
                , energies_exp(1,1), size(energies_exp, 1), C_t(1,1), size(C_t, 1) &
                , (0.d0,0.d0), C_t_dt_one(1,1), size(C_t_dt_one, 1))

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , zeigenvec_z(1,1), size(zeigenvec_z, 1), C_t_dt_one(1,1), size(C_t_dt_one, 1) &
                , (0.d0,0.d0), C_t_dt_two(1,1), size(C_t_dt_two, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                             &
                , lasermu_z(1,1), size(lasermu_z, 1), C_t_dt_two(1,1), size(C_t_dt_two, 1) &
                , (0.d0,0.d0), C_t_dt_three(1,1), size(C_t_dt_three, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                     &
                , zeigenvec_z(1,1), size(zeigenvec_z, 1), C_t_dt_three(1,1), size(C_t_dt_three, 1) &
                , (0.d0,0.d0), C_t_dt_four(1,1), size(C_t_dt_four, 1))

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                   &
                , zeigenvec_y(1,1), size(zeigenvec_y, 1), C_t_dt_four(1,1), size(C_t_dt_four, 1) &
                , (0.d0,0.d0), C_t_dt_five(1,1), size(C_t_dt_five, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                               &
                , lasermu_y(1,1), size(lasermu_y, 1), C_t_dt_five(1,1), size(C_t_dt_five, 1) &
                , (0.d0,0.d0), C_t_dt_six(1,1), size(C_t_dt_six, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , zeigenvec_y(1,1), size(zeigenvec_y, 1), C_t_dt_six(1,1), size(C_t_dt_six, 1) &
                , (0.d0,0.d0), C_t_dt_seven(1,1), size(C_t_dt_seven, 1))

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                     &
                , zeigenvec_x(1,1), size(zeigenvec_x, 1), C_t_dt_seven(1,1), size(C_t_dt_seven, 1) &
                , (0.d0,0.d0), C_t_dt_eight(1,1), size(C_t_dt_eight, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , lasermu_x(1,1), size(lasermu_x, 1), C_t_dt_eight(1,1), size(C_t_dt_eight, 1) &
                , (0.d0,0.d0), C_t_dt_nine(1,1), size(C_t_dt_nine, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                   &
                , zeigenvec_x(1,1), size(zeigenvec_x, 1), C_t_dt_nine(1,1), size(C_t_dt_nine, 1) &
                , (0.d0,0.d0), C_t_dt(1,1), size(C_t_dt, 1))

      C_t = C_t_dt

    elseif (propa.eq.1) then

      ! Error prop to dt^3

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                   &
                , zeigenvec_z(1,1), size(zeigenvec_z, 1), C_t(1,1), size(C_t, 1) &
                , (0.d0,0.d0), C_t_dt_one(1,1), size(C_t_dt_one, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                             &
                , lasermu_z(1,1), size(lasermu_z, 1), C_t_dt_one(1,1), size(C_t_dt_one, 1) &
                , (0.d0,0.d0), C_t_dt_two(1,1), size(C_t_dt_two, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , zeigenvec_z(1,1), size(zeigenvec_z, 1), C_t_dt_two(1,1), size(C_t_dt_two, 1) &
                , (0.d0,0.d0), C_t_dt_three(1,1), size(C_t_dt_three, 1))

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                     &
                , zeigenvec_y(1,1), size(zeigenvec_y, 1), C_t_dt_three(1,1), size(C_t_dt_three, 1) &
                , (0.d0,0.d0), C_t_dt_four(1,1), size(C_t_dt_four, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                               &
                , lasermu_y(1,1), size(lasermu_y, 1), C_t_dt_four(1,1), size(C_t_dt_four, 1) &
                , (0.d0,0.d0), C_t_dt_five(1,1), size(C_t_dt_five, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                   &
                , zeigenvec_y(1,1), size(zeigenvec_y, 1), C_t_dt_five(1,1), size(C_t_dt_five, 1) &
                , (0.d0,0.d0), C_t_dt_six(1,1), size(C_t_dt_six, 1))

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , zeigenvec_x(1,1), size(zeigenvec_x, 1), C_t_dt_six(1,1), size(C_t_dt_six, 1) &
                , (0.d0,0.d0), C_t_dt_seven(1,1), size(C_t_dt_seven, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , lasermu_x(1,1), size(lasermu_x, 1), C_t_dt_seven(1,1), size(C_t_dt_seven, 1) &
                , (0.d0,0.d0), C_t_dt_eight(1,1), size(C_t_dt_eight, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                     &
                , zeigenvec_x(1,1), size(zeigenvec_x, 1), C_t_dt_eight(1,1), size(C_t_dt_eight, 1) &
                , (0.d0,0.d0), C_t_dt_nine(1,1), size(C_t_dt_nine, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                     &
                , energies_exp(1,1), size(energies_exp, 1), C_t_dt_nine(1,1), size(C_t_dt_nine, 1) &
                , (0.d0,0.d0), C_t_tmp(1,1), size(C_t_tmp, 1))

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                           &
                , zeigenvec_z(1,1), size(zeigenvec_z, 1), C_t_tmp(1,1), size(C_t_tmp, 1) &
                , (0.d0,0.d0), C_t_dt_one(1,1), size(C_t_dt_one, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                             &
                , lasermu_z(1,1), size(lasermu_z, 1), C_t_dt_one(1,1), size(C_t_dt_one, 1) &
                , (0.d0,0.d0), C_t_dt_two(1,1), size(C_t_dt_two, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , zeigenvec_z(1,1), size(zeigenvec_z, 1), C_t_dt_two(1,1), size(C_t_dt_two, 1) &
                , (0.d0,0.d0), C_t_dt_three(1,1), size(C_t_dt_three, 1))

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                     &
                , zeigenvec_y(1,1), size(zeigenvec_y, 1), C_t_dt_three(1,1), size(C_t_dt_three, 1) &
                , (0.d0,0.d0), C_t_dt_four(1,1), size(C_t_dt_four, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                               &
                , lasermu_y(1,1), size(lasermu_y, 1), C_t_dt_four(1,1), size(C_t_dt_four, 1) &
                , (0.d0,0.d0), C_t_dt_five(1,1), size(C_t_dt_five, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                   &
                , zeigenvec_y(1,1), size(zeigenvec_y, 1), C_t_dt_five(1,1), size(C_t_dt_five, 1) &
                , (0.d0,0.d0), C_t_dt_six(1,1), size(C_t_dt_six, 1))

      call zgemm( "C", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , zeigenvec_x(1,1), size(zeigenvec_x, 1), C_t_dt_six(1,1), size(C_t_dt_six, 1) &
                , (0.d0,0.d0), C_t_dt_seven(1,1), size(C_t_dt_seven, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                 &
                , lasermu_x(1,1), size(lasermu_x, 1), C_t_dt_seven(1,1), size(C_t_dt_seven, 1) &
                , (0.d0,0.d0), C_t_dt_eight(1,1), size(C_t_dt_eight, 1))

      call zgemm( "N", "N", NRoots+1, 1, NRoots+1, (1.d0,0.d0)                                     &
                , zeigenvec_x(1,1), size(zeigenvec_x, 1), C_t_dt_eight(1,1), size(C_t_dt_eight, 1) &
                , (0.d0,0.d0), C_t_dt(1,1), size(C_t_dt, 1))

      C_t = C_t_dt

    endif

    !Start : FULL
    momx = 0.d0
    momy = 0.d0
    momz = 0.d0
    do j = 1, NRoots+1
      do i = 1, NRoots+1
        momx = momx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        momy = momy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        momz = momz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
      enddo
    enddo
    mom_X(iter) = momx
    mom_Y(iter) = momy
    mom_Z(iter) = momz
    !Generic linear polarization
    HHGmom_Z(iter) = (pol_pump(1)*mom_X(iter)+pol_pump(2)*mom_Y(iter)+pol_pump(3)*mom_Z(iter))*WF(iter)
    HHGmom_noWF(iter) = (pol_pump(1)*mom_X(iter)+pol_pump(2)*mom_Y(iter)+pol_pump(3)*mom_Z(iter))
    if (field_type == 'bcr') then
      HHGmom_b(iter) = (pol_pump_b(1)*mom_X(iter)+pol_pump_b(2)*mom_Y(iter)+pol_pump_b(3)*mom_Z(iter))*WF(iter)
    endif
    write(58,'(f19.7,2x,f19.12)')  iter_dt, mom_X(iter)
    write(59,'(f19.7,2x,f19.12)')  iter_dt, mom_Y(iter)
    write(60,'(f19.7,2x,f19.12)')  iter_dt, mom_Z(iter)
    write(100,'(f19.7,2x,f19.12)') iter_dt, WF(iter)
    write(108,'(f19.7,2x,f19.12)') iter_dt, mom_X(iter)*WF(iter)
    write(109,'(f19.7,2x,f19.12)') iter_dt, mom_Y(iter)*WF(iter)
    write(110,'(f19.7,2x,f19.12)') iter_dt, mom_Z(iter)*WF(iter)
    write(62,'(f19.7,2x,f19.12)')  iter_dt, HHGmom_Z(iter)
    write(63,'(f19.7,2x,f19.12)')  iter_dt, HHGmom_noWF(iter)
    sum_norm = dble(dconjg(C_t(1,1))*C_t(1,1))
    do i = 2, NRoots+1
      sum_norm = sum_norm + dble(dconjg(C_t(i,1))*C_t(i,1))
    enddo
    write(140,*) iter_dt, sum_norm
    write(141,*) iter_dt, (dble(dconjg(C_t(i,1))*C_t(i,1)), i=1,2) 
    sum_ion=0.d0
    do i = 1, NRoots+1
      if (energies(i) .lt. (Ion_pot + energies(1))) then
        sum_ion = sum_ion + dble(dconjg(C_t(i,1))*C_t(i,1))
      endif
    enddo
    write(142,*) iter_dt, 1.d0-sum_ion
    write(143,*) iter_dt, 1.d0-dble(dconjg(C_t(1,1))*C_t(1,1))
    !End : FULL  

    !Start : CISGBC
    if (CISGBC) then

      momGBBGx = 0.d0
      momGBBGy = 0.d0
      momGBBGz = 0.d0
      momGCCGx = 0.d0
      momGCCGy = 0.d0
      momGCCGz = 0.d0
      momBBx   = 0.d0
      momBBy   = 0.d0
      momBBz   = 0.d0
      momBCCBx = 0.d0
      momBCCBy = 0.d0
      momBCCBz = 0.d0
      momCCx   = 0.d0
      momCCy   = 0.d0
      momCCz   = 0.d0

      ! GG
      momGGx = dip_mom_x(1,1) * dble(dconjg(C_t(1,1)) * C_t(1,1))
      momGGy = dip_mom_y(1,1) * dble(dconjg(C_t(1,1)) * C_t(1,1))
      momGGz = dip_mom_z(1,1) * dble(dconjg(C_t(1,1)) * C_t(1,1))
      ! GB
      do j = 2, nb_CISGBC
        momGBBGx = momGBBGx + dip_mom_x(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        momGBBGy = momGBBGy + dip_mom_y(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        momGBBGz = momGBBGz + dip_mom_z(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
      enddo
      ! GC
      do j = nb_CISGBC+1, NRoots+1
        momGCCGx = momGCCGx + dip_mom_x(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        momGCCGy = momGCCGy + dip_mom_y(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        momGCCGz = momGCCGz + dip_mom_z(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
      enddo
      ! BB
      do i = 2, nb_CISGBC
        do j = 2, nb_CISGBC
          momBBx = momBBx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          momBBy = momBBy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          momBBz = momBBz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! BC
      do i = 2, nb_CISGBC
        do j = nb_CISGBC+1, NRoots+1
          momBCCBx = momBCCBx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          momBCCBy = momBCCBy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          momBCCBz = momBCCBz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! CC
      do i = nb_CISGBC+1, NRoots+1
        do j = nb_CISGBC+1, NRoots+1
          momCCx = momCCx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          momCCy = momCCy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          momCCz = momCCz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo

      momtotal_X(iter) = momGGx + 2.d0*momGBBGx + 2.d0*momGCCGx + momBBx + 2.d0*momBCCBx + momCCx 
      momtotal_Y(iter) = momGGy + 2.d0*momGBBGy + 2.d0*momGCCGy + momBBy + 2.d0*momBCCBy + momCCy 
      momtotal_Z(iter) = momGGz + 2.d0*momGBBGz + 2.d0*momGCCGz + momBBz + 2.d0*momBCCBz + momCCz 
      momGG_X   (iter) = momGGx
      momGG_Y   (iter) = momGGy
      momGG_Z   (iter) = momGGz
      momGBBG_X(iter) = 2.d0*momGBBGx
      momGBBG_Y(iter) = 2.d0*momGBBGy
      momGBBG_Z(iter) = 2.d0*momGBBGz
      momGCCG_X(iter) = 2.d0*momGCCGx
      momGCCG_Y(iter) = 2.d0*momGCCGy
      momGCCG_Z(iter) = 2.d0*momGCCGz
      momBB_X  (iter) = momBBx
      momBB_Y  (iter) = momBBy
      momBB_Z  (iter) = momBBz
      momBCCB_X(iter) = 2.d0*momBCCBx
      momBCCB_Y(iter) = 2.d0*momBCCBy
      momBCCB_Z(iter) = 2.d0*momBCCBz
      momCC_X  (iter) = momCCx
      momCC_Y  (iter) = momCCy
      momCC_Z  (iter) = momCCz

      momtotal(iter) = (pol_pump(1)*momtotal_X(iter) + pol_pump(2)*momtotal_Y(iter) + pol_pump(3)*momtotal_Z(iter)) * WF(iter)
      momGG   (iter) = (pol_pump(1)*momGG_X   (iter) + pol_pump(2)*momGG_Y   (iter) + pol_pump(3)*momGG_Z   (iter)) * WF(iter)
      momGBBG (iter) = (pol_pump(1)*momGBBG_X (iter) + pol_pump(2)*momGBBG_Y (iter) + pol_pump(3)*momGBBG_Z (iter)) * WF(iter)
      momGCCG (iter) = (pol_pump(1)*momGCCG_X (iter) + pol_pump(2)*momGCCG_Y (iter) + pol_pump(3)*momGCCG_Z (iter)) * WF(iter)
      momBB   (iter) = (pol_pump(1)*momBB_X   (iter) + pol_pump(2)*momBB_Y   (iter) + pol_pump(3)*momBB_Z   (iter)) * WF(iter)
      momBCCB (iter) = (pol_pump(1)*momBCCB_X (iter) + pol_pump(2)*momBCCB_Y (iter) + pol_pump(3)*momBCCB_Z (iter)) * WF(iter)
      momCC   (iter) = (pol_pump(1)*momCC_X   (iter) + pol_pump(2)*momCC_Y   (iter) + pol_pump(3)*momCC_Z   (iter)) * WF(iter)

      write(111,'(f19.7,2x,f19.12)') iter_dt, momtotal(iter)
      write(112,'(f19.7,2x,f19.12)') iter_dt, momGG   (iter)
      write(113,'(f19.7,2x,f19.12)') iter_dt, momGBBG (iter)
      write(114,'(f19.7,2x,f19.12)') iter_dt, momGCCG (iter)
      write(115,'(f19.7,2x,f19.12)') iter_dt, momBB   (iter)
      write(116,'(f19.7,2x,f19.12)') iter_dt, momBCCB (iter)
      write(117,'(f19.7,2x,f19.12)') iter_dt, momCC   (iter)
    endif
    !End : CISGBC 

    !Start : POSTCIS 
    if (POSTCIS) then
      if (iter.eq.1) then
        write(*,*) 'X_POSTCIS', X_POSTCIS
        write(*,*) 'A_POSTCIS', A_POSTCIS
        write(*,*) 'B_POSTCIS', B_POSTCIS
        write(*,*) 'C_POSTCIS', C_POSTCIS
      endif
      open(206, file='logXX.dat')
      open(207, file='logXA.dat')
      open(208, file='logXB.dat')
      open(209, file='logXC.dat')
      open(210, file='logXN.dat')
      open(211, file='logAA.dat')
      open(212, file='logAB.dat')
      open(213, file='logAC.dat')
      open(214, file='logAN.dat')
      open(215, file='logBB.dat')
      open(216, file='logBC.dat')
      open(217, file='logBN.dat')
      open(218, file='logCC.dat')
      open(219, file='logCN.dat')
      open(220, file='logNN.dat')  
      ! 11
      oneonemomx = 0.d0
      oneonemomy = 0.d0
      oneonemomz = 0.d0
      oneonemomx = oneonemomx + dip_mom_x(1,1) * dble(dconjg(C_t(1,1)) * C_t(1,1))
      oneonemomy = oneonemomy + dip_mom_y(1,1) * dble(dconjg(C_t(1,1)) * C_t(1,1))
      oneonemomz = oneonemomz + dip_mom_z(1,1) * dble(dconjg(C_t(1,1)) * C_t(1,1))
      ! XX: 11, 1X, X1 and XX
      ! 11
      XXmomx = oneonemomx
      XXmomy = oneonemomy
      XXmomz = oneonemomz
      ! 1X and X1
      do j = 2, X_POSTCIS+1
        XXmomx = XXmomx + dip_mom_x(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        XXmomy = XXmomy + dip_mom_y(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        XXmomz = XXmomz + dip_mom_z(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
      enddo
      do i = 2, X_POSTCIS+1
        XXmomx = XXmomx + dip_mom_x(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        XXmomy = XXmomy + dip_mom_y(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        XXmomz = XXmomz + dip_mom_z(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
      enddo
      ! XX
      do i = 2, X_POSTCIS+1
        do j = 2, X_POSTCIS+1
          XXmomx = XXmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XXmomy = XXmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XXmomz = XXmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! AA: 11, 1A, A1 and AA
      ! 11
      AAmomx = oneonemomx
      AAmomy = oneonemomy
      AAmomz = oneonemomz
      ! 1A and A1
      do j = X_POSTCIS+2, A_POSTCIS+1
        AAmomx = AAmomx + dip_mom_x(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        AAmomy = AAmomy + dip_mom_y(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        AAmomz = AAmomz + dip_mom_z(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
      enddo
      do i = X_POSTCIS+2, A_POSTCIS+1
        AAmomx = AAmomx + dip_mom_x(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        AAmomy = AAmomy + dip_mom_y(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        AAmomz = AAmomz + dip_mom_z(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
      enddo
      ! AA 
      do i = X_POSTCIS+2, A_POSTCIS+1
        do j = X_POSTCIS+2, A_POSTCIS+1
          AAmomx = AAmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          AAmomy = AAmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          AAmomz = AAmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! BB: 11, 1B, B1 and BB
      ! 11
      BBmomx = oneonemomx
      BBmomy = oneonemomy
      BBmomz = oneonemomz
      ! 1B and B1
      do j = A_POSTCIS+2, B_POSTCIS+1
        BBmomx = BBmomx + dip_mom_x(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        BBmomy = BBmomy + dip_mom_y(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        BBmomz = BBmomz + dip_mom_z(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
      enddo
      do i = A_POSTCIS+2, B_POSTCIS+1
        BBmomx = BBmomx + dip_mom_x(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        BBmomy = BBmomy + dip_mom_y(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        BBmomz = BBmomz + dip_mom_z(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
      enddo
      ! BB
      do i = A_POSTCIS+2, B_POSTCIS+1
        do j = A_POSTCIS+2, B_POSTCIS+1
          BBmomx = BBmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BBmomy = BBmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BBmomz = BBmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! CC: 11, 1C, C1 and CC
      ! 11
      CCmomx = oneonemomx
      CCmomy = oneonemomy
      CCmomz = oneonemomz
      ! 1C and C1
      do j = B_POSTCIS+2, C_POSTCIS+1
        CCmomx = CCmomx + dip_mom_x(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        CCmomy = CCmomy + dip_mom_y(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        CCmomz = CCmomz + dip_mom_z(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
      enddo
      do i = B_POSTCIS+2, C_POSTCIS+1
        CCmomx = CCmomx + dip_mom_x(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        CCmomy = CCmomy + dip_mom_y(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        CCmomz = CCmomz + dip_mom_z(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
      enddo
      ! CC
      do i = B_POSTCIS+2, C_POSTCIS+1
        do j = B_POSTCIS+2, C_POSTCIS+1
          CCmomx = CCmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          CCmomy = CCmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          CCmomz = CCmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! NN: 11, 1N, N1 and NN
      ! 11
      NNmomx = oneonemomx
      NNmomy = oneonemomy
      NNmomz = oneonemomz
      ! 1N and N1
      do j = C_POSTCIS+2, NRoots+1
        NNmomx = NNmomx + dip_mom_x(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        NNmomy = NNmomy + dip_mom_y(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
        NNmomz = NNmomz + dip_mom_z(1,j) * dble(dconjg(C_t(1,1)) * C_t(j,1))
      enddo
      do i = C_POSTCIS+2, NRoots+1
        NNmomx = NNmomx + dip_mom_x(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        NNmomy = NNmomy + dip_mom_y(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
        NNmomz = NNmomz + dip_mom_z(i,1) * dble(dconjg(C_t(i,1)) * C_t(1,1))
      enddo
      ! NN
      do i = C_POSTCIS+2, NRoots+1
        do j = C_POSTCIS+2, NRoots+1
          NNmomx = NNmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          NNmomy = NNmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          NNmomz = NNmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! XA: 11, XA and AX
      ! 11
      XAmomx = oneonemomx
      XAmomy = oneonemomy
      XAmomz = oneonemomz
      ! XA and AX
      do i = 2, X_POSTCIS+1
        do j = X_POSTCIS+2, A_POSTCIS+1
          XAmomx = XAmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XAmomy = XAmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XAmomz = XAmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = X_POSTCIS+2, A_POSTCIS+1
        do j = 2, X_POSTCIS+1
          XAmomx = XAmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XAmomy = XAmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XAmomz = XAmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! XB: 11, XB and BX
      ! 11
      XBmomx = oneonemomx
      XBmomy = oneonemomy
      XBmomz = oneonemomz
      ! XB and BX
      do i = 2, X_POSTCIS+1
        do j = A_POSTCIS+2, B_POSTCIS+1
          XBmomx = XBmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XBmomy = XBmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XBmomz = XBmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = A_POSTCIS+2, B_POSTCIS+1
        do j = 2, X_POSTCIS+1
          XBmomx = XBmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XBmomy = XBmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XBmomz = XBmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! XC: 11, XC and CX
      ! 11
      XCmomx = oneonemomx
      XCmomy = oneonemomy
      XCmomz = oneonemomz
      ! XC and CX
      do i = 2, X_POSTCIS+1
        do j = B_POSTCIS+2, C_POSTCIS+1
          XCmomx = XCmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XCmomy = XCmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XCmomz = XCmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = B_POSTCIS+2, C_POSTCIS+1
        do j = 2, X_POSTCIS+1
          XCmomx = XCmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XCmomy = XCmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XCmomz = XCmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! XN: 11, XN and NX
      ! 11
      XNmomx = oneonemomx
      XNmomy = oneonemomy
      XNmomz = oneonemomz
      ! XN and NX
      do i = 2, X_POSTCIS+1
        do j = C_POSTCIS+2, NRoots+1
          XNmomx = XNmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XNmomy = XNmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XNmomz = XNmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = C_POSTCIS+2, NRoots+1
        do j = 2, X_POSTCIS+1
          XNmomx = XNmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XNmomy = XNmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          XNmomz = XNmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! AB: 11, AB and BA
      ! 11
      ABmomx = oneonemomx
      ABmomy = oneonemomy
      ABmomz = oneonemomz
      ! AB and BA
      do i = X_POSTCIS+2, A_POSTCIS+1
        do j = A_POSTCIS+2, B_POSTCIS+1
          ABmomx = ABmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ABmomy = ABmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ABmomz = ABmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = A_POSTCIS+2, B_POSTCIS+1
        do j = X_POSTCIS+2, A_POSTCIS+1
          ABmomx = ABmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ABmomy = ABmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ABmomz = ABmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! AC: 11, AC and CA
      ! 11
      ACmomx = oneonemomx
      ACmomy = oneonemomy
      ACmomz = oneonemomz
      ! AC and CA
      do i = X_POSTCIS+2, A_POSTCIS+1
        do j = B_POSTCIS+2, C_POSTCIS+1
          ACmomx = ACmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ACmomy = ACmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ACmomz = ACmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = B_POSTCIS+2,C_POSTCIS+1
        do j = X_POSTCIS+2,A_POSTCIS+1
          ACmomx = ACmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ACmomy = ACmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ACmomz = ACmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! AN: 11, AN and NA
      ! 11
      ANmomx = oneonemomx
      ANmomy = oneonemomy
      ANmomz = oneonemomz
      ! AN and NA
      do i = X_POSTCIS+2, A_POSTCIS+1
        do j = C_POSTCIS+2, NRoots+1
          ANmomx = ANmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ANmomy = ANmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ANmomz = ANmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = C_POSTCIS+2, NRoots+1
        do j = X_POSTCIS+2, A_POSTCIS+1
          ANmomx = ANmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ANmomy = ANmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          ANmomz = ANmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! BC: 11, BC and CB
      ! 11
      BCmomx = oneonemomx
      BCmomy = oneonemomy
      BCmomz = oneonemomz
      ! BC and CB
      do i = A_POSTCIS+2, B_POSTCIS+1
        do j = B_POSTCIS+2, C_POSTCIS+1
          BCmomx = BCmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BCmomy = BCmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BCmomz = BCmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = B_POSTCIS+2, C_POSTCIS+1
        do j = A_POSTCIS+2, B_POSTCIS+1
          BCmomx = BCmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BCmomy = BCmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BCmomz = BCmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! BN: 11, BN and NB
      ! 11
      BNmomx = oneonemomx
      BNmomy = oneonemomy
      BNmomz = oneonemomz
      ! BN and NB
      do i = A_POSTCIS+2, B_POSTCIS+1
        do j = C_POSTCIS+2, NRoots+1
          BNmomx = BNmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BNmomy = BNmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BNmomz = BNmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      do i = C_POSTCIS+2, NRoots+1
        do j = A_POSTCIS+2, B_POSTCIS+1
          BNmomx = BNmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BNmomy = BNmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          BNmomz = BNmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! CN: 11, CN and NC
      ! 11
      CNmomx = oneonemomx
      CNmomy = oneonemomy
      CNmomz = oneonemomz
      do i = B_POSTCIS+2, C_POSTCIS+1
        do j = C_POSTCIS+2, NRoots+1
          CNmomx = CNmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          CNmomy = CNmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          CNmomz = CNmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      ! CN and NC
      do i = C_POSTCIS+2, NRoots+1
        do j = B_POSTCIS+2, C_POSTCIS+1
          CNmomx = CNmomx + dip_mom_x(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          CNmomy = CNmomy + dip_mom_y(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
          CNmomz = CNmomz + dip_mom_z(i,j) * dble(dconjg(C_t(i,1)) * C_t(j,1))
        enddo
      enddo
      XXmom_X(iter) = XXmomx
      XXmom_Y(iter) = XXmomy
      XXmom_Z(iter) = XXmomz
      XAmom_X(iter) = XAmomx
      XAmom_Y(iter) = XAmomy 
      XAmom_Z(iter) = XAmomz
      XBmom_X(iter) = XBmomx
      XBmom_Y(iter) = XBmomy 
      XBmom_Z(iter) = XBmomz 
      XCmom_X(iter) = XCmomx 
      XCmom_Y(iter) = XCmomy 
      XCmom_Z(iter) = XCmomz 
      XNmom_X(iter) = XNmomx 
      XNmom_Y(iter) = XNmomy 
      XNmom_Z(iter) = XNmomz 
      AAmom_X(iter) = AAmomx
      AAmom_Y(iter) = AAmomy
      AAmom_Z(iter) = AAmomz
      ABmom_X(iter) = ABmomx 
      ABmom_Y(iter) = ABmomy 
      ABmom_Z(iter) = ABmomz
      ACmom_X(iter) = ACmomx 
      ACmom_Y(iter) = ACmomy 
      ACmom_Z(iter) = ACmomz 
      ANmom_X(iter) = ANmomx 
      ANmom_Y(iter) = ANmomy 
      ANmom_Z(iter) = ANmomz 
      BBmom_X(iter) = BBmomx
      BBmom_Y(iter) = BBmomy
      BBmom_Z(iter) = BBmomz
      BCmom_X(iter) = BCmomx 
      BCmom_Y(iter) = BCmomy 
      BCmom_Z(iter) = BCmomz 
      CCmom_X(iter) = CCmomx
      CCmom_Y(iter) = CCmomy
      CCmom_Z(iter) = CCmomz
      CNmom_X(iter) = CNmomx 
      CNmom_Y(iter) = CNmomy 
      CNmom_Z(iter) = CNmomz
      BNmom_X(iter) = BNmomx 
      BNmom_Y(iter) = BNmomy 
      BNmom_Z(iter) = BNmomz 
      NNmom_X(iter) = NNmomx
      NNmom_Y(iter) = NNmomy
      NNmom_Z(iter) = NNmomz
      XXHHGmom(iter) = (pol_pump(1)*XXmom_X(iter)+pol_pump(2)*XXmom_Y(iter)+pol_pump(3)*XXmom_Z(iter))*WF(iter)
      XAHHGmom(iter) = (pol_pump(1)*XAmom_X(iter)+pol_pump(2)*XAmom_Y(iter)+pol_pump(3)*XAmom_Z(iter))*WF(iter)
      XBHHGmom(iter) = (pol_pump(1)*XBmom_X(iter)+pol_pump(2)*XBmom_Y(iter)+pol_pump(3)*XBmom_Z(iter))*WF(iter)
      XCHHGmom(iter) = (pol_pump(1)*XCmom_X(iter)+pol_pump(2)*XCmom_Y(iter)+pol_pump(3)*XCmom_Z(iter))*WF(iter)
      XNHHGmom(iter) = (pol_pump(1)*XNmom_X(iter)+pol_pump(2)*XNmom_Y(iter)+pol_pump(3)*XNmom_Z(iter))*WF(iter)
      AAHHGmom(iter) = (pol_pump(1)*AAmom_X(iter)+pol_pump(2)*AAmom_Y(iter)+pol_pump(3)*AAmom_Z(iter))*WF(iter)
      ABHHGmom(iter) = (pol_pump(1)*ABmom_X(iter)+pol_pump(2)*ABmom_Y(iter)+pol_pump(3)*ABmom_Z(iter))*WF(iter)
      ACHHGmom(iter) = (pol_pump(1)*ACmom_X(iter)+pol_pump(2)*ACmom_Y(iter)+pol_pump(3)*ACmom_Z(iter))*WF(iter)
      ANHHGmom(iter) = (pol_pump(1)*ANmom_X(iter)+pol_pump(2)*ANmom_Y(iter)+pol_pump(3)*ANmom_Z(iter))*WF(iter)
      BBHHGmom(iter) = (pol_pump(1)*BBmom_X(iter)+pol_pump(2)*BBmom_Y(iter)+pol_pump(3)*BBmom_Z(iter))*WF(iter)
      BCHHGmom(iter) = (pol_pump(1)*BCmom_X(iter)+pol_pump(2)*BCmom_Y(iter)+pol_pump(3)*BCmom_Z(iter))*WF(iter)
      BNHHGmom(iter) = (pol_pump(1)*BNmom_X(iter)+pol_pump(2)*BNmom_Y(iter)+pol_pump(3)*BNmom_Z(iter))*WF(iter)
      CCHHGmom(iter) = (pol_pump(1)*CCmom_X(iter)+pol_pump(2)*CCmom_Y(iter)+pol_pump(3)*CCmom_Z(iter))*WF(iter)
      CNHHGmom(iter) = (pol_pump(1)*CNmom_X(iter)+pol_pump(2)*CNmom_Y(iter)+pol_pump(3)*CNmom_Z(iter))*WF(iter)
      NNHHGmom(iter) = (pol_pump(1)*NNmom_X(iter)+pol_pump(2)*NNmom_Y(iter)+pol_pump(3)*NNmom_Z(iter))*WF(iter)
      write(300,'(f19.7,2x,3f19.12)') iter_dt, XXmom_X(iter), XXmom_Y(iter), XXmom_Z(iter)
      write(301,'(f19.7,2x,3f19.12)') iter_dt, AAmom_X(iter), AAmom_Y(iter), AAmom_Z(iter)
      write(302,'(f19.7,2x,3f19.12)') iter_dt, BBmom_X(iter), BBmom_Y(iter), BBmom_Z(iter)
      write(303,'(f19.7,2x,3f19.12)') iter_dt, CCmom_X(iter), CCmom_Y(iter), CCmom_Z(iter)
      write(304,'(f19.7,2x,3f19.12)') iter_dt, NNmom_X(iter), NNmom_Y(iter), NNmom_Z(iter)
      write(305,'(f19.7,2x,3f19.12)') iter_dt, XAmom_X(iter), XAmom_Y(iter), XAmom_Z(iter)
      write(306,'(f19.7,2x,3f19.12)') iter_dt, XBmom_X(iter), XBmom_Y(iter), XBmom_Z(iter)
      write(307,'(f19.7,2x,3f19.12)') iter_dt, XCmom_X(iter), XCmom_Y(iter), XCmom_Z(iter)
      write(308,'(f19.7,2x,3f19.12)') iter_dt, XNmom_X(iter), XNmom_Y(iter), XNmom_Z(iter)
      write(309,'(f19.7,2x,3f19.12)') iter_dt, ABmom_X(iter), ABmom_Y(iter), ABmom_Z(iter)
      write(310,'(f19.7,2x,3f19.12)') iter_dt, ACmom_X(iter), ACmom_Y(iter), ACmom_Z(iter)
      write(311,'(f19.7,2x,3f19.12)') iter_dt, ANmom_X(iter), ANmom_Y(iter), ANmom_Z(iter)
      write(312,'(f19.7,2x,3f19.12)') iter_dt, BCmom_X(iter), BCmom_Y(iter), BCmom_Z(iter)
      write(313,'(f19.7,2x,3f19.12)') iter_dt, BNmom_X(iter), BNmom_Y(iter), BNmom_Z(iter)
      write(314,'(f19.7,2x,3f19.12)') iter_dt, CNmom_X(iter), CNmom_Y(iter), CNmom_Z(iter)
      write(117,'(f19.7,2x,f19.12)')  iter_dt, XXHHGmom(iter)
      write(118,'(f19.7,2x,f19.12)')  iter_dt, XAHHGmom(iter) 
      write(119,'(f19.7,2x,f19.12)')  iter_dt, XBHHGmom(iter)
      write(120,'(f19.7,2x,f19.12)')  iter_dt, XCHHGmom(iter)
      write(121,'(f19.7,2x,f19.12)')  iter_dt, XNHHGmom(iter)
      write(122,'(f19.7,2x,f19.12)')  iter_dt, AAHHGmom(iter)
      write(123,'(f19.7,2x,f19.12)')  iter_dt, ABHHGmom(iter)
      write(124,'(f19.7,2x,f19.12)')  iter_dt, ACHHGmom(iter) 
      write(125,'(f19.7,2x,f19.12)')  iter_dt, ANHHGmom(iter) 
      write(126,'(f19.7,2x,f19.12)')  iter_dt, BBHHGmom(iter) 
      write(127,'(f19.7,2x,f19.12)')  iter_dt, BCHHGmom(iter) 
      write(128,'(f19.7,2x,f19.12)')  iter_dt, BNHHGmom(iter) 
      write(129,'(f19.7,2x,f19.12)')  iter_dt, CCHHGmom(iter) 
      write(130,'(f19.7,2x,f19.12)')  iter_dt, CNHHGmom(iter) 
      write(131,'(f19.7,2x,f19.12)')  iter_dt, NNHHGmom(iter) 
    endif

    !Start : POSTCISMO
    if (POSTCISMO) then
      onemomcismoz        = 0.d0
      twomomcismoz        = 0.d0
      threemomcismoz      = 0.d0
      fourmomcismoz       = 0.d0
      onemomcismozmoocc   = 0.d0
      twomomcismozmoocc   = 0.d0
      threemomcismozmoocc = 0.d0
      fourmomcismozmoocc  = 0.d0

      !Compute contribution of the specified OCCMO to the time-dependent dipole moment

      dipzMO(:,:) = pol_pump(1)*dipxMO(:,:) + pol_pump(2)*dipyMO(:,:) + pol_pump(3)*dipzMO(:,:)

      !Eq. 34
      !$OMP PARALLEL &
      !$OMP PRIVATE(onemomcismozmoocc_local) &
      !$OMP SHARED(onemomcismozmoocc)
      onemomcismozmoocc_local = 0.d0
      !$OMP do SCHEDULE(dynamic,8) 
      do k = 2, NRoots+1
        do a = 1, NV_a
          onemomcismozmoocc_local = onemomcismozmoocc_local &
                                  + real( dconjg(C_t(1,1)) * C_t(k,1) * 2.d0 * amps_a(k,OCCMO,a) * dipzMO(OCCMO,NO_a+a) &
                                        + dconjg(C_t(k,1)) * C_t(1,1) * 2.d0 * amps_a(k,OCCMO,a) * dipzMO(NO_a+a,OCCMO) ) 
        enddo
        do a = 1, NV_b
          onemomcismozmoocc_local = onemomcismozmoocc_local &
                                  + real( dconjg(C_t(1,1)) * C_t(k,1) * 2.d0 * amps_b(k,OCCMO,a) * dipzMO(OCCMO,NO_b+a) &
                                        + dconjg(C_t(k,1)) * C_t(1,1) * 2.d0 * amps_b(k,OCCMO,a) * dipzMO(NO_b+a,OCCMO) )
        enddo
      enddo
      !$OMP end do NOWAIT
      !$OMP CRITICAL(acc_critical)
      onemomcismozmoocc = onemomcismozmoocc + onemomcismozmoocc_local
      !$OMP end CRITICAL(acc_critical)
      !$OMP end PARALLEL
      zeromomcismozmoocc=0.d0
      zeromomcismozmoocc=real(dconjg(C_t(1,1))*C_t(1,1)*2.d0*dipzMO(OCCMO,OCCMO))
      write(778,*) 'ciao', dconjg(C_t(1,1))*C_t(1,1), dipzMO(OCCMO,OCCMO)

      onemomcismozmoocc=onemomcismozmoocc+zeromomcismozmoocc

      if (.not.mo1) then  
      !Eq. 36
      !$OMP PARALLEL &
      !$OMP PRIVATE(twomomcismozmoocc_local) &
      !$OMP SHARED(twomomcismozmoocc)
      twomomcismozmoocc_local = 0.d0
      !$OMP do SCHEDULE(dynamic,8) 
      do k = 2, NRoots+1
        do w = 2, NRoots+1
          do a = 1, NV_a
            do b = 1, NV_a
              twomomcismozmoocc_local = twomomcismozmoocc_local &
                                      + real(dconjg(C_t(k,1))*C_t(w,1)*2.d0*amps_a(k,OCCMO,a)*amps_a(w,OCCMO,b)*dsqrt(2.d0)*dipzMO(NO_a+a,NO_a+b))
            enddo
          enddo
          do a = 1, NV_b
            do b = 1, NV_b
              twomomcismozmoocc_local = twomomcismozmoocc_local &
                                      + real(dconjg(C_t(k,1))*C_t(w,1)*2.d0*amps_b(k,OCCMO,a)*amps_b(w,OCCMO,b)*dsqrt(2.d0)*dipzMO(NO_b+a,NO_b+b))
            enddo
          enddo
        enddo
      enddo
      !$OMP end do NOWAIT
      !$OMP CRITICAL(acc_critical)
      twomomcismozmoocc = twomomcismozmoocc + twomomcismozmoocc_local
      !$OMP end CRITICAL(acc_critical)
      !$OMP end PARALLEL

      !Eq. 35
      !$OMP PARALLEL &
      !$OMP PRIVATE(threemomcismozmoocc_local) &
      !$OMP SHARED(threemomcismozmoocc)
      threemomcismozmoocc_local = 0.d0
      !$OMP do SCHEDULE(dynamic,8) 
      do k = 2, NRoots+1
        do w = 2, NRoots+1
          do j = 1, NO_a
            do a = 1, NV_a
              threemomcismozmoocc_local = threemomcismozmoocc_local &
                                        - real(dconjg(C_t(k,1))*C_t(w,1)*2.d0*amps_a(k,OCCMO,a)*amps_a(w,j,a)*dsqrt(2.d0)*dipzMO(j,OCCMO))
            enddo
          enddo
          do j = 1, NO_b
            do a = 1, NV_b
              threemomcismozmoocc_local = threemomcismozmoocc_local &
                                        - real(dconjg(C_t(k,1))*C_t(w,1)*2.d0*amps_b(k,OCCMO,a)*amps_b(w,j,a)*dsqrt(2.d0)*dipzMO(j,OCCMO))
            enddo
          enddo
        enddo
      enddo
      !$OMP end do NOWAIT
      !$OMP CRITICAL(acc_critical)
      threemomcismozmoocc = threemomcismozmoocc + threemomcismozmoocc_local
      !$OMP end CRITICAL(acc_critical)
      !$OMP end PARALLEL

      !Eq. 37
      !$OMP PARALLEL &
      !$OMP PRIVATE(fourmomcismozmoocc_local) &
      !$OMP SHARED(fourmomcismozmoocc)
      fourmomcismozmoocc_local = 0.d0
      !$OMP do SCHEDULE(dynamic,8) 
      do k = 2, NRoots+1
        do w = 2, NRoots+1
          do a = 1, NV_a
            do g = 1, NO_a
              fourmomcismozmoocc_local = fourmomcismozmoocc_local &
                                       + real( dconjg(C_t(k,1))*C_t(w,1)*2.d0*amps_a(k,OCCMO,a) &
                                             * amps_a(w,OCCMO,a)*dsqrt(2.d0) & 
                                             * (dipzMO(g,g)-dipzMO(OCCMO,OCCMO)+dipzMO(NO_a+a,NO_a+a)))
            enddo
          enddo
          do a = 1, NV_b
            do g = 1, NO_b
              fourmomcismozmoocc_local = fourmomcismozmoocc_local &
                                       + real( dconjg(C_t(k,1))*C_t(w,1)*2.d0*amps_b(k,OCCMO,a) &
                                             * amps_b(w,OCCMO,a)*dsqrt(2.d0) &
                                             * (dipzMO(g,g)-dipzMO(OCCMO,OCCMO)+dipzMO(NO_b+a,NO_b+a)))
            enddo
          enddo
        enddo
      enddo
      !$OMP end do NOWAIT
      !$OMP CRITICAL(acc_critical)
      fourmomcismozmoocc = fourmomcismozmoocc + fourmomcismozmoocc_local
      !$OMP end CRITICAL(acc_critical)
      !$OMP end PARALLEL
      endif

      !Sum of Eqs 34, 35, 36 and 37 of notes.pdf
      mooccmomcismoz(iter) = onemomcismozmoocc + twomomcismozmoocc + threemomcismozmoocc + fourmomcismozmoocc
      write(700,'(f19.7,2x,f19.12)') iter_dt, mooccmomcismoz(iter)      

      !Eq. 34
      moocconemomcismoz(iter) = onemomcismozmoocc
      !Eq. 35
      moocctwomomcismoz(iter) = twomomcismozmoocc 
      !Eq. 36
      mooccthreemomcismoz(iter) = threemomcismozmoocc 
      !Eq. 37
      mooccfourmomcismoz(iter) = fourmomcismozmoocc

      mooccmomcismoz(iter)=mooccmomcismoz(iter)*WF(iter)
      moocconemomcismoz(iter)=moocconemomcismoz(iter)*WF(iter)
      moocctwomomcismoz(iter)=moocctwomomcismoz(iter)*WF(iter)
      mooccthreemomcismoz(iter)=mooccthreemomcismoz(iter)*WF(iter)
      mooccfourmomcismoz(iter)=mooccfourmomcismoz(iter)*WF(iter)

      write(500,*) iter_dt, mooccmomcismoz(iter)
      if (.not.mo1) then
        write(501,*) iter_dt, moocconemomcismoz(iter)
        write(502,*) iter_dt, moocctwomomcismoz(iter)
        write(503,*) iter_dt, mooccthreemomcismoz(iter)
        write(504,*) iter_dt, mooccfourmomcismoz(iter)
      endif
    endif
    !End : POSTCISMO

  enddo ! iter

  !End : TIME PROPAGATION

  !AFTER THE PROPOAGATION CONSTRUCTION of THE HHG
  !Start : FULL HHG
  Cmom_Z = (1.d0, 0.d0) * HHGmom_Z
  call cft_1z(Cmom_Z, td_max_iter, 1, td_max_iter, td_max_iter, -1, CHH_Z)
  if (field_type=='bcr') then
    Cmom_b = (1.d0, 0.d0) * HHGmom_b
    call cft_1z(Cmom_b, td_max_iter, 1, td_max_iter, td_max_iter, -1, CHH_b)
  endif 
  !End : FULL HHG

  !Start : CISGBC HHG
  if (CISGBC) then
    Cmomtotal = (1.d0, 0.d0) * momtotal
    CmomGG    = (1.d0, 0.d0) * momGG
    CmomGBBG  = (1.d0, 0.d0) * momGBBG
    CmomGCCG  = (1.d0, 0.d0) * momGCCG
    CmomBB    = (1.d0, 0.d0) * momBB
    CmomBCCB  = (1.d0, 0.d0) * momBCCB
    CmomCC    = (1.d0, 0.d0) * momCC
    call cft_1z(Cmomtotal, td_max_iter, 1, td_max_iter, td_max_iter, -1, totalHHG)
    call cft_1z(CmomGG   , td_max_iter, 1, td_max_iter, td_max_iter, -1, GGHHG)
    call cft_1z(CmomGBBG , td_max_iter, 1, td_max_iter, td_max_iter, -1, GBBGHHG)
    call cft_1z(CmomGCCG , td_max_iter, 1, td_max_iter, td_max_iter, -1, GCCGHHG)
    call cft_1z(CmomBB   , td_max_iter, 1, td_max_iter, td_max_iter, -1, BBHHG)
    call cft_1z(CmomBCCB , td_max_iter, 1, td_max_iter, td_max_iter, -1, BCCBHHG)
    call cft_1z(CmomCC   , td_max_iter, 1, td_max_iter, td_max_iter, -1, CCHHG)
  endif
  !End : CISGBC HHG

  !Start : POSTCIS HHG
  if (POSTCIS) then
    CXXHHGmom = (1.d0, 0.d0) * XXHHGmom
    CXAHHGmom = (1.d0, 0.d0) * XAHHGmom
    CXBHHGmom = (1.d0, 0.d0) * XBHHGmom
    CXCHHGmom = (1.d0, 0.d0) * XCHHGmom
    CXNHHGmom = (1.d0, 0.d0) * XNHHGmom
    CAAHHGmom = (1.d0, 0.d0) * AAHHGmom
    CABHHGmom = (1.d0, 0.d0) * ABHHGmom
    CACHHGmom = (1.d0, 0.d0) * ACHHGmom
    CANHHGmom = (1.d0, 0.d0) * ANHHGmom
    CBBHHGmom = (1.d0, 0.d0) * BBHHGmom
    CBCHHGmom = (1.d0, 0.d0) * BCHHGmom
    CBNHHGmom = (1.d0, 0.d0) * BNHHGmom
    CCCHHGmom = (1.d0, 0.d0) * CCHHGmom
    CCNHHGmom = (1.d0, 0.d0) * CNHHGmom
    CNNHHGmom = (1.d0, 0.d0) * NNHHGmom
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CXXHHGmom, td_max_iter, 1, nfft, nfft, -1, CXXHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CXAHHGmom, td_max_iter, 1, nfft, nfft, -1, CXAHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CXBHHGmom, td_max_iter, 1, nfft, nfft, -1, CXBHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CXCHHGmom, td_max_iter, 1, nfft, nfft, -1, CXCHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CXNHHGmom, td_max_iter, 1, nfft, nfft, -1, CXNHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CAAHHGmom, td_max_iter, 1, nfft, nfft, -1, CAAHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CABHHGmom, td_max_iter, 1, nfft, nfft, -1, CABHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CACHHGmom, td_max_iter, 1, nfft, nfft, -1, CACHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CANHHGmom, td_max_iter, 1, nfft, nfft, -1, CANHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CBBHHGmom, td_max_iter, 1, nfft, nfft, -1, CBBHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CBCHHGmom, td_max_iter, 1, nfft, nfft, -1, CBCHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CBNHHGmom, td_max_iter, 1, nfft, nfft, -1, CBNHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CCCHHGmom, td_max_iter, 1, nfft, nfft, -1, CCCHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CCNHHGmom, td_max_iter, 1, nfft, nfft, -1, CCNHH)
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(CNNHHGmom, td_max_iter, 1, nfft, nfft, -1, CNNHH)
  endif
  !End : POSTCIS HHG

  !Start : POSTCISMO HHG
  if (POSTCISMO) then
    if (.not.mo1) then
      mo1tmp = (1.d0, 0.d0) * moocconemomcismoz
      mo2tmp = (1.d0, 0.d0) * moocctwomomcismoz
      mo3tmp = (1.d0, 0.d0) * mooccthreemomcismoz
      mo4tmp = (1.d0, 0.d0) * mooccfourmomcismoz
    endif
    motmp = (1.d0, 0.d0) * mooccmomcismoz

    if (.not.mo1) then
      nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
      call cft_1z(mo1tmp, td_max_iter, 1, nfft, nfft, -1, HH1)
      nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
      call cft_1z(mo2tmp, td_max_iter, 1, nfft, nfft, -1, HH2)
      nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
      call cft_1z(mo3tmp, td_max_iter, 1, nfft, nfft, -1, HH3)
      nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
      call cft_1z(mo4tmp, td_max_iter, 1, nfft, nfft, -1, HH4)
    endif
    nfft = good_fft_order_1dz(td_max_iter, 1, 2*td_max_iter)
    call cft_1z(motmp, td_max_iter, 1, nfft, nfft, -1, HHt)
  endif
  !End : POSTCISMO HHG

  open(40, file='logHHG.dat')
  open(42, file='logHHG_b.dat')
  open(45, file='phaseHHG.dat')
  if(POSTCISMO) then
    if (.not.mo1) then
      open(221, file='logHHGmo1.dat')
      open(222, file='logHHGmo2.dat')
      open(223, file='logHHGmo3.dat')
      open(224, file='logHHGmo4.dat')
    endif
    open(225, file='logHHGmo.dat')
  endif

  !Start: WRITING HHG
  do iter = 1, td_max_iter

    write(40,*) dble(iter-1)/(dt*td_max_iter)*2.d0*PI/omega0_pump, dlog(zabs(CHH_Z(iter))**2)/dlog(10.d0)
    write(42,*) dble(iter-1)/(dt*td_max_iter)*2.d0*PI/omega0_pump, dlog(zabs(CHH_b(iter))**2)/dlog(10.d0)
    write(45,*) dble(iter-1)/(dt*td_max_iter)*2.d0*PI/omega0_pump, atan2(dimag(CHH_Z(iter)), dble(CHH_Z(iter)))
    if (CISGBC) then
      write(200,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(totalHHG(iter))**2)/dlog(10.d0)
      write(201,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(GGHHG(iter))**2)/dlog(10.d0)
      write(202,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(GBBGHHG(iter))**2)/dlog(10.d0)
      write(203,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(GCCGHHG(iter))**2)/dlog(10.d0)
      write(204,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(BBHHG(iter))**2)/dlog(10.d0)
      write(205,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(BCCBHHG(iter))**2)/dlog(10.d0)
      write(206,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CCHHG(iter))**2)/dlog(10.d0)
    endif
    if (POSTCIS) then
      write(206,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CXXHH(iter))**2)/dlog(10.d0)
      write(207,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CXAHH(iter))**2)/dlog(10.d0)
      write(208,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CXBHH(iter))**2)/dlog(10.d0)
      write(209,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CXCHH(iter))**2)/dlog(10.d0)
      write(210,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CXNHH(iter))**2)/dlog(10.d0)
      write(211,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CAAHH(iter))**2)/dlog(10.d0)
      write(212,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CABHH(iter))**2)/dlog(10.d0)
      write(213,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CACHH(iter))**2)/dlog(10.d0)
      write(214,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CANHH(iter))**2)/dlog(10.d0)
      write(215,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CBBHH(iter))**2)/dlog(10.d0)
      write(216,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CBCHH(iter))**2)/dlog(10.d0)
      write(217,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CBNHH(iter))**2)/dlog(10.d0)
      write(218,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CCCHH(iter))**2)/dlog(10.d0)
      write(219,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CCNHH(iter))**2)/dlog(10.d0)
      write(220,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(CNNHH(iter))**2)/dlog(10.d0)
    endif
    if (POSTCISMO) then
      if (.not.mo1) then
        write(221,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(HH1(iter))**2)/dlog(10.d0)
        write(222,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(HH2(iter))**2)/dlog(10.d0)
        write(223,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(HH3(iter))**2)/dlog(10.d0)
        write(224,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(HH4(iter))**2)/dlog(10.d0)
      endif
      write(225,*) dble(iter-1)/(dt*nfft)*2.d0*PI/omega0_pump, dlog(zabs(HHt(iter))**2)/dlog(10.d0)
    endif
  enddo

  !Start : Smoothed HHG spectrum
  CHH_S = 0.d0
  shhg = nsigma*omega0_pump
  do i = 1, td_max_iter
    do j = 1, td_max_iter
      CHH_S(i) = CHH_S(i) + zabs(CHH_Z(j))**2*dexp(-((j-i)*2.d0*PI/(dt*td_max_iter))**2/shhg**2)
    enddo
  enddo
  open(41, file='logHHGs.dat')
    do iter = 1, td_max_iter
      write(41,*) dble(iter-1)/(dt*td_max_iter)*2.d0*PI/omega0_pump, dlog(CHH_S(iter))/dlog(10.d0)
    enddo
  close(41)
  !End : Smoothed HHG spectrum

  write(*,*) 
  write(*,*) '***************************************'
  write(*,*) '***     end OF THE PROPAGATION      ***'
  write(*,*) '***************************************'
  write(*,*)

  close(51)
  close(40)
  close(42)
  close(43)
  close(44)
  close(52)
  close(53)
  close(54)
  close(55)
  close(56)
  close(57)
  close(58)
  close(59)
  close(60)
  close(61)
  close(62)
  close(63)
  close(90)
  close(100)
  close(140)
  close(141)
  close(142) 
  close(143)
  close(144)

  if (POSTCIS) then
    close(206)
    close(207)
    close(208)
    close(209)
    close(210)
    close(211)
    close(212)
    close(213)
    close(214)
    close(215)
    close(216)
    close(217)
    close(218)
    close(219)
    close(220)
  endif

  if (POSTCISMO) then
    close(700)
    close(500)
    close(501)
    close(502)
    close(503)
    close(504)
    close(221)
    close(222)
    close(223)
    close(224)
    close(225)
  endif

  deallocate( energies, virtualene_a, amps_a, virtualene_b, amps_b, amplitudessq_cis, energies_exp, CHH_b, WF &
            , dip_mom_x, dip_mom_y, dip_mom_z, lifetimes, rfit, C_t, zeigenvec_x, zeigenvec_y, zeigenvec_z    &
            , dip_eigenval_x, dip_eigenval_y, dip_eigenval_z, lasermu_x, lasermu_y, lasermu_z, C_t_tmp        &
            , C_t_dt_one, C_t_dt_two, C_t_dt_three, C_t_dt_four, C_t_dt_five, C_t_dt_six, C_t_dt_seven        &
            , C_t_dt_eight, C_t_dt_nine, C_t_dt, mom_X, mom_Y, mom_Z, HHGmom_Z, HHGmom_b, HHGmom_noWF, CHH_Z  &
            , Cmom_Z, Cmom_b, CHH_S )

  if (POSTCISMO) then
    deallocate(dipxMO, mo_dipole_y, mo_dipole_z, momcismoz, mooccmomcismoz, moocconemomcismoz, &
               moocctwomomcismoz, mooccthreemomcismoz, mooccfourmomcismoz, HH1, HH2, HH3, HH4, &
               HHt, mo1tmp, mo2tmp, mo3tmp, mo4tmp, motmp)
  endif

  if (CISGBC) then
    deallocate(momtotal_X, momtotal_Y, momtotal_Z, momGG_X, momGG_Y, momGG_Z, momGBBG_X, momGBBG_Y, momGBBG_Z, & 
               momGCCG_X, momGCCG_Y, momGCCG_Z, momBB_X, momBB_Y, momBB_Z, momBCCB_X, momBCCB_Y, momBCCB_Z,    &
               momCC_X, momCC_Y, momCC_Z, momGG, momGBBG, momGCCG, momBB, momBCCB, momCC, momtotal, Cmomtotal, &
               totalHHG, CmomGG, CmomGBBG, CmomGCCG, CmomBB, CmomBCCB, CmomCC, GGHHG, GBBGHHG, GCCGHHG, BBHHG, &
               BCCBHHG, CCHHG)
  endif

  if (POSTCIS) then
    deallocate(XXmom_X, XXmom_Y, XXmom_Z, XAmom_X, XAmom_Y, XAmom_Z, XBmom_x, XBmom_Y, XBmom_Z, XCmom_X, XCmom_Y, XCmom_Z,    &
               XNmom_X, XNmom_Y, XNmom_Z, AAmom_X, AAmom_Y, AAmom_Z, ABmom_X, ABmom_Y, ABmom_Z, ACmom_X, ACmom_Y, ACmom_Z,    &
               ANmom_X, ANmom_Y, ANmom_Z, BBmom_X, BBmom_Y, BBmom_Z, BCmom_X, BCmom_Y, BCmom_Z, BNmom_X, BNmom_Y, BNmom_Z,    &
               CCmom_X, CCmom_Y, CCmom_Z, CNmom_X, CNmom_Y, CNmom_Z, NNmom_X, NNmom_Y, NNmom_Z, XXHHGmom, XAHHGmom, XBHHGmom, &
               XCHHGmom, XNHHGmom, AAHHGmom, ABHHGmom, ACHHGmom, ANHHGmom, BBHHGmom, BCHHGmom, BNHHGmom, CCHHGmom, CNHHGmom,  &
               NNHHGmom, CXXHHGmom, CXAHHGmom, CXBHHGmom, CXCHHGmom, CXNHHGmom, CAAHHGmom, CABHHGmom, CACHHGmom, CANHHGmom,   &
               CBBHHGmom, CBCHHGmom, CBNHHGmom, CCCHHGmom, CCNHHGmom, CNNHHGmom, CXXHH, CXAHH, CXBHH, CXCHH, CXNHH, CAAHH,    &
               CABHH, CACHH, CANHH, CBBHH, CBCHH, CBNHH, CCCHH, CCNHH, CNNHH)
  endif

end
 
! ---

subroutine field_def(iter_dt, tau0_pump, potx, poty, potz)

  implicit none

  include 'constants.include.F'
  
  double precision, intent(in)  :: iter_dt, tau0_pump
  double precision, intent(out) :: potx, poty, potz

  double precision              :: env, omega1_pump_b

  PROVIDE field_type
  PROVIDE a0_pump phi_pump pol_pump pol_pump_b a1_pump_b omega0_pump omega1_pump delta_phi_pump
  
  if (field_type=='lin') then

    ! linear

    write(51,'(f19.7,2x,f19.12)') iter_dt, dsin(omega0_pump * (iter_dt + phi_pump))
    write(52,'(f19.7,2x,f19.12)') iter_dt, pol_pump(1) * a0_pump * (dcos(PI * (iter_dt - tau0_pump) / (2.d0 * tau0_pump)))**2
    write(53,'(f19.7,2x,f19.12)') iter_dt, pol_pump(2) * a0_pump * (dcos(PI * (iter_dt - tau0_pump) / (2.d0 * tau0_pump)))**2
    write(54,'(f19.7,2x,f19.12)') iter_dt, pol_pump(3) * a0_pump * (dcos(PI * (iter_dt - tau0_pump) / (2.d0 * tau0_pump)))**2

    env = (dcos(PI * (iter_dt - tau0_pump)/(2.d0 * tau0_pump)))**2

    potx = dsin(omega0_pump * (iter_dt + phi_pump)) * pol_pump(1) * a0_pump * env
    poty = dsin(omega0_pump * (iter_dt + phi_pump)) * pol_pump(2) * a0_pump * env
    potz = dsin(omega0_pump * (iter_dt + phi_pump)) * pol_pump(3) * a0_pump * env

  elseif (field_type=='bcr') then

    ! ??

    PROVIDE q_bcr

    omega1_pump_b = dble(q_bcr) * omega0_pump
    env = (dcos(PI * (iter_dt - tau0_pump) / (2.d0 * tau0_pump)))**2

    potx = (dcos(omega0_pump * (iter_dt + phi_pump)) + dcos(omega1_pump_b * (iter_dt + phi_pump))) * pol_pump  (1) * a0_pump   * env &
         + (dsin(omega0_pump * (iter_dt + phi_pump)) - dsin(omega1_pump_b * (iter_dt + phi_pump))) * pol_pump_b(1) * a1_pump_b * env
    poty = (dcos(omega0_pump * (iter_dt + phi_pump)) + dcos(omega1_pump_b * (iter_dt + phi_pump))) * pol_pump  (2) * a0_pump   * env &
         + (dsin(omega0_pump * (iter_dt + phi_pump)) - dsin(omega1_pump_b * (iter_dt + phi_pump))) * pol_pump_b(2) * a1_pump_b * env
    potz = (dcos(omega0_pump * (iter_dt + phi_pump)) + dcos(omega1_pump_b * (iter_dt + phi_pump))) * pol_pump  (3) * a0_pump   * env &
         + (dsin(omega0_pump * (iter_dt + phi_pump)) - dsin(omega1_pump_b * (iter_dt + phi_pump))) * pol_pump_b(3) * a1_pump_b * env

    write(52,'(f19.7,2x,f19.12,2x,f19.12,2x,f19.12)') iter_dt, potx, poty, potz
    write(90,'(f19.7,2x,f19.12)') iter_dt, dsqrt(potx**2+ poty**2+potz**2)

  elseif (field_type=='otc') then

    ! ??

    env = (dcos(PI * (iter_dt - tau0_pump) / (2.d0 * tau0_pump)))**2

    potx = dsin(omega0_pump   * (iter_dt + phi_pump))                  * pol_pump  (1) * a0_pump   * env &
         + dsin(omega1_pump_b * (iter_dt + phi_pump + delta_phi_pump)) * pol_pump_b(1) * a1_pump_b * env 
    poty = dsin(omega0_pump   * (iter_dt + phi_pump))                  * pol_pump  (2) * a0_pump   * env &
         + dsin(omega1_pump_b * (iter_dt + phi_pump + delta_phi_pump)) * pol_pump_b(2) * a1_pump_b * env 
    potz = dsin(omega0_pump   * (iter_dt + phi_pump))                  * pol_pump  (3) * a0_pump   * env &
         + dsin(omega1_pump_b * (iter_dt + phi_pump + delta_phi_pump)) * pol_pump_b(3) * a1_pump_b * env 

  elseif (field_type=='cir') then

    ! ??
  
    env = (dcos(PI * (iter_dt - tau0_pump) / (2.d0 * tau0_pump)))**2

    potx = dcos(omega0_pump   * (iter_dt + phi_pump)) * pol_pump  (1) * a0_pump   * env &
         + dsin(omega1_pump_b * (iter_dt + phi_pump)) * pol_pump_b(1) * a1_pump_b * env
    poty = dcos(omega0_pump   * (iter_dt + phi_pump)) * pol_pump  (2) * a0_pump   * env &
         + dsin(omega1_pump_b * (iter_dt + phi_pump)) * pol_pump_b(2) * a1_pump_b * env
    potz = dcos(omega0_pump   * (iter_dt + phi_pump)) * pol_pump  (3) * a0_pump   * env &
         + dsin(omega1_pump_b * (iter_dt + phi_pump)) * pol_pump_b(3) * a1_pump_b * env

  endif

  return
end

! ---


