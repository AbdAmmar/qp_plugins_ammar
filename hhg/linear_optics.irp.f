
! ---

subroutine linear_optics(N, energies, dip_mom_x, dip_mom_y, dip_mom_z)

  implicit none

  include 'constants.include.F'

  integer,          intent(in) :: N
  double precision, intent(in) :: energies(N+1)
  double precision, intent(in) :: dip_mom_x(N+1,N+1), dip_mom_y(N+1,N+1), dip_mom_z(N+1,N+1)

  integer                      :: i, j, io
  integer                      :: NOmega
  double precision             :: osc(N+1)
  double precision             :: Omegaev
  double precision             :: static_pol(9)
  complex*16, allocatable      :: Omega(:)
  complex*16, allocatable      :: polarizab_x(:), polarizab_y(:), polarizab_z(:)
  complex*16, allocatable      :: polarizab(:)

  ! ---

  !PROVIDE Omegae Omegai dOmega
  !PROVIDE broad
  !PROVIDE AU
  double precision :: Omegae, Omegai, dOmega, broad

  ! ---

  NOmega = int((Omegae - Omegai) / dOmega + 0.5 + 1)

  allocate(Omega(NOmega))
  allocate(polarizab_x(NOmega), polarizab_y(NOmega), polarizab_z(NOmega))
  allocate(polarizab(NOmega))

  ! conversion to Hartree
  Omegai = Omegai / au_to_eV
  Omegae = Omegae / au_to_eV
  dOmega = dOmega / au_to_eV
  broad  = broad  / au_to_eV

  ! calculating the static polarizability tensor
  static_pol = 0.d0 

  do i = 2, N+1
    static_pol(1) = static_pol(1) + 2.d0*dip_mom_x(1,i)*dip_mom_x(1,i)/(energies(i)-energies(1)) !XX
    static_pol(2) = static_pol(2) + 2.d0*dip_mom_x(1,i)*dip_mom_y(1,i)/(energies(i)-energies(1)) !XY
    static_pol(3) = static_pol(3) + 2.d0*dip_mom_x(1,i)*dip_mom_z(1,i)/(energies(i)-energies(1)) !XZ
    static_pol(5) = static_pol(5) + 2.d0*dip_mom_y(1,i)*dip_mom_y(1,i)/(energies(i)-energies(1)) !YY
    static_pol(6) = static_pol(6) + 2.d0*dip_mom_y(1,i)*dip_mom_z(1,i)/(energies(i)-energies(1)) !YZ
    static_pol(9) = static_pol(9) + 2.d0*dip_mom_z(1,i)*dip_mom_z(1,i)/(energies(i)-energies(1)) !ZZ 
  enddo
  static_pol(4) = static_pol(2) !YX
  static_pol(7) = static_pol(3) !ZX
  static_pol(8) = static_pol(6) !ZY

  open(85, file='static_pol.dat')
    do i = 1, 3
      j = (i-1)*3 + 1
      write(85,'(3f7.3)') static_pol(j), static_pol(j+1), static_pol(j+2)
    enddo 
  close(85)

  ! Calculating the dynamic polarizability tensor
  do io = 1, NOmega
    Omega(io) = dcmplx(Omegai + (io-1)*dOmega, broad)
  enddo

  polarizab_x = (0.d0,0.d0)
  do io = 1, NOmega
    do i = 2, N+1
      polarizab_x(io) = polarizab_x(io) + dip_mom_x(1,i) * dip_mom_x(1,i)/(energies(1)-energies(i)+Omega(io)) &
                                        + dip_mom_x(1,i) * dip_mom_x(1,i)/(energies(1)-energies(i)-Omega(io))
    enddo
  enddo

  open(unit=81, file='linear_optics_x.dat', status='unknown', form='formatted')
    do io = 1, NOmega
      Omegaev = real(Omega(io))
      write(81,'(f7.3,2e12.4)') Omegaev, aimag(polarizab_x(io)), real(polarizab_x(io))
    enddo
  close(81)

  polarizab_y = (0.d0,0.d0)
  do io = 1, NOmega
    do i = 2, N+1
      polarizab_y(io) = polarizab_y(io) - dip_mom_y(1,i) * dip_mom_y(1,i)/(energies(1)-energies(i)+Omega(io)) &
                                        - dip_mom_y(1,i) * dip_mom_y(1,i)/(energies(1)-energies(i)-Omega(io))
    enddo
  enddo

  open(unit=82, file='linear_optics_y.dat', status='unknown', form='formatted')
    do io = 1, NOmega
      Omegaev = real(Omega(io))
      write(82,'(f7.3,2e12.4)')  Omegaev, aimag(polarizab_y(io)), real(polarizab_y(io))
    enddo
  close(82)

  polarizab_z = (0.d0,0.d0)
  do io = 1, NOmega
    do i = 2, N+1
      polarizab_z(io) = polarizab_z(io) + dip_mom_z(1,i) * dip_mom_z(1,i)/(energies(i)-energies(1)-Omega(io)) &
                                        + dip_mom_z(1,i) * dip_mom_z(1,i)/(energies(i)-energies(1)+Omega(io))  
    enddo
  enddo

  open(unit=83, file='linear_optics_z.dat', status='unknown', form='formatted')
    do io = 1, NOmega
      Omegaev = real(Omega(io))
      write(83,'(f7.3,2e12.4)')  Omegaev, aimag(polarizab_z(io)), real(polarizab_z(io))
    enddo
  close(83)

  osc = 0.d0
  do i = 2, N+1
    osc(i) = 2.d0 * dabs(energies(1)-energies(i)) * (dip_mom_x(1,i)**2 + dip_mom_y(1,i)**2 + dip_mom_z(1,i)**2) / 3.d0
    write(*,*) i, dabs(energies(1)-energies(i)), osc(i)
  enddo

  polarizab = 0.d0
  do io = 1, NOmega
    do i = 2, N+1
      polarizab(io) = polarizab(io) - osc(i) / (dabs(energies(1)-energies(i)) * (energies(1)-energies(i)+Omega(io)))
    enddo
  enddo

  open(unit=84, file= 'linear_optics.dat', status='unknown', form='formatted')
    do io = 1, NOmega
      Omegaev = real(Omega(io))
      write(84,'(f7.3,2e12.4)') Omegaev, real(polarizab(io)), aimag(polarizab(io))
    enddo
  close(84)

  deallocate(Omega)
  deallocate(polarizab_x, polarizab_y, polarizab_z)
  deallocate(polarizab)

end subroutine linear_optics

! ---

