===
hhg
===

Plugin based on the code "Light" written by E. Luppi and E. Coccia.



Installation
============

To install and compile this plugin, follow these steps:

1. Navigate to the plugins directory:

    .. code-block:: sh
    
    cd $QP_ROOT/plugins

2. Download the plugin using `qp_plugins`:

    .. code-block:: sh

    qp_plugins download https://gitlab.com/AbdAmmar/qp_plugins_ammar

3. Change to the plugin directory:

     .. code-block:: sh

     cd qp_plugins_ammar

4. Install the plugin `hhg`:

    .. code-block:: sh

    qp_plugins install hhg

5. Navigate to the source directory:

     .. code-block:: sh

     cd $QP_ROOT/src/hhg

6. Compile the plugin using `ninja`:

     .. code-block:: sh

     ninja


Following these steps will ensure that the plugin is properly installed and compiled.


Run a Calculation
=================

In this section, we illustrate how to run a calculation using this plugin by taking the water molecule as example.

1. Create an EZFIO:

   .. code-block:: sh

      EZFIO=H2O
      basis=cc-pvdz
      # after creating the H2O.xyz file:
      qp create_ezfio -b ${basis} ${EZFIO}.xyz -o $EZFIO
      qp set_file $EZFIO

2. Run a Hartree-Fock (HF) calculation:

   .. code-block:: sh

      qp run scf

3. Choose the number of states (ground state + excited states):

   .. code-block:: sh

      qp set determinants n_states 21

4. Run a CIS calculation:

   .. code-block:: sh

      qp run cis

5. Choose the parameters (the parameters are in the `EZFIO.cfg` file), for example:

   .. code-block:: sh

      # Number of time steps
      qp set hhg td_max_iter 12000
      # Delta t in atomic units
      qp set hhg dt 0.05
      # Polarization of laser
      qp set hhg pol_pump "[0.0,0.0,1.0]"
      # Frequency of laser
      qp set hhg omega0_pump 0.05695419
      # E0 (laser amplitude in atomic units)
      qp set hhg a0_pump 0.05345224838248487691
      # Optical cycles
      qp set hhg oc 5
      # Ionization potential
      qp set hhg ion_pot 0.493
      # Smoothed HHG spectrum
      qp set hhg nsigma 3
      # Escape length which the electron can travel
      qp set hhg dist_escape 1
      # Escape length which the electron can travel
      qp set hhg dist1_escape 16.48

6. Run the HHG calculation:

   .. code-block:: sh

      qp run hhg

