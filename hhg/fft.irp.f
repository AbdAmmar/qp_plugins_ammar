! ---

function allowed(nr)

  ! find if the fft dimension is a good one
  ! a "bad one" is either not implemented (as on IBM with ESSL)
  ! or implemented but with awful performances (most other cases)

  implicit none

  integer :: nr
  logical :: allowed
  integer :: pwr (5)
  integer :: mr, i, fac, p, maxpwr
  integer :: factors( 5 ) = (/ 2, 3, 5, 7, 11 /)

  ! find the factors of the fft dimension

  mr  = nr
  pwr = 0
  factors_loop: do i = 1, 5
    fac = factors (i)
    maxpwr = NINT ( LOG( DBLE (mr) ) / LOG( DBLE (fac) ) ) + 1
    do p = 1, maxpwr
      if ( mr == 1 ) exit factors_loop
      if ( MOD (mr, fac) == 0 ) then
        mr = mr / fac
        pwr (i) = pwr (i) + 1
      endif
    enddo
  end do factors_loop

  if(nr /= (mr * 2**pwr(1) * 3**pwr(2) * 5**pwr(3) * 7**pwr(4) * 11**pwr(5))) then
    print*, ' error in allowed: what ?!?', 1
    stop
  endif

  if(mr /= 1) then

    ! fft dimension contains factors > 11 : no good in any case

    allowed = .false.

  else

    allowed = ((pwr(4) == 0) .and. (pwr(5) == 0))

  endif

  return
end

! ---

integer function good_fft_order_1dz(nr, np, nfft_max)

!    
!    This function find a "good" fft order value grather or equal to "nr"
!    the check about the max fft dimension is done using the nfftx_1d parameter
!
!    nr  (input) tentative order n of a fft
!            
!    np  (input) restrict the search of the order in the ensamble of multiples of np
!
!    nfft_max (input) overwrites the global nfftx
!            
!    Output: the same if n is a good number
!         the closest higher number that is good
!         an fft order is not good if not implemented (as on IBM with ESSL)
!         or implemented but with awful performances (most other cases)
!

  implicit none

  integer, intent(in) :: nr
  integer, intent(in) :: np, nfft_max

  integer           :: nfftxx
  integer, external :: good_fft_order

  ! set the local max-allowed-values
  nfftxx = nfft_max

  ! call to the 3D routine
  good_fft_order_1dz = good_fft_order(nr, np, nfftxx) 

end

! ---

integer function good_fft_order(nr, np, nfft_max)

!    
!    This function find a "good" fft order value grather or equal to "nr"
!
!    nr  (input) tentative order n of a fft
!            
!    np  (input) restrict the search of the order in the ensamble of multiples of np
!
!    nfft_max (input) overwrites the global nfftx
!            
!    Output: the same if n is a good number
!         the closest higher number that is good
!         an fft order is not good if not implemented (as on IBM with ESSL)
!         or implemented but with awful performances (most other cases)
!

  implicit none

  integer, intent(in) :: nr
  integer, intent(in) :: np, nfft_max

  integer           :: new, nfftxx
  logical, external :: allowed

  ! set the local max-allowed-values
  nfftxx = nfft_max

  new = nr
  do while(((.not. allowed(new)) .or. (mod(new, np) /= 0)) .and. (new <= nfftxx))
    new = new + 1
  end do

  if(new > nfftxx) then
    print*, ' problem in good_fft_order: fft order too large ', new
    stop
  endif

  good_fft_order = new
  
  return
end

! ---

!         FFT along "z" 
subroutine cft_1z(c, c_dim, nsl, nz, ldz, isign, cout)

!     driver routine for nsl 1d complex fft's of length nz 
!     ldz >= nz is the distance between sequences to be transformed
!     (ldz>nz is used on some architectures to reduce memory conflicts)
!     input  :  c(ldz*nsl)   (complex)
!     output : cout(ldz*nsl) (complex - NOTA BENE: transform is not in-place!)
!     isign > 0 : forward (f(G)=>f(R)), isign <0 backward (f(R) => f(G))
!     Up to "ndims=3" initializations (for different combinations of input
!     parameters nz, nsl, ldz) are stored and re-used if available

  implicit none

  integer, intent(in) :: isign
  integer, intent(in) :: c_dim, nsl, nz, ldz
  complex*16          :: c(c_dim), cout(c_dim) 

  double precision    :: tscale
  integer             :: i, err, idir, ip
  integer, save       :: zdims(3,3) = -1
  integer, save       :: icurrent = 1
  logical             :: done

  !   ltabl   Dimension of the tables of factors calculated at the
  !           initialization stage

  !C_POINTER, SAVE :: fw_planz(3) = 0
  !C_POINTER, SAVE :: bw_planz(3) = 0
  integer*8, SAVE :: fw_planz(3) = 0
  integer*8, SAVE :: bw_planz(3) = 0

  !   Pointers to the "C" structures containing FFT factors ( PLAN )
  !   C_POINTER is defined in include/fft_defs.h
  !   for 32bit executables, C_POINTER is integer(4)
  !   for 64bit executables, C_POINTER is integer(8)

  if(nsl < 0) then
    print*, ' error in fft_scalar: cft_1z nsl out of range ', nsl
    stop
  end if

  !
  !   Here initialize table only if necessary
  !

  DO ip = 1, 3

    !   first check if there is already a table initialized
    !   for this combination of parameters

    done = (nz == zdims(1,ip))
    IF (done) EXIT
  END DO

  IF( .NOT. done ) THEN

     !   no table exist for these parameters
     !   initialize a new one

     IF(fw_planz(icurrent) /= 0) CALL DESTROY_PLAN_1D(fw_planz(icurrent))
     IF(bw_planz(icurrent) /= 0) CALL DESTROY_PLAN_1D(bw_planz(icurrent))
     idir = -1
     CALL CREATE_PLAN_1D(fw_planz(icurrent), nz, idir) 
     idir =  1
     CALL CREATE_PLAN_1D(bw_planz(icurrent), nz, idir) 

     zdims(1,icurrent) = nz 
     zdims(2,icurrent) = nsl
     zdims(3,icurrent) = ldz
     ip = icurrent
     icurrent = MOD(icurrent, 3) + 1

  END IF

  !
  !   Now perform the FFTs using machine specific drivers
  !

  IF (isign < 0) THEN
    CALL FFT_Z_STICK(fw_planz( ip), c(1), ldz, nsl)
    cout( 1 : ldz * nsl ) = c( 1 : ldz * nsl ) / nz
  ELSE IF (isign > 0) THEN
    CALL FFT_Z_STICK(bw_planz( ip), c(1), ldz, nsl)
    cout( 1 : ldz * nsl ) = c( 1 : ldz * nsl )
  END IF

  RETURN
END


