program hhg

  implicit none

  BEGIN_DOC
  ! TODO : Put the documentation of the program here
  END_DOC

  print *, ' start a HHG calculation'

  read_wf = .True.
  SOFT_TOUCH read_wf

  !call run_cis()
  call run_propagation_laser_cossquare()

end

! ---

subroutine run_cis()

  implicit none
  integer          :: i
  double precision :: coef_th

  !coef_th = save_threshold
  coef_th = 0.d0

  if(pseudo_sym) then
    call H_apply_cis_sym
  else
    call H_apply_cis
  endif

  print *, ''
  print *, ' N_det = ', N_det
  print *, ' ******************************'
  print *, ' Energies of the states:'
  do i = 1, N_states
    print *, i, CI_energy(i)
  enddo

  if (N_states > 1) then
    print *, ''
    print *, ' ******************************************************'
    print *, ' Excitation energies (au)                     (eV)'
    do i = 2, N_states
      print *, i, CI_energy(i) - CI_energy(1), (CI_energy(i) - CI_energy(1))/0.0367502d0
    enddo
    print *, ''
  endif

  call ezfio_set_cis_energy(CI_energy)
  psi_coef = ci_eigenvectors
  SOFT_TOUCH psi_coef
  call save_wavefunction_truncated(coef_th)

end

! ---
 
subroutine run_propagation_laser_cossquare()

  implicit none
  integer :: NRoots

  PROVIDE N_int
  PROVIDE N_det
  PROVIDE n_states
  PROVIDe psi_coef

  NRoots = n_states - 1

  if(NRoots .ge. 1) then
    call propagation_laser_cossquare(NRoots)
  else
    print*, ' no enouph states ! n_states = ', n_states
    print*, ' try with larger n_states'
    stop
  endif

end

! ---

