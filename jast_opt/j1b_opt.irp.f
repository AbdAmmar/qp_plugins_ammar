program j1b_opt

  BEGIN_DOC
! TODO : Put the documentation of the program here
  END_DOC

  implicit none

  my_grid_becke  = .True.
  my_n_pt_r_grid = 30
  my_n_pt_a_grid = 50
  touch my_grid_becke my_n_pt_r_grid my_n_pt_a_grid

  call main()

end

! ---

subroutine main()

  implicit none
  integer                    :: iA, ipoint, jpoint
  double precision           :: r1(3), r2(3), A_center(3)
  double precision           :: a_A, c_A, u12, v1, v2, w1, w2, d1A_sq, d2A_sq, int_A
  double precision, external :: jmu

  PROVIDE mu_erf j1b_pen j1b_coeff

  do iA = 1, nucl_num
    A_center(1:3) = nucl_coord(iA,1:3)
    a_A           = j1b_pen   (iA)
    c_A           = j1b_coeff (iA)

    int_A = 0.d0
    do ipoint = 1, n_points_final_grid
      r1(1) = final_grid_points(1,ipoint)
      r1(2) = final_grid_points(2,ipoint)
      r1(3) = final_grid_points(3,ipoint)

      d1A_sq = (r1(1) - A_center(1)) * (r1(1) - A_center(1)) &
             + (r1(2) - A_center(2)) * (r1(2) - A_center(2)) &
             + (r1(3) - A_center(3)) * (r1(3) - A_center(3))

      w1 = dexp(-d1A_sq) * final_weight_at_r_vector(ipoint)
      v1 = c_A * dexp(-a_A * d1A_sq)

      do jpoint = 1, n_points_final_grid
        r2(1) = final_grid_points(1,jpoint)
        r2(2) = final_grid_points(2,jpoint)
        r2(3) = final_grid_points(3,jpoint)

        d2A_sq = (r2(1) - A_center(1)) * (r2(1) - A_center(1)) &
               + (r2(2) - A_center(2)) * (r2(2) - A_center(2)) &
               + (r2(3) - A_center(3)) * (r2(3) - A_center(3))

        w2  = dexp(-d2A_sq) * final_weight_at_r_vector(jpoint)
        v2  = c_A * dexp(-a_A * d2A_sq)
        u12 = jmu(r1, r2)
  
        int_A += (u12 + 0.5d0 * (v1 + v2)) * w1 * w2
      enddo
    enddo

    print *, iA, int_A
  enddo

end subroutine main

! ---

double precision function jmu(r1, r2)

  include 'utils/constants.include.F'

  implicit none
  double precision, intent(in) :: r1(3), r2(3)
  double precision             :: tmp, d12

  d12 = dsqrt( (r2(1) - r1(1)) * (r2(1) - r1(1)) &
             + (r2(2) - r1(2)) * (r2(2) - r1(2)) &
             + (r2(3) - r1(3)) * (r2(3) - r1(3)) )

  tmp = mu_erf * d12
  
  jmu = 0.5d0 * d12 * (1.d0 - derf(tmp)) - 0.5d0 * inv_sq_pi * dexp(-tmp*tmp) / mu_erf

  return
end function jmu

! ---

