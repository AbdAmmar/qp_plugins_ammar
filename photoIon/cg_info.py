
import sys, os

QP_PATH = os.environ["QP_ROOT"]+"/external/ezfio/Python/"
sys.path.insert(0, QP_PATH)

from ezfio import ezfio

import numpy as np

# _____________________________________________________________________________
#
def read_cg_expo(file_name):

    print(' reading {} ...'.format(file_name))
    with open(file_name) as ff:
        lines = ff.readlines()

    cg_expo_re = np.zeros((cg_num, l_num))
    cg_expo_im = np.zeros((cg_num, l_num))

    iline = 0
    for l in range(l_num):
        for i in range(cg_num):
            line = lines[iline].split()
            cg_expo_re[i,l] = float( line[0] )
            cg_expo_im[i,l] = float( line[1] )
            iline += 1

    ezfio.set_cg_opt_cg_expo_re(cg_expo_re.T)
    ezfio.set_cg_opt_cg_expo_im(cg_expo_im.T)

    return()

# ---

def read_cg_coef(file_name):

    print(' reading {} ...'.format(file_name))
    with open(file_name) as ff:
        lines = ff.readlines()

    cg_coef_re = np.zeros((cg_num, l_num, k_num))
    cg_coef_im = np.zeros((cg_num, l_num, k_num))

    iline = 0
    for k in range(k_num):
        for l in range(l_num):
            for i in range(cg_num):
                line = lines[iline].split()
                cg_coef_re[i,l,k] = float( line[0] )
                cg_coef_im[i,l,k] = float( line[1] )
                iline += 1

    ezfio.set_cg_opt_cg_coef_re(cg_coef_re.T)
    ezfio.set_cg_opt_cg_coef_im(cg_coef_im.T)

    return()

# ---

def read_phase_shift(file_name):

    print(' reading {} ...'.format(file_name))
    with open(file_name) as ff:
        lines = ff.readlines()

    phase_shift = np.zeros((l_num, k_num))

    iline = 0
    for k in range(k_num):
        for l in range(l_num):
            line = lines[iline].split()
            phase_shift[l,k] = float( line[0] )
            iline += 1

    ezfio.set_cg_opt_phase_shift(phase_shift.T)

    return()
# _____________________________________________________________________________


# 6-31g
#v_ion = [ -20.5603806066, -1.3563969468, -0.7102370666,  -0.5606215537, -0.5013811196 ]
# 3-21g
#       [ -20.4281145888, -1.3294775850 , -0.6857022885, -0.5376070389 , -0.4796071253 ]



ezfio.set_file( sys.argv[1] )
mo_ind = int(sys.argv[2]) 
v_ion = float(sys.argv[3]) 

cg_num = 30
l_num  = 5
k_num  = 18
ezfio.set_cg_opt_cg_num(cg_num)
ezfio.set_cg_opt_l_num(l_num)
ezfio.set_cg_opt_k_num(k_num)

k_grid = [ 0.1 + 0.1*i for i in range(k_num) ]
ezfio.set_cg_opt_k_grid(k_grid)

read_cg_expo("cg_data/cg_expo.txt")
read_cg_coef(f"cg_data/cg_coef_dist_{mo_ind}.txt")
read_phase_shift(f"cg_data/phase_shift_dist_{mo_ind}.txt")

ezfio.set_photoion_mo_ind(mo_ind)
ne_eff = 2
ezfio.set_photoion_ne_eff(ne_eff)
#ezfio.set_photoion_v_ion(v_ion[mo_ind-1])
ezfio.set_photoion_v_ion(v_ion)


