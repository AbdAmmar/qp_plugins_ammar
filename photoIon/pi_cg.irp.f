program pi_cg

  implicit none

  call run()

end


! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
subroutine run()

  implicit none

  double precision, parameter   :: pi = dacos(-1.d0)
  double precision, parameter   :: c_vel = 137.03599d0
  double precision, parameter   :: to_Mb = 5.29177210903d0 * 5.29177210903d0
  complex*16,       parameter   :: Jcomp = (0.d0,1.d0)

  integer                       :: i, i_ke
  integer                       :: max_primG, l_max
  double precision              :: ke, E_ke, tmpke_LG, tmpke_VG 
  complex*16                    :: A0tmp_LG, A0tmp_VG, A2tmp_LG, A2tmp_VG

  double precision, allocatable :: nucl_pos(:,:)
  double precision, allocatable :: tmp_expo_re(:,:), tmp_expo_im(:,:)
  double precision, allocatable :: tmp_coef_re(:,:,:), tmp_coef_im(:,:,:)
  double precision, allocatable :: ephot_grid0(:)
  double precision, allocatable :: CS_LG0(:), CS_VG0(:), beta_LG0(:), beta_VG0(:)
  complex*16      , allocatable :: cg_expo(:,:), cg_coef(:,:,:)
  
  complex*16, external          :: A0_LG, A0_VG, A2_LG, A2_VG

  max_primG = maxval(ao_prim_num) 

  allocate( nucl_pos(ao_num,3) )
  nucl_pos(:,:) = 0.d0
  do i = 1, ao_num
    nucl_pos(i,1:3) = nucl_coord(ao_nucl(i), 1:3)
  enddo

  PROVIDE cg_num l_num k_num mo_ind ne_eff
  PROVIDE v_ion

  l_max = l_num - 1

  ! ---

  allocate( tmp_expo_re(cg_num, 0:l_max), tmp_expo_im(cg_num, 0:l_max) )
  call ezfio_get_cg_opt_cg_expo_re(tmp_expo_re)
  call ezfio_get_cg_opt_cg_expo_im(tmp_expo_im)

  allocate( cg_expo(cg_num, 0:l_max) )
  cg_expo(cg_num, 0:l_max) = (0.d0, 0.d0)

  ! - sign: because we fit f_l instead of f_l*
  cg_expo = tmp_expo_re - Jcomp * tmp_expo_im

  deallocate( tmp_expo_re, tmp_expo_im )

  ! ---

  allocate( tmp_coef_re(cg_num, 0:l_max, k_num), tmp_coef_im(cg_num, 0:l_max, k_num) )
  call ezfio_get_cg_opt_cg_coef_re(tmp_coef_re)
  call ezfio_get_cg_opt_cg_coef_im(tmp_coef_im)

  allocate( cg_coef(cg_num, 0:l_max, k_num) )
  cg_coef(:,:,:) = (0.d0, 0.d0)

  ! - sign: because we fit f_l instead of f_l*
  cg_coef = tmp_coef_re - Jcomp * tmp_coef_im

  deallocate( tmp_coef_re, tmp_coef_im )

  ! ---

  PROVIDE phase_shift

  PROVIDE k_grid

  ! ---

  allocate( ephot_grid0(k_num), CS_LG0(k_num), CS_VG0(k_num), beta_LG0(k_num), beta_VG0(k_num) )
  ephot_grid0(:) = 0.d0
  CS_LG0(:)      = 0.d0
  CS_VG0(:)      = 0.d0
  beta_LG0(:)    = 0.d0 
  beta_VG0(:)    = 0.d0

  do i_ke = 1, k_num
  !do i_ke = 1, 1

    ke   = k_grid(i_ke)
    E_ke = 0.5d0 * ke * ke + dabs(v_ion)

    ephot_grid0(i_ke) = E_ke

    tmpke_LG = to_Mb * dble(ne_eff) * 8.d0 * pi * E_ke / (3.d0 * ke * c_vel)
    tmpke_VG = to_Mb * dble(ne_eff) * 8.d0 * pi / (3.d0 * ke * c_vel * E_ke)

    A0tmp_LG = A0_LG( l_max, ao_num, max_primG, nucl_pos, ao_power, ao_prim_num                     &
                    , ao_coef_normalized_ordered_transp, ao_expo_ordered_transp, mo_coef(:, mo_ind) &
                    , cg_num, cg_coef(:,:,i_ke), cg_expo )

    A0tmp_VG = A0_VG( l_max, ao_num, max_primG, nucl_pos, ao_power, ao_prim_num                     &
                    , ao_coef_normalized_ordered_transp, ao_expo_ordered_transp, mo_coef(:, mo_ind) &
                    , cg_num, cg_coef(:,:,i_ke), cg_expo )

    A2tmp_LG = A2_LG( l_max, ao_num, max_primG, nucl_pos, ao_power, ao_prim_num                     &
                    , ao_coef_normalized_ordered_transp, ao_expo_ordered_transp, mo_coef(:, mo_ind) &
                    , cg_num, cg_coef(:,:,i_ke), cg_expo, phase_shift(:,i_ke) )

    A2tmp_VG = A2_VG( l_max, ao_num, max_primG, nucl_pos, ao_power, ao_prim_num                     &
                    , ao_coef_normalized_ordered_transp, ao_expo_ordered_transp, mo_coef(:, mo_ind) &
                    , cg_num, cg_coef(:,:,i_ke), cg_expo, phase_shift(:,i_ke) )

    CS_LG0(i_ke) = tmpke_LG * A0tmp_LG 
    CS_VG0(i_ke) = tmpke_VG * A0tmp_VG

    beta_LG0(i_ke) = dsqrt(5.d0) * A2tmp_LG / A0tmp_LG 
    beta_VG0(i_ke) = dsqrt(5.d0) * A2tmp_VG / A0tmp_VG 
  enddo

  deallocate( nucl_pos )
  deallocate( cg_expo, cg_coef )

  call ezfio_set_photoIon_ephot_grid(ephot_grid0)
  call ezfio_set_photoIon_cs_lg(CS_LG0)
  call ezfio_set_photoIon_cs_vg(CS_VG0)
  call ezfio_set_photoIon_beta_lg(beta_LG0)
  call ezfio_set_photoIon_beta_vg(beta_VG0)

  deallocate( ephot_grid0, CS_LG0 , CS_VG0 , beta_LG0 , beta_VG0 )


end subroutine run
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *






!_____________________________________________________________________________________________
!_____________________________________________________________________________________________

complex*16 function A0_LG(l_max                                                     &
            , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
            , ng, coefcG, expocG)

  implicit none

  integer,          intent(in)  :: l_max, ng, AO_nb, max_primG
  double precision, intent(in)  :: nucl_pos(AO_nb,3)
  integer,          intent(in)  :: nb_primG(AO_nb), AOpower(AO_nb,3)
  double precision, intent(in)  :: AOcoef(max_primG,AO_nb), AOexpo(max_primG,AO_nb)
  double precision, intent(in)  :: mocoef(AO_nb)
  complex*16,       intent(in)  :: coefcG(ng,0:l_max), expocG(ng,0:l_max)

  complex*16, external          :: M_LG

  integer                       :: l, m, mu
  complex*16                    :: Mlmmu_LG

  A0_LG = (0.d0,0.d0)
  do l = 0, l_max
    do m = -l, l
      do mu = -1, 1
        Mlmmu_LG = M_LG(l, m, mu                                                  &
          , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
          , ng, coefcG(:,l), expocG(:,l))

        A0_LG = A0_LG + Mlmmu_LG * conjg(Mlmmu_LG)
      enddo
    enddo
  enddo

  return
end function A0_LG
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________




!_____________________________________________________________________________________________
!_____________________________________________________________________________________________

complex*16 function A0_VG(l_max                                                     &
            , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
            , ng, coefcG, expocG)

  implicit none

  integer,          intent(in)  :: l_max, ng, AO_nb, max_primG
  double precision, intent(in)  :: nucl_pos(AO_nb,3)
  integer,          intent(in)  :: nb_primG(AO_nb), AOpower(AO_nb,3)
  double precision, intent(in)  :: AOcoef(max_primG,AO_nb), AOexpo(max_primG,AO_nb)
  double precision, intent(in)  :: mocoef(AO_nb)
  complex*16,       intent(in)  :: coefcG(ng,0:l_max), expocG(ng,0:l_max)

  complex*16, external          :: M_VG

  integer                       :: l, m, mu
  complex*16                    :: Mlmmu_VG

  A0_VG = (0.d0,0.d0)
  do l = 0, l_max
    do m = -l, l
      do mu = -1, 1
        Mlmmu_VG = M_VG(l, m, mu                                                  &
          , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
          , ng, coefcG(:,l), expocG(:,l))

        A0_VG = A0_VG + Mlmmu_VG * conjg(Mlmmu_VG)
      enddo
    enddo
  enddo

  return
end function A0_VG
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________





!_____________________________________________________________________________________________
!_____________________________________________________________________________________________

complex*16 function A2_LG(l_max                                                     &
            , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
            , ng, coefcG, expocG, phshift)

  implicit none

  double precision,   parameter :: pi = dacos(-1.d0)
  complex*16,         parameter :: Jcomp = (0.d0,1.d0)

  integer,          intent(in)  :: l_max, ng, AO_nb, max_primG
  double precision, intent(in)  :: nucl_pos(AO_nb,3)
  integer,          intent(in)  :: nb_primG(AO_nb), AOpower(AO_nb,3)
  double precision, intent(in)  :: AOcoef(max_primG,AO_nb), AOexpo(max_primG,AO_nb)
  double precision, intent(in)  :: mocoef(AO_nb)
  complex*16,       intent(in)  :: coefcG(ng,0:l_max), expocG(ng,0:l_max)
  double precision, intent(in)  :: phshift(0:l_max)

  double precision, external    :: WIGNER_COEFF
  complex*16,       external    :: M_LG

  integer                       :: l1, l2, m1, m2, mu1, mu2
  integer                       :: l2_min, l2_max
  double precision              :: l12, m12
  double precision              :: wl, wmu, wm
  complex*16                    :: tmpl, sum_l, sum_m1, sum_m2, sum_mu2
  complex*16                    :: Mlmmu1_LG, Mlmmu2_LG

  sum_l = (0.d0,0.d0)
  do l1 = 0, l_max

    l2_min = max(0    ,abs(l1-2))
    l2_max = min(l_max,   (l1+2))
    do l2 = l2_min, l2_max

      l12 = dabs( modulo(dble(l1+l2),2.d0) )
      if(l12.GT.1d-7) cycle

      wl   = WIGNER_COEFF(dble(l1), dble(l2), 2.d0, 0.d0, 0.d0)
      tmpl = wl                                                      &
           * dsqrt( (2.d0*dble(l1)+1.d0) * (2.d0*dble(l2)+1.d0) )    &
           * (-1.d0*Jcomp)**dble(l1-l2)                              &
           * zexp( Jcomp * (phshift(l1)-phshift(l2)) )

      sum_m1 = (0.d0,0.d0)
      do m1 = -l1, l1
        do mu1 = -1, 1

          Mlmmu1_LG = (-1.d0)**(dble(m1-mu1)) * M_LG(l1, m1, mu1                            &
                    , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
                    , ng, coefcG(:,l1), expocG(:,l1))

          sum_mu2 = (0.d0,0.d0)
          do mu2 = -1, 1
            wmu = WIGNER_COEFF(1.d0, 1.d0, 2.d0, dble(mu1), dble(-mu2))

            sum_m2 = (0.d0,0.d0)
            do m2 = -l2, l2
              m12 = dabs( dble(m2-m1-mu2+mu1) )
              if(m12.gt.1d-7) cycle

              wm        = WIGNER_COEFF(dble(l1), dble(l2), 2.d0, dble(-m1), dble(m2))
              Mlmmu2_LG = M_LG(l2, m2, mu2                                                      &
                        , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
                        , ng, coefcG(:,l2), expocG(:,l2))

              sum_m2 = sum_m2 + wm * conjg(Mlmmu2_LG)
            enddo

            sum_mu2 = sum_mu2 + wmu * sum_m2
          enddo

          sum_m1 = sum_m1 + Mlmmu1_LG * sum_mu2
        enddo
      enddo

      sum_l = sum_l + tmpl * sum_m1
    enddo
  enddo

  A2_LG = sum_l * dsqrt(6.d0)

  return
end function A2_LG
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________





!_____________________________________________________________________________________________
!_____________________________________________________________________________________________

complex*16 function A2_VG(l_max                                                     &
            , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
            , ng, coefcG, expocG, phshift)

  implicit none

  double precision,   parameter :: pi = dacos(-1.d0)
  complex*16,         parameter :: Jcomp = (0.d0,1.d0)

  integer,          intent(in)  :: l_max, ng, AO_nb, max_primG
  double precision, intent(in)  :: nucl_pos(AO_nb,3)
  integer,          intent(in)  :: nb_primG(AO_nb), AOpower(AO_nb,3)
  double precision, intent(in)  :: AOcoef(max_primG,AO_nb), AOexpo(max_primG,AO_nb)
  double precision, intent(in)  :: mocoef(AO_nb)
  complex*16,       intent(in)  :: coefcG(ng,0:l_max), expocG(ng,0:l_max)
  double precision, intent(in)  :: phshift(0:l_max)

  double precision, external    :: WIGNER_COEFF
  complex*16,       external    :: M_VG

  integer                       :: l1, l2, m1, m2, mu1, mu2
  integer                       :: l2_min, l2_max
  double precision              :: l12, m12
  double precision              :: wl, wmu, wm
  complex*16                    :: tmpl, sum_l, sum_m1, sum_m2, sum_mu2
  complex*16                    :: Mlmmu1_VG, Mlmmu2_VG

  sum_l = (0.d0,0.d0)
  do l1 = 0, l_max

    l2_min = max(0    ,abs(l1-2))
    l2_max = min(l_max,   (l1+2))
    do l2 = l2_min, l2_max

      l12 = dabs( modulo(dble(l1+l2),2.d0) )
      if(l12.GT.1d-7) cycle

      wl   = WIGNER_COEFF(dble(l1), dble(l2), 2.d0, 0.d0, 0.d0)
      tmpl = wl                                                      &
           * dsqrt( (2.d0*dble(l1)+1.d0) * (2.d0*dble(l2)+1.d0) )    &
           * (-1.d0*Jcomp)**dble(l1-l2)                              &
           * zexp( Jcomp * (phshift(l1)-phshift(l2)) )

      sum_m1 = (0.d0,0.d0)
      do m1 = -l1, l1
        do mu1 = -1, 1

          Mlmmu1_VG = (-1.d0)**(dble(m1-mu1)) * M_VG(l1, m1, mu1                            &
                    , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
                    , ng, coefcG(:,l1), expocG(:,l1))

          sum_mu2 = (0.d0,0.d0)
          do mu2 = -1, 1
            wmu = WIGNER_COEFF(1.d0, 1.d0, 2.d0, dble(mu1), dble(-mu2))

            sum_m2 = (0.d0,0.d0)
            do m2 = -l2, l2
              m12 = dabs( dble(m2-m1-mu2+mu1) )
              if(m12.gt.1d-7) cycle

              wm        = WIGNER_COEFF(dble(l1), dble(l2), 2.d0, dble(-m1), dble(m2))
              Mlmmu2_VG = M_VG(l2, m2, mu2                                                      &
                        , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
                        , ng, coefcG(:,l2), expocG(:,l2))

              sum_m2 = sum_m2 + wm * conjg(Mlmmu2_VG)
            enddo

            sum_mu2 = sum_mu2 + wmu * sum_m2
          enddo

          sum_m1 = sum_m1 + Mlmmu1_VG * sum_mu2
        enddo
      enddo

      sum_l = sum_l + tmpl * sum_m1
    enddo
  enddo

  A2_VG = sum_l * dsqrt(6.d0)

  return
end function A2_VG
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________






!_____________________________________________________________________________________________
!_____________________________________________________________________________________________

complex*16 function M_LG(l, m, mu                                                   &
            , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
            , ng, ccoef, cexpo)

  implicit none

  complex*16, parameter         :: Jcomp = (0.d0,1.d0)

  integer,          intent(in)  :: l, m, mu, ng, AO_nb, max_primG
  double precision, intent(in)  :: nucl_pos(AO_nb,3)
  integer,          intent(in)  :: nb_primG(AO_nb), AOpower(AO_nb,3)
  double precision, intent(in)  :: AOcoef(max_primG,AO_nb), AOexpo(max_primG,AO_nb)
  double precision, intent(in)  :: mocoef(AO_nb)
  complex*16,       intent(in)  :: ccoef(ng), cexpo(ng)

  complex*16, external          :: I3dcG_CART

  integer                       :: i, ii, s 
  double precision              :: coefi, expoi
  complex*16                    :: sum_ii, sum_s
  complex*16                    :: tmps1, tmps2, tmps
  integer,          allocatable :: pow(:), ni(:)
  double precision, allocatable :: Ri(:)

  allocate( pow(3), ni(3), Ri(3) )

  M_LG = (0.d0,0.d0)
  do i = 1, AO_nb
    Ri(:) = nucl_pos(i,:)
    ni(:) = AOpower(i,:)

    sum_ii = (0.d0,0.d0)
    do ii = 1, nb_primG(i)
      coefi = AOcoef(ii,i)
      expoi = AOexpo(ii,i)

      sum_s = (0.d0,0.d0)
      do s = 1, ng

        if(mu.eq.(0)) then 
          pow  = (/ 0 , 0 , 1 /)
          tmps = I3dcG_CART(pow, ni, expoi, Ri, l, m, cexpo(s))
        elseif( (mu.eq.(-1)) .or. (mu.eq.(+1)) ) then 
          pow   = (/ 1 , 0 , 0 /)
          tmps1 = I3dcG_CART(pow, ni, expoi, Ri, l, m, cexpo(s))
          pow   = (/ 0 , 1 , 0 /)
          tmps2 = I3dcG_CART(pow, ni, expoi, Ri, l, m, cexpo(s))
          select case(mu)
            case(-1)
              tmps =   ( tmps1 - Jcomp*tmps2 ) / dsqrt(2.d0)
            case(+1)
              tmps = - ( tmps1 + Jcomp*tmps2 ) / dsqrt(2.d0)
          end select
        else
          print *, ' mu should be -1, 0 or +1 ! '
          stop
        endif

        sum_s = sum_s + ccoef(s) * tmps
      enddo

      sum_ii = sum_ii + coefi * sum_s
    enddo

    M_LG = M_LG + mocoef(i) * sum_ii
  enddo

  deallocate( pow, ni, Ri )


  return
end function M_LG
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________






!_____________________________________________________________________________________________
!_____________________________________________________________________________________________

complex*16 function M_VG(l, m, mu                                                   &
            , AO_nb, max_primG, nucl_pos, AOpower, nb_primG, AOcoef, AOexpo, mocoef &
            , ng, ccoef, cexpo)

  implicit none

  complex*16, parameter         :: Jcomp = (0.d0,1.d0)

  integer,          intent(in)  :: l, m, mu, ng, AO_nb, max_primG
  double precision, intent(in)  :: nucl_pos(AO_nb,3)
  integer,          intent(in)  :: nb_primG(AO_nb), AOpower(AO_nb,3)
  double precision, intent(in)  :: AOcoef(max_primG,AO_nb), AOexpo(max_primG,AO_nb)
  double precision, intent(in)  :: mocoef(AO_nb)
  complex*16,       intent(in)  :: ccoef(ng), cexpo(ng)

  complex*16, external          :: I3dcG_CART

  integer                       :: i, ii, s 
  double precision              :: coefi, expoi
  complex*16                    :: sum_ii, sum_s
  complex*16                    :: tmps1, tmps2, tmpsx, tmpsy, tmps
  integer,          allocatable :: pow(:), ni(:), nni(:)
  double precision, allocatable :: Ri(:)

  allocate( pow(3), ni(3), Ri(3), nni(3) )

  pow  = (/ 0 , 0 , 0 /)

  M_VG = (0.d0,0.d0)
  do i = 1, AO_nb
    Ri(:) = nucl_pos(i,:)
    ni(:) = AOpower(i,:)

    sum_ii = (0.d0,0.d0)
    do ii = 1, nb_primG(i)
      coefi = AOcoef(ii,i)
      expoi = AOexpo(ii,i)

      sum_s = (0.d0,0.d0)
      do s = 1, ng

        if(mu.eq.(0)) then 

          if(ni(3).eq.0) then
            nni  = (/ ni(1) , ni(2) , 1 /)
            tmps = (-2.d0*expoi) * I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
          else
            nni   = (/ ni(1) , ni(2) , ni(3)-1 /)
            tmps1 = I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
            nni   = (/ ni(1) , ni(2) , ni(3)+1 /)
            tmps2 = I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
            tmps  = dble(ni(3)) * tmps1 - 2.d0 * expoi * tmps2
          endif

        elseif( (mu.eq.(-1)) .or. (mu.eq.(+1)) ) then 

          if(ni(1).eq.0) then
            nni   = (/ 1 , ni(2) , ni(3) /)
            tmpsx = (-2.d0*expoi) * I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
          else
            nni   = (/ ni(1)-1 , ni(2) , ni(3) /)
            tmps1 = I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
            nni   = (/ ni(1)+1 , ni(2) , ni(3) /)
            tmps2 = I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
            tmpsx = dble(ni(1)) * tmps1 - 2.d0 * expoi * tmps2
          endif

          if(ni(2).eq.0) then
            nni   = (/ ni(1) , 1 , ni(3) /)
            tmpsy = (-2.d0*expoi) * I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
          else
            nni   = (/ ni(1) , ni(2)-1 , ni(3) /)
            tmps1 = I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
            nni   = (/ ni(1) , ni(2)+1 , ni(3) /)
            tmps2 = I3dcG_CART(pow, nni, expoi, Ri, l, m, cexpo(s))
            tmpsy = dble(ni(2)) * tmps1 - 2.d0 * expoi * tmps2
          endif

          select case(mu)
            case(-1)
              tmps =   ( tmpsx - Jcomp*tmpsy ) / dsqrt(2.d0)
            case(+1)
              tmps = - ( tmpsx + Jcomp*tmpsy ) / dsqrt(2.d0)
          end select

        else
          print *, ' mu should be -1, 0 or +1 ! '
          stop
        endif

        sum_s = sum_s + ccoef(s) * tmps
      enddo

      sum_ii = sum_ii + coefi * sum_s
    enddo

    M_VG = M_VG + mocoef(i) * sum_ii
  enddo

  deallocate( pow, ni, Ri, nni )


  return
end function M_VG
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________
!_____________________________________________________________________________________________
