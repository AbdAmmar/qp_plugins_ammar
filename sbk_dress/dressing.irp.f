
! ---

double precision function delta_IJ_v0(det_I, det_J, Nint, e0)

  BEGIN_DOC
  !
  ! naive implementation of
  ! 
  ! delta_IJ = \sum_{alpha} < I | H | alpha > < alpha | H | J > / (E0 - < alpha | H | alpha > )
  !
  END_DOC

  use bitmasks
  implicit none

  integer,           intent(in) :: Nint
  integer(bit_kind), intent(in) :: det_I(Nint,2), det_J(Nint,2)
  double precision,  intent(in) :: e0

  logical                       :: ok, same_s
  integer                       :: degree, i0
  integer                       :: s1, s2, h1, h2, p1, p2, ih, ip
  integer(bit_kind)             :: det_tmp1(Nint,2), det_tmp2(Nint,2), det_tmp3(Nint,2), det_a(Nint,2)
  double precision              :: e_ia, e_ja, e_aa

  logical,          external    :: is_in_wavefunction
  double precision, external    :: diag_2x2

  delta_IJ_v0 = 0.d0

  call get_excitation_degree(det_I, det_J, degree, Nint)

  if(degree > 4) return

  ! ---

  i0 = n_core_orb + 1

  ! ---

  do s1 = 1, 2

    do h1 = i0, mo_num
      call apply_hole(det_I, s1, h1, det_tmp1, ok, Nint)
      if(.not. ok) cycle

      do p1 = i0, mo_num
        call apply_particle(det_tmp1, s1, p1, det_a, ok, Nint)
        if(.not. ok) cycle

        if(is_in_wavefunction(det_a, Nint)) cycle

        call get_excitation_degree(det_J, det_a, degree, Nint)
        if(degree > 2) cycle 

        call i_H_j(det_I, det_a, Nint, e_ia)
        if(dabs(e_ia) .lt. 1d-14) cycle

        call i_H_j(det_J, det_a, Nint, e_ja)
        if(dabs(e_ja) .lt. 1d-14) cycle

        call i_H_j(det_a, det_a, Nint, e_aa)

        !delta_IJ_v0 += e_ia * e_ja / (e0 - e_aa)
        delta_IJ_v0 += diag_2x2(e_ia, e_ja, e0, e_aa)
      enddo
    enddo
  enddo

  ! ---

  do s1 = 1, 2
    do s2 = s1, 2

      same_s = .false.
      if(s1 .eq. s2) same_s = .true.

      do h1 = i0, mo_num
        call apply_hole(det_I, s1, h1, det_tmp1, ok, Nint)
        if(.not. ok) cycle

        ih = i0
        if(same_s) ih = h1 + 1

        do h2 = ih, mo_num
          call apply_hole(det_tmp1, s2, h2, det_tmp2, ok, Nint)
          if(.not. ok) cycle

          do p1 = i0, mo_num
            if(p1 == h1) cycle
            if(same_s .and. (p1 == h2)) cycle
            call apply_particle(det_tmp2, s1, p1, det_tmp3, ok, Nint)
            if(.not. ok) cycle

            ip = i0
            if(same_s) ip = p1 + 1

            do p2 = ip, mo_num
              if(p2 == h2) cycle
              if(same_s .and. (p2 == h1)) cycle
              call apply_particle(det_tmp3, s2, p2, det_a, ok, Nint)
              if(.not. ok) cycle

              if(is_in_wavefunction(det_a, Nint)) cycle

              call get_excitation_degree(det_J, det_a, degree, Nint)
              if(degree > 2) cycle 

              call i_H_j(det_I, det_a, Nint, e_ia)
              if(dabs(e_ia) .lt. 1d-14) cycle

              call i_H_j(det_J, det_a, Nint, e_ja)
              if(dabs(e_ja) .lt. 1d-14) cycle

              call i_H_j(det_a, det_a, Nint, e_aa)

              !delta_IJ_v0 += e_ia * e_ja / (e0 - e_aa)
              delta_IJ_v0 += diag_2x2(e_ia, e_ja, e0, e_aa)
            enddo
          enddo
        enddo
      enddo
    enddo
  enddo

end function delta_IJ_v0

! ---

double precision function delta_IJ_v1(det_I, det_J, Nint, e0)

  BEGIN_DOC
  ! 
  ! delta_IJ = \sum_{alpha} < I | H | alpha > < alpha | H | J > / (E0 - < alpha | H | alpha > )
  !
  END_DOC

  use bitmasks
  implicit none

  integer,           intent(in) :: Nint
  integer(bit_kind), intent(in) :: det_I(Nint,2), det_J(Nint,2)
  double precision,  intent(in) :: e0

  logical                       :: ok, same_s
  integer                       :: degree
  integer                       :: s1, s2, h1, h2, p1, p2, ih, ip
  integer                       :: i, j, a, b
  integer                       :: idx_h, idx_p
  integer                       :: nO, nO1, nO2, nV, nV1, nV2, nO_a, nO_b, nV_a, nV_b
  integer(bit_kind)             :: det_a(Nint,2), det_tmp1(Nint,2), det_tmp2(Nint,2), det_tmp3(Nint,2)
  double precision              :: e_ia, e_ja, e_aa, e2
  integer, allocatable          :: occ(:,:), vir(:,:), nO_s(:), nV_s(:)

  logical,          external    :: is_in_wavefunction
  double precision, external    :: diag_2x2

  delta_IJ_v1 = 0.d0

  call get_excitation_degree(det_I, det_J, degree, Nint)

  if(degree > 4) return

  ! I -> S(I) + D(I)

  ! List of occupied/virtual alpha/beta active MOs

  nO_a = elec_alpha_num - n_core_orb
  nO_b = elec_beta_num  - n_core_orb
  nV_a = mo_num - elec_alpha_num - n_core_orb
  nV_b = mo_num - elec_beta_num  - n_core_orb
  nO   = max(nO_a, nO_b)
  nV   = max(nV_a, nV_b)

  allocate(no_S(2), nV_s(2))
  nO_s(1) = nO_a
  nO_s(2) = nO_b
  nV_s(1) = nV_a
  nV_s(2) = nV_b

  allocate(occ(nO,2), vir(nV,2))
  occ = 0
  vir = 0

  do s1 = 1, 2
    idx_h = 1
    idx_p = 1
    do i = 1, n_act_orb
      h1 = list_act(i)
      call apply_hole(det_I, s1, h1, det_tmp1, ok, Nint)
      if(ok) then
        occ(idx_h,s1) = h1
        idx_h = idx_h + 1
      else
        vir(idx_p,s1) = h1
        idx_p = idx_p + 1
      endif
    enddo
  enddo

  ! ---

  do s1 = 1, 2
    nO1 = nO_s(s1)
    nV1 = nV_s(s1)

    do i = 1, nO1
      h1 = occ(i,s1) 
      call apply_hole(det_I, s1, h1, det_tmp1, ok, Nint)
      if(.not. ok) cycle

      do a = 1, nV1
        p1 = vir(a,s1)
        !if(p1 == h1) cycle
        call apply_particle(det_tmp1, s1, p1, det_a, ok, Nint)
        if(.not. ok) cycle

        if(is_in_wavefunction(det_a, Nint)) cycle

        call get_excitation_degree(det_J, det_a, degree, Nint)
        if(degree > 2) cycle 

        call i_H_j(det_I, det_a, Nint, e_ia)
        if(dabs(e_ia) .lt. 1d-14) cycle

        call i_H_j(det_J, det_a, Nint, e_ja)
        if(dabs(e_ja) .lt. 1d-14) cycle

        call i_H_j(det_a, det_a, Nint, e_aa)

        !delta_IJ_v1 += e_ia * e_ja / (e0 - e_aa)
        delta_IJ_v1 += diag_2x2(e_ia, e_ja, e0, e_aa)
      enddo
    enddo
  enddo

  ! ---

  do s1 = 1, 2
    nO1 = nO_s(s1)
    nV1 = nV_s(s1)

    do s2 = s1, 2
      nO2 = nO_s(s2)
      nV2 = nV_s(s2)

      same_s = .false.
      if(s1 .eq. s2) same_s = .true.

      do i = 1, nO1
        h1 = occ(i,s1) 
        call apply_hole(det_I, s1, h1, det_tmp1, ok, Nint)
        if(.not. ok) cycle

        ih = 1
        if(same_s) ih = i + 1

        do j = ih, nO2
          h2 = occ(j,s2)
          call apply_hole(det_tmp1, s2, h2, det_tmp2, ok, Nint)
          if(.not. ok) cycle

          do a = 1, nV1
            p1 = vir(a,s1)
            !if(p1 == h1) cycle
            call apply_particle(det_tmp2, s1, p1, det_tmp3, ok, Nint)
            if(.not. ok) cycle

            ip = 1
            if(same_s) ip = a + 1

            do b = ip, nV2
              p2 = vir(b,s2)
              !if(p2 == h2) cycle
              call apply_particle(det_tmp3, s2, p2, det_a, ok, Nint)
              if(.not. ok) cycle

              if(is_in_wavefunction(det_a, Nint)) cycle

              call get_excitation_degree(det_J, det_a, degree, Nint)
              if(degree > 2) cycle 

              call i_H_j(det_I, det_a, Nint, e_ia)
              if(dabs(e_ia) .lt. 1d-14) cycle

              call i_H_j(det_J, det_a, Nint, e_ja)
              if(dabs(e_ja) .lt. 1d-14) cycle

              call i_H_j(det_a, det_a, Nint, e_aa)

              !delta_IJ_v1 += e_ia * e_ja / (e0 - e_aa)
              delta_IJ_v1 += diag_2x2(e_ia, e_ja, e0, e_aa)
            enddo
          enddo
        enddo
      enddo
    enddo
  enddo

  deallocate(occ, vir)
  deallocate(no_S, nV_s)

end function delta_IJ_v1

! ---

double precision function diag_2x2(e_ia, e_ja, e0, e_aa)

  BEGIN_DOC
  !
  ! Diagonalization 2 by 2 to avoid /0 in e1 x e2 / (e0 - e_aa)
  ! 
  !  |                             |
  !  | -e0 - lambda       e_ja     |
  !  |                             | = 0  ==> lambda = 0.5 [ delta_E +- sqrt(4 e_ia e_ja + delta_E^2) ]
  !  |    e_ia       e_aa - lambda |
  !  |                             |
  !
  !  with delta_E = e0 - e_aa
  !
  END_DOC  

  implicit none
  double precision, intent(in)  :: e_ia, e_ja, e0, e_aa
  double precision              :: delta_E, e_ij, tmp

  diag_2x2 = 0.d0
  
  e_ij = e_ia * e_ja
  if(dabs(e_ij) .lt. 1.d-14) return

  delta_E = e0 - e_aa

  tmp = dsqrt(delta_E * delta_E + e_ij + e_ij + e_ij + e_ij)
  if(delta_E < 0.d0) tmp = -tmp

  diag_2x2 = 0.5d0 * (tmp - delta_E)

end function diag_2x2

! ---

