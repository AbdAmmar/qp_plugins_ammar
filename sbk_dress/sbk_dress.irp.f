
! ---

program sbk_dress

  BEGIN_DOC
  ! TODO : Put the documentation of the program here
  END_DOC

  implicit none
  print *, 'Hello world'

  read_wf = .true.
  touch read_wf

  Provide N_int 

  call test()

end

! ---

subroutine test()
  
  implicit none
  integer                    :: i, j
  double precision           :: e_ij, e0
  double precision, external :: delta_IJ_v0
  double precision, external :: delta_IJ_v1

  e0 = ci_energy(1) - nuclear_repulsion
  print *, ' e0 = ', e0

  i = 1
  j = 1

  e_ij = delta_IJ_v0(psi_det(:,:,i), psi_det(:,:,j), N_int, e0)
  print *, ' dressing energy v0 = ', e_ij

  e_ij = delta_IJ_v1(psi_det(:,:,i), psi_det(:,:,j), N_int, e0)
  print *, ' dressing energy v1 = ', e_ij

!  do i = 1, N_det
!    do j = 1, N_det
!      e_ij = delta_IJ_v1(psi_det(:,:,i), psi_det(:,:,j), N_int, e0)
!      print *, ' dressing energy = ', e_ij
!    enddo
!  enddo

end subroutine test

! ---

