
DOUBLE PRECISION FUNCTION WIGNER_COEFF(J1,J2,J3,M1,M2)
    IMPLICIT NONE
    DOUBLE PRECISION, INTENT(IN) :: J1, J2, J3, M1, M2
    DOUBLE PRECISION, EXTERNAL   :: CG_COEFF
    DOUBLE PRECISION             :: M3
    M3 = M1 + M2
    WIGNER_COEFF = CG_COEFF(J1,J2,J3,M1,M2)
    WIGNER_COEFF = WIGNER_COEFF * (-1.d0)**(-J1+J2-M3) / DSQRT(2.d0*J3+1.d0)
    RETURN
END FUNCTION WIGNER_COEFF

!***********************************************************************************************************************************
!  Description:  Calculates Clebsch-Gordan coefficients.
!***********************************************************************************************************************************
DOUBLE PRECISION FUNCTION CG_COEFF(J1,J2,J3,M1,M2)

    IMPLICIT NONE
    DOUBLE PRECISION, INTENT(IN)  :: J1, J2, J3, M1, M2
    LOGICAL, EXTERNAL             :: ISFRAC
    INTEGER                       :: I, K
    DOUBLE PRECISION              :: M3, SUMK, TERM
    DOUBLE PRECISION, ALLOCATABLE :: FACT(:)

    ! Compute table of factorials.
    ALLOCATE( FACT(0:99) )
    FACT(0) = 1.0D0
    DO I = 1, 99
      FACT(I) = I * FACT(I-1)
    END DO

    M3 = M1 + M2
    ! Check for invalid input.
    IF( ISFRAC(J1+J2+J3) .OR. ISFRAC(J1+M1)     .OR. ISFRAC(J2+M2) .OR.  &
        ISFRAC(J3+M3)    .OR. ISFRAC(-J1+J3-M2) .OR. ISFRAC(-J2+J3+M1)   ) THEN
      PRINT *, ' Invalid input.'
      STOP
    END IF

    ! Check for conditions that give CG_COEFF = 0.
    IF ( (J3 .LT. DABS(J1-J2)) .OR.  (J3 .GT. (J1+J2))     .OR.  &
         (DABS(M1) .GT. J1)    .OR.  (DABS(M2) .GT. J2)    .OR.  &
         (DABS(M3) .GT. J3)                                      ) THEN
      CG_COEFF = 0.0D0
    ELSE
      ! Compute Clebsch-Gordan coefficient.
      CG_COEFF = DSQRT((J3+J3+1) / FACT(NINT(J1+J2+J3+1)))
      CG_COEFF = CG_COEFF * DSQRT( FACT(NINT(J1+J2-J3)) * FACT(NINT(J2+J3-J1)) &
                                 * FACT(NINT(J3+J1-J2))                        )
      CG_COEFF = CG_COEFF * DSQRT( FACT(NINT(J1+M1)) * FACT(NINT(J1-M1)) &
                                 * FACT(NINT(J2+M2)) * FACT(NINT(J2-M2)) &
                                 * FACT(NINT(J3+M3)) * FACT(NINT(J3-M3)) )
      SUMK = 0.0D0
      DO K = 0, 99
        IF( (J1+J2-J3-K) .LT. (0.0D0) ) CYCLE
        IF( (J3-J1-M2+K) .LT. (0.0D0) ) CYCLE
        IF( (J3-J2+M1+K) .LT. (0.0D0) ) CYCLE
        IF( (J1-M1-K   ) .LT. (0.0D0) ) CYCLE
        IF( (J2+M2-K   ) .LT. (0.0D0) ) CYCLE
        TERM = FACT(NINT(J1+J2-J3-K))*FACT(NINT(J3-J1-M2+K))*FACT(NINT(J3-J2+M1+K))*FACT(NINT(J1-M1-K))*FACT(NINT(J2+M2-K))*FACT(K)
        IF(MOD(K,2) .EQ. 1) TERM = -TERM
        SUMK = SUMK + 1.0D0/TERM
      END DO
      CG_COEFF = CG_COEFF * SUMK
    END IF

    DEALLOCATE(FACT)
    RETURN
END FUNCTION CG_COEFF

!  Return .TRUE. if the argument has a fractional part, and .FALSE. if the argument is an integer.
FUNCTION ISFRAC (X) RESULT (Y)
    IMPLICIT NONE
    DOUBLE PRECISION, INTENT(IN) :: X
    LOGICAL                      :: Y
    DOUBLE PRECISION, PARAMETER  :: EPS = 1.0D-8
    IF ((DABS(X)-INT(DABS(X))) .GT. EPS) THEN
       Y = .TRUE.
    ELSE
       Y = .FALSE.
    END IF
    RETURN
END FUNCTION ISFRAC
!***********************************************************************************************************************************
