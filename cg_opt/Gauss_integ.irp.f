! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
! 
!                 3d multicenter integral with complex Gaussians:
!               
!       /+oo /+oo /+oo
!      /    /    /
!     |    |    | 
!     | dx | dy | dz  r^l (Y_l^m)^* exp(-\alpha_l) x^{px} y^{py} z^{pz} (x-X)^{nx} (y-Y)^{ny} (z-Z)^{nz} exp(-\beta (r_vec-R_vec)^2)
!     |    |    | 
!    /    /    /
!   /-oo /-oo /-oo
!
!                 alpha_l is complex and Re(alpha_l) > 0
!                 R_vec = (X,Y,Z)
!
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

complex*16 function I3dcG_CART(pow, ni, expoi, Ri, nl, m, cexpo)

    ! ___________________________________________
    !
    ! input
    !     pow    : (px,py,pz)
    !     ni     : (nx,ny,nz)
    !     expoi  : expo of bound wavefunction
    !     Ri     : (XA,YA,ZA)
    !     nl & m : continuum partial wave ang nb
    !     cexpo  : complex expos
    ! ___________________________________________

    implicit none

    real*8, parameter            :: pi = dacos(-1.d0)

    integer,          intent(in) :: pow(3), ni(3), nl, m
    double precision, intent(in) :: expoi, Ri(3)
    complex*16,       intent(in) :: cexpo

    double precision, external   :: C1_factor
    double precision, external   :: fact
    complex*16, external         :: C2_factor, I1dcG_CART

    integer                      :: k, h, j, p, i1, i2, j1, j2, k1, k2
    integer                      :: k_max, j_max, i1_max, j1_max, k1_max
    integer                      :: abs_m
    double precision             :: Ri_square
    complex*16                   :: gama, Cx, Cy, Cz
    complex*16                   :: sum_k, sum_h, sum_j, sum_p
    complex*16                   :: stmp, sum_x, sum_y, sum_z

    abs_m = abs(m)

    gama      = expoi + cexpo
    Ri_square = Ri(1) * Ri(1) + Ri(2) * Ri(2) + Ri(3) * Ri(3)
    Cx        = expoi * Ri(1) / gama
    Cy        = expoi * Ri(2) / gama
    Cz        = expoi * Ri(3) / gama

    sum_k = (0.d0,0.d0)
    k_max = int( 0.5d0 * dble(nl-abs_m) )      
    do k  = 0, k_max

      sum_h = (0.d0,0.d0)
      do h  = 0, k

        sum_j = (0.d0,0.d0)
        j_max = k - h
        do j  = 0, j_max

          sum_p = (0.d0,0.d0)
          do p  = 0, abs_m

            ! integral x
            i1_max = 2*(k-h-j) + p + pow(1)
            sum_x  = I1dcG_CART(i1_max, 0.d0, ni(1), Ri(1), gama, Cx, .true.)

            ! integral y
            j1_max = 2*j + abs_m - p + pow(2)
            sum_y  = I1dcG_CART(j1_max, 0.d0, ni(2), Ri(2), gama, Cy, .true.)

            ! integral z
            k1_max = 2*h + nl - 2*k - abs_m + pow(3)
            sum_z  = I1dcG_CART(k1_max, 0.d0, ni(3), Ri(3), gama, Cz, .true.)

            sum_p = sum_p + binom(abs_m, p) * conjg(C2_factor(m,p)) * sum_x * sum_y * sum_z
          enddo

          sum_j = sum_j + sum_p / ( fact(j) * fact(k-h-j) )
        enddo

        sum_h = sum_h + sum_j / fact(h)
      enddo    

      sum_k = sum_k + C1_factor(nl,abs_m,k) * fact(k) * sum_h
    enddo

    stmp       = zexp( - expoi * cexpo * Ri_square / gama ) 
    I3dcG_CART = dsqrt( (2.d0*nl+1.d0)/(4.d0*pi) * (fact(nl-abs_m) / fact(nl+abs_m)) ) * stmp * sum_k

    return
end function I3dcG_CART

! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -










! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
! 
!                 1d multicenter integral with complex Gaussians:
!               
!                 /+oo 
!                /   
!               |    
!               | dx  (x-A)^{na} (x-B)^{nb} exp(- \gamma (x-C)^2)
!               |  
!              /   
!             /-oo
!
!                 \gamma is complex and Re(\gamma) > 0
!                 C is complex
!
!
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

complex*16 function I1dcG_CART(na, A, nb, B, gama, C, prop)

    implicit none

    integer,          intent(in) :: na, nb
    double precision, intent(in) :: A, B
    complex*16,       intent(in) :: gama, C
    logical,          intent(in) :: prop

    complex*16, external         :: int_G 

    integer                      :: i1, i2
    complex*16                   :: xtmp

    if( prop ) then
    ! if prop = True, then C = (alpha A + beta B)/(alpha+beta)

      if( dabs(A-B).lt.(1d-10) ) then
        I1dcG_CART = int_G(na+nb, gama)
      else
        I1dcG_CART = (0.d0,0.d0)
        do i1 = 0, na
          xtmp = (0.d0,0.d0)
          do i2 = 0, nb
            xtmp = xtmp + binom(nb, i2) * (C-B)**dble(nb-i2) * int_G(i1+i2, gama)
          enddo
          I1dcG_CART = I1dcG_CART + xtmp * binom(na, i1) * (C-A)**dble(na-i1)
        enddo
      endif

    else

      print *, ' not implemented yet '
      stop

    endif

    return
end function I1dcG_CART
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -



! -----------------------------------------------------------------------
double precision function C1_factor(nl, abs_m, k)

    implicit none
    integer, intent(in)        :: nl, abs_m, k
    real*8                     :: C1tmp
    double precision, external :: fact

    C1tmp     = fact(2*nl-2*k) / ( fact(k)      &
              * fact(nl-k) * fact(nl-2*k-abs_m) )
    C1_factor = (-1.d0)**dble(k) * (0.5d0)**dble(nl) * C1tmp

    return
end function C1_factor 
! -----------------------------------------------------------------------


! -----------------------------------------------------------------------
complex*16 function C2_factor(m, p)

    implicit none
    complex*16, parameter      :: Jcomp = (0.d0,1.d0)
    integer, intent(in)        :: m, p
    double precision, external :: fact

    if( m.ge.0 ) then
        C2_factor = (-1.d0)**dble(m) * Jcomp**dble(m-p)
    else
        C2_factor = (-1.d0*Jcomp)**dble(abs(m)-p)
    endif

    return
end function C2_factor 
! -----------------------------------------------------------------------



! -----------------------------------------------------------------------
complex*16 function int_G(ii, cgamma)

    implicit none
    real*8, parameter          :: pi = dacos(-1.d0)
    integer,    intent(in)     :: ii
    complex*16, intent(in)     :: cgamma
    double precision, external :: fact

    if( MOD(ii,2).eq.0 ) then
      int_G = (0.5d0)**dble(ii) * dsqrt(pi) * fact(ii) &
            / ( fact(ii/2) * cgamma**(dble(ii+1)/2.d0) )
    else
      int_G = 0.d0
    endif

    return
end function int_G
! -----------------------------------------------------------------------



! -----------------------------------------------------------------------
double precision function r1d_Ginteg(Xi, nxi, betap, Xj, nxj, betaq)

    implicit none

    integer,          intent(in) :: nxi, nxj
    double precision, intent(in) :: Xi, Xj, betap, betaq

    double precision, external   :: int_rG 

    integer                      :: i1, i2, j1, j2
    real*8                       :: x1_tmp, x2_tmp, betapq
    real*8                       :: distij
    real*8                       :: tmpj

    x1_tmp = betaq * ( Xj - Xi ) / ( betap + betaq )
    x2_tmp = betap * ( Xi - Xj ) / ( betap + betaq )
    betapq = betap + betaq

    distij = dabs( Xi - Xj )
    if( distij.lt.(1.d-7) ) then

      r1d_Ginteg = int_rG(nxi+nxj, betapq)

    else

      r1d_Ginteg = 0.d0
      do i1 = 0, nxi

        tmpj = 0.d0
        do i2 = 0, nxj
          tmpj = tmpj                                 &
               + binom(nxj,i2) * x2_tmp**dble(nxj-i2) &
               * int_rG(i1+i2, betapq)
        enddo

        r1d_Ginteg = r1d_Ginteg                       &
                   + binom(nxi,i1) * x1_tmp**dble(nxi-i1) * tmpj
      enddo

    endif

    return
end function r1d_Ginteg
! -----------------------------------------------------------------------



! -----------------------------------------------------------------------
double precision function int_rG(ii, rbeta)

    implicit none
    real*8, parameter          :: pi = dacos(-1.d0)
    integer, intent(in)        :: ii
    real*8,  intent(in)        :: rbeta
    double precision, external :: fact

    if( MOD(ii,2).eq.0 ) then
      int_rG = (0.5d0)**dble(ii) * dsqrt(pi) * fact(ii) &
             / ( fact(ii/2) * rbeta**(dble(ii+1)/2.d0) )
    else
      int_rG = 0.d0
    endif

    return
end function int_rG
! -----------------------------------------------------------------------



! -----------------------------------------------------------------------
! binom(m,n) = m! / ( n! (m-n)! )
!real*8 function my_binom(m,n)
!
!    implicit none
!
!    integer, intent(in) :: m, n
!
!    if(m.lt.n) then
!      print *, ' error in binom input'
!      stop
!    endif
!
!    my_binom = dgamma(dble(m+1))  &
!          / ( dgamma(dble(n+1)) * dgamma(dble(m-n+1)) ) 
!
!    return
!end function my_binom
! -----------------------------------------------------------------------



