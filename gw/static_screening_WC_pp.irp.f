
! ---

subroutine static_screening_WC_pp(ispin, nOO, nVV, lambda, Omega, rho, WC)

  BEGIN_DOC
  ! Compute the VVVV block of the static screening W for the pp-BSE
  END_DOC

  implicit none
  include 'parameters.h'

  integer,          intent(in)  :: ispin
  integer,          intent(in)  :: nOO
  integer,          intent(in)  :: nVV
  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: WC(nVV,nVV)

  double precision, external    :: Kronecker_delta

  double precision              :: chi
  double precision              :: eps
  integer                       :: a,b,c,d,ab,cd,m



! Initialization

  WC(:,:) = 0d0

!---------------!
! Singlet block !
!---------------!

  if(ispin == 1) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
      do b=a,mo_num-nR_orb
        ab = ab + 1
        cd = 0
        do c=nO_orb+1,mo_num-nR_orb
          do d=c,mo_num-nR_orb
            cd = cd + 1

              chi = 0d0
              do m=1,nS_exc
                eps = Omega(m)**2 + eta**2
                chi = chi + rho(a,c,m)*rho(b,d,m)*Omega(m)/eps &
                          + rho(a,d,m)*rho(b,c,m)*Omega(m)/eps
              enddo

              WC(ab,cd) = + 4d0*lambda*chi/sqrt((1d0 + Kronecker_delta(a,b))*(1d0 + Kronecker_delta(c,d)))

          end do
        end do
      end do
    end do

  end if

!---------------!
! Triplet block !
!---------------!

  if(ispin == 2) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
      do b=a+1,mo_num-nR_orb
        ab = ab + 1
        cd = 0
        do c=nO_orb+1,mo_num-nR_orb
          do d=c+1,mo_num-nR_orb
            cd = cd + 1

            chi = 0d0
            do m=1,nS_exc
              eps = Omega(m)**2 + eta**2
              chi = chi + rho(a,c,m)*rho(b,d,m)*Omega(m)/eps &
                        - rho(a,d,m)*rho(b,c,m)*Omega(m)/eps
            enddo
           
            WC(ab,cd) = + 4d0*lambda*chi

          end do
        end do
      end do
    end do

  end if

!---------------!
! SpinO_orbrb block !
!---------------!

  if(ispin == 4) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
      do b=a+1,mo_num-nR_orb
        ab = ab + 1
        cd = 0
        do c=nO_orb+1,mo_num-nR_orb
          do d=c+1,mo_num-nR_orb
            cd = cd + 1

            chi = 0d0
            do m=1,nS_exc
              eps = Omega(m)**2 + eta**2
              chi = chi + rho(a,c,m)*rho(b,d,m)*Omega(m)/eps &
                        - rho(a,d,m)*rho(b,c,m)*Omega(m)/eps
            enddo
           
            WC(ab,cd) = + 2d0*lambda*chi

          end do
        end do
      end do
    end do

  end if

end subroutine static_screening_WC_pp
