
! ---

subroutine QP_graph(eHF, SigX, Vxc, Omega, rho_T, eGWlin, eGW)

  BEGIN_DOC
  !
  ! Compute the graphical solution of the QP equation
  ! using Newton's algorithm 
  !
  END_DOC

  implicit none
  include 'parameters.h'

  double precision, intent(in)  :: eHF(mo_num)
  double precision, intent(in)  :: SigX(mo_num)
  double precision, intent(in)  :: Vxc(mo_num)
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho_T(nS_exc,mo_num,mo_num)
  double precision, intent(in)  :: eGWlin(mo_num)
  double precision, intent(out) :: eGW(mo_num)

  integer,          parameter   :: maxIt = 64
  double precision, parameter   :: thresh = 1d-6
  double precision, external    :: SigmaC, dSigmaC

  integer                       :: p, q, ia
  integer                       :: nIt
  double precision              :: sigC, dsigC
  double precision              :: f, df
  double precision              :: w

  ! Run Newton's algorithm to find the root
 
  do p = nC_orb+1, mo_num-nR_orb

    write(*,*) '-----------------'
    write(*,'(A10,I3)') 'Orbital ', p
    write(*,*) '-----------------'

    w   = eGWlin(p)
    nIt = 0
    f   = 1.d0
    write(*,'(A3,I3,A1,1X,3F15.9)') 'It.',nIt,':',w*HaToeV,f
    
    do while((abs(f) > thresh) .and. (nIt < maxIt))
    
      nIt = nIt + 1

      !sigC  =  SigmaC(p, w, eHF(1), Omega(1), rho_T(1,1,1))
      !dsigC = dSigmaC(p, w, eHF(1), Omega(1), rho_T(1,1,1))
      call SigmaC_val_der(p, w, eHF(1), Omega(1), rho_T(1,1,1), sigC, dsigC)

      f  = w - eHF(p) - SigX(p) - sigC + Vxc(p)
      df = 1.d0 - dsigC
      w  = w - f / df

      write(*,'(A3,I3,A1,1X,3F15.9)') 'It.', nIt, ':', w*HaToeV, f, sigC
    enddo
 
    if(nIt == maxIt) then 

      write(*,*) 'Newton root search has not converged!'
      stop

    else

      eGW(p) = w
      write(*,'(A32,F16.10)') 'Quasiparticle energy (eV)   ', eGW(p)*HaToeV
      write(*,*)

    endif
          
  enddo

  return
end subroutine QP_graph

! ---

subroutine SigmaC_val_der(p, w, e, Omega, rho_T, sigmaC, dsigmaC)

  BEGIN_DOC
  !
  ! Compute value & derivative of the correlation part of the self-energy
  !
  ! TODO
  ! - adapt for ispin ?
  !
  END_DOC

  implicit none

  integer,          intent(in)  :: p
  double precision, intent(in)  :: w
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho_T(nS_exc,mo_num,mo_num)
  double precision, intent(out) :: sigmaC, dsigmaC

  integer                       :: i, a, jb
  double precision              :: eps, eps_2, tmp, eta_2, div

  PROVIDE eta

  SigmaC  = 0.d0
  dSigmaC = 0.d0

  eta_2 = eta * eta

  do i = nC_orb+1, nO_orb
    do jb = 1, nS_exc

      eps   = w - e(i) + Omega(jb)
      eps_2 = eps * eps
      tmp   = rho_T(jb,p,i)
      div   = tmp / (eps_2 + eta_2)

      SigmaC  = SigmaC  + 2.d0 * tmp * tmp * eps / (eps_2 + eta_2)
      dSigmaC = dSigmaC - 2.d0 * div * div * (eps_2 - eta_2)
    enddo
  enddo

  do a = nO_orb+1, mo_num-nR_orb
    do jb = 1, nS_exc

      eps   = w - e(a) - Omega(jb)
      eps_2 = eps * eps
      tmp   = rho_T(jb,a,p)
      div   = tmp / (eps_2 + eta_2)

      SigmaC  = SigmaC  + 2.d0 * tmp * tmp * eps / (eps_2 + eta_2)
      dSigmaC = dSigmaC - 2.d0 * div * div * (eps_2 - eta_2)
    enddo
  enddo

  return
end subroutine SigmaC_val_der

! ---

