
! ---

subroutine excitation_density(XpY_T, rho_T)

  BEGIN_DOC
  !
  ! Compute excitation densities
  !
  ! the transposate of XpY is used for performance issue
  !
  END_DOC

  implicit none
  double precision, intent(in)  :: XpY_T(nS_exc,nS_exc)
  double precision, intent(out) :: rho_T(nS_exc,mo_num,mo_num)
  integer                       :: ia, jb, p, q, j, b

  PROVIDE ERI_MO

  rho_T(:,:,:) = 0.d0

  !$OMP PARALLEL &
  !$OMP SHARED(nC_orb,mo_num,nR_orb,nO_orb,nS_exc,rho_T,ERI_MO,XpY_T) &
  !$OMP PRIVATE(q,p,jb,ia) &
  !$OMP DEFAULT(NONE)
  !$OMP DO
  do q = nC_orb+1, mo_num-nR_orb
     do p = nC_orb+1, mo_num-nR_orb
        jb = 0
        do j = nC_orb+1, nO_orb
           do b = nO_orb+1, mo_num-nR_orb
              jb = jb + 1
              do ia = 1, nS_exc
                 rho_T(ia,p,q) = rho_T(ia,p,q) + ERI_MO(p,j,q,b) * XpY_T(ia,jb)
                 !rho(p,q,ia) = rho(p,q,ia) + ERI_MO(p,j,q,b) * XpY(jb,ia)
              enddo
           enddo
        enddo
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

end subroutine excitation_density

! ---

subroutine excitation_density_lr(X_T, Y_T, rho_T)

  BEGIN_DOC
  !
  ! Compute excitation densities
  !
  ! the transposate of X and Y is used for performance issue
  !
  END_DOC

  implicit none
  double precision, intent(in)  :: X_T(nS_exc,nS_exc)
  double precision, intent(in)  :: Y_T(nS_exc,nS_exc)
  double precision, intent(out) :: rho_T(nS_exc,mo_num,mo_num)
  integer                       :: ia, jb, p, q, j, b

  PROVIDE ERI_MO

  rho_T(:,:,:) = 0.d0

  !$OMP PARALLEL &
  !$OMP SHARED(nC_orb,mo_num,nR_orb,nO_orb,nS_exc,rho_T,ERI_MO,X_T,Y_T) &
  !$OMP PRIVATE(q,p,jb,ia) &
  !$OMP DEFAULT(NONE)
  !$OMP DO
  do q = nC_orb+1, mo_num-nR_orb
     do p = nC_orb+1, mo_num-nR_orb
        jb = 0
        do j = nC_orb+1, nO_orb
           do b = nO_orb+1, mo_num-nR_orb
              jb = jb + 1
              do ia = 1, nS_exc
                 rho_T(ia,p,q) = rho_T(ia,p,q) + ERI_MO(p,b,q,j) * X_T(ia,jb) &
                                               + ERI_MO(p,j,q,b) * Y_T(ia,jb)
              enddo
           enddo
        enddo
     enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

end subroutine excitation_density_lr

! ---

