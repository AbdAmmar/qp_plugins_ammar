
! ---

BEGIN_PROVIDER [double precision, ERI_AO, (ao_num, ao_num, ao_num, ao_num)]

  implicit none
  integer                    :: i, j, k, l
  double precision           :: integral
  double precision, external :: get_ao_two_e_integral

  PROVIDE ao_integrals_map

  do j = 1, ao_num
    do i = 1, ao_num
      do l = 1, ao_num
        do k = 1, ao_num

          !  < 1:k, 2:l | 1:i, 2:j > =  < 1:i, 2:j | 1:k, 2:l > 
          integral = get_ao_two_e_integral(i, j, k, l, ao_integrals_map)

          ERI_AO(k,l,i,j) = integral
        enddo
      enddo
    enddo
  enddo

END_PROVIDER

! ---

BEGIN_PROVIDER [double precision, ERI_MO, (mo_num, mo_num, mo_num, mo_num)]

  implicit none
  double precision, allocatable :: a1(:,:,:,:)
  double precision, allocatable :: a2(:,:,:,:)

  PROVIDE ERI_AO
  PROVIDE use_lr

  if(use_lr) then

    PROVIDE mo_r_coef mo_l_coef

    allocate(a2(ao_num,ao_num,ao_num,mo_num))

    call dgemm( 'T', 'N', ao_num*ao_num*ao_num, mo_num, ao_num, 1.d0 &
              , ERI_AO(1,1,1,1), ao_num, mo_l_coef(1,1), ao_num      &
              , 0.d0 , a2(1,1,1,1), ao_num*ao_num*ao_num)

    allocate(a1(ao_num,ao_num,mo_num,mo_num))

    call dgemm( 'T', 'N', ao_num*ao_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_l_coef(1,1), ao_num          &
              , 0.d0, a1(1,1,1,1), ao_num*ao_num*mo_num)

    deallocate(a2)
    allocate(a2(ao_num,mo_num,mo_num,mo_num))

    call dgemm( 'T', 'N', ao_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a1(1,1,1,1), ao_num, mo_r_coef(1,1), ao_num          &
              , 0.d0, a2(1,1,1,1), ao_num*mo_num*mo_num)

    deallocate(a1)

    call dgemm( 'T', 'N', mo_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_r_coef(1,1), ao_num          &
              , 0.d0, ERI_MO(1,1,1,1), mo_num*mo_num*mo_num)

    deallocate(a2)

    ! This is equivalent to
    !ERI_MO = 0.d0
    !integer :: i, j, k, l, a, b, c, d
    !do i = 1, mo_num
    !  do j = 1, mo_num
    !    do k = 1, mo_num
    !      do l = 1, mo_num
    !        do a = 1, ao_num
    !          do b = 1, ao_num
    !            do c = 1, ao_num
    !              do d = 1, ao_num
    !                ERI_MO(i,j,k,l) += ERI_AO(a,b,c,d) * mo_l_coef(a,i) * mo_l_coef(b,j) * mo_r_coef(c,k) * mo_r_coef(d,l)
    !              enddo
    !            enddo
    !          enddo
    !        enddo
    !      enddo
    !    enddo
    !  enddo
    !enddo

  else

    PROVIDE mo_coef

    allocate(a2(ao_num,ao_num,ao_num,mo_num))

    call dgemm( 'T', 'N', ao_num*ao_num*ao_num, mo_num, ao_num, 1.d0 &
              , ERI_AO(1,1,1,1), ao_num, mo_coef(1,1), ao_num        &
              , 0.d0 , a2(1,1,1,1), ao_num*ao_num*ao_num)

    allocate(a1(ao_num,ao_num,mo_num,mo_num))

    call dgemm( 'T', 'N', ao_num*ao_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_coef(1,1), ao_num            &
              , 0.d0, a1(1,1,1,1), ao_num*ao_num*mo_num)

    deallocate(a2)
    allocate(a2(ao_num,mo_num,mo_num,mo_num))

    call dgemm( 'T', 'N', ao_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a1(1,1,1,1), ao_num, mo_coef(1,1), ao_num            &
              , 0.d0, a2(1,1,1,1), ao_num*mo_num*mo_num)

    deallocate(a1)

    call dgemm( 'T', 'N', mo_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_coef(1,1), ao_num            &
              , 0.d0, ERI_MO(1,1,1,1), mo_num*mo_num*mo_num)

    deallocate(a2)

  end if

END_PROVIDER

! ---

