
! ---

subroutine oscillator_strength(maxS, Omega, XpY, XmY, os)

  BEGIN_DOC
  ! Compute linear response
  END_DOC

  implicit none
  include 'parameters.h'

  integer,intent(in)            :: maxS
  double precision,intent(in)   :: Omega(nS_exc)
  double precision,intent(in)   :: XpY(nS_exc,nS_exc)
  double precision,intent(in)   :: XmY(nS_exc,nS_exc)
  double precision,intent(out)  :: os(nS_exc)
  
  integer                       :: ia,jb,i,j,a,b
  integer                       :: ixyz

  double precision,allocatable  :: f(:,:)


! Memory allocation

  allocate(f(maxS,ncart))

! Initialization
   
  f(:,:) = 0d0

! Compute dipole moments and oscillator strengths

  PROVIDE dipole_int

  do ia=1,maxS
    do ixyz=1,ncart
      jb = 0
      do j=nC_orb+1,nO_orb
        do b=nO_orb+1,mo_num-nR_orb
          jb = jb + 1
          f(ia,ixyz) = f(ia,ixyz) + dipole_int(j,b,ixyz)*XpY(ia,jb)
        end do
      end do
    end do
  end do
  f(:,:) = sqrt(2d0)*f(:,:)

  do ia=1,maxS
    os(ia) = 2d0/3d0*Omega(ia)*sum(f(ia,:)**2)
  end do
 
  write(*,*) '---------------------------------------------------------------'
  write(*,*) '                Transition dipole moment (au)                  '
  write(*,*) '---------------------------------------------------------------'
  write(*,'(A3,5A12)') '#','X','Y','Z','dip. str.','osc. str.'
  write(*,*) '---------------------------------------------------------------'
  do ia=1,maxS
    write(*,'(I3,5F12.6)') ia,(f(ia,ixyz),ixyz=1,ncart),sum(f(ia,:)**2),os(ia)
  end do
  write(*,*) '---------------------------------------------------------------'
  write(*,*)

end subroutine oscillator_strength
