program g0w0

  implicit none

  BEGIN_DOC
  ! TODO : Put the documentation of the program here
  END_DOC

  PROVIDE use_lr
  if(use_lr) then
    my_grid_becke  = .True.
    my_n_pt_r_grid = 30
    my_n_pt_a_grid = 50
    touch my_grid_becke my_n_pt_r_grid my_n_pt_a_grid
  endif

  call main()

end

! ---

subroutine main()

  implicit none
  logical :: BSE, TDA_W, TDA

  BSE   = .false.
  TDA_W = .false.
  TDA   = .false.

  call G0W0_calc(BSE, TDA_W, TDA)

end
