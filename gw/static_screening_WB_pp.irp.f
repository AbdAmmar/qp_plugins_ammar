
! ---

subroutine static_screening_WB_pp(ispin, nOO, nVV, lambda, Omega, rho, WB)

  BEGIN_DOC
  ! Compute the VVOO block of the static screening W for the pp-BSE
  END_DOC

  implicit none
  include 'parameters.h'

  integer,          intent(in)  :: ispin, nOO, nVV
  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: WB(nVV,nOO)

  double precision,external     :: Kronecker_delta
  double precision              :: chi
  double precision              :: eps
  integer                       :: a,b,i,j,ab,ij,m


! Initialization

  WB(:,:) = 0d0

!---------------!
! Singlet block !
!---------------!

  if(ispin == 1) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
      do b=a,mo_num-nR_orb
        ab = ab + 1
        ij = 0
        do i=nC_orb+1,nO_orb
          do j=i,nO_orb
            ij = ij + 1

            chi = 0d0
            do m=1,nS_exc
              eps = Omega(m)**2 + eta**2
              chi = chi + rho(a,i,m)*rho(b,j,m)*Omega(m)/eps &
                        + rho(a,j,m)*rho(b,i,m)*Omega(m)/eps
            enddo
           
            WB(ab,ij) = + 4d0*lambda*chi/sqrt((1d0 + Kronecker_delta(a,b))*(1d0 + Kronecker_delta(i,j)))

          end do
        end do
      end do
    end do

  end if

!---------------!
! Triplet block !
!---------------!

  if(ispin == 2) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
     do b=a+1,mo_num-nR_orb
        ab = ab + 1
        ij = 0
        do i=nC_orb+1,nO_orb
         do j=i+1,nO_orb
            ij = ij + 1

            chi = 0d0
            do m=1,nS_exc
              eps = Omega(m)**2 + eta**2
              chi = chi + rho(a,i,m)*rho(b,j,m)*Omega(m)/eps &
                        - rho(a,j,m)*rho(b,i,m)*Omega(m)/eps
            enddo

            WB(ab,ij) = + 4d0*lambda*chi

          end do
        end do
      end do
    end do

  end if

!---------------!
! SpinO_orbrb block !
!---------------!

  if(ispin == 4) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
     do b=a+1,mo_num-nR_orb
        ab = ab + 1
        ij = 0
        do i=nC_orb+1,nO_orb
         do j=i+1,nO_orb
            ij = ij + 1

            chi = 0d0
            do m=1,nS_exc
              eps = Omega(m)**2 + eta**2
              chi = chi + rho(a,i,m)*rho(b,j,m)*Omega(m)/eps &
                        - rho(a,j,m)*rho(b,i,m)*Omega(m)/eps
            enddo

            WB(ab,ij) = + 2d0*lambda*chi

          end do
        end do
      end do
    end do

  end if

end subroutine static_screening_WB_pp
