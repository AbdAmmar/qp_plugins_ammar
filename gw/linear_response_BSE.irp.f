
! ---

subroutine linear_response_BSE(ispin, dRPA, TDA, BSE, lambda, e, A_BSE, B_BSE, Ec, Omega, XpY, XmY)

  BEGIN_DOC
  ! Compute linear response with BSE additional terms
  END_DOC

  implicit none
  include 'parameters.h'

  integer,          intent(in)  :: ispin
  logical,          intent(in)  :: dRPA, TDA, BSE
  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(in)  :: A_BSE(nS_exc,nS_exc)
  double precision, intent(in)  :: B_BSE(nS_exc,nS_exc)
  double precision, intent(out) :: Ec
  double precision, intent(out) :: Omega(nS_exc)
  double precision, intent(out) :: XpY(nS_exc,nS_exc)
  double precision, intent(out) :: XmY(nS_exc,nS_exc)

  double precision              :: trace_matrix
  double precision,allocatable  :: A(:,:)
  double precision,allocatable  :: B(:,:)
  double precision,allocatable  :: ApB(:,:)
  double precision,allocatable  :: AmB(:,:)
  double precision,allocatable  :: AmBSq(:,:)
  double precision,allocatable  :: AmBIv(:,:)
  double precision,allocatable  :: Z(:,:)


! Memory allocation

  allocate(A(nS_exc,nS_exc), B(nS_exc,nS_exc), ApB(nS_exc,nS_exc), AmB(nS_exc,nS_exc), AmBSq(nS_exc,nS_exc), AmBIv(nS_exc,nS_exc), Z(nS_exc,nS_exc))

! Build A and B matrices 

  call linear_response_A_matrix(ispin, dRPA, lambda, e, A(1,1))

  if(BSE) A(:,:) = A(:,:) - A_BSE(:,:)

! Tamm-Dancoff approximation

  if(TDA) then
 
    B(:,:)   = 0.d0
    XpY(:,:) = A(:,:)
    call diagonalize_matrix(nS_exc, XpY(1,1), Omega(1))
    XpY(:,:) = transpose(XpY(:,:))
    XmY(:,:) = XpY(:,:)

  else

    call linear_response_B_matrix(ispin, dRPA, lambda, B(1,1))

    if(BSE) B(:,:) = B(:,:) - B_BSE(:,:)

    ! Build A + B and A - B matrices 

    ApB = A + B
    AmB = A - B

  ! Diagonalize linear response matrix

    call diagonalize_matrix(nS_exc, AmB(1,1), Omega(1))

    if(minval(Omega) < 0d0) &
      call print_warning('You may have instabilities in linear response: A-B is not positive definite!!')

    call ADAt(nS_exc, AmB(1,1), 1.d0*sqrt(Omega), AmBSq(1,1))
    call ADAt(nS_exc, AmB(1,1), 1.d0/sqrt(Omega), AmBIv(1,1))

    Z = matmul(AmBSq, matmul(ApB, AmBSq))

   call diagonalize_matrix(nS_exc, Z(1,1), Omega(1))

    if(minval(Omega) < 0d0) & 
      call print_warning('You may have instabilities in linear response: negative excitations!!')

    Omega = sqrt(Omega)

    XpY = matmul(transpose(Z), AmBSq)
    call DA(nS_exc, 1d0/sqrt(Omega), XpY(1,1))

    XmY = matmul(transpose(Z),AmBIv)
    call DA(nS_exc, 1d0*sqrt(Omega), XmY(1,1))
 
  end if

  ! Compute the RPA correlation energy

    Ec = 0.5d0*(sum(Omega) - trace_matrix(nS_exc,A))

end subroutine linear_response_BSE
