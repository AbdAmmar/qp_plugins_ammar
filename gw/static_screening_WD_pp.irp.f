
! ---

subroutine static_screening_WD_pp(ispin, nOO, nVV, lambda, Omega, rho, WD)

  BEGIN_DOC
  ! Compute the OOOO block of the static screening W for the pp-BSE
  END_DOC

  implicit none
  include 'parameters.h'

  integer,          intent(in)  :: ispin
  integer,          intent(in)  :: nOO
  integer,          intent(in)  :: nVV
  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: WD(nOO,nOO)

  double precision,external     :: Kronecker_delta

  double precision              :: chi
  double precision              :: eps
  integer                       :: i,j,k,l,ij,kl,m


! Initialization

  WD(:,:) = 0d0

!---------------!
! Singlet block !
!---------------!

  if(ispin == 1) then

    ij = 0
    do i=nC_orb+1,nO_orb
      do j=i,nO_orb
        ij = ij + 1
        kl = 0
        do k=nC_orb+1,nO_orb
          do l=k,nO_orb
            kl = kl + 1

            chi = 0d0
            do m=1,nS_exc
              eps = Omega(m)**2 + eta**2
              chi = chi + rho(i,k,m)*rho(j,l,m)*Omega(m)/eps &
                        + rho(i,l,m)*rho(j,k,m)*Omega(m)/eps
            enddo
 
            WD(ij,kl) = + 4d0*lambda*chi/sqrt((1d0 + Kronecker_delta(i,j))*(1d0 + Kronecker_delta(k,l)))

          end do
        end do
      end do
    end do

  end if

!---------------!
! Triplet block !
!---------------!

  if(ispin == 2) then

    ij = 0
    do i=nC_orb+1,nO_orb
      do j=i+1,nO_orb
        ij = ij + 1
        kl = 0
        do k=nC_orb+1,nO_orb
          do l=k+1,nO_orb
            kl = kl + 1

            chi = 0d0
            do m=1,nS_exc
              eps = Omega(m)**2 + eta**2
              chi = chi + rho(i,k,m)*rho(j,l,m)*Omega(m)/eps &
                        - rho(i,l,m)*rho(j,k,m)*Omega(m)/eps
            enddo
 
            WD(ij,kl) = + 4d0*lambda*chi

          end do
        end do
      end do
    end do

  end if

!---------------!
! SpinO_orbrb block !
!---------------!

  if(ispin == 4) then

    ij = 0
    do i=nC_orb+1,nO_orb
      do j=i+1,nO_orb
        ij = ij + 1
        kl = 0
        do k=nC_orb+1,nO_orb
          do l=k+1,nO_orb
            kl = kl + 1

            chi = 0d0
            do m=1,nS_exc
              eps = Omega(m)**2 + eta**2
              chi = chi + rho(i,k,m)*rho(j,l,m)*Omega(m)/eps &
                        - rho(i,l,m)*rho(j,k,m)*Omega(m)/eps
            enddo
 
            WD(ij,kl) = + 2d0*lambda*chi

          end do
        end do
      end do
    end do

  end if

end subroutine static_screening_WD_pp
