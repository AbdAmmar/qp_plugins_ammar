
! ---

subroutine Bethe_Salpeter_pp_so(TDA_W, TDA, eW, eGW, EcBSE)

  BEGIN_DOC
  ! Compute the Bethe-Salpeter excitation energies at the pp level
  END_DOC

  implicit none

  logical,          intent(in)  :: TDA_W, TDA
  double precision, intent(in)  :: eW(mo_num)
  double precision, intent(in)  :: eGW(mo_num)
  double precision, intent(out) :: EcBSE(nspin)

  integer                       :: ispin
  integer                       :: isp_W

  integer                       :: nOO
  integer                       :: nVV

  double precision              :: EcRPA
  double precision,allocatable  :: OmRPA(:)
  double precision,allocatable  :: XpY_RPA(:,:)
  double precision,allocatable  :: XmY_RPA(:,:)
  double precision,allocatable  :: rho_RPA(:,:,:)

  double precision,allocatable  :: Omega1(:)
  double precision,allocatable  :: X1(:,:)
  double precision,allocatable  :: Y1(:,:)

  double precision,allocatable  :: Omega2(:)
  double precision,allocatable  :: X2(:,:)
  double precision,allocatable  :: Y2(:,:)

  double precision,allocatable  :: WB(:,:)
  double precision,allocatable  :: WC(:,:)
  double precision,allocatable  :: WD(:,:)


!---------------------------------
! Compute RPA screening 
!---------------------------------

  isp_W = 3
  EcRPA = 0d0

  ! Memory allocation

  allocate(OmRPA(nS_exc),XpY_RPA(nS_exc,nS_exc),XmY_RPA(nS_exc,nS_exc),rho_RPA(mo_num,mo_num,nS_exc))

  call linear_response(isp_W, .true., TDA_W, 1.d0, eW, EcRPA, OmRPA(1), XpY_RPA(1,1), XmY_RPA(1,1))
  call excitation_density(XpY_RPA(1,1), rho_RPA(1,1,1))

  write(*,*) '****************'
  write(*,*) '*** SpinO_orbrbs ***'
  write(*,*) '****************'
  write(*,*) 

  ispin = 1
  EcBSE(:) = 0d0

  nOO = nO_orb*(nO_orb-1)/2
  nVV = nV_orb*(nV_orb-1)/2

  allocate(Omega1(nVV),X1(nVV,nVV),Y1(nOO,nVV), &
           Omega2(nOO),X2(nVV,nOO),Y2(nOO,nOO), &
           WB(nVV,nOO),WC(nVV,nVV),WD(nOO,nOO))

  if(.not.TDA) call static_screening_WB_pp(4, nOO, nVV, 1d0, OmRPA, rho_RPA, WB)
  call static_screening_WC_pp(4, nOO, nVV, 1d0, OmRPA, rho_RPA, WC)
  call static_screening_WD_pp(4, nOO, nVV, 1d0, OmRPA, rho_RPA, WD)

  ! Compute BSE excitation energies

  call linear_response_pp_BSE(4, TDA, .true., nOO, nVV, 1d0, eGW, WB, WC, WD, Omega1, X1, Y1,Omega2, X2, Y2, EcBSE(ispin))

  call print_excitation('pp-BSE (N+2)', isp_W, nVV, Omega1)
  call print_excitation('pp-BSE (N-2)', isp_W, nOO, Omega2)

end subroutine Bethe_Salpeter_pp_so
