
! ---

subroutine Bethe_Salpeter_dynamic_perturbation(eW, eGW, OmRPA, rho_RPA, OmBSE, XpY, XmY)

  BEGIN_DOC
  ! Compute dynamical effects via perturbation theory for BSE
  END_DOC

  implicit none
  include 'parameters.h'

  double precision,intent(in)   :: eW(mo_num)
  double precision,intent(in)   :: eGW(mo_num)
  double precision,intent(in)   :: OmRPA(nS_exc)
  double precision,intent(in)   :: rho_RPA(mo_num,mo_num,nS_exc)
  double precision,intent(in)   :: OmBSE(nS_exc)
  double precision,intent(in)   :: XpY(nS_exc,nS_exc)
  double precision,intent(in)   :: XmY(nS_exc,nS_exc)

  integer                       :: ia

  integer                       :: maxS = 10
  double precision              :: gapGW

  double precision,allocatable  :: OmDyn(:)
  double precision,allocatable  :: ZDyn(:)
  double precision,allocatable  :: X(:)
  double precision,allocatable  :: Y(:)

  double precision,allocatable  ::  Ap_dyn(:,:)
  double precision,allocatable  :: ZAp_dyn(:,:)

  double precision,allocatable  ::  Bp_dyn(:,:)
  double precision,allocatable  :: ZBp_dyn(:,:)

  double precision,allocatable  ::  Am_dyn(:,:)
  double precision,allocatable  :: ZAm_dyn(:,:)

  double precision,allocatable  ::  Bm_dyn(:,:)
  double precision,allocatable  :: ZBm_dyn(:,:)

! Memory allocation

  maxS = min(nS_exc,maxS)
  allocate(OmDyn(maxS),ZDyn(maxS),X(nS_exc),Y(nS_exc),Ap_dyn(nS_exc,nS_exc),ZAp_dyn(nS_exc,nS_exc))

  if(.not.dTDA) allocate(Am_dyn(nS_exc,nS_exc),ZAm_dyn(nS_exc,nS_exc),Bp_dyn(nS_exc,nS_exc),ZBp_dyn(nS_exc,nS_exc),Bm_dyn(nS_exc,nS_exc),ZBm_dyn(nS_exc,nS_exc))

  if(dTDA) then 
    write(*,*)
    write(*,*) '*** dynamical TDA activated ***'
    write(*,*)
  end if

  gapGW = eGW(nO_orb+1) - eGW(nO_orb) 

  write(*,*) '---------------------------------------------------------------------------------------------------'
  write(*,*) ' First-order dynamical correction to static Bethe-Salpeter excitation energies                     '
  write(*,*) '---------------------------------------------------------------------------------------------------'
  write(*,'(A57,F10.6,A3)') ' BSE neutral excitation must be lower than the GW gap = ',gapGW*HaToeV,' eV'
  write(*,*) '---------------------------------------------------------------------------------------------------'
  write(*,'(2X,A5,1X,A20,1X,A20,1X,A20,1X,A20)') '#','Static (eV)','Dynamic (eV)','Correction (eV)','Renorm. (eV)'
  write(*,*) '---------------------------------------------------------------------------------------------------'

  do ia=1,maxS

    X(:) = 0.5d0*(XpY(ia,:) + XmY(ia,:))
    Y(:) = 0.5d0*(XpY(ia,:) - XmY(ia,:))

    ! First-order correction 

    if(dTDA) then 

      ! Resonant part of the BSE correction for dynamical TDA

      call Bethe_Salpeter_A_matrix_dynamic(1d0, eGW, OmRPA, rho_RPA, OmBSE(ia), Ap_dyn, ZAp_dyn)

      ZDyn(ia)  = dot_product(X,matmul(ZAp_dyn,X))
      OmDyn(ia) = dot_product(X,matmul( Ap_dyn,X))

    else

      ! Resonant and anti-resonant part of the BSE correction

      call Bethe_Salpeter_AB_matrix_dynamic(1d0, eGW, OmRPA, rho_RPA, OmBSE(ia), Ap_dyn, Am_dyn, Bp_dyn, Bm_dyn)

      ! Renormalization factor of the resonant and anti-resonant parts

      call Bethe_Salpeter_ZAB_matrix_dynamic(1d0, eGW, OmRPA, rho_RPA, OmBSE(ia), ZAp_dyn, ZAm_dyn, ZBp_dyn, ZBm_dyn)

      ZDyn(ia)  = dot_product(X, matmul(ZAp_dyn, X)) &
                - dot_product(Y, matmul(ZAm_dyn, Y)) &
                + dot_product(X, matmul(ZBp_dyn, Y)) & 
                - dot_product(Y, matmul(ZBm_dyn, X))  

      OmDyn(ia) = dot_product(X, matmul(Ap_dyn, X)) &
                - dot_product(Y, matmul(Am_dyn, Y)) &
                + dot_product(X, matmul(Bp_dyn, Y)) & 
                - dot_product(Y, matmul(Bm_dyn, X))  

    end if

    ZDyn(ia)  = 1d0/(1d0 - ZDyn(ia))
    OmDyn(ia) = ZDyn(ia)*OmDyn(ia)

    write(*,'(2X,I5,5X,F15.6,5X,F15.6,5X,F15.6,5X,F15.6)') & 
      ia,OmBSE(ia)*HaToeV,(OmBSE(ia)+OmDyn(ia))*HaToeV,OmDyn(ia)*HaToeV,ZDyn(ia)

  end do
  write(*,*) '---------------------------------------------------------------------------------------------------'
  write(*,*) 

end subroutine Bethe_Salpeter_dynamic_perturbation
