
! ---

subroutine linear_response_C_pp(ispin, nOO, nVV, lambda, e, C_pp)

  BEGIN_DOC
  ! Compute the C matrix of the pp channel
  END_DOC

  implicit none
  include 'parameters.h'

  integer,          intent(in)   :: ispin
  integer,          intent(in)   :: nOO
  integer,          intent(in)   :: nVV
  double precision, intent(in)   :: lambda
  double precision, intent(in)   :: e(mo_num)
  double precision, intent(out)  :: C_pp(nVV,nVV)
  
  double precision              :: eF
  double precision,external     :: Kronecker_delta

  integer                       :: a,b,c,d,ab,cd


! Define the chemical potential
 
! eF = e(nO_orb) + e(nO_orb+1)
  eF = 0d0

! Build C matrix for the singlet manifold

  if(ispin == 1) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
     do b=a,mo_num-nR_orb
        ab = ab + 1
        cd = 0
        do c=nO_orb+1,mo_num-nR_orb
         do d=c,mo_num-nR_orb
            cd = cd + 1
 
            C_pp(ab,cd) = + (e(a) + e(b) - eF)*Kronecker_delta(a,c)*Kronecker_delta(b,d) &
                          + lambda*(ERI_MO(a,b,c,d) + ERI_MO(a,b,d,c))/sqrt((1d0 + Kronecker_delta(a,b))*(1d0 + Kronecker_delta(c,d)))
 
          end do
        end do
      end do
    end do

  end if

! Build C matrix for the triplet manifold, or alpha-alpha block, or in the spin-orbital basis

  if(ispin == 2 .or. ispin == 4) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
     do b=a+1,mo_num-nR_orb
        ab = ab + 1
        cd = 0
        do c=nO_orb+1,mo_num-nR_orb
         do d=c+1,mo_num-nR_orb
            cd = cd + 1
 
            C_pp(ab,cd) = + (e(a) + e(b) - eF)*Kronecker_delta(a,c)*Kronecker_delta(b,d) & 
                          + lambda*(ERI_MO(a,b,c,d) - ERI_MO(a,b,d,c))
 
          end do
        end do
      end do
    end do

  end if

! Build the alpha-beta block of the C matrix

  if(ispin == 3) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
     do b=nO_orb+1,mo_num-nR_orb
        ab = ab + 1
        cd = 0
        do c=nO_orb+1,mo_num-nR_orb
         do d=nO_orb+1,mo_num-nR_orb
            cd = cd + 1
 
            C_pp(ab,cd) = + (e(a) + e(b) - eF)*Kronecker_delta(a,c)*Kronecker_delta(b,d) & 
                          + lambda*ERI_MO(a,b,c,d)
 
          end do
        end do
      end do
    end do

  end if

end subroutine linear_response_C_pp
