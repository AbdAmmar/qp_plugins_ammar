
! ---

subroutine BSE2_static_kernel_KB(lambda, eW, W, KB2_sta)

  BEGIN_DOC
  ! Compute the second-order static BSE kernel for the coupling block (only for singlets!)
  END_DOC

  implicit none

  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: eW(mo_num)
  double precision, intent(in)  :: W(mo_num,mo_num,mo_num,mo_num)
  double precision, intent(out) :: KB2_sta(nS_exc,nS_exc)

  double precision              :: dem, num
  integer                       :: i, j, k, l
  integer                       :: a, b, c, d
  integer                       :: ia, jb


!------------------------------------------------
! Compute BSE2 kernel
!------------------------------------------------

  ia = 0
  do i=nC_orb+1,nO_orb
    do a=nO_orb+1,mo_num-nR_orb
      ia = ia + 1

      jb = 0
      do j=nC_orb+1,nO_orb
        do b=nO_orb+1,mo_num-nR_orb
          jb = jb + 1

          do k=nC_orb+1,nO_orb
            do c=nO_orb+1,mo_num-nR_orb

              dem = - (eW(c) - eW(k))
              num = 2d0*W(b,k,i,c)*W(a,c,j,k)

              KB2_sta(ia,jb) =  KB2_sta(ia,jb) - num*dem/(dem**2 + eta**2)

              dem = + (eW(c) - eW(k))
              num = 2d0*W(b,c,i,k)*W(a,k,j,c) 

              KB2_sta(ia,jb) =  KB2_sta(ia,jb) + num*dem/(dem**2 + eta**2)

            end do
          end do

          do c=nO_orb+1,mo_num-nR_orb
            do d=nO_orb+1,mo_num-nR_orb

              dem = - (eW(c) + eW(d))
              num = 2d0*W(a,b,c,d)*W(c,d,i,j)

              KB2_sta(ia,jb) =  KB2_sta(ia,jb) + num*dem/(dem**2 + eta**2)

            end do
          end do

          do k=nC_orb+1,nO_orb
            do l=nC_orb+1,nO_orb

              dem = - (eW(k) + eW(l))
              num = 2d0*W(a,b,k,l)*W(k,l,i,j) 

              KB2_sta(ia,jb) =  KB2_sta(ia,jb) - num*dem/(dem**2 + eta**2)

            end do
          end do

        end do
      end do

    end do
  end do

end subroutine BSE2_static_kernel_KB
