
! ***

subroutine static_kernel_W(lambda, Om, rho, W)

  BEGIN_DOC
  ! Compute the second-order static BSE kernel for the resonant block (only for singlets!)
  END_DOC

  implicit none

  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: Om(nS_exc)
  double precision, intent(in)  :: rho(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: W(mo_num,mo_num,mo_num,mo_num)

  double precision              :: chi
  integer                       :: p, q, r, s
  integer                       :: m
  double precision              :: dem


!------------------------------------------------
! Compute static screening (physicist's notation)
!------------------------------------------------

  do p=1,mo_num
    do q=1,mo_num
      do r=1,mo_num
        do s=1,mo_num

          chi = 0d0
          do m=1,nS_exc
            dem = Om(m)**2 + eta**2
            chi = chi + rho(p,q,m)*rho(r,s,m)*Om(m)/dem
          enddo

          W(p,s,q,r) = - lambda*ERI_MO(p,s,q,r) + 4d0*lambda*chi

        enddo
      enddo
    enddo
  enddo

end subroutine static_kernel_W
