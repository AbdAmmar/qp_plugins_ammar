
! ---

subroutine Bethe_Salpeter(TDA_W, TDA, eW, eGW, EcBSE)

  BEGIN_DOC
  ! Compute the Bethe-Salpeter excitation energies
  END_DOC

  implicit none
  
  logical,          intent(in)  :: TDA_W, TDA
  double precision, intent(in)  :: eW(mo_num)
  double precision, intent(in)  :: eGW(mo_num)
  double precision, intent(out) :: EcBSE(nspin)

  integer                       :: ispin
  integer                       :: isp_W

  double precision              :: EcRPA
  double precision, allocatable :: OmRPA(:)
  double precision, allocatable :: XpY_RPA(:,:)
  double precision, allocatable :: XmY_RPA(:,:)
  double precision, allocatable :: rho_RPA(:,:,:)

  double precision, allocatable :: OmBSE(:,:)
  double precision, allocatable :: XpY_BSE(:,:,:)
  double precision, allocatable :: XmY_BSE(:,:,:)

  double precision, allocatable :: KA_sta(:,:)
  double precision, allocatable :: KB_sta(:,:)

  double precision, allocatable :: W(:,:,:,:)
  double precision, allocatable :: KA2_sta(:,:)
  double precision, allocatable :: KB2_sta(:,:)


! Memory allocation

  allocate(OmRPA(nS_exc),XpY_RPA(nS_exc,nS_exc),XmY_RPA(nS_exc,nS_exc),rho_RPA(mo_num,mo_num,nS_exc), &
           KA_sta(nS_exc,nS_exc),KB_sta(nS_exc,nS_exc),OmBSE(nS_exc,nspin),XpY_BSE(nS_exc,nS_exc,nspin),XmY_BSE(nS_exc,nS_exc,nspin))

!---------------------------------
! Compute (singlet) RPA screening 
!---------------------------------

  isp_W = 1
  EcRPA = 0d0

  call linear_response(isp_W, .true., TDA_W, 1.d0, eW, EcRPA, OmRPA(1), XpY_RPA(1,1), XmY_RPA(1,1))
  call excitation_density(XpY_RPA(1,1), rho_RPA(1,1,1))

  call BSE_static_kernel_KA(1.d0, OmRPA(1), rho_RPA(1,1,1), KA_sta(1,1))
  call BSE_static_kernel_KB(1.d0, OmRPA(1), rho_RPA(1,1,1), KB_sta(1,1))

!-------------------
! Singlet manifold
!-------------------

 if(singlet) then

    ispin = 1
    EcBSE(ispin) = 0d0

    ! Second-order BSE static kernel
  
    allocate(W(mo_num,mo_num,mo_num,mo_num), KA2_sta(nS_exc,nS_exc), KB2_sta(nS_exc,nS_exc))
    KA2_sta(:,:) = 0d0
    KB2_sta(:,:) = 0d0

    if(BSE2) then 

      write(*,*) '*** Second-order BSE static kernel activated! ***'
      call static_kernel_W(1.d0, OmRPA(1), rho_RPA(1,1,1), W(1,1,1,1))
      call BSE2_static_kernel_KA(1.d0, eW(1), W(1,1,1,1), KA2_sta(1,1))

      if(.not.TDA) call BSE2_static_kernel_KB(1.d0, eW(1), W(1,1,1,1), KB2_sta(1,1))

    end if

    ! Compute BSE excitation energies

    call linear_response_BSE( ispin, .true., TDA, .true., 1.d0, eGW,KA_sta-KA2_sta, KB_sta-KB2_sta &
                            , EcBSE(ispin), OmBSE(1,ispin), XpY_BSE(1,1,ispin), XmY_BSE(1,1,ispin) )

    call print_excitation('BSE@GW      ', ispin, nS_exc, OmBSE(1,ispin))
    call print_transition_vectors(.true., OmBSE(1,ispin), XpY_BSE(1,1,ispin), XmY_BSE(1,1,ispin))

    !-------------------------------------------------
    ! Compute the dynamical screening at the BSE level
    !-------------------------------------------------

    if(dBSE) then

      ! Compute dynamic correction for BSE via perturbation theory (iterative or renormalized)
 
      if(evDyn) then
 
        call Bethe_Salpeter_dynamic_perturbation_iterative( eGW, OmRPA(1), rho_RPA(1,1,1) &
                                                          , OmBSE(1,ispin), XpY_BSE(1,1,ispin), XmY_BSE(1,1,ispin))
      else
 
        call Bethe_Salpeter_dynamic_perturbation( eW, eGW, OmRPA(1), rho_RPA(1,1,1) &
                                                , OmBSE(1,ispin), XpY_BSE(1,1,ispin), XmY_BSE(1,1,ispin))
      end if

    end if
 
  end if

!-------------------
! Triplet manifold
!-------------------

 if(triplet) then

    ispin = 2
    EcBSE(ispin) = 0d0

    ! Compute BSE excitation energies

    call linear_response_BSE( ispin, .true., TDA, .true., 1d0, eGW, KA_sta(1,1), KB_sta(1,1) &
                            , EcBSE(ispin), OmBSE(1,ispin), XpY_BSE(1,1,ispin), XmY_BSE(1,1,ispin) )
    call print_excitation('BSE@GW      ', ispin, nS_exc, OmBSE(1,ispin))
    call print_transition_vectors(.false., OmBSE(1,ispin), XpY_BSE(1,1,ispin), XmY_BSE(1,1,ispin))

    !-------------------------------------------------
    ! Compute the dynamical screening at the BSE level
    !-------------------------------------------------

    if(dBSE) then

      ! Compute dynamic correction for BSE via perturbation theory (iterative or renormalized)

      if(evDyn) then
     
        call Bethe_Salpeter_dynamic_perturbation_iterative( eGW, OmRPA(1), rho_RPA(1,1,1) &
                                                          , OmBSE(1,ispin), XpY_BSE(1,1,ispin), XmY_BSE(1,1,ispin))
      else
     
        call Bethe_Salpeter_dynamic_perturbation( eW, eGW, OmRPA(1), rho_RPA(1,1,1) &
                                                , OmBSE(1,ispin), XpY_BSE(1,1,ispin), XmY_BSE(1,1,ispin))
      end if

    end if

  end if

end subroutine Bethe_Salpeter
