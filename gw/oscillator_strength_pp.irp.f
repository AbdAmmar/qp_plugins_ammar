
! ---

subroutine oscillator_strength_pp(nOO, nVV, maxOO, maxVV, Omega1, X1, Y1, Omega2, X2, Y2, os1, os2)

  BEGIN_DOC
  ! Compute linear response
  END_DOC

  implicit none
  include 'parameters.h'

  integer, intent(in)           :: nOO
  integer, intent(in)           :: nVV
  integer, intent(in)           :: maxOO
  integer, intent(in)           :: maxVV
  double precision, intent(in)  :: Omega1(nVV)
  double precision, intent(in)  :: X1(nVV,nVV)
  double precision, intent(in)  :: Y1(nOO,nVV)
  double precision, intent(in)  :: Omega2(nOO)
  double precision, intent(in)  :: X2(nVV,nOO)
  double precision, intent(in)  :: Y2(nOO,nOO)
  double precision, intent(out) :: os1(nVV)
  double precision, intent(out) :: os2(nOO)
  
  integer                       :: a,b,c,d,ab,cd
  integer                       :: i,j,k,l,ij,kl
  integer                       :: ixyz

  double precision,allocatable  :: f1(:,:)
  double precision,allocatable  :: f2(:,:)


! Memory allocation

  allocate(f1(maxVV,ncart),f2(maxOO,ncart))

! Initialization
   
  f1(:,:) = 0d0
  f2(:,:) = 0d0

! Compute dipole moments and oscillator strengths

  PROVIDE dipole_int

  do ab=1,maxVV
    do ixyz=1,ncart

      cd = 0
      do c=nO_orb+1,mo_num-nR_orb
        do d=c,mo_num-nR_orb
          cd = cd + 1
          f1(ab,ixyz) = f1(ab,ixyz) + dipole_int(c,d,ixyz)*X1(cd,ab)
        end do
      end do

      kl = 0
      do k=nC_orb+1,nO_orb
        do l=k,nO_orb
          kl = kl + 1
          f1(ab,ixyz) = f1(ab,ixyz) + dipole_int(k,l,ixyz)*Y1(kl,ab)
        end do
      end do

    end do
  end do
  f1(:,:) = sqrt(2d0)*f1(:,:)

  do ab=1,maxVV
    os1(ab) = +2d0/3d0*abs(Omega1(ab))*sum(f1(ab,:)**2)
  end do


  do ij=1,maxOO
    do ixyz=1,ncart

      cd = 0
      do c=nO_orb+1,mo_num-nR_orb
        do d=c,mo_num-nR_orb
          cd = cd + 1
          f2(ij,ixyz) = f2(ij,ixyz) + dipole_int(c,d,ixyz)*X2(cd,ij)
        end do
      end do

      kl = 0
      do k=nC_orb+1,nO_orb
        do l=k,nO_orb
          kl = kl + 1
          f2(ij,ixyz) = f2(ij,ixyz) + dipole_int(k,l,ixyz)*Y2(kl,ij)
        end do
      end do

    end do
  end do
  f2(:,:) = sqrt(2d0)*f2(:,:)

  do ij=1,maxOO
    os2(ij) = 2d0/3d0*abs(Omega2(ij))*sum(f2(ij,:)**2)
  end do
 
  write(*,*) '---------------------------------------------------------------'
  write(*,*) '           Transition dipole moment N -> N+2 (au)              '
  write(*,*) '---------------------------------------------------------------'
  write(*,'(A3,5A12)') '#','X','Y','Z','dip. str.','osc. str.'
  write(*,*) '---------------------------------------------------------------'
  do ab=1,maxVV
    write(*,'(I3,5F12.6)') ab,(f1(ab,ixyz),ixyz=1,ncart),sum(f1(ab,:)**2),os1(ab)
  end do
  write(*,*)
  write(*,*) '---------------------------------------------------------------'
  write(*,*) '           Transition dipole moment N -> N-2 (au)              '
  write(*,*) '---------------------------------------------------------------'
  do ij=1,maxOO
    write(*,'(I3,5F12.6)') ij,(f2(ij,ixyz),ixyz=1,ncart),sum(f2(ij,:)**2),os2(ij)
  end do
  write(*,*) '---------------------------------------------------------------'
  write(*,*)

end subroutine oscillator_strength_pp
