
! ---

subroutine linear_response_D_pp(ispin, nOO, nVV, lambda, e, D_pp)

  BEGIN_DOC
  ! Compute the D matrix of the pp channel
  END_DOC

  implicit none
  include 'parameters.h'

  integer,          intent(in)  :: ispin
  integer,          intent(in)  :: nOO
  integer,          intent(in)  :: nVV
  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: D_pp(nOO,nOO)
  
  double precision              :: eF
  double precision,external     :: Kronecker_delta

  integer                       :: i,j,k,l,ij,kl


! Define the chemical potential

! eF = e(nO_orb) + e(nO_orb+1)
  eF = 0d0
 
! Build the D matrix for the singlet manifold

  if(ispin == 1) then

    ij = 0
    do i=nC_orb+1,nO_orb
     do j=i,nO_orb
        ij = ij + 1
        kl = 0
        do k=nC_orb+1,nO_orb
         do l=k,nO_orb
            kl = kl + 1
 
            D_pp(ij,kl) = - (e(i) + e(j) - eF)*Kronecker_delta(i,k)*Kronecker_delta(j,l) & 
                          + lambda*(ERI_MO(i,j,k,l) + ERI_MO(i,j,l,k))/sqrt((1d0 + Kronecker_delta(i,j))*(1d0 + Kronecker_delta(k,l)))
 
          end do
        end do
      end do
    end do

  end if

! Build the D matrix for the triplet manifold, the alpha-alpha block, or in the spin-orbital basis 

  if(ispin == 2 .or. ispin == 4) then

    ij = 0
    do i=nC_orb+1,nO_orb
     do j=i+1,nO_orb
        ij = ij + 1
        kl = 0
        do k=nC_orb+1,nO_orb
         do l=k+1,nO_orb
            kl = kl + 1
 
            D_pp(ij,kl) = - (e(i) + e(j) - eF)*Kronecker_delta(i,k)*Kronecker_delta(j,l) & 
                          + lambda*(ERI_MO(i,j,k,l) - ERI_MO(i,j,l,k))
 
          end do
        end do
      end do
    end do

  end if

! Build the alpha-beta block of the D matrix

  if(ispin == 3) then

    ij = 0
    do i=nC_orb+1,nO_orb
     do j=nC_orb+1,nO_orb
        ij = ij + 1
        kl = 0
        do k=nC_orb+1,nO_orb
         do l=nC_orb+1,nO_orb
            kl = kl + 1
 
            D_pp(ij,kl) = - (e(i) + e(j) - eF)*Kronecker_delta(i,k)*Kronecker_delta(j,l) & 
                          + lambda*ERI_MO(i,j,k,l)
 
          end do
        end do
      end do
    end do

  end if

end subroutine linear_response_D_pp
