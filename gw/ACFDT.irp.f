
! ---

subroutine ACFDT(dRPA, TDA_W, TDA, BSE, eW, e, EcAC)


  BEGIN_DOC
  ! Compute the correlation energy via the adiabatic connection fluctuation dissipation theorem
  END_DOC

  implicit none
  include 'quadrature.h'

  logical,          intent(in)  :: dRPA, TDA_W, TDA, BSE
  double precision, intent(in)  :: eW(mo_num)
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: EcAC(nspin)

  integer                       :: ispin
  integer                       :: isp_W
  integer                       :: iAC
  double precision              :: lambda
  double precision, allocatable :: Ec(:,:)

  double precision              :: EcRPA
  double precision, allocatable :: WA(:,:)
  double precision, allocatable :: WB(:,:)
  double precision, allocatable :: OmRPA(:)
  double precision, allocatable :: XpY_RPA(:,:)
  double precision, allocatable :: XmY_RPA(:,:)
  double precision, allocatable :: rho_RPA(:,:,:)

  double precision, allocatable :: Omega(:,:)
  double precision, allocatable :: XpY(:,:,:)
  double precision, allocatable :: XmY(:,:,:)


! Memory allocation

  allocate(Ec(nAC,nspin))
  allocate(WA(nS_exc,nS_exc),WB(nS_exc,nS_exc),OmRPA(nS_exc),XpY_RPA(nS_exc,nS_exc),XmY_RPA(nS_exc,nS_exc),rho_RPA(mo_num,mo_num,nS_exc))
  allocate(Omega(nS_exc,nspin),XpY(nS_exc,nS_exc,nspin),XmY(nS_exc,nS_exc,nspin))

! Antisymmetrized kernel version

  if(exchange_kernel) then

    write(*,*)
    write(*,*) '*** Exchange kernel version ***'
    write(*,*)

  end if

  EcAC(:) = 0.d0
  Ec(:,:) = 0.d0

! Compute (singlet) RPA screening 

  isp_W = 1
  EcRPA = 0.d0

  call linear_response(isp_W, .true., TDA_W, 1.d0, eW, EcRPA, OmRPA(1), XpY_RPA(1,1), XmY_RPA(1,1))
  call excitation_density(XpY_RPA(1,1), rho_RPA(1,1,1))

  call BSE_static_kernel_KA(1d0, OmRPA(1), rho_RPA(1,1,1), WA(1,1))
  call BSE_static_kernel_KB(1d0, OmRPA(1), rho_RPA(1,1,1), WB(1,1))

! Singlet manifold

  if(singlet) then

    ispin = 1

    write(*,*) '--------------'
    write(*,*) 'Singlet states'
    write(*,*) '--------------'
    write(*,*) 

    write(*,*) '-----------------------------------------------------------------------------------'
    write(*,'(2X,A15,1X,A30,1X,A30)') 'lambda','Ec(lambda)','Tr(K x P_lambda)'
    write(*,*) '-----------------------------------------------------------------------------------'

    do iAC=1,nAC
 
      lambda = rAC(iAC)

      if(doXBS) then

        call linear_response(isp_W, .true., TDA_W, lambda, eW, EcRPA, OmRPA(1), XpY_RPA(1,1), XmY_RPA(1,1))
        call excitation_density(XpY_RPA(1,1), rho_RPA(1,1,1))
!       call print_excitation('W^lambda:   ',isp_W,nS_exc,OmRPA)

        call BSE_static_kernel_KA(lambda, OmRPA(1), rho_RPA(1,1,1), WA(1,1))
        call BSE_static_kernel_KB(lambda, OmRPA(1), rho_RPA(1,1,1), WB(1,1))

      end if

      call linear_response_BSE( ispin, dRPA, TDA, BSE, lambda, e, WA(1,1), WB(1,1) &
                              , EcAC(ispin), Omega(1,ispin), XpY(1,1,ispin), XmY(1,1,ispin))

      call ACFDT_correlation_energy(ispin, XpY(1,1,ispin), XmY(1,1,ispin), Ec(iAC,ispin))

      write(*,'(2X,F15.6,1X,F30.15,1X,F30.15)') lambda,EcAC(ispin),Ec(iAC,ispin)

    end do

    EcAC(ispin) = 0.5d0*dot_product(wAC, Ec(:,ispin))

    if(exchange_kernel) EcAC(ispin) = 0.5d0*EcAC(ispin)

    write(*,*) '-----------------------------------------------------------------------------------'
    write(*,'(2X,A50,1X,F15.6)') ' Ec(AC) via Gauss-Legendre quadrature:',EcAC(ispin)
    write(*,*) '-----------------------------------------------------------------------------------'
    write(*,*)

  end if
 
! Triplet manifold

  if(triplet) then

    ispin = 2

    write(*,*) '--------------'
    write(*,*) 'Triplet states'
    write(*,*) '--------------'
    write(*,*) 

    write(*,*) '-----------------------------------------------------------------------------------'
    write(*,'(2X,A15,1X,A30,1X,A30)') 'lambda','Ec(lambda)','Tr(K x P_lambda)'
    write(*,*) '-----------------------------------------------------------------------------------'

    do iAC = 1, nAC
 
      lambda = rAC(iAC)

      if(doXBS) then

        call linear_response(isp_W, .true., TDA_W, lambda, eW, EcRPA, OmRPA(1), XpY_RPA(1,1), XmY_RPA(1,1))
        call excitation_density(XpY_RPA(1,1), rho_RPA(1,1,1))

        call BSE_static_kernel_KA(lambda, OmRPA(1), rho_RPA(1,1,1), WA(1,1))
        call BSE_static_kernel_KB(lambda, OmRPA(1), rho_RPA(1,1,1), WB(1,1))

      end if  

      call linear_response_BSE( ispin, dRPA, TDA,BSE, lambda, e, WA(1,1), WB(1,1) &
                              , EcAC(ispin), Omega(1,ispin), XpY(1,1,ispin), XmY(1,1,ispin))

      call ACFDT_correlation_energy(ispin, XpY(1,1,ispin), XmY(1,1,ispin), Ec(iAC,ispin))

      write(*,'(2X,F15.6,1X,F30.15,1X,F30.15)') lambda,EcAC(ispin),Ec(iAC,ispin)

    end do

    EcAC(ispin) = 0.5d0*dot_product(wAC,Ec(:,ispin))

    if(exchange_kernel) EcAC(ispin) = 1.5d0*EcAC(ispin)

    write(*,*) '-----------------------------------------------------------------------------------'
    write(*,'(2X,A50,1X,F15.6)') ' Ec(AC) via Gauss-Legendre quadrature:',EcAC(ispin)
    write(*,*) '-----------------------------------------------------------------------------------'
    write(*,*)

  end if

end subroutine ACFDT
