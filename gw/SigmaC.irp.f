
! ---

double precision function SigmaC(p, w, e, Omega, rho_T)

  BEGIN_DOC
  !
  ! Compute diagonal of the correlation part of the self-energy
  !
  ! TODO
  ! - adapt for ispin ?
  !
  END_DOC

  implicit none

  integer,          intent(in) :: p
  double precision, intent(in) :: w
  double precision, intent(in) :: e(mo_num)
  double precision, intent(in) :: Omega(nS_exc)
  double precision, intent(in) :: rho_T(nS_exc,mo_num,mo_num)

  integer                      :: i, a, jb
  double precision             :: eps, tmp, eta_2

  PROVIDE eta

  SigmaC = 0.d0

  if(eta .lt. 1d-12) then

    do i = nC_orb+1, nO_orb
      do jb = 1, nS_exc

        eps = w - e(i) + Omega(jb)
        tmp = rho_T(jb,p,i)

        SigmaC = SigmaC + 2.d0 * tmp * tmp / eps
      enddo
    enddo

    do a = nO_orb+1, mo_num-nR_orb
      do jb = 1, nS_exc

        eps = w - e(a) - Omega(jb)
        tmp = rho_T(jb,a,p)

        SigmaC = SigmaC + 2.d0 * tmp * tmp / eps
      enddo
    enddo

  else

    eta_2 = eta * eta

    ! Occupied part of the correlation self-energy
    do i = nC_orb+1, nO_orb
      do jb = 1, nS_exc

        eps = w - e(i) + Omega(jb)
        tmp = rho_T(jb,p,i)

        SigmaC = SigmaC + 2.d0 * tmp * tmp * eps / (eps*eps + eta_2)
      enddo
    enddo

    ! Virtual part of the correlation self-energy
    do a = nO_orb+1, mo_num-nR_orb
      do jb = 1, nS_exc

        eps = w - e(a) - Omega(jb)
        tmp = rho_T(jb,a,p)

        SigmaC = SigmaC + 2.d0 * tmp * tmp * eps / (eps*eps + eta_2)
      enddo
    enddo

  endif

end function SigmaC

! ---

