
! ---

subroutine ACFDT_correlation_energy(ispin, XpY, XmY, EcAC)

  BEGIN_DOC
  ! Compute the correlation energy via the adiabatic connection formula
  END_DOC

  implicit none
  include 'parameters.h'

  integer,          intent(in)  :: ispin
  double precision, intent(in)  :: XpY(nS_exc,nS_exc)
  double precision, intent(in)  :: XmY(nS_exc,nS_exc)
  double precision, intent(out) :: EcAC

  integer                       :: i,j,a,b
  integer                       :: ia,jb,kc
  double precision              :: delta_spin
  double precision              :: delta_Kx
  double precision,allocatable  :: Ap(:,:)
  double precision,allocatable  :: Bp(:,:)
  double precision,allocatable  :: X(:,:)
  double precision,allocatable  :: Y(:,:)
  double precision,external     :: trace_matrix


! Singlet or triplet manifold?

  delta_spin = 0d0
  if(ispin == 1) delta_spin = +1d0
  if(ispin == 2) delta_spin = -1d0

! Exchange kernel

  delta_Kx = 0d0
  if(exchange_kernel) delta_Kx = 1d0
  
! Memory allocation

  allocate(Ap(nS_exc,nS_exc),Bp(nS_exc,nS_exc),X(nS_exc,nS_exc),Y(nS_exc,nS_exc))

! Compute Aiajb = (ia|bj) and Biajb = (ia|jb)

  ia = 0
  do i=nC_orb+1,nO_orb
    do a=nO_orb+1,mo_num-nR_orb
      ia = ia + 1
      jb = 0
      do j=nC_orb+1,nO_orb
        do b=nO_orb+1,mo_num-nR_orb
          jb = jb + 1

            Ap(ia,jb) = (1d0 + delta_spin)*ERI_MO(i,b,a,j) & 
                      - delta_Kx*ERI_MO(i,b,j,a)

            Bp(ia,jb) = (1d0 + delta_spin)*ERI_MO(i,j,a,b) &
                      - delta_Kx*ERI_MO(i,j,b,a)

        enddo
      enddo
    enddo
  enddo

! Compute Tr(K x P_lambda)

  X(:,:) = 0.5d0*(XpY(:,:) + XmY(:,:))
  Y(:,:) = 0.5d0*(XpY(:,:) - XmY(:,:))

  EcAC = trace_matrix(nS_exc,matmul(X,matmul(Bp,transpose(Y))) + matmul(Y,matmul(Bp,transpose(X)))) &
       + trace_matrix(nS_exc,matmul(X,matmul(Ap,transpose(X))) + matmul(Y,matmul(Ap,transpose(Y)))) &
       - trace_matrix(nS_exc,Ap)

end subroutine ACFDT_correlation_energy

