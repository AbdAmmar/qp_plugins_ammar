
BEGIN_PROVIDER [double precision, exchange_AO, (ao_num,ao_num)]

  BEGIN_DOC
  !
  ! Compute exchange matrix in the AO basis
  !
  END_DOC

  implicit none
  integer :: mu, nu, la, si

  exchange_AO = 0.d0

  PROVIDE use_lr

  if(use_lr) then

    PROVIDE TCSCF_density_matrix_ao_tot

    do nu = 1, ao_num
      do si = 1, ao_num
        do la = 1, ao_num
          do mu = 1, ao_num
            exchange_AO(mu,nu) = exchange_AO(mu,nu) - TCSCF_density_matrix_ao_tot(la,si) * ERI_AO(mu,la,si,nu)
          enddo
        enddo
      enddo
    enddo

  else 

    PROVIDE SCF_density_matrix_ao

    do nu = 1, ao_num
      do si = 1, ao_num
        do la = 1, ao_num
          do mu = 1, ao_num
            exchange_AO(mu,nu) = exchange_AO(mu,nu) - SCF_density_matrix_ao(la,si) * ERI_AO(mu,la,si,nu)
          enddo
        enddo
      enddo
    enddo

  endif

END_PROVIDER

! ---

BEGIN_PROVIDER [double precision, fock_exchange_potential_MO, (mo_num)]
  
  BEGIN_DOC
  !
  ! Compute the exchange potential in the MO basis
  !
  ! NOTE THAT THE SIGN IS NOT INCLUDED HERE BUT OUTSIDE
  ! 
  END_DOC

  implicit none

  integer :: mu, nu
  integer :: q

  fock_exchange_potential_MO = 0.d0

  PROVIDE use_lr

  if(use_lr) then

    PROVIDE mo_l_coef mo_r_coef

    if(elec_alpha_num .eq. elec_beta_num) then
      PROVIDE V_XC_TC_mo_alpha
      fock_exchange_potential_MO = 2.d0 * V_XC_TC_mo_alpha
    else
      PROVIDE V_XC_TC_mo_alpha V_XC_TC_mo_beta
      fock_exchange_potential_MO = V_XC_TC_mo_alpha + V_XC_TC_mo_beta
    endif

  else

    PROVIDE mo_coef

    do q = 1, mo_num
      do mu = 1, ao_num
        do nu = 1, ao_num
          fock_exchange_potential_MO(q) = fock_exchange_potential_MO(q) + mo_coef(mu,q) * exchange_AO(mu,nu) * mo_coef(nu,q)
        enddo
      enddo
    enddo

  endif

END_PROVIDER

! ---

BEGIN_PROVIDER [double precision, V_XC_TC_mo_alpha, (mo_num)]

  BEGIN_DOC
  !
  ! TC-V_XC for G0W0: alpha part in MO
  !
  END_DOC

  implicit none
  integer                       :: q
  double precision, allocatable :: tmp(:,:)

  PROVIDE mo_l_coef mo_r_coef

  ! 2e-term

  PROVIDE V_XC_TC_ao_alpha

  allocate(tmp(mo_num,mo_num))
  call ao_to_mo_bi_ortho(V_XC_TC_ao_alpha, size(V_XC_TC_ao_alpha, 1), tmp, size(tmp, 1))

  do q = 1, mo_num
    V_XC_TC_mo_alpha(q) = tmp(q,q)
  enddo
  deallocate(tmp)

  ! 3e-term

  if(three_body_h_tc) then
    PROVIDE V_XC_TC_3e_mo_diag_alpha
    V_XC_TC_mo_alpha += V_XC_TC_3e_mo_diag_alpha
  endif

END_PROVIDER

! ---

BEGIN_PROVIDER [double precision, V_XC_TC_mo_beta, (mo_num)]

  BEGIN_DOC
  !
  ! TC-V_XC for G0W0: beta part in MO
  !
  END_DOC

  implicit none
  integer                       :: q
  double precision, allocatable :: tmp(:,:)

  PROVIDE mo_l_coef mo_r_coef

  ! 2e-term

  PROVIDE V_XC_TC_ao_beta

  allocate(tmp(mo_num,mo_num))
  call ao_to_mo_bi_ortho(V_XC_TC_ao_beta, size(V_XC_TC_ao_beta, 1), tmp, size(tmp, 1))

  do q = 1, mo_num
    V_XC_TC_mo_beta(q) = tmp(q,q)
  enddo

  deallocate(tmp)

  ! 3e-term

  if(three_body_h_tc) then
    PROVIDE V_XC_TC_3e_mo_diag_beta
    V_XC_TC_mo_beta += V_XC_TC_3e_mo_diag_beta
  endif

END_PROVIDER

! ---

 BEGIN_PROVIDER [double precision, V_XC_TC_ao_alpha, (ao_num, ao_num)]
&BEGIN_PROVIDER [double precision, V_XC_TC_ao_beta , (ao_num, ao_num)]

  BEGIN_DOC
  !
  ! TC-V_XC for G0W0: alpha & beta part in AO
  !
  END_DOC

  implicit none
  integer                       :: i, j, k, l
  double precision              :: density_a, density_b, I_kjli
  double precision              :: t0, t1
  double precision, allocatable :: tmp_a(:,:), tmp_b(:,:)

  PROVIDE ao_two_e_tc_tot
  PROVIDE mo_l_coef mo_r_coef
  PROVIDE TCSCF_density_matrix_ao_alpha TCSCF_density_matrix_ao_beta

  V_XC_TC_ao_alpha = 0.d0
  V_XC_TC_ao_beta  = 0.d0

  !$OMP PARALLEL DEFAULT (NONE)                                                       &
  !$OMP PRIVATE (i, j, k, l, density_a, density_b, tmp_a, tmp_b, I_kjli)              &
  !$OMP SHARED  (ao_num, TCSCF_density_matrix_ao_alpha, TCSCF_density_matrix_ao_beta, &
  !$OMP          ao_two_e_tc_tot, V_XC_TC_ao_alpha, V_XC_TC_ao_beta)

  allocate(tmp_a(ao_num,ao_num), tmp_b(ao_num,ao_num))
  tmp_a = 0.d0
  tmp_b = 0.d0

  !$OMP DO
  do j = 1, ao_num
    do l = 1, ao_num
      density_a = TCSCF_density_matrix_ao_alpha(l,j)
      density_b = TCSCF_density_matrix_ao_beta (l,j)

      do i = 1, ao_num
        do k = 1, ao_num

          I_kjli = ao_two_e_tc_tot(k,j,l,i)

          tmp_a(k,i) -= density_a * I_kjli
          tmp_b(k,i) -= density_b * I_kjli
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO NOWAIT

  !$OMP CRITICAL
  do i = 1, ao_num
    do j = 1, ao_num
      V_XC_TC_ao_alpha(j,i) += tmp_a(j,i)
      V_XC_TC_ao_beta (j,i) += tmp_b(j,i)
    enddo
  enddo
  !$OMP END CRITICAL

  deallocate(tmp_a, tmp_b)
  !$OMP END PARALLEL

END_PROVIDER 

! ---

