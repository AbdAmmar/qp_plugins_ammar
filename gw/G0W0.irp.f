
! ---

subroutine G0W0_calc(BSE, TDA_W, TDA)

  BEGIN_DOC
  !
  ! perform one-shot GW
  !
  ! TODO
  ! avoid computing ia and jb indices in the loops to allow vectorization
  !
  !
  END_DOC

  implicit none
  include 'quadrature.h'

  logical, intent(in)           :: BSE, TDA_W, TDA

  logical                       :: print_W = .true.
  integer                       :: ispin
  double precision              :: EcRPA
  double precision              :: EcBSE(nspin)
  double precision              :: EcAC(nspin)
  double precision              :: EcppBSE(nspin)
  double precision              :: EcGM
  double precision              :: eSCF
  double precision, allocatable :: SigC(:)
  double precision, allocatable :: Z(:)
  double precision, allocatable :: OmRPA(:)
  double precision, allocatable :: XpY_RPA(:,:), X_RPA(:,:)
  double precision, allocatable :: XmY_RPA(:,:), Y_RPA(:,:)
  double precision, allocatable :: rho_RPA_T(:,:,:)
  double precision, allocatable :: eHF(:)
  double precision, allocatable :: eGWlin(:)
  double precision, allocatable :: SigX(:), Vxc(:) 

  ! Output variables

  double precision              :: eGW(mo_num)


  write(*,*)
  write(*,*)'************************************************'
  write(*,*)'|          One-shot G0W0 calculation           |'
  write(*,*)'************************************************'
  write(*,*)

  EcRPA = 0d0

  if(COHSEX) then 
    write(*,*) 'COHSEX approximation activated!'
    write(*,*)
  end if

  if(TDA_W) then 
    write(*,*) 'Tamm-Dancoff approximation for dynamic screening!'
    write(*,*)
  end if

  if(TDA) then 
    write(*,*) 'Tamm-Dancoff approximation activated!'
    write(*,*)
  end if

  ! TODO always 1 ?
  ! Spin manifold
  ispin = 1

  allocate(sigc(mo_num))
  allocate(z(mo_num))
  allocate(OmRPA(nS_exc))
  allocate(rho_RPA_T(nS_exc,mo_num,mo_num))
  allocate(egwlin(mo_num))
  allocate(ehf(mo_num))
  allocate(Sigx(mo_num), Vxc(mo_num))
  allocate(XPY_RPA(nS_exc,nS_exc), XmY_RPA(nS_exc,nS_exc))

  if(use_lr) then
    PROVIDE Fock_matrix_tc_diag_mo_tot
    eHF(1:mo_num) = Fock_matrix_tc_diag_mo_tot(1:mo_num)
  else
    PROVIDE Fock_matrix_diag_mo
    eHF(1:mo_num) = Fock_matrix_diag_mo(1:mo_num)
  endif

  ! XpY_RPA & XmY_RPA output are transposate
  call linear_response(ispin, .true., TDA_W, 1.d0, eHF(1), EcRPA, OmRPA(1), XpY_RPA(1,1), XmY_RPA(1,1))

  if(print_W) call print_excitation('RPA@HF      ', ispin, nS_exc, OmRPA(1))

  ! Compute screening & spectral weights
  if(use_lr) then

    allocate(X_RPA(nS_exc,nS_exc), Y_RPA(nS_exc,nS_exc))

    ! X_RPA & Y_RPA are transposate
    X_RPA = 0.5d0 * (XpY_RPA + XmY_RPA)
    Y_RPA = 0.5d0 * (XpY_RPA - XmY_RPA)

    call excitation_density_lr(X_RPA(1,1), Y_RPA(1,1), rho_RPA_T(1,1,1))

    deallocate(X_RPA, Y_RPA)

  else

    call excitation_density(XpY_RPA(1,1), rho_RPA_T(1,1,1))

  endif


  !------------------------!
  ! Compute GW self-energy !
  !------------------------!

  if(regularize) then 

    call regularized_self_energy_correlation_diag(eHF, OmRPA(1), rho_RPA_T(1,1,1), EcGM, SigC(1))
    call regularized_renormalization_factor(eHF, OmRPA(1), rho_RPA_T(1,1,1), Z(1))

  else

    call self_energy_correlation_diag(eHF, OmRPA(1), rho_RPA_T(1,1,1), EcGM, SigC(1))
    call renormalization_factor(eHF, OmRPA(1), rho_RPA_T(1,1,1), Z(1))

  endif

  !-----------------------------------!
  ! Solve the quasi-particle equation !
  !-----------------------------------!

  PROVIDE self_energy_exchange_diag
  PROVIDE fock_exchange_potential_MO

  eGWlin(:) = eHF(:) + Z(:)*(self_energy_exchange_diag(:) + SigC(:) - fock_exchange_potential_MO(:))

  ! Linearized or graphical solution?
  if(linearize) then 

    write(*,*) ' *** Quasiparticle energies obtained by linearization *** '
    write(*,*)

    eGW(:) = eGWlin(:)

  else 

    write(*,*) ' *** Quasiparticle energies obtained by root search (experimental) *** '
    write(*,*)

    SigX(1:mo_num) = self_energy_exchange_diag (1:mo_num)
    Vxc (1:mo_num) = fock_exchange_potential_MO(1:mo_num)
    call QP_graph(eHF(1), SigX(1), Vxc(1), OmRPA(1), rho_RPA_T(1,1,1), eGWlin(1), eGW(1))

  endif

  ! Compute the RPA correlation energy
  call linear_response(ispin, .true., TDA_W, 1.d0, eGW(1), EcRPA, OmRPA(1), XpY_RPA(1,1), XmY_RPA(1,1))

  !--------------!
  ! Dump results !
  !--------------!

  PROVIDE nuclear_repulsion

  if(use_lr) then
    PROVIDE TC_HF_energy
    eSCF = TC_HF_energy
  else
    PROVIDE SCF_energy
    eSCF = SCF_energy
  endif

  call print_G0W0(eHF, SigC(1), Z(1), eGW(1), EcRPA, EcGM)

  deallocate(SigC, Z, OmRPA, rho_RPA_T, eGWlin, SigX, Vxc)
  deallocate(XpY_RPA, XmY_RPA)

  ! Perform BSE calculation

  if(BSE) then

    call Bethe_Salpeter(TDA_W, TDA, eHF, eGW(1), EcBSE)

    if(exchange_kernel) then
 
      EcBSE(1) = 0.5d0*EcBSE(1)
      EcBSE(2) = 1.5d0*EcBSE(2)
 
    endif

    write(*,*)
    write(*,*)'-------------------------------------------------------------------------------'
    write(*,'(2X,A50,F20.10,A3)') 'Tr@BSE@G0W0 correlation energy (singlet) =',EcBSE(1),' au'
    write(*,'(2X,A50,F20.10,A3)') 'Tr@BSE@G0W0 correlation energy (triplet) =',EcBSE(2),' au'
    write(*,'(2X,A50,F20.10,A3)') 'Tr@BSE@G0W0 correlation energy           =',EcBSE(1) + EcBSE(2),' au'
    write(*,'(2X,A50,F20.10,A3)') 'Tr@BSE@G0W0 total energy                 =',nuclear_repulsion + eSCF + EcBSE(1) + EcBSE(2),' au'
    write(*,*)'-------------------------------------------------------------------------------'
    write(*,*)

    !   Compute the BSE correlation energy via the adiabatic connection 

    if(doACFDT) then

      write(*,*) '-------------------------------------------------------------'
      write(*,*) ' Adiabatic connection version of BSE@G0W0 correlation energy '
      write(*,*) '-------------------------------------------------------------'
      write(*,*) 

      if(doXBS) then 

        write(*,*) '*** scaled screening version (XBS) ***'
        write(*,*)

      end if

      call ACFDT(.true., TDA_W, TDA, BSE, eHF, eGW(1), EcAC)

      write(*,*)
      write(*,*)'-------------------------------------------------------------------------------'
      write(*,'(2X,A50,F20.10,A3)') 'AC@BSE@G0W0 correlation energy (singlet) =',EcAC(1),' au'
      write(*,'(2X,A50,F20.10,A3)') 'AC@BSE@G0W0 correlation energy (triplet) =',EcAC(2),' au'
      write(*,'(2X,A50,F20.10,A3)') 'AC@BSE@G0W0 correlation energy           =',EcAC(1) + EcAC(2),' au'
      write(*,'(2X,A50,F20.10,A3)') 'AC@BSE@G0W0 total energy                 =',nuclear_repulsion + eSCF + EcAC(1) + EcAC(2),' au'
      write(*,*)'-------------------------------------------------------------------------------'
      write(*,*)

    endif
  endif

  if(ppBSE) then

    call Bethe_Salpeter_pp(TDA_W, TDA, eHF, eGW(1), EcppBSE)

    write(*,*)
    write(*,*)'-------------------------------------------------------------------------------'
    write(*,'(2X,A50,F20.10,A3)') 'Tr@ppBSE@G0W0 correlation energy (singlet) =',EcppBSE(1),' au'
    write(*,'(2X,A50,F20.10,A3)') 'Tr@ppBSE@G0W0 correlation energy (triplet) =',3d0*EcppBSE(2),' au'
    write(*,'(2X,A50,F20.10,A3)') 'Tr@ppBSE@G0W0 correlation energy           =',EcppBSE(1) + 3d0*EcppBSE(2),' au'
    write(*,'(2X,A50,F20.10,A3)') 'Tr@ppBSE@G0W0 total energy                 =',nuclear_repulsion + eSCF + EcppBSE(1) + 3d0*EcppBSE(2),' au'
    write(*,*)'-------------------------------------------------------------------------------'
    write(*,*)

  endif

  deallocate(eHF)

  return
end subroutine G0W0_calc

