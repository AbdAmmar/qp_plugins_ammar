
! ---
 
subroutine renormalization_factor(e, Omega, rho_T, Z)

  BEGIN_DOC
  !
  ! Compute renormalization factor for GW
  !
  END_DOC

  implicit none
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho_T(nS_exc,mo_num,mo_num)
  double precision, intent(out) :: Z(mo_num)
  integer                       :: p, i, a, jb
  double precision              :: eps, eta_2, tmp

  Z(:) = 0d0

  PROVIDE COHSEX

  if(COHSEX) then
    
    Z(:) = 1.d0
    return
 
  else

    eta_2 = 0.d0

    do p = nC_orb+1, mo_num-nR_orb
      do i = nC_orb+1, nO_orb
        do jb = 1, nS_exc

          eps = e(p) - e(i) + Omega(jb) 
          tmp = rho_T(jb,p,i) * eps / (eps*eps + eta_2)

          Z(p) = Z(p) - 2.d0 * tmp * tmp
        enddo
      enddo
    enddo

    do p = nC_orb+1, mo_num-nR_orb
      do a = nO_orb+1, mo_num-nR_orb
        do jb = 1, nS_exc

          eps = e(p) - e(a) - Omega(jb) 
          tmp = rho_T(jb,a,p) * eps / (eps*eps + eta_2)

          Z(p) = Z(p) - 2.d0 * tmp * tmp
        enddo
      enddo
    enddo

  endif

  ! Compute renormalization factor from derivative of SigC
  Z(:) = 1.d0 / (1.d0 - Z(:))

  return
end subroutine renormalization_factor

! ---

