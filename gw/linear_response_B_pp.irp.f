
! ---

subroutine linear_response_B_pp(ispin, nOO, nVV, lambda, B_pp)

  BEGIN_DOC
  ! Compute the B matrix of the pp channel
  END_DOC

  implicit none
  include 'parameters.h'

  integer,          intent(in)  :: ispin
  integer,          intent(in)  :: nOO
  integer,          intent(in)  :: nVV
  double precision, intent(in)  :: lambda
  double precision, intent(out) :: B_pp(nVV,nOO)
  
  double precision,external     :: Kronecker_delta
  integer                       :: a,b,i,j,ab,ij


! Build B matrix for the singlet manifold
 
  if(ispin == 1) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
      do b=a,mo_num-nR_orb
        ab = ab + 1
        ij = 0
        do i=nC_orb+1,nO_orb
          do j=i,nO_orb
            ij = ij + 1
 
            B_pp(ab,ij) = lambda*(ERI_MO(a,b,i,j) + ERI_MO(a,b,j,i))/sqrt((1d0 + Kronecker_delta(a,b))*(1d0 + Kronecker_delta(i,j)))
 
          end do
        end do
      end do
    end do

  end if

! Build the B matrix for the triplet manifold, or alpha-alpha, or in the spin-orbital basis

  if(ispin == 2 .or. ispin == 4) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
     do b=a+1,mo_num-nR_orb
        ab = ab + 1
        ij = 0
        do i=nC_orb+1,nO_orb
         do j=i+1,nO_orb
            ij = ij + 1
 
            B_pp(ab,ij) = lambda*(ERI_MO(a,b,i,j) - ERI_MO(a,b,j,i))
 
          end do
        end do
      end do
    end do

  end if

! Build the alpha-beta block of the B matrix

  if(ispin == 3) then

    ab = 0
    do a=nO_orb+1,mo_num-nR_orb
     do b=nO_orb+1,mo_num-nR_orb
        ab = ab + 1
        ij = 0
        do i=nC_orb+1,nO_orb
         do j=nC_orb+1,nO_orb
            ij = ij + 1
 
            B_pp(ab,ij) = lambda*ERI_MO(a,b,i,j)
 
          end do
        end do
      end do
    end do

  end if

end subroutine linear_response_B_pp
