
! ---

subroutine BSE_static_kernel_KA(lambda, Omega, rho, WA)

  BEGIN_DOC
  ! Compute the BSE static kernel for the resonant block
  END_DOC

  implicit none

  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: WA(nS_exc,nS_exc)

  double precision              :: chi
  double precision              :: eps
  integer                       :: i, j, a, b, ia, jb, kc


! Initialize 

  WA(:,:) = 0d0

  ia = 0
  do i=nC_orb+1,nO_orb
    do a=nO_orb+1,mo_num-nR_orb
      ia = ia + 1
      jb = 0
      do j=nC_orb+1,nO_orb
        do b=nO_orb+1,mo_num-nR_orb
          jb = jb + 1

          chi = 0d0
          do kc=1,nS_exc
            eps = Omega(kc)**2 + eta**2
            chi = chi + rho(i,j,kc)*rho(a,b,kc)*Omega(kc)/eps
          enddo

          WA(ia,jb) = WA(ia,jb) + lambda*ERI_MO(i,b,j,a) - 4d0*lambda*chi

        enddo
      enddo
    enddo
  enddo

end subroutine BSE_static_kernel_KA
