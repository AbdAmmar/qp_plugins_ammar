==
gw
==

This module includes code based on the **QuAcK** project, developed 
by [Pierre-Francois Loos](https://github.com/pfloos). **QuAcK** is an 
open-source quantum chemistry package that can perform various types 
of GW and DFT calculations.

You can find the original code repository [here](https://github.com/pfloos/QuAcK).


