
! ---

subroutine Bethe_Salpeter_A_matrix_dynamic(lambda, eGW, OmRPA, rho_RPA, OmBSE, A_dyn, ZA_dyn)

  BEGIN_DOC
  ! Compute the dynamic part of the Bethe-Salpeter equation matrices
  END_DOC

  implicit none
  include 'parameters.h'

  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: eGW(mo_num)
  double precision, intent(in)  :: OmRPA(nS_exc)
  double precision, intent(in)  :: rho_RPA(mo_num,mo_num,nS_exc)
  double precision, intent(in)  :: OmBSE
  double precision, intent(out) :: A_dyn(nS_exc,nS_exc)
  double precision, intent(out) :: ZA_dyn(nS_exc,nS_exc)
  
  integer                       :: maxS
  double precision              :: chi
  double precision              :: eps
  integer                       :: i,j,a,b,ia,jb,kc


! Initialization

   A_dyn(:,:) = 0d0
  ZA_dyn(:,:) = 0d0

! Number of poles taken into account 

  maxS = nS_exc

! Build dynamic A matrix

  ia = 0
  do i=nC_orb+1,nO_orb
    do a=nO_orb+1,mo_num-nR_orb
      ia = ia + 1
      jb = 0
      do j=nC_orb+1,nO_orb
        do b=nO_orb+1,mo_num-nR_orb
          jb = jb + 1
 
          chi = 0d0

          do kc=1,maxS
            eps = OmRPA(kc)
            chi = chi + rho_RPA(i,j,kc)*rho_RPA(a,b,kc)*eps/(eps**2 + eta**2)
          enddo

          A_dyn(ia,jb) = A_dyn(ia,jb) - 4d0*lambda*chi

          chi = 0d0

          do kc=1,maxS

            eps = + OmBSE - OmRPA(kc) - (eGW(a) - eGW(j))
            chi = chi + rho_RPA(i,j,kc)*rho_RPA(a,b,kc)*eps/(eps**2 + eta**2)

            eps = + OmBSE - OmRPA(kc) - (eGW(b) - eGW(i))
            chi = chi + rho_RPA(i,j,kc)*rho_RPA(a,b,kc)*eps/(eps**2 + eta**2)

          enddo

          A_dyn(ia,jb) = A_dyn(ia,jb) - 2d0*lambda*chi

          chi = 0d0
          do kc=1,maxS

            eps = + OmBSE - OmRPA(kc) - (eGW(a) - eGW(j))
            chi = chi + rho_RPA(i,j,kc)*rho_RPA(a,b,kc)*(eps**2 - eta**2)/(eps**2 + eta**2)**2

            eps = + OmBSE - OmRPA(kc) - (eGW(b) - eGW(i))
            chi = chi + rho_RPA(i,j,kc)*rho_RPA(a,b,kc)*(eps**2 - eta**2)/(eps**2 + eta**2)**2

          enddo

          ZA_dyn(ia,jb) = ZA_dyn(ia,jb) + 2d0*lambda*chi

        enddo
      enddo
    enddo
  enddo

end subroutine Bethe_Salpeter_A_matrix_dynamic
