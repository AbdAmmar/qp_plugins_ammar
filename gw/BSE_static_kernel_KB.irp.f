
! ---

subroutine BSE_static_kernel_KB(lambda, Omega, rho, KB)

  BEGIN_DOC
  ! Compute the BSE static kernel for the coupling block
  END_DOC

  implicit none

  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: KB(nS_exc,nS_exc)

  double precision              :: chi
  double precision              :: eps
  integer                       :: i, j, a, b, ia, jb, kc


! Initialize 

  KB(:,:) = 0d0

  ia = 0
  do i=nC_orb+1,nO_orb
    do a=nO_orb+1,mo_num-nR_orb
      ia = ia + 1
      jb = 0
      do j=nC_orb+1,nO_orb
        do b=nO_orb+1,mo_num-nR_orb
          jb = jb + 1

          chi = 0d0
          do kc=1,nS_exc
            eps = Omega(kc)**2 + eta**2
            chi = chi + rho(i,b,kc)*rho(a,j,kc)*Omega(kc)/eps
          enddo

          KB(ia,jb) = KB(ia,jb) + lambda*ERI_MO(i,j,b,a) - 4d0*lambda*chi

        enddo
      enddo
    enddo
  enddo

end subroutine BSE_static_kernel_KB
