! ---

BEGIN_PROVIDER [double precision, dipole_int, (mo_num,mo_num,3)]

  implicit none

  PROVIDE mo_dipole_x mo_dipole_y mo_dipole_z

  dipole_int(:,:,1) = mo_dipole_x(:,:)
  dipole_int(:,:,2) = mo_dipole_y(:,:)
  dipole_int(:,:,3) = mo_dipole_z(:,:)

END_PROVIDER

! ---


