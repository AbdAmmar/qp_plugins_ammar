
! ---

BEGIN_PROVIDER[double precision, self_energy_exchange_diag, (mo_num)]

  BEGIN_DOC
  !
  ! Compute exchange part of the self-energy in the MO basis
  !
  END_DOC

  implicit none
  integer :: mu, nu, q

  self_energy_exchange_diag = 0.d0

  PROVIDE use_lr

  if(use_lr) then

    PROVIDE mo_l_coef mo_r_coef

    do q = 1, mo_num
      do mu = 1, ao_num
        do nu = 1, ao_num
          self_energy_exchange_diag(q) = self_energy_exchange_diag(q) + mo_l_coef(mu,q) * exchange_AO(mu,nu) * mo_r_coef(nu,q) 
        enddo
      enddo
    enddo

  else

    PROVIDE mo_coef

    do q = 1, mo_num
      do mu = 1, ao_num
        do nu = 1, ao_num
          self_energy_exchange_diag(q) = self_energy_exchange_diag(q) + mo_coef(mu,q) * exchange_AO(mu,nu) * mo_coef(nu,q) 
        enddo
      enddo
    enddo

  endif

END_PROVIDER

! ---

