
! ---

double precision function dSigmaC(p, w, e, Omega, rho_T)

  BEGIN_DOC
  !
  ! Compute the derivative of the correlation part of the self-energy
  !
  ! TODO
  ! - adapt for ispin ?
  !
  END_DOC

  implicit none

  integer,          intent(in)  :: p
  double precision, intent(in)  :: w
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho_T(nS_exc,mo_num,mo_num)

  integer                       :: i, a, jb
  double precision              :: eps, eps_2, tmp, eta_2, div

  PROVIDE eta

  dSigmaC = 0.d0

  if(eta .lt. 1d-12) then

    do i = nC_orb+1, nO_orb
      do jb = 1, nS_exc

        eps = w - e(i) + Omega(jb)
        tmp = rho_T(jb,p,i)
        div = tmp / eps

        dSigmaC = dSigmaC - 2.d0 * div * div
      enddo
    enddo

    do a = nO_orb+1, mo_num-nR_orb
      do jb = 1, nS_exc

        eps = w - e(a) - Omega(jb)
        tmp = rho_T(jb,a,p)
        div = tmp / eps

        dSigmaC = dSigmaC - 2.d0 * div * div
      enddo
    enddo

  else

    eta_2 = eta * eta

    do i = nC_orb+1, nO_orb
      do jb = 1, nS_exc

        eps   = w - e(i) + Omega(jb)
        eps_2 = eps * eps
        tmp   = rho_T(jb,p,i)
        div   = tmp / (eps_2 + eta_2) 

        dSigmaC = dSigmaC - 2.d0 * div * div * (eps_2 - eta_2)
      enddo
    enddo

    do a = nO_orb+1, mo_num-nR_orb
      do jb = 1, nS_exc

        eps   = w - e(a) - Omega(jb)
        eps_2 = eps * eps
        tmp   = rho_T(jb,a,p)
        div   = tmp / (eps_2 + eta_2) 

        dSigmaC = dSigmaC - 2.d0 * div * div * (eps_2 - eta_2)
      enddo
    enddo

  endif

end function dSigmaC

! ---

