
! ---

subroutine print_G0W0(eHF, SigC, Z, eGW, EcRPA, EcGM)

  BEGIN_DOC
  !
  ! Print one-electron energies and other stuff for G0W0
  !
  END_DOC

  implicit none
  include 'parameters.h'

  double precision, intent(in) :: EcRPA
  double precision, intent(in) :: EcGM
  double precision, intent(in) :: eHF(mo_num)
  double precision, intent(in) :: SigC(mo_num)
  double precision, intent(in) :: Z(mo_num)
  double precision, intent(in) :: eGW(mo_num)

  integer                      :: p, HOMO, LUMO
  double precision             :: Gap

  PROVIDE nuclear_repulsion
  PROVIDE SCF_energy

  ! HOMO and LUMO

  HOMO = nO_orb
  LUMO = HOMO + 1
  Gap = eGW(LUMO)-eGW(HOMO)

  ! Dump results

  write(*,*)'-------------------------------------------------------------------------------'
  write(*,*)'  One-shot G0W0 calculation'
  write(*,*)'-------------------------------------------------------------------------------'
  write(*,'(1X,A1,1X,A3,1X,A1,1X,A15,1X,A1,1X,A15,1X,A1,1X,A15,1X,A1,1X,A15,1X,A1,1X)') &
            '|','#','|','e_HF (eV)','|','Sig_c (eV)','|','Z','|','e_QP (eV)','|'
  write(*,*)'-------------------------------------------------------------------------------'

  do p = 1, mo_num
    write(*,'(1X,A1,1X,I3,1X,A1,1X,F15.6,1X,A1,1X,F15.6,1X,A1,1X,F15.6,1X,A1,1X,F15.6,1X,A1,1X)') &
    '|',p,'|',eHF(p)*HaToeV,'|',SigC(p)*HaToeV,'|',Z(p),'|',eGW(p)*HaToeV,'|'
  enddo

  write(*,*)'-------------------------------------------------------------------------------'
  write(*,'(2X,A30,F15.6,A3)') 'G0W0 HOMO      energy:',eGW(HOMO)*HaToeV,' eV'
  write(*,'(2X,A30,F15.6,A3)') 'G0W0 LUMO      energy:',eGW(LUMO)*HaToeV,' eV'
  write(*,'(2X,A30,F15.6,A3)') 'G0W0 HOMO-LUMO gap   :',Gap*HaToeV,' eV'
  write(*,*)'-------------------------------------------------------------------------------'
  write(*,'(2X,A30,F15.6,A3)') 'RPA@G0W0 total energy      :',nuclear_repulsion + SCF_energy + EcRPA,' au'
  write(*,'(2X,A30,F15.6,A3)') 'RPA@G0W0 correlation energy:',EcRPA,' au'
  write(*,'(2X,A30,F15.6,A3)') 'GM@G0W0  total energy      :',nuclear_repulsion + SCF_energy + EcGM,' au'
  write(*,'(2X,A30,F15.6,A3)') 'GM@G0W0  correlation energy:',EcGM,' au'
  write(*,*)'-------------------------------------------------------------------------------'
  write(*,*)

  return
end subroutine print_G0W0

! ---


