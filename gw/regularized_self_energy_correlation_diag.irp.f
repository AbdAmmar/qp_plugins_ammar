
! ---

subroutine regularized_self_energy_correlation_diag(e, Omega, rho_T, EcGM, SigC)

  BEGIN_DOC
  !
  ! Compute diagonal of the correlation part of the regularized self-energy
  ! 
  END_DOC

  implicit none
  include 'parameters.h'

  double precision, intent(in)  :: e(mo_num)
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho_T(nS_exc,mo_num,mo_num)
  double precision, intent(out) :: SigC(mo_num)
  double precision, intent(out) :: EcGM

  integer                       :: i, a, p, q, jb
  double precision              :: eps

  double precision              :: kappa, kappa_2, tmp
  double precision              :: fk


  SigC(:) = 0d0

  !-----------------------------------------!
  ! Parameters for regularized calculations !
  !-----------------------------------------!

  kappa = 1.d0
  kappa_2 = kappa * kappa


  PROVIDE COHSEX

  if(COHSEX) then

    !-----------------------------
    ! COHSEX static self-energy
    !-----------------------------

    ! COHSEX: SEX part of the COHSEX correlation self-energy
    do p = nC_orb+1, mo_num-nR_orb
      do i = nC_orb+1, nO_orb
        do jb = 1, nS_exc

          tmp = rho_T(jb,p,i)

          SigC(p) = SigC(p) + 4.d0 * tmp * tmp / Omega(jb)
        enddo
      enddo
    enddo
 
    ! COHSEX: COH part of the COHSEX correlation self-energy
    do p = nC_orb+1, mo_num-nR_orb
      do q = nC_orb+1, mo_num-nR_orb
        do jb = 1, nS_exc

          tmp = rho_T(jb,p,q)

          SigC(p) = SigC(p) - 2.d0 * tmp * tmp / Omega(jb)
        enddo
      enddo
    enddo

    ! GM correlation energy
    EcGM = 0d0
    do i = nC_orb+1, nO_orb
      EcGM = EcGM - SigC(i)
    enddo

  else

    !-----------------------------
    ! GW self-energy
    !-----------------------------
 
    ! Occupied part of the correlation self-energy 
    do p = nC_orb+1, mo_num-nR_orb
      do i = nC_orb+1, nO_orb
        do jb = 1, nS_exc

          eps = e(p) - e(i) + Omega(jb)
          fk  = (1.d0 - exp(-2.d0*eps*eps/kappa_2)) / eps
          tmp = rho_T(jb,p,i)

          SigC(p) = SigC(p) + 2.d0 * tmp * tmp * fk
        enddo
      enddo
    enddo
 
    ! Virtual part of the correlation self-energy
    do p = nC_orb+1, mo_num-nR_orb
      do a = nO_orb+1, mo_num-nR_orb
        do jb = 1, nS_exc

          eps = e(p) - e(a) - Omega(jb)
          fk  = (1.d0 - exp(-2.d0*eps*eps/kappa_2)) / eps
          tmp = rho_T(jb,a,p)

          SigC(p) = SigC(p) + 2.d0 * tmp * tmp * fk
        enddo
      enddo
    enddo

    ! GM correlation energy
    EcGM = 0.d0
    do i = nC_orb+1, nO_orb
      do a = nO_orb+1, mo_num-nR_orb
        do jb = 1, nS_exc

          eps = e(a) - e(i) + Omega(jb)
          fk  = (1.d0 - exp(-2.d0*eps*eps/kappa_2)) / eps
          tmp = rho_T(jb,a,i)

          EcGM = EcGM - 4.d0 * tmp * tmp * fk
        enddo
      enddo
    enddo

  endif

end subroutine regularized_self_energy_correlation_diag


