
! ---

subroutine Bethe_Salpeter_dynamic_perturbation_iterative(eGW, dipole_tmp, OmRPA, rho_RPA, OmBSE, XpY, XmY)

  BEGIN_DOC
  ! Compute self-consistently the dynamical effects via perturbation theory for BSE
  END_DOC

  implicit none
  include 'parameters.h'

  double precision, intent(in) :: eGW(mo_num)
  double precision, intent(in) :: dipole_tmp(mo_num,mo_num,ncart)
  double precision, intent(in) :: OmRPA(nS_exc)
  double precision, intent(in) :: rho_RPA(mo_num,mo_num,nS_exc)
  double precision, intent(in) :: OmBSE(nS_exc)
  double precision, intent(in) :: XpY(nS_exc,nS_exc)
  double precision, intent(in) :: XmY(nS_exc,nS_exc)

  integer                       :: ia

  integer                       :: maxS = 10
  double precision              :: gapGW

  integer                       :: nS_excCF
  integer                       :: maxSCF = 10
  double precision              :: Conv
  double precision              :: thresh = 1d-3


  double precision,allocatable  :: OmDyn(:)
  double precision,allocatable  :: OmOld(:)
  double precision,allocatable  :: X(:)
  double precision,allocatable  :: Y(:)

  double precision,allocatable  ::  Ap_dyn(:,:)
  double precision,allocatable  :: ZAp_dyn(:,:)

  double precision,allocatable  ::  Bp_dyn(:,:)

  double precision,allocatable  ::  Am_dyn(:,:)
  double precision,allocatable  :: ZAm_dyn(:,:)

  double precision,allocatable  ::  Bm_dyn(:,:)

! Memory allocation

  maxS = min(nS_exc,maxS)
  allocate(OmDyn(maxS),OmOld(maxS),X(nS_exc),Y(nS_exc),Ap_dyn(nS_exc,nS_exc),ZAp_dyn(nS_exc,nS_exc))

  if(.not.dTDA) &
    allocate(Am_dyn(nS_exc,nS_exc),ZAm_dyn(nS_exc,nS_exc),Bp_dyn(nS_exc,nS_exc),Bm_dyn(nS_exc,nS_exc))

  if(dTDA) then
    write(*,*)
    write(*,*) '*** dynamical TDA activated ***'
    write(*,*)
  end if

  gapGW = eGW(nO_orb+1) - eGW(nO_orb) 

  Conv = 1d0
  nS_excCF = 0
  OmOld(1:maxS) = OmBSE(1:maxS)

  write(*,*) '---------------------------------------------------------------------------------------------------'
  write(*,*) ' First-order dynamical correction to static Bethe-Salpeter excitation energies                     '
  write(*,*) '---------------------------------------------------------------------------------------------------'
  write(*,'(A57,F10.6,A3)') ' BSE neutral excitation must be lower than the GW gap = ',gapGW*HaToeV,' eV'
  write(*,*) '---------------------------------------------------------------------------------------------------'
  write(*,*)

  do while(Conv > thresh .and. nS_excCF < maxSCF)

    nS_excCF = nS_excCF + 1

    write(*,*) '---------------------------------------------------------------------------------------------------'
    write(*,'(2X,A15,I3)') 'Iteration n.',nS_excCF
    write(*,*) '---------------------------------------------------------------------------------------------------'
    write(*,'(2X,A5,1X,A20,1X,A20,1X,A20,A20)') '#','Static (eV)','Dynamic (eV)','Correction (eV)','Convergence (eV)'
    write(*,*) '---------------------------------------------------------------------------------------------------'

    do ia=1,maxS

      X(:) = 0.5d0*(XpY(ia,:) + XmY(ia,:))
      Y(:) = 0.5d0*(XpY(ia,:) - XmY(ia,:))
 
      ! First-order correction 
 
      if(dTDA) then 

       ! Resonant part of the BSE correction
 
        call Bethe_Salpeter_A_matrix_dynamic(1.d0, eGW, OmRPA(1), rho_RPA(1,1,1), OmOld(ia), Ap_dyn(1,1), ZAp_dyn(1,1))
 
        OmDyn(ia) = dot_product(X(:), matmul(Ap_dyn(:,:), X(:)))
 
      else
 
        ! Anti-resonant part of the BSE correction
 
        call Bethe_Salpeter_AB_matrix_dynamic( eta, 1.d0, eGW, OmRPA(1), rho_RPA(1,1,1), OmOld(ia) &
                                             , Ap_dyn(1,1), Am_dyn(1,1), Bp_dyn(1,1), Bm_dyn(1,1) )
 
        OmDyn(ia) = dot_product(X(:),matmul(Ap_dyn(:,:),X(:))) &
                  - dot_product(Y(:),matmul(Am_dyn(:,:),Y(:))) &
                  + dot_product(X(:),matmul(Bp_dyn(:,:),Y(:))) &
                  - dot_product(Y(:),matmul(Bm_dyn(:,:),X(:)))

      end if
 
      write(*,'(2X,I5,5X,F15.6,5X,F15.6,5X,F15.6,5X,F15.6)') & 
        ia,OmBSE(ia)*HaToeV,(OmBSE(ia)+OmDyn(ia))*HaToeV,OmDyn(ia)*HaToeV,(OmBSE(ia) + OmDyn(ia) - OmOld(ia))*HaToeV
 
    end do

    Conv = maxval(abs(OmBSE(1:maxS) + OmDyn(:) - OmOld(:)))*HaToeV
    OmOld(:) = OmBSE(1:maxS) + OmDyn(:)

    write(*,*) '---------------------------------------------------------------------------------------------------'
    write(*,'(2X,A20,1X,F10.6)') ' Convergence = ',Conv
    write(*,*) '---------------------------------------------------------------------------------------------------'
    write(*,*) 

  end do

! Did it actually converge?

  if(nS_excCF == maxSCF) then

    write(*,*)
    write(*,*)'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    write(*,*)'                 Convergence failed                 '
    write(*,*)'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    write(*,*)

  endif

end subroutine Bethe_Salpeter_dynamic_perturbation_iterative
