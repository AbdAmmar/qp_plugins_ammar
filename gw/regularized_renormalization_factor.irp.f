
! ---

subroutine regularized_renormalization_factor(e, Omega, rho_T, Z)

  BEGIN_DOC
  !
  ! Compute the regularized version of the GW renormalization factor
  !
  END_DOC

  implicit none

  double precision, intent(in)  :: e(mo_num)
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: rho_T(nS_exc,mo_num,mo_num)
  double precision, intent(out) :: Z(mo_num)

  integer                       :: i, a, p, jb
  double precision              :: eps, tmp
  double precision              :: kappa, kappa_2
  double precision              :: fk, dfk


  Z(:) = 0d0

  !-----------------------------------------!
  ! Parameters for regularized calculations !
  !-----------------------------------------!

  kappa = 1d0
  kappa_2 = kappa * kappa


  PROVIDE COHSEX

  if(COHSEX) then

    ! static COHSEX approximation
    
    Z(:) = 1d0
    return
 
  else

    ! Occupied part of the correlation self-energy
    do p = nC_orb+1, mo_num-nR_orb
      do i = nC_orb+1, nO_orb
        do jb = 1, nS_exc

          eps = e(p) - e(i) + Omega(jb) 
          fk  = (1d0 - exp(-2.d0*eps*eps/kappa_2)) / eps
          dfk = - fk/eps + 4.d0*kappa_2*exp(-2.d0*eps*eps/kappa_2)
          tmp = rho_T(jb,p,i)

          Z(p) = Z(p) - 2.d0 * tmp * tmp * dfk
        enddo
      enddo
    enddo

    ! Virtual part of the correlation self-energy
    do p = nC_orb+1, mo_num-nR_orb
      do a = nO_orb+1, mo_num-nR_orb
        do jb = 1, nS_exc

          eps = e(p) - e(a) - Omega(jb) 
          fk  = (1.d0 - exp(-2.d0*eps*eps/kappa_2)) / eps
          dfk = - fk/eps + 4.d0*kappa_2*exp(-2.d0*eps*eps/kappa_2)
          tmp = rho_T(jb,a,p)

          Z(p) = Z(p) - 2.d0 * tmp * tmp * dfk
        enddo
      enddo
    enddo

  endif

  ! Compute renormalization factor from derivative of SigC
  Z(:) = 1d0/(1d0 - Z(:))

end subroutine regularized_renormalization_factor
