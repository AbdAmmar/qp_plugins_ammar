
! ---

BEGIN_PROVIDER [double precision, V_XC_TC_3e_mo_diag_alpha, (mo_num)]

  implicit none
  integer                       :: a, i, j, o
  double precision              :: I_aij_aij, I_aij_ija, I_aij_jai, I_aij_aji, I_aij_iaj, I_aij_jia
  double precision, allocatable :: tmp(:)

  PROVIDE mo_l_coef mo_r_coef
  PROVIDE V_XC_TC_3e_mo_diag_CS

  o = elec_beta_num + 1
  call give_integrals_3_body_bi_ort(1, 1, 1, 1, 1, 1, I_aij_aij)

  V_XC_TC_3e_mo_diag_alpha = V_XC_TC_3e_mo_diag_CS

  !$OMP PARALLEL DEFAULT (NONE)                                                                  &
  !$OMP PRIVATE (a, i, j, I_aij_aij, I_aij_ija, I_aij_jai, I_aij_aji, I_aij_iaj, I_aij_jia, tmp) &
  !$OMP SHARED  (mo_num, o, elec_alpha_num, elec_beta_num, V_XC_TC_3e_mo_diag_alpha)

  allocate(tmp(mo_num))
  tmp = 0.d0

  !$OMP DO
  do a = 1, mo_num

    ! ---

    do j = o, elec_alpha_num
      do i = 1, elec_beta_num

        call give_integrals_3_body_bi_ort(a, i, j, a, i, j, I_aij_aij)
        call give_integrals_3_body_bi_ort(a, i, j, i, j, a, I_aij_ija)
        call give_integrals_3_body_bi_ort(a, i, j, j, a, i, I_aij_jai)
        call give_integrals_3_body_bi_ort(a, i, j, a, j, i, I_aij_aji)
        call give_integrals_3_body_bi_ort(a, i, j, i, a, j, I_aij_iaj)
        call give_integrals_3_body_bi_ort(a, i, j, j, i, a, I_aij_jia)

        tmp(a) -= 0.5d0 * ( 2.d0 * I_aij_aij &
                          +        I_aij_ija &
                          +        I_aij_jai &
                          -        I_aij_aji &
                          -        I_aij_iaj &
                          - 2.d0 * I_aij_jia )

      enddo
    enddo

    ! ---

    do j = 1, elec_beta_num
      do i = o, elec_alpha_num

        call give_integrals_3_body_bi_ort(a, i, j, a, i, j, I_aij_aij)
        call give_integrals_3_body_bi_ort(a, i, j, i, j, a, I_aij_ija)
        call give_integrals_3_body_bi_ort(a, i, j, j, a, i, I_aij_jai)
        call give_integrals_3_body_bi_ort(a, i, j, a, j, i, I_aij_aji)
        call give_integrals_3_body_bi_ort(a, i, j, i, a, j, I_aij_iaj)
        call give_integrals_3_body_bi_ort(a, i, j, j, i, a, I_aij_jia)

        tmp(a) -= 0.5d0 * ( 2.d0 * I_aij_aij &
                          +        I_aij_ija &
                          +        I_aij_jai &
                          -        I_aij_aji &
                          - 2.d0 * I_aij_iaj &
                          -        I_aij_jia )
      enddo
    enddo

    ! ---

    do j = o, elec_alpha_num
      do i = o, elec_alpha_num

        call give_integrals_3_body_bi_ort(a, i, j, a, i, j, I_aij_aij)
        call give_integrals_3_body_bi_ort(a, i, j, i, j, a, I_aij_ija)
        call give_integrals_3_body_bi_ort(a, i, j, j, a, i, I_aij_jai)
        call give_integrals_3_body_bi_ort(a, i, j, a, j, i, I_aij_aji)
        call give_integrals_3_body_bi_ort(a, i, j, i, a, j, I_aij_iaj)
        call give_integrals_3_body_bi_ort(a, i, j, j, i, a, I_aij_jia)

        tmp(a) -= 0.5d0 * ( I_aij_aij &
                          + I_aij_ija &
                          + I_aij_jai &
                          - I_aij_aji &
                          - I_aij_iaj &
                          - I_aij_jia )
      enddo
    enddo

    ! ---

  enddo
  !$OMP END DO NOWAIT

  !$OMP CRITICAL
  do a = 1, mo_num
    V_XC_TC_3e_mo_diag_alpha(a) += tmp(a)
  enddo
  !$OMP END CRITICAL

  deallocate(tmp)
  !$OMP END PARALLEL

END_PROVIDER

! ---

BEGIN_PROVIDER [double precision, V_XC_TC_3e_mo_diag_beta, (mo_num)]

  implicit none
  integer                       :: a, i, j, o
  double precision              :: I_aij_aij, I_aij_ija, I_aij_jai, I_aij_aji, I_aij_iaj, I_aij_jia
  double precision, allocatable :: tmp(:)

  PROVIDE mo_l_coef mo_r_coef
  PROVIDE V_XC_TC_3e_mo_diag_CS

  o = elec_beta_num + 1
  call give_integrals_3_body_bi_ort(1, 1, 1, 1, 1, 1, I_aij_aij)

  V_XC_TC_3e_mo_diag_beta = V_XC_TC_3e_mo_diag_CS

  !$OMP PARALLEL DEFAULT (NONE)                                                                  &
  !$OMP PRIVATE (a, i, j, I_aij_aij, I_aij_ija, I_aij_jai, I_aij_aji, I_aij_iaj, I_aij_jia, tmp) &
  !$OMP SHARED  (mo_num, o, elec_alpha_num, elec_beta_num, V_XC_TC_3e_mo_diag_beta)

  allocate(tmp(mo_num))
  tmp = 0.d0

  !$OMP DO
  do a = 1, mo_num

    ! ---

    do j = o, elec_alpha_num
      do i = 1, elec_beta_num

        call give_integrals_3_body_bi_ort(a, i, j, a, i, j, I_aij_aij)
        call give_integrals_3_body_bi_ort(a, i, j, a, j, i, I_aij_aji)
        call give_integrals_3_body_bi_ort(a, i, j, i, a, j, I_aij_iaj)

        tmp(a) -= 0.5d0 * ( 2.d0 * I_aij_aij &
                          -        I_aij_aji &
                          -        I_aij_iaj )
      enddo
    enddo

    ! ---

    do j = 1, elec_beta_num
      do i = o, elec_alpha_num

        call give_integrals_3_body_bi_ort(a, i, j, a, i, j, I_aij_aij)
        call give_integrals_3_body_bi_ort(a, i, j, a, j, i, I_aij_aji)
        call give_integrals_3_body_bi_ort(a, i, j, j, i, a, I_aij_jia)

        tmp(a) -= 0.5d0 * ( 2.d0 * I_aij_aij &
                          -        I_aij_aji &
                          -        I_aij_jia )
      enddo
    enddo

    ! ---

    do j = o, elec_alpha_num
      do i = o, elec_alpha_num

        call give_integrals_3_body_bi_ort(a, i, j, a, i, j, I_aij_aij)
        call give_integrals_3_body_bi_ort(a, i, j, a, j, i, I_aij_aji)

        tmp(a) -= 0.5d0 * ( I_aij_aij &
                          - I_aij_aji )
      enddo
    enddo

    ! ---

  enddo
  !$OMP END DO NOWAIT

  !$OMP CRITICAL
  do a = 1, mo_num
    V_XC_TC_3e_mo_diag_beta(a) += tmp(a)
  enddo
  !$OMP END CRITICAL

  deallocate(tmp)
  !$OMP END PARALLEL

END_PROVIDER 

! ---

BEGIN_PROVIDER [double precision, V_XC_TC_3e_mo_diag_CS, (mo_num)]

  implicit none
  integer                       :: a, i, j
  double precision              :: I_aij_aij, I_aij_ija, I_aij_jai, I_aij_aji, I_aij_iaj, I_aij_jia
  double precision, allocatable :: tmp(:)

  PROVIDE mo_l_coef mo_r_coef
  call give_integrals_3_body_bi_ort(1, 1, 1, 1, 1, 1, I_aij_aij)

  V_XC_TC_3e_mo_diag_CS = 0.d0

  !$OMP PARALLEL DEFAULT (NONE)                                                                  &
  !$OMP PRIVATE (a, i, j, I_aij_aij, I_aij_ija, I_aij_jai, I_aij_aji, I_aij_iaj, I_aij_jia, tmp) &
  !$OMP SHARED  (mo_num, elec_beta_num, V_XC_TC_3e_mo_diag_CS)

  allocate(tmp(mo_num))
  tmp = 0.d0

  !$OMP DO
  do a = 1, mo_num

    do j = 1, elec_beta_num
      do i = 1, elec_beta_num

        call give_integrals_3_body_bi_ort(a, i, j, a, i, j, I_aij_aij)
        call give_integrals_3_body_bi_ort(a, i, j, i, j, a, I_aij_ija)
        call give_integrals_3_body_bi_ort(a, i, j, j, a, i, I_aij_jai)
        call give_integrals_3_body_bi_ort(a, i, j, a, j, i, I_aij_aji)
        call give_integrals_3_body_bi_ort(a, i, j, i, a, j, I_aij_iaj)
        call give_integrals_3_body_bi_ort(a, i, j, j, i, a, I_aij_jia)

        tmp(a) -= 0.5d0 * ( 4.d0 * I_aij_aij &
                          +        I_aij_ija &
                          +        I_aij_jai &
                          - 2.d0 * I_aij_aji &
                          - 2.d0 * I_aij_iaj &
                          - 2.d0 * I_aij_jia )

      enddo
    enddo
  enddo
  !$OMP END DO NOWAIT

  !$OMP CRITICAL
  do a = 1, mo_num
    V_XC_TC_3e_mo_diag_CS(a) += tmp(a)
  enddo
  !$OMP END CRITICAL

  deallocate(tmp)
  !$OMP END PARALLEL

END_PROVIDER 

! ---


