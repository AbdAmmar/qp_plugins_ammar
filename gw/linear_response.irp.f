
! ---

subroutine linear_response(ispin, dRPA, TDA, lambda, e, Ec, Omega, XpY, XmY)

  BEGIN_DOC
  !
  ! Compute linear response
  ! 
  END_DOC

  implicit none

  integer,          intent(in)  :: ispin
  logical,          intent(in)  :: dRPA, TDA
  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: Ec
  double precision, intent(out) :: Omega(nS_exc)
  double precision, intent(out) :: XpY(nS_exc,nS_exc)
  double precision, intent(out) :: XmY(nS_exc,nS_exc)

  integer                       :: i, j, k
  double precision, allocatable :: A(:,:)
  double precision, allocatable :: B(:,:)
  double precision, allocatable :: ApB(:,:)
  double precision, allocatable :: AmB(:,:)
  double precision, allocatable :: AmBSq(:,:)
  double precision, allocatable :: AmBIv(:,:)
  double precision, allocatable :: Z(:,:)
  double precision, allocatable :: tmp(:,:)
  double precision, external    :: trace_matrix


  allocate(A(nS_exc,nS_exc), B(nS_exc,nS_exc), ApB(nS_exc,nS_exc), AmB(nS_exc,nS_exc), AmBSq(nS_exc,nS_exc), AmBIv(nS_exc,nS_exc))
  allocate(Z(nS_exc,nS_exc), tmp(nS_exc,nS_exc))

  call linear_response_A_matrix(ispin, dRPA, lambda, e(1), A(1,1))

  if(TDA) then

    ! Tamm-Dancoff approximation
 
    B(:,:)   = 0d0
    XpY(:,:) = A(:,:)
    call diagonalize_matrix(nS_exc, XpY(1,1), Omega(1))
    ! the transposate of XpY is used for performance issue
    XpY(:,:) = transpose(XpY(:,:))
    XmY(:,:) = XpY(:,:)

  else

    call linear_response_B_matrix(ispin, dRPA, lambda, B(1,1))

    ! Build A + B and A - B matrices 

    ApB = A + B
    AmB = A - B

    ! Diagonalize linear response matrix
    call diagonalize_matrix(nS_exc, AmB(1,1), Omega(1))

    if(minval(Omega) < 0d0) &
      call print_warning('You may have instabilities in linear response: A-B is not positive definite!!')

    !do ia=1,nS_exc
    !  if(Omega(ia) < 0d0) Omega(ia) = 0d0
    !end do

    call ADAt(nS_exc, AmB(1,1), 1.d0*dsqrt(Omega), AmBSq(1,1))
    call ADAt(nS_exc, AmB(1,1), 1.d0/dsqrt(Omega), AmBIv(1,1))

    call dgemm( 'N', 'N', nS_exc, nS_exc, nS_exc, 1.d0             &
              , ApB(1,1), size(ApB, 1), AmBSq(1,1), size(AmBSq, 1) &
              , 0.d0, tmp(1,1), size(tmp, 1))

    call dgemm( 'N', 'N', nS_exc, nS_exc, nS_exc, 1.d0             &
              , AmBSq(1,1), size(AmBSq, 1), tmp(1,1), size(tmp, 1) &
              , 0.d0, Z, size(Z, 1))

   call diagonalize_matrix(nS_exc, Z(1,1), Omega(1))

    if(minval(Omega) < 0d0) & 
      call print_warning('You may have instabilities in linear response: negative excitations!!')
 
    !do ia=1,nS_exc
    !  if(Omega(ia) < 0d0) Omega(ia) = 0d0
    !end do

    Omega = dsqrt(Omega)

    ! the transposate of XpY is used for performance issue
    call dgemm( 'T', 'N', nS_exc, nS_exc, nS_exc, 1d0          &
              , Z(1,1), size(Z, 1), AmBSq(1,1), size(AmBSq, 1) &
              , 0.d0, XpY(1,1), size(XpY, 1))
    call dgemm( 'T', 'N', nS_exc, nS_exc, nS_exc, 1d0          &
              , Z(1,1), size(Z, 1), AmBIv(1,1), size(AmBIv, 1) &
              , 0.d0, XmY(1,1), size(XmY, 1))
    call DA(nS_exc, 1.d0/dsqrt(Omega), XpY(1,1))
    call DA(nS_exc, 1.d0*dsqrt(Omega), XmY(1,1))

    !call dgemm( 'T', 'N', nS_exc, nS_exc, nS_exc, 1d0          &
    !          , AmBSq(1,1), size(AmBSq, 1), Z(1,1), size(Z, 1) &
    !          , 0.d0, XpY(1,1), size(XpY, 1))
    !call dgemm( 'T', 'N', nS_exc, nS_exc, nS_exc, 1d0          &
    !          , AmBIv(1,1), size(AmBIv, 1), Z(1,1), size(Z, 1) &
    !          , 0.d0, XmY(1,1), size(XmY, 1))
    !call AD(nS_exc, XpY(1,1), 1.d0/dsqrt(Omega))
    !call AD(nS_exc, XmY(1,1), 1.d0*dsqrt(Omega))

  end if

  ! Compute the RPA correlation energy

  Ec = 0.5d0*(sum(Omega) - trace_matrix(nS_exc, A))

end subroutine linear_response

! ---

subroutine linear_response_A_matrix(ispin, dRPA, lambda, e, A_lr)

  BEGIN_DOC
  !
  ! Compute linear response
  !
  ! TODO
  ! - write the code for each dRPA separately
  ! - initialize A(:,:) with e(:) to avoid Kronecker_delta
  !
  END_DOC

  implicit none
  integer,          intent(in)  :: ispin
  logical,          intent(in)  :: dRPA
  double precision, intent(in)  :: lambda
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: A_lr(nS_exc,nS_exc)

  double precision, external    :: Kronecker_delta

  integer                       :: i, j, a, b, ia, jb
  double precision              :: delta_dRPA

  PROVIDE ERI_MO

  ! Direct RPA

  delta_dRPA = 0.d0
  if(dRPA) delta_dRPA = 1.d0

  if(ispin == 1) then 

    ! Build A matrix for single manifold
    ia = 0
    do i = nC_orb+1, nO_orb
      do a = nO_orb+1, mo_num-nR_orb
        ia = ia + 1
        jb = 0
        do j = nC_orb+1, nO_orb
          do b = nO_orb+1, mo_num-nR_orb
            jb = jb + 1
 
            A_lr(ia,jb) = (e(a) - e(i))*Kronecker_delta(i,j)*Kronecker_delta(a,b) + 2.d0*lambda*ERI_MO(i,b,a,j) - (1.d0 - delta_dRPA)*lambda*ERI_MO(i,b,j,a)
          enddo
        enddo
      enddo
    enddo
  endif

  if(ispin == 2) then

    ! Build A matrix for triplet manifold
    ia = 0
    do i = nC_orb+1, nO_orb
      do a = nO_orb+1, mo_num-nR_orb
        ia = ia + 1
        jb = 0
        do j = nC_orb+1, nO_orb
          do b = nO_orb+1, mo_num-nR_orb
            jb = jb + 1
 
            A_lr(ia,jb) = (e(a) - e(i))*Kronecker_delta(i,j)*Kronecker_delta(a,b) - (1.d0 - delta_dRPA)*lambda*ERI_MO(i,b,j,a)
          enddo
        enddo
      enddo
    enddo
  endif

  if(ispin == 3) then

    ! Build A matrix for spin orbitals
    ia = 0
    do i = nC_orb+1, nO_orb
      do a = nO_orb+1, mo_num-nR_orb
        ia = ia + 1
        jb = 0
        do j = nC_orb+1, nO_orb
          do b = nO_orb+1, mo_num-nR_orb
            jb = jb + 1
 
            A_lr(ia,jb) = (e(a) - e(i))*Kronecker_delta(i,j)*Kronecker_delta(a,b) + lambda*ERI_MO(i,b,a,j) - (1.d0 - delta_dRPA)*lambda*ERI_MO(i,b,j,a)
          enddo
        enddo
      enddo
    enddo
  endif

  return
end subroutine linear_response_A_matrix

! ---

subroutine linear_response_B_matrix(ispin, dRPA, lambda, B_lr)

  BEGIN_DOC
  !
  ! Compute linear response
  !
  ! TODO
  ! - initialize B(:,:) with e(:) to avoid Kronecker_delta
  ! - tmp for 2xlambda
  !
  END_DOC

  implicit none

  integer,          intent(in)  :: ispin
  logical,          intent(in)  :: dRPA
  double precision, intent(in)  :: lambda
  double precision, intent(out) :: B_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb

  PROVIDE ERI_MO

  ! Direct RPA

  if(dRPA) then

    if(ispin == 1) then
      ! Build B matrix for singlet manifold

      ia = 0
      do i = nC_orb+1, nO_orb
        do a = nO_orb+1, mo_num-nR_orb
          ia = ia + 1
          jb = 0
          do j = nC_orb+1, nO_orb
            do b = nO_orb+1, mo_num-nR_orb
              jb = jb + 1
 
              B_lr(ia,jb) = 2.d0*lambda*ERI_MO(i,j,a,b)
            enddo
          enddo
        enddo
      enddo
    endif

    if(ispin == 2) then
      ! Build B matrix for triplet manifold

      B_lr = 0.d0
    endif

    if(ispin == 3) then
      ! Build B matrix for spin orbitals 

      ia = 0
      do i = nC_orb+1, nO_orb
        do a = nO_orb+1, mo_num-nR_orb
          ia = ia + 1
          jb = 0
          do j = nC_orb+1, nO_orb
            do b = nO_orb+1, mo_num-nR_orb
              jb = jb + 1
 
              B_lr(ia,jb) = lambda*ERI_MO(i,j,a,b)
            enddo
          enddo
        enddo
      enddo
    endif

  else

    if(ispin == 1) then
      ! Build B matrix for singlet manifold

      ia = 0
      do i = nC_orb+1, nO_orb
        do a = nO_orb+1, mo_num-nR_orb
          ia = ia + 1
          jb = 0
          do j = nC_orb+1, nO_orb
            do b = nO_orb+1, mo_num-nR_orb
              jb = jb + 1
 
              B_lr(ia,jb) = lambda * (2.d0*ERI_MO(i,j,a,b) - ERI_MO(i,j,b,a))
            enddo
          enddo
        enddo
      enddo
    endif

    if(ispin == 2) then
      ! Build B matrix for triplet manifold

      ia = 0
      do i = nC_orb+1, nO_orb
        do a = nO_orb+1, mo_num-nR_orb
          ia = ia + 1
          jb = 0
          do j = nC_orb+1, nO_orb
            do b = nO_orb+1, mo_num-nR_orb
              jb = jb + 1
 
              B_lr(ia,jb) = -lambda*ERI_MO(i,j,b,a)
            enddo
          enddo
        enddo
      enddo
    endif

    if(ispin == 3) then
      ! Build B matrix for spin orbitals 

      ia = 0
      do i = nC_orb+1, nO_orb
        do a = nO_orb+1, mo_num-nR_orb
          ia = ia + 1
          jb = 0
          do j = nC_orb+1, nO_orb
            do b = nO_orb+1, mo_num-nR_orb
              jb = jb + 1
 
              B_lr(ia,jb) = lambda * (ERI_MO(i,j,a,b) - ERI_MO(i,j,b,a))
            enddo
          enddo
        enddo
      enddo
    endif

  endif

  return
end subroutine linear_response_B_matrix

! ---

