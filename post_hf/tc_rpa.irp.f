program tc_rpa

  BEGIN_DOC
  !
  ! run a TC-dRPA or TC-RPAx
  !
  END_DOC

  implicit none

  my_grid_becke = .True.
  PROVIDE tc_grid1_a tc_grid1_r
  my_n_pt_r_grid = tc_grid1_r
  my_n_pt_a_grid = tc_grid1_a
  touch my_grid_becke my_n_pt_r_grid my_n_pt_a_grid

  print *, ' j2e_type = ', j2e_type
  print *, ' j1e_type = ', j1e_type
  print *, ' env_type = ', env_type

  if(tc_integ_type .eq. "numeric") then
    my_extra_grid_becke  = .True.
    PROVIDE tc_grid2_a tc_grid2_r
    my_n_pt_r_extra_grid = tc_grid2_r
    my_n_pt_a_extra_grid = tc_grid2_a
    touch my_extra_grid_becke my_n_pt_r_extra_grid my_n_pt_a_extra_grid

    call write_int(6, my_n_pt_r_extra_grid, 'radial  internal grid over')
    call write_int(6, my_n_pt_a_extra_grid, 'angular internal grid over')
  endif

  !call main()
  call compute_ERPA()

end

! ---

subroutine main()

  implicit none
  integer                       :: i, j, ia
  double precision              :: Ec
  double precision, allocatable :: e0(:)
  double precision, allocatable :: Omega1(:), Omega2(:)
  double precision, allocatable :: R1(:,:), R2(:,:), R3(:,:), R4(:,:)
  double precision, allocatable :: L1(:,:), L2(:,:), L3(:,:), L4(:,:)

  PROVIDE mo_l_coef mo_r_coef
  PROVIDE nS_exc
  PROVIDE spin_adap
  PROVIDE spin_rpa
  PROVIDE Fock_matrix_tc_diag_mo_tot
  PROVIDE var_tc

  if(var_tc) then

    ! TODO
    ! call vartc_linear_response
    print*, " VAR-TC-RPA is not implemented yet"
    stop

    !print*, " start VAR-TC-RPA of dimension:", nS_exc 
  
  else
  
    print*, " start TC-RPA of dimension:", nS_exc
  
    allocate(e0(mo_num))
    e0(1:mo_num) = Fock_matrix_tc_diag_mo_tot(1:mo_num)
  
    allocate(Omega1(nS_exc), Omega2(nS_exc))
    allocate(R1(nS_exc,nS_exc), R2(nS_exc,nS_exc), R3(nS_exc,nS_exc), R4(nS_exc,nS_exc))
    allocate(L1(nS_exc,nS_exc), L2(nS_exc,nS_exc), L3(nS_exc,nS_exc), L4(nS_exc,nS_exc))
    call tc_linear_response(e0, Ec, Omega1, Omega2, R1, R2, R3, R4, L1, L2, L3, L4)

    print *, ' TC-RPA correlation energy =', Ec

    print*, ' Omega of TC-RPA:'
    do ia = 1, nS_exc
      !print*, Omega1(ia), Omega2(nS_exc-ia+1), dabs(Omega1(ia) + Omega2(nS_exc-ia+1))
      print*, Omega2(ia)
    enddo

    deallocate(Omega1, Omega2, R1, R2, R3, R4, L1, L2, L3, L4)

  endif

end

! ---

subroutine compute_ERPA()

  implicit none
  integer                       :: ia
  double precision              :: Ec_sing, Ec_trip, Ec_tot
  double precision              :: Eref, E
  double precision, allocatable :: e0(:)
  double precision, allocatable :: Omega1(:), Omega2(:)
  double precision, allocatable :: R1(:,:), R2(:,:), R3(:,:), R4(:,:)
  double precision, allocatable :: L1(:,:), L2(:,:), L3(:,:), L4(:,:)

  provide TC_HF_energy
  PROVIDE nS_exc
  PROVIDE spin_adap
  PROVIDE Fock_matrix_tc_diag_mo_tot

  allocate(e0(mo_num))
  e0(1:mo_num) = Fock_matrix_tc_diag_mo_tot(1:mo_num)

  Eref = TC_HF_energy
 
  if(spin_adap) then

    PROVIDE drpa

    if(drpa) then

      allocate(Omega1(nS_exc), Omega2(nS_exc))
      allocate(R1(nS_exc,nS_exc), R2(nS_exc,nS_exc), R3(nS_exc,nS_exc), R4(nS_exc,nS_exc))
      allocate(L1(nS_exc,nS_exc), L2(nS_exc,nS_exc), L3(nS_exc,nS_exc), L4(nS_exc,nS_exc))

      call print_memory_usage()

      spin_rpa = 1
      TOUCH spin_rpa
      print*, " start TC-dRPA of dimension", nS_exc
      print*, " spin = ", spin_rpa

      call tc_linear_response(e0, Ec_sing, Omega1(1), Omega2(1), R1(1,1), R2(1,1), R3(1,1), R4(1,1), L1(1,1), L2(1,1), L3(1,1), L4(1,1))

      print *, ' Ec_sing = ', Ec_sing
      print *, ' Omega (singlet):'
      write(*, '(10000(F15.7, 2X))') (Omega2(ia), ia = 1, nS_exc)

      ! ---

      spin_rpa = 3
      TOUCH spin_rpa
      print*, " start TC-dRPA of dimension", nS_exc
      print*, " spin = ", spin_rpa

      call tc_linear_response(e0, Ec_trip, Omega1(1), Omega2(1), R1(1,1), R2(1,1), R3(1,1), R4(1,1), L1(1,1), L2(1,1), L3(1,1), L4(1,1))

      print *, ' Ec_trip = ', 3.0d0*Ec_trip
      print *, ' Omega (triplet):'
      write(*, '(10000(F15.7, 2X))') (Omega2(ia), ia = 1, nS_exc)

      deallocate(Omega1, Omega2, R1, R2, R3, R4, L1, L2, L3, L4)

      Ec_tot = Ec_sing + 3.0d0*Ec_trip
 
      print *, ' Eref = ', Eref
      print *, ' Ec   = ', Ec_tot
      print *, ' Etot = ', Eref + Ec_tot

    else

      allocate(Omega1(nS_exc), Omega2(nS_exc))
      allocate(R1(nS_exc,nS_exc), R2(nS_exc,nS_exc), R3(nS_exc,nS_exc), R4(nS_exc,nS_exc))
      allocate(L1(nS_exc,nS_exc), L2(nS_exc,nS_exc), L3(nS_exc,nS_exc), L4(nS_exc,nS_exc))

      call print_memory_usage()

      spin_rpa = 1
      TOUCH spin_rpa
      print*, " start TC-RPAx of dimension", nS_exc
      print*, " spin = ", spin_rpa

      call tc_linear_response(e0, Ec_sing, Omega1(1), Omega2(1), R1(1,1), R2(1,1), R3(1,1), R4(1,1), L1(1,1), L2(1,1), L3(1,1), L4(1,1))

      print *, ' Ec_sing = ', Ec_sing
      print *, ' Omega (singlet):'
      write(*, '(10000(F15.7, 2X))') (Omega2(ia), ia = 1, nS_exc)

      ! ---

      spin_rpa = 3
      TOUCH spin_rpa
      print*, " start TC-RPAx of dimension", nS_exc
      print*, " spin = ", spin_rpa

      call tc_linear_response(e0, Ec_trip, Omega1(1), Omega2(1), R1(1,1), R2(1,1), R3(1,1), R4(1,1), L1(1,1), L2(1,1), L3(1,1), L4(1,1))

      print *, ' Ec_trip = ', 3.0d0*Ec_trip
      print *, ' Omega (triplet):'
      write(*, '(10000(F15.7, 2X))') (Omega2(ia), ia = 1, nS_exc)

      deallocate(Omega1, Omega2, R1, R2, R3, R4, L1, L2, L3, L4)

      Ec_tot = Ec_sing + 3.0d0*Ec_trip
 
      print *, ' Eref = ', Eref
      print *, ' Ec   = ', Ec_tot, 0.5d0*Ec_tot
      print *, ' Etot = ', Eref + Ec_tot, Eref + 0.5d0*Ec_tot

    endif

  else

    print*, " start TC-RPA of dimension:", nS_exc

    allocate(Omega1(nS_exc), Omega2(nS_exc))
    allocate(R1(nS_exc,nS_exc), R2(nS_exc,nS_exc), R3(nS_exc,nS_exc), R4(nS_exc,nS_exc))
    allocate(L1(nS_exc,nS_exc), L2(nS_exc,nS_exc), L3(nS_exc,nS_exc), L4(nS_exc,nS_exc))

    call print_memory_usage()

    call tc_linear_response(e0, Ec_tot, Omega1(1), Omega2(1), R1(1,1), R2(1,1), R3(1,1), R4(1,1), L1(1,1), L2(1,1), L3(1,1), L4(1,1))

    print*, ' Omega:'
    write(*, '(10000(F15.7, 2X))') (Omega2(ia), ia = 1, nS_exc)

    deallocate(Omega1, Omega2, R1, R2, R3, R4, L1, L2, L3, L4)

    print *, ' Eref = ', Eref
    if(.not. drpa) then
      print *, ' Ec   = ', Ec_tot, 0.5d0*Ec_tot
      print *, ' Etot = ', Eref + Ec_tot, Eref + 0.5d0*Ec_tot
    else
      print *, ' Ec   = ', Ec_tot
      print *, ' Etot = ', Eref + Ec_tot
    endif

  endif

end

