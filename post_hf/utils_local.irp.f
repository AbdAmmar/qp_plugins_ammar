
! ---

subroutine diagonalize_nonsym_matrix(N, A, L, e_re, thr_d, thr_nd)

  BEGIN_DOC
  !
  ! Diagonalize a non-symmetric matrix
  ! 
  ! Output
  !   right-eigenvectors are saved in A
  !    left-eigenvectors are saved in L
  !         eigenvalues  are saved in e = e_re + i e_im
  !
  END_DOC

  implicit none

  integer,          intent(in)    :: N
  double precision, intent(in)    :: thr_d, thr_nd
  double precision, intent(inout) :: A(N,N)
  double precision, intent(out)   :: e_re(N), L(N,N)

  integer                         :: i, j, ii
  integer                         :: lwork, info
  double precision                :: accu_d, accu_nd
  integer,          allocatable   :: iorder(:), deg_num(:)
  double precision, allocatable   :: Atmp(:,:), Ltmp(:,:), work(:), e_im(:)
  double precision, allocatable   :: S(:,:)

  PROVIDE imp_bio

  print*, ' Starting a non-Hermitian diagonalization ...'
  print*, ' Good Luck ;)'

  print*, ' imp_bio = ', imp_bio

  ! ---
  ! diagonalize

  allocate(Atmp(N,N), e_im(N))
  Atmp(1:N,1:N) = A(1:N,1:N)

  allocate(work(1))
  lwork = -1 
  call dgeev('V', 'V', N, Atmp, N, e_re, e_im, L, N, A, N, work, lwork, info)
  if(info .gt. 0) then
    print*,'dgeev failed !!', info
    stop
  endif

  lwork = max(int(work(1)), 1)
  deallocate(work)
  allocate(work(lwork))

  call dgeev('V', 'V', N, Atmp, N, e_re, e_im, L, N, A, N, work, lwork, info)
  if(info .ne. 0) then 
    print*,'dgeev failed !!', info
    stop
  endif

  deallocate(Atmp, WORK)


  ! ---
  ! check if eigenvalues are real

  i  = 1
  ii = 0
  do while(i .le. N)
    if(dabs(e_im(i)) .gt. 1.d-12) then
      ii = ii + 1
      print*, ' Warning: complex eigenvalue !'
      print*, i, e_re(i), e_im(i)
      if(dabs(e_im(i)/e_re(i)) .lt. 1.d-6) then
        print*, ' small enouph to be igored'
      else
        print*, ' IMAGINARY PART IS SIGNIFANT !!!'
      endif
    endif
    i = i + 1
  enddo
  if(ii .eq. 0) print*, ' congratulations :) eigenvalues are real-valued !!'


  ! ---
  ! track & sort the real eigenvalues 

  allocate(Atmp(N,N), Ltmp(N,N), iorder(N))

  do i = 1, N
    iorder(i) = i
  enddo
  call dsort(e_re, iorder, N)

  Atmp(:,:) = A(:,:) 
  Ltmp(:,:) = L(:,:) 
  do i = 1, N
    do j = 1, N
      A(j,i) = Atmp(j,iorder(i))
      L(j,i) = Ltmp(j,iorder(i))
    enddo
  enddo

  deallocate(Atmp, Ltmp, iorder)




  ! ---
  ! check bi-orthog
 
  allocate(S(N,N))
  call check_biorthog(N, N, L, A, accu_d, accu_nd, S, thr_d, thr_nd, .false.)

  if((accu_nd .lt. thr_nd) .and. (dabs(accu_d-dble(N))/dble(N) .lt. thr_d)) then

    print *, ' lapack vectors are normalized and bi-orthogonalized'

  elseif((accu_nd .lt. thr_nd) .and. (dabs(accu_d - dble(N)) .gt. thr_d)) then

    print *, ' lapack vectors are not normalized but bi-orthogonalized'

    call check_biorthog_binormalize(N, N, L, A, thr_d, thr_nd, .true.)
    call check_biorthog(N, N, L, A, accu_d, accu_nd, S, thr_d, thr_nd, .true.)

  else

    print *, ' lapack vectors are not normalized neither bi-orthogonalized'

    allocate(deg_num(N))
    call reorder_degen_eigvec(N, deg_num, e_re, L, A)
    call impose_biorthog_degen_eigvec(N, deg_num, e_re, L, A)
    deallocate(deg_num)

    call check_biorthog(N, N, L, A, accu_d, accu_nd, S, thr_d, thr_nd, .false.)
    if((accu_nd .lt. thr_nd) .and. (dabs(accu_d-dble(N))/dble(N) .lt. thr_d)) then
      print *, ' lapack vectors are now normalized and bi-orthogonalized'
    elseif((accu_nd .lt. thr_nd) .and. (dabs(accu_d - dble(N)) .gt. thr_d)) then
      print *, ' lapack vectors are now not normalized but bi-orthogonalized'
      call check_biorthog_binormalize(N, N, L, A, thr_d, thr_nd, .true.)
      call check_biorthog(N, N, L, A, accu_d, accu_nd, S, thr_d, thr_nd, .true.)
    else
      print*, ' bi-orthogonalization failed !'
      if(imp_bio) then
        deallocate(S)
        stop
      endif
    endif

  endif

  deallocate(S)
  return

end

! ---

  
