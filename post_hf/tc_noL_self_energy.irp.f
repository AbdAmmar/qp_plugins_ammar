
! ---

subroutine tc_noL_sigmac_val_der_diag(e0, eta, Omega, VR_pqn, VL_pqn, sig_c, Z_norm)

  implicit none
  double precision, intent(in)  :: e0(mo_num), eta
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: VR_pqn(mo_num,mo_num,nS_exc)
  double precision, intent(in)  :: VL_pqn(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: sig_c(mo_num), Z_norm(mo_num)

  integer                       :: i, j, a, b, p, n, jb
  double precision              :: tmp1, tmp2, tmp12
  double precision              :: dif, dif2
  double precision              :: eta2
  double precision              :: tmp_sig, tmp_Z

  PROVIDE EFF_TCx_MO_chem

  eta2 = eta * eta

  !$OMP PARALLEL                               &
  !$OMP DEFAULT(NONE)                          &
  !$OMP PRIVATE(p, n, i, a, tmp1, tmp2, tmp12, &
  !$OMP         dif, dif2, tmp_sig, tmp_Z)     &
  !$OMP SHARED(mo_num, nS_exc, elec_alpha_num, &
  !$OMP        VR_pqn, VL_pqn, e0, Omega,      &
  !$OMP        eta2, sig_c, Z_norm)
  !$OMP DO
  do p = 1, mo_num 

    tmp_sig = 0.d0
    tmp_Z   = 0.d0
    do n = 1, nS_exc

      do i = 1, elec_alpha_num
        tmp1  = VR_pqn(i,p,n)
        tmp2  = VL_pqn(p,i,n)
        tmp12 = tmp1 * tmp2
        dif   = e0(p) + Omega(n) - e0(i)
        dif2  = dif * dif

        tmp_sig += 2.d0 * tmp12 * dif / (dif2 + eta2)
        tmp_Z   -= 2.d0 * tmp12 * (dif2 - eta2) / ((dif2 - eta2)**2 + 4.d0*dif2*eta2)
      enddo ! i

      do a = elec_alpha_num+1, mo_num
        tmp1  = VR_pqn(p,a,n)
        tmp2  = VL_pqn(a,p,n)
        tmp12 = tmp1 * tmp2
        dif   = e0(p) - Omega(n) - e0(a)
        dif2  = dif * dif

        tmp_sig += 2.d0 * tmp12 * dif / (dif2 + eta2)
        tmp_Z   -= 2.d0 * tmp12 * (dif2 - eta2) / ((dif2 - eta2)**2 + 4.d0*dif2*eta2)
      enddo ! a

    enddo ! n

    sig_c (p) = tmp_sig
    Z_norm(p) = 1.d0 / (1.d0 - tmp_Z)
  enddo ! p
  !$OMP END DO
  !$OMP END PARALLEL

  return
end

! ---

subroutine tc_noL_sigmac_val_der(p, w, e0, eta, Omega, VR_pqn, VL_pqn, sig_c, dsig_c)

  implicit none
  integer,          intent(in)  :: p
  double precision, intent(in)  :: e0(mo_num), eta, w
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: VR_pqn(mo_num,mo_num,nS_exc)
  double precision, intent(in)  :: VL_pqn(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: sig_c, dsig_c

  integer                       :: i, j, a, b, n, jb
  double precision              :: tmp1, tmp2, tmp12
  double precision              :: dif, dif2
  double precision              :: eta2

  PROVIDE EFF_TC_noL_2e_MO 

  eta2 = eta * eta

  sig_c  = 0.d0
  dsig_c = 0.d0

  do n = 1, nS_exc

    do i = 1, elec_alpha_num
      tmp1  = VR_pqn(i,p,n)
      tmp2  = VL_pqn(p,i,n)
      dif   = w + Omega(n) - e0(i)
      dif2  = dif * dif
      tmp12 = tmp1 * tmp2

      sig_c  += 2.d0 * tmp12 * dif / (dif2 + eta2)
      dsig_c -= 2.d0 * tmp12 * (dif2 - eta2) / ((dif2 - eta2)**2 + 4.d0*dif2*eta2)
    enddo ! i

    do a = elec_alpha_num+1, mo_num
      tmp1  = VR_pqn(p,a,n)
      tmp2  = VL_pqn(a,p,n)
      dif   = w - Omega(n) - e0(a)
      dif2  = dif * dif
      tmp12 = tmp1 * tmp2

      sig_c  += 2.d0 * tmp12 * dif / (dif2 + eta2)
      dsig_c -= 2.d0 * tmp12 * (dif2 - eta2) / ((dif2 - eta2)**2 + 4.d0*dif2*eta2)
    enddo ! a

  enddo ! n

  return
end

! ---

subroutine tc_noL_screened_integ(X1, Y1, X2, Y2, VR_pqn, VL_pqn)

  implicit none
  double precision, intent(in)  :: X1(nS_exc,nS_exc), Y1(nS_exc,nS_exc)
  double precision, intent(in)  :: X2(nS_exc,nS_exc), Y2(nS_exc,nS_exc)
  double precision, intent(out) :: VR_pqn(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: VL_pqn(mo_num,mo_num,nS_exc)

  integer                       :: p, q, i, a, ia, n
  double precision              :: tmp1, tmp2

  PROVIDE EFF_TCx_MO_chem

  !$OMP PARALLEL                                &
  !$OMP DEFAULT(NONE)                           &
  !$OMP PRIVATE(p, q, n, i, a, ia, tmp1, tmp2)  &
  !$OMP SHARED(mo_num, nS_exc, elec_alpha_num,  &
  !$OMP        X1, Y1, X2, Y2, EFF_TCx_MO_chem, &
  !$OMP        VR_pqn, VL_pqn)
  !$OMP DO COLLAPSE(3)
  do p = 1, mo_num 
    do q = 1, mo_num 
      do n = 1, nS_exc

        ia = 0
        tmp1 = 0.d0
        tmp2 = 0.d0
        do i = 1, elec_alpha_num
          do a = elec_alpha_num+1, mo_num
            ia = ia + 1
            tmp1 += X1(ia,n) * EFF_TCx_MO_chem(p,q,i,a) + Y1(ia,n) * EFF_TCx_MO_chem(p,q,a,i)
            tmp2 += X2(ia,n) * EFF_TCx_MO_chem(p,q,i,a) + Y2(ia,n) * EFF_TCx_MO_chem(p,q,a,i)
          enddo
        enddo

        VR_pqn(p,q,n) = tmp1
        VL_pqn(p,q,n) = tmp2
      enddo ! n
    enddo ! q
  enddo ! p
  !$OMP END DO
  !$OMP END PARALLEL

  return
end

! ---


