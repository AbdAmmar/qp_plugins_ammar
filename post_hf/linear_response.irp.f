
! ---

subroutine linear_response(e, Ec, Omega, XpY, XmY)

  BEGIN_DOC
  !
  ! Solving the RPA problem by diagonalizing:
  !
  !          (A-B)^0.5 (A+B) (A-B)^0.5 Z = Z Omega^2
  !
  ! X + Y = (A - B)^+0.5 Z Omega^-0.5
  ! X - Y = (A - B)^-0.5 Z Omega^+0.5
  !
  ! X = 0.5 * (A - B)^+0.5 Z Omega^-0.5 + 0.5 * (A - B)^-0.5 Z Omega^+0.5
  ! Y = 0.5 * (A - B)^+0.5 Z Omega^-0.5 - 0.5 * (A - B)^-0.5 Z Omega^+0.5
  !
  ! A - B is assumed to be posidive-definite
  !
  END_DOC

  implicit none

  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: Ec
  double precision, intent(out) :: Omega(nS_exc)
  double precision, intent(out) :: XpY(nS_exc,nS_exc)
  double precision, intent(out) :: XmY(nS_exc,nS_exc)

  integer                       :: ia, jb
  double precision              :: Omega_eps
  double precision, allocatable :: A(:,:), B(:,:)
  double precision, allocatable :: AmBSq(:,:), AmBIv(:,:)
  double precision, allocatable :: Z(:,:)
  double precision, allocatable :: tmp(:,:)
  double precision, allocatable :: X(:,:)

  PROVIDE spin_adap
  PROVIDE spin_rpa
  PROVIDE dRPA
  PROVIDE TDA

  if(dRPA) then
    print *, ' start a dRPA'
  else
    print *, ' start a RPAx'
  endif
  if(spin_adap) then
    print *, ' spin = ', spin_rpa
  endif

  if(TDA) then

    print *, ' Tamm-Dancoff approximation will be used'

    ! B = -B = 0 ==> A X = X Omega & A Y = - Y Omega
    !            ==> Y = 0 
    !            ==> X+Y = X-Y = X

    call linear_response_Amat(e, XpY(1,1))
    !do ia = 1, nS_exc
    !  write(*,'(1000(F15.7, 2X))') XpY(ia,:)
    !enddo

    ! Compute the RPA correlation energy
    Ec = 0.d0
    do ia = 1, nS_exc
      Ec = Ec - 0.5d0 * XpY(ia,ia)
    enddo

    call diagonalize_sym_matrix(nS_exc, XpY(1,1), Omega(1))
    XmY = XpY

  else

    Omega_eps = 1d-14

    print *, ' We suppose (A-B) is positive definite'
    print *, ' This may not be true for RPAx'

    allocate(A(nS_exc,nS_exc), B(nS_exc,nS_exc))

    call linear_response_Amat(e, A(1,1))
    call linear_response_Bmat(B(1,1))

    ! Compute the RPA correlation energy
    Ec = 0.d0
    do ia = 1, nS_exc
      Ec = Ec - 0.5d0 * A(ia,ia)
    enddo

    ! optimize memory usage
    A = A + B       ! A + B
    B = A - 2.d0*B  ! A - B

    call diagonalize_sym_matrix(nS_exc, B(1,1), Omega(1))
    Omega = Omega + Omega_eps
    if(minval(Omega) < 0.d0) then
      print*, ' You may have instabilities in linear response: A-B is not positive definite!!'
      print*, Omega
      Omega = dabs(Omega)
      !stop
    endif

    allocate(tmp(nS_exc,nS_exc))
    allocate(AmBSq(nS_exc,nS_exc), AmBIv(nS_exc,nS_exc))

    ! (A-B)^+0.5

    !$OMP PARALLEL        &
    !$OMP DEFAULT(NONE)   &
    !$OMP PRIVATE(ia, jb) &
    !$OMP SHARED(nS_exc, tmp, B, Omega)
    !$OMP DO
    do ia = 1, nS_exc
      do jb = 1, nS_exc
        tmp(jb,ia) = B(ia,jb) * dsqrt(Omega(jb))
      enddo
    enddo
    !$OMP END DO
    !$OMP END PARALLEL

    call dgemm( 'N', 'N', nS_exc, nS_exc, nS_exc, 1.d0     &
              , B(1,1), size(B, 1), tmp(1,1), size(tmp, 1) &
              , 0.d0, AmBSq(1,1), size(AmBSq, 1))

    ! (A-B)^-0.5

    !$OMP PARALLEL        &
    !$OMP DEFAULT(NONE)   &
    !$OMP PRIVATE(ia, jb) &
    !$OMP SHARED(nS_exc, tmp, B, Omega)
    !$OMP DO
    do ia = 1, nS_exc
      do jb = 1, nS_exc
        tmp(jb,ia) = B(ia,jb) / (dsqrt(Omega(jb)) + 1d-14)
      enddo
    enddo
    !$OMP END DO
    !$OMP END PARALLEL

    call dgemm( 'N', 'N', nS_exc, nS_exc, nS_exc, 1.d0     &
              , B(1,1), size(B, 1), tmp(1,1), size(tmp, 1) &
              , 0.d0, AmBIv(1,1), size(AmBIv, 1))

    deallocate(B)
    allocate(Z(nS_exc,nS_exc))

    call dgemm( 'N', 'N', nS_exc, nS_exc, nS_exc, 1.d0         &
              , A(1,1), size(A, 1), AmBSq(1,1), size(AmBSq, 1) &
              , 0.d0, tmp(1,1), size(tmp, 1))
    deallocate(A)

    call dgemm( 'N', 'N', nS_exc, nS_exc, nS_exc, 1.d0             &
              , AmBSq(1,1), size(AmBSq, 1), tmp(1,1), size(tmp, 1) &
              , 0.d0, Z(1,1), size(Z, 1))

    call diagonalize_sym_matrix(nS_exc, Z(1,1), Omega(1))
    Omega = Omega + Omega_eps
    if(minval(Omega) < 0d0) then
      print*, ' You have instabilities in linear response: negative excitations!!'
      print*, Omega
      !stop
      do ia = 1, nS_exc
        if(Omega(ia) < 0d0) then
          Omega(ia) = 0d0
        endif
      enddo
    endif

    Omega = dsqrt(Omega)

    ! X+Y

    !$OMP PARALLEL        &
    !$OMP DEFAULT(NONE)   &
    !$OMP PRIVATE(ia, jb) &
    !$OMP SHARED(nS_exc, tmp, Z, Omega)
    !$OMP DO
    do ia = 1, nS_exc
      do jb = 1, nS_exc
        tmp(jb,ia) = Z(jb,ia) / (dsqrt(Omega(ia)) + 1d-14)
      enddo
    enddo
    !$OMP END DO
    !$OMP END PARALLEL

    call dgemm( 'N', 'N', nS_exc, nS_exc, nS_exc, 1.d0             &
              , AmBSq(1,1), size(AmBSq, 1), tmp(1,1), size(tmp, 1) &
              , 0.d0, XpY(1,1), size(XpY, 1))

    ! X-Y

    !$OMP PARALLEL        &
    !$OMP DEFAULT(NONE)   &
    !$OMP PRIVATE(ia, jb) &
    !$OMP SHARED(nS_exc, tmp, Z, Omega)
    !$OMP DO
    do ia = 1, nS_exc
      do jb = 1, nS_exc
        tmp(jb,ia) = Z(jb,ia) * dsqrt(Omega(ia))
      enddo
    enddo
    !$OMP END DO
    !$OMP END PARALLEL

    call dgemm( 'N', 'N', nS_exc, nS_exc, nS_exc, 1.d0             &
              , AmBIv(1,1), size(AmBIv, 1), tmp(1,1), size(tmp, 1) &
              , 0.d0, XmY(1,1), size(XmY, 1))

    deallocate(Z, AmBSq, AmBIv)

    ! Check normalization
    allocate(X(nS_exc,nS_exc))

    X = 0.5d0 * (XpY + XmY)
    call dgemm( 'T', 'N', nS_exc, nS_exc, nS_exc, +1.d0 &
              , X(1,1), size(X, 1), X(1,1), size(X, 1)  &
              , 0.d0, tmp(1,1), size(tmp, 1))

    X = 0.5d0 * (XpY - XmY)
    call dgemm( 'T', 'N', nS_exc, nS_exc, nS_exc, -1.d0 &
              , X(1,1), size(X, 1), X(1,1), size(X, 1)  &
              , 1.d0, tmp(1,1), size(tmp, 1))

   deallocate(X)

    do ia = 1, nS_exc
      if(dabs(tmp(ia,ia) - 1.d0) .gt. 1.d-8) then
        print*, ' problem in normalization !!'
        print *, ' X_n^T X_n - Y_n^T Y_n = ', tmp(ia,ia)
        print *, ' for n = ', ia
        if(tmp(ia,ia) .lt. 0.d0) then
          print *, ' !!! Warning !!!'
          print *, ' (X Y)_{n} is not eta-positive for n =', ia
          print *, ' -Omega will be used instead of Omega for this n'
          Omega(ia) = -Omega(ia)
        endif
      endif
    enddo

    deallocate(tmp)

  endif

  ! Compute the RPA correlation energy
  Ec = Ec + 0.5d0 * sum(Omega)

end

! ---

