
! ---

subroutine tc_linear_response_Amat(e, A_lr)

  implicit none
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: A_lr(nS_exc,nS_exc)

  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2
  double precision, external    :: Kronecker_delta

  PROVIDE nS_exc
  PROVIDE spin_adap

  call wall_time(t1)

  if(.not.spin_adap) then
    call tc_spinorbital_lr_Amat(e, A_lr)
    return
  endif

  PROVIDE spin_rpa
  PROVIDE dRPA

  if(dRPA) then

    PROVIDE EFF_dTC_MO_chem

    ! --- ---
    ! dRPA
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building A matrix for dRPA: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) + 2.d0 * EFF_dTC_MO    (a,j,i,b)
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) + 2.d0 * EFF_dTC_MO_chem(a,i,j,b)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building A matrix for dRPA: triplet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b)
            enddo
          enddo
        enddo
      enddo

    endif

    ! --- ---
    ! --- ---

  else

    PROVIDE EFF_TCx_MO_chem

    ! --- ---
    ! RPAx
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building A matrix for RPAx: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) + 2.d0 * EFF_TCx_MO    (a,j,i,b) - EFF_TCx_MO     (a,j,b,i)
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) + 2.d0 * EFF_TCx_MO_chem(a,i,j,b) - EFF_TCx_MO_chem(a,b,j,i)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building A matrix for RPAx: triplet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) - EFF_TCx_MO    (a,j,b,i)
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) - EFF_TCx_MO_chem(a,b,j,i)
            enddo
          enddo
        enddo
      enddo

    endif

    ! --- ---
    ! --- ---

  endif

  call wall_time(t2)
  print *, " WALL TIME for TC-Amat (min)", (t2-t1)/60.d0

  return
end

! ---

subroutine tc_linear_response_Bmat(B_lr)

  implicit none

  double precision, intent(out) :: B_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2

  call wall_time(t1)

  PROVIDE nS_exc
  PROVIDE spin_adap
  if(.not.spin_adap) then
    call tc_spinorbital_lr_Bmat(B_lr)
    return
  endif

  PROVIDE spin_rpa
  PROVIDE dRPA

  if(dRPA) then

    PROVIDE EFF_dTC_MO_chem

    ! --- ---
    ! dRPA
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building B matrix for dRPA: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !B_lr(ia,jb) = 2.d0 * EFF_dTC_MO    (a,b,i,j)
              B_lr(ia,jb) = 2.d0 * EFF_dTC_MO_chem(a,i,b,j)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building B matrix for dRPA: triplet manifold'

      B_lr = 0.d0

    endif

    ! --- ---
    ! --- ---

  else

    PROVIDE EFF_TCx_MO_chem

    ! --- ---
    ! RPAx
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building B matrix for RPAx: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !B_lr(ia,jb) = 2.d0 * EFF_TCx_MO    (a,b,i,j) - EFF_TCx_MO     (a,b,j,i)
              B_lr(ia,jb) = 2.d0 * EFF_TCx_MO_chem(a,i,b,j) - EFF_TCx_MO_chem(a,j,b,i)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building B matrix for RPAx: triplet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !B_lr(ia,jb) = - EFF_TCx_MO    (a,b,j,i)
              B_lr(ia,jb) = - EFF_TCx_MO_chem(a,j,b,i)
            enddo
          enddo
        enddo
      enddo

    endif

    ! --- ---
    ! --- ---

  endif

  call wall_time(t2)
  print *, " WALL TIME for TC-Bmat (min)", (t2-t1)/60.d0

  return
end

! ---

subroutine tc_linear_response_Cmat(C_lr)

  implicit none

  double precision, intent(out) :: C_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2

  call wall_time(t1)

  PROVIDE nS_exc
  PROVIDE spin_adap
  if(.not.spin_adap) then
    call tc_spinorbital_lr_Cmat(C_lr)
    return
  endif

  PROVIDE spin_rpa
  PROVIDE dRPA

  if(dRPA) then

    PROVIDE EFF_dTC_MO_chem

    ! --- ---
    ! dRPA
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building B matrix for dRPA: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !C_lr(ia,jb) = 2.d0 * EFF_dTC_MO    (i,j,a,b)
              C_lr(ia,jb) = 2.d0 * EFF_dTC_MO_chem(i,a,j,b)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building B matrix for dRPA: triplet manifold'

      C_lr = 0.d0

    endif

    ! --- ---
    ! --- ---

  else

    PROVIDE EFF_TCx_MO_chem

    ! --- ---
    ! RPAx
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building C matrix for RPAx: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !C_lr(ia,jb) = 2.d0 * EFF_TCx_MO    (i,j,a,b) - EFF_TCx_MO     (i,j,b,a)
              C_lr(ia,jb) = 2.d0 * EFF_TCx_MO_chem(i,a,j,b) - EFF_TCx_MO_chem(i,b,j,a)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building C matrix for RPAx: triplet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !C_lr(ia,jb) = - EFF_TCx_MO    (i,j,b,a)
              C_lr(ia,jb) = - EFF_TCx_MO_chem(i,b,j,a)
            enddo
          enddo
        enddo
      enddo

    endif

    ! --- ---
    ! --- ---

  endif

  call wall_time(t2)
  print *, " WALL TIME for TC-Bmat (min)", (t2-t1)/60.d0

  return
end

! ---

subroutine tc_spinorbital_lr_Amat(e, A_lr)

  implicit none
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: A_lr(nS_exc,nS_exc)

  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2
  double precision              :: sigma_i, sigma_j, sigma_a, sigma_b
  double precision              :: tmp
  double precision, external    :: Kronecker_delta
  double precision, external    :: EFF_dTC_spinorb
  double precision, external    :: EFF_TCx_spinorb

  PROVIDE nS_exc
  PROVIDE dRPA

  print *, ' PROVIDING TC-Amat in spin-orbitals ...'
  call wall_time(t1)

  tmp = EFF_dTC_spinorb(1, +1.d0, 1, +1.d0, 1, +1.d0, 1, +1.d0)
  tmp = EFF_TCx_spinorb(1, +1.d0, 1, +1.d0, 1, +1.d0, 1, +1.d0)

  if(dRPA) then

    ! --- ---
    ! dRPA
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + EFF_dTC_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  else

    ! --- ---
    ! RPAx
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b)   &
                        + EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b)   &
                        + EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b)   &
                        + EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b)   &
                        + EFF_TCx_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - EFF_TCx_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  endif

  call wall_time(t2)
  print *, ' WALL TIME for TC-Amat (min)', (t2-t1)/60.d0

  return
end

! ---

subroutine tc_spinorbital_lr_Bmat(B_lr)

  implicit none

  double precision, intent(out) :: B_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2
  double precision              :: sigma_i, sigma_j, sigma_a, sigma_b
  double precision              :: tmp
  double precision, external    :: EFF_dTC_spinorb
  double precision, external    :: EFF_TCx_spinorb

  print *, ' PROVIDING TC-Bmat in spin-orbitals ...'
  call wall_time(t1)

  tmp = EFF_dTC_spinorb(1, +1.d0, 1, +1.d0, 1, +1.d0, 1, +1.d0)
  tmp = EFF_TCx_spinorb(1, +1.d0, 1, +1.d0, 1, +1.d0, 1, +1.d0)

  PROVIDE nS_exc
  PROVIDE dRPA

  if(dRPA) then

    ! --- ---
    ! dRPA
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_dTC_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) 
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  else

    ! --- ---
    ! RPAx
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = EFF_TCx_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - EFF_TCx_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  endif

  call wall_time(t2)
  print *, ' WALL TIME for TC-Bmat (min)', (t2-t1)/60.d0

  return
end

! ---

subroutine tc_spinorbital_lr_Cmat(C_lr)

  implicit none

  double precision, intent(out) :: C_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2
  double precision              :: sigma_i, sigma_j, sigma_a, sigma_b
  double precision              :: tmp
  double precision, external    :: EFF_dTC_spinorb
  double precision, external    :: EFF_TCx_spinorb

  print *, ' PROVIDING TC-Cmat in spin-orbitals ...'
  call wall_time(t1)

  tmp = EFF_dTC_spinorb(1, +1.d0, 1, +1.d0, 1, +1.d0, 1, +1.d0)
  tmp = EFF_TCx_spinorb(1, +1.d0, 1, +1.d0, 1, +1.d0, 1, +1.d0)

  PROVIDE nS_exc
  PROVIDE dRPA

  if(dRPA) then

    ! --- ---
    ! dRPA
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_dTC_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  else

    ! --- ---
    ! RPAx
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            C_lr(ia,jb) = EFF_TCx_spinorb(i, sigma_i, j, sigma_j, a, sigma_a, b, sigma_b) &
                        - EFF_TCx_spinorb(i, sigma_i, j, sigma_j, b, sigma_b, a, sigma_a)
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  endif

  call wall_time(t2)
  print *, ' WALL TIME for TC-Cmat (min)', (t2-t1)/60.d0

  return
end

! ---

