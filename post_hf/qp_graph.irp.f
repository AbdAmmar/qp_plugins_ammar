

! ---
! from Titou's Quack code

subroutine QP_graph(e0, eta, Omega, X1, Y1, X2, Y2, sig_c, Z_norm)


  BEGIN_DOC
  !
  ! Compute the graphical solution of the QP equation
  ! using Newton's algorithm 
  !
  END_DOC

  implicit none
  double precision, intent(in)  :: e0(mo_num), eta
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: X1(nS_exc,nS_exc), Y1(nS_exc,nS_exc)
  double precision, intent(in)  :: X2(nS_exc,nS_exc), Y2(nS_exc,nS_exc)
  double precision, intent(out) :: sig_c(mo_num), Z_norm(mo_num)

  integer,          parameter   :: maxIt = 64
  double precision, parameter   :: thresh = 1d-6
  double precision, allocatable :: sig_c_lin(:)

  integer                       :: p
  integer                       :: nIt
  double precision              :: sigC, dsigC
  double precision              :: f, df
  double precision              :: w

  ! inital guess
  allocate(sig_c_lin(mo_num))
  call tc_noL_sigmac_val_der_diag(e0, eta, Omega(1), X1(1,1), Y1(1,1), X2(1,1), Y2(1,1), sig_c_lin(1), Z_norm(1))
  do p = 1, mo_num
    sig_c_lin(p) = e0(p) + Z_norm(p) * sig_c_lin(p)
  enddo

  do p = 1, mo_num

    write(*,*) '-----------------'
    write(*,'(A10,I3)') 'Orbital ', p
    write(*,*) '-----------------'

    w   = sig_c_lin(p)
    nIt = 0
    f   = 1.d0
    write(*,'(A3,I3,A1,1X,3F15.9)') 'It.', nIt, ':', w, f

    do while((abs(f) > thresh) .and. (nIt < maxIt))

      nIt = nIt + 1

      call tc_noL_sigmac_val_der(p, w, e0(1), eta, Omega(1), X1(1,1), Y1(1,1), X2(1,1), Y2(1,1), sigC, dsigC)

      f  = w - e0(p) - sigC
      df = 1.d0 - dsigC
      w  = w - f / df

      write(*,'(A3,I3,A1,1X,3F15.9)') 'It.', nIt, ':', w, f, sigC
    enddo

    if(nIt == maxIt) then

      write(*,*) 'Newton root search has not converged!'
      stop

    else

      sig_c(p) = w
      write(*,'(A32,F16.10)') 'Quasiparticle energy ', sig_c(p)
      write(*,*)

    endif

  enddo

  deallocate(sig_c_lin)

  return
end

