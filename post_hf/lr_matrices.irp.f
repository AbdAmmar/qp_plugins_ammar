
! ---

subroutine linear_response_Amat(e, A_lr)

  implicit none
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: A_lr(nS_exc,nS_exc)

  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2
  double precision, external    :: Kronecker_delta
  double precision, external    :: get_two_e_integral

  PROVIDE nS_exc
  PROVIDE spin_adap

  print *, " PROVIDING Amat ..."
  call wall_time(t1)

  if(.not.spin_adap) then
    call spinorbital_lr_Amat(e, A_lr(1,1))
    return
  endif

  PROVIDE spin_rpa
  PROVIDE dRPA
  !PROVIDE ERI_MO
  PROVIDE mo_integrals_map

  if(dRPA) then

    ! --- ---
    ! dRPA
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building A matrix for dRPA: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) + 2.d0 * ERI_MO(a,j,i,b)
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) + 2.d0 * get_two_e_integral(a, j, i, b, mo_integrals_map)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building A matrix for dRPA: triplet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b)
            enddo
          enddo
        enddo
      enddo

    endif

    ! --- ---
    ! --- ---

  else

    ! --- ---
    ! RPAx
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building A matrix for RPAx: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) + 2.d0 * ERI_MO(a,j,i,b) - ERI_MO(a,j,b,i)
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                          + 2.d0 * get_two_e_integral(a, j, i, b, mo_integrals_map)       &
                          - get_two_e_integral(a, j, b, i, mo_integrals_map)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building A matrix for RPAx: triplet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) - ERI_MO(a,j,b,i)
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) - get_two_e_integral(a, j, b, i, mo_integrals_map)
            enddo
          enddo
        enddo
      enddo

    endif

    ! --- ---
    ! --- ---

  endif


  call wall_time(t2)
  print *, " WALL TIME for Amat (min)", (t2-t1)/60.d0

  return
end

! ---

subroutine linear_response_Bmat(B_lr)

  implicit none

  double precision, intent(out) :: B_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2
  double precision, external    :: get_two_e_integral

  PROVIDE nS_exc
  PROVIDE spin_adap

  print *, " PROVIDING Bmat ..."
  call wall_time(t1)

  if(.not.spin_adap) then
    call spinorbital_lr_Bmat(B_lr)
    return
  endif

  PROVIDE spin_rpa
  PROVIDE dRPA
  !PROVIDE ERI_MO
  PROVIDE mo_integrals_map

  if(dRPA) then

    ! --- ---
    ! dRPA
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building B matrix for dRPA: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !B_lr(ia,jb) = 2.d0 * ERI_MO(a,b,i,j)
              B_lr(ia,jb) = 2.d0 * get_two_e_integral(a, b, i, j, mo_integrals_map)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building B matrix for dRPA: triplet manifold'

      B_lr = 0.d0

    endif

    ! --- ---
    ! --- ---

  else

    ! --- ---
    ! RPAx
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building B matrix for RPAx: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !B_lr(ia,jb) = 2.d0 * ERI_MO(a,b,i,j) - ERI_MO(a,b,j,i)
              B_lr(ia,jb) = 2.d0 * get_two_e_integral(a, b, i, j, mo_integrals_map) - get_two_e_integral(a, b, j, i, mo_integrals_map)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print*, ' Building B matrix for RPAx: triplet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              !B_lr(ia,jb) = - ERI_MO(a,b,j,i)
              B_lr(ia,jb) = - get_two_e_integral(a, b, j, i, mo_integrals_map)
            enddo
          enddo
        enddo
      enddo

    endif

    ! --- ---
    ! --- ---

  endif

  call wall_time(t2)
  print *, " WALL TIME for Bmat (min)", (t2-t1)/60.d0

  return
end

! ---

subroutine spinorbital_lr_Amat(e, A_lr)

  implicit none
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: A_lr(nS_exc,nS_exc)

  integer                       :: i, j, a, b, ia, jb
  double precision              :: sigma_i, sigma_j, sigma_a, sigma_b
  double precision              :: tmp
  double precision, external    :: Kronecker_delta
  double precision, external    :: ERI_spinorb

  PROVIDE nS_exc
  PROVIDE dRPA

  ! to PROVIDE
  tmp = ERI_spinorb(1, +1.d0, 1, +1.d0, 1, +1.d0, 1, +1.d0)

  if(dRPA) then

    ! --- ---
    ! dRPA
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  else

    ! --- ---
    ! RPAx
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)   &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)   &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)   &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            A_lr(ia,jb) = ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b) &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) &
                        + ERI_spinorb(a, sigma_a, j, sigma_j, i, sigma_i, b, sigma_b)   &
                        - ERI_spinorb(a, sigma_a, j, sigma_j, b, sigma_b, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  endif

  return
end

! ---

subroutine spinorbital_lr_Bmat(B_lr)

  implicit none

  double precision, intent(out) :: B_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb
  double precision              :: sigma_i, sigma_j, sigma_a, sigma_b
  double precision              :: tmp
  double precision, external    :: ERI_spinorb

  PROVIDE nS_exc
  PROVIDE dRPA

  ! to PROVIDE
  tmp = ERI_spinorb(1, +1.d0, 1, +1.d0, 1, +1.d0, 1, +1.d0)

  if(dRPA) then

    ! --- ---
    ! dRPA
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) 
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  else

    ! --- ---
    ! RPAx
    ! --- ---

    ia = 0
    ! --- --- ---
    ! ia: up-up
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-dn
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: up-dn
    ! --- --- ---
    do i = 1, elec_alpha_num
      sigma_i = +1.d0
      do a = elec_beta_num+1, mo_num
        sigma_a = -1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- --- ---
    ! ia: dn-up
    ! --- --- ---
    do i = 1, elec_beta_num
      sigma_i = -1.d0
      do a = elec_alpha_num+1, mo_num
        sigma_a = +1.d0
        ia = ia + 1

        jb = 0
        ! --- --- ---
        ! jb: up-up
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-dn
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: up-dn
        ! --- --- ---
        do j = 1, elec_alpha_num
          sigma_j = +1.d0
          do b = elec_beta_num+1, mo_num
            sigma_b = -1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
        ! --- --- ---
        ! jb: dn-up
        ! --- --- ---
        do j = 1, elec_beta_num
          sigma_j = -1.d0
          do b = elec_alpha_num+1, mo_num
            sigma_b = +1.d0
            jb = jb + 1

            B_lr(ia,jb) = ERI_spinorb(a, sigma_a, b, sigma_b, i, sigma_i, j, sigma_j) &
                        - ERI_spinorb(a, sigma_a, b, sigma_b, j, sigma_j, i, sigma_i)
          enddo
        enddo
      enddo
    enddo

    ! --- ---
    ! --- ---

  endif

  return
end

! ---

