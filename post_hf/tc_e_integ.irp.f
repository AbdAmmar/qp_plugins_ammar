
! ---

BEGIN_PROVIDER [double precision, EFF_dTC_MO_v0, (mo_num, mo_num, mo_num, mo_num)]

  BEGIN_DOC 
  ! 
  ! EFF_dTC_MO_v0(p,q,r,s) = < p q | V(12) | r s > 
  !                        + \sum_i < p q i | L(123) | r s i >
  !
  ! the potential V(12) contains ALL TWO-E CONTRIBUTION OF THE TC-HAMILTONIAN
  !
  ! WE SUPPOSE HERE : Na = Nb
  !
  END_DOC

  implicit none
  integer          :: p, q, r, s, i
  double precision :: t1, t2
  double precision :: integral

  print *, ' PROVIDING EFF_dTC_MO_v0 ...'
  call wall_time(t1)

  PROVIDE mo_bi_ortho_tc_two_e_chemist

  integral = mo_bi_ortho_tc_two_e_chemist(1,1,1,1)
  call give_integrals_3_body_bi_ort(1, 1, 1, 1, 1, 1, integral)

  !$OMP PARALLEL                         &
  !$OMP DEFAULT(NONE)                    &
  !$OMP PRIVATE(p, q, r, s, i, integral) &
  !$OMP SHARED(mo_num, elec_beta_num, EFF_dTC_MO_v0, mo_bi_ortho_tc_two_e_chemist)
  !$OMP DO COLLAPSE(3)
  do s = 1, mo_num
    do r = 1, mo_num
      do q = 1, mo_num 
        do p = 1, mo_num
          EFF_dTC_MO_v0(p,q,r,s) = mo_bi_ortho_tc_two_e_chemist(p,r,q,s)
 
          ! Na = Nb
          do i = 1, elec_beta_num
            call give_integrals_3_body_bi_ort(p, q, i, r, s, i, integral)
            EFF_dTC_MO_v0(p,q,r,s) += 2.d0 * (-integral)
          enddo
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  FREE mo_bi_ortho_tc_two_e_chemist

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING EFF_dTC_MO_v0 (min)', (t2-t1)/60.d0
        
END_PROVIDER 

! ---

BEGIN_PROVIDER [double precision, EFF_TCx_MO_v0, (mo_num, mo_num, mo_num, mo_num)]

  BEGIN_DOC 
  ! 
  ! EFF_TCx_MO_v0(p,q,r,s) = < p q | V(12) | r s > 
  !                        + \sum_i [ < p q i | L(123)| r s i >
  !                                 - < p q i | L(123)| r i s >
  !                                 - < p q i | L(123)| i s r > ]
  !
  ! the potential V(12) contains ALL TWO-E CONTRIBUTION OF THE TC-HAMILTONIAN
  !
  ! WE SUPPOSE HERE : Na = Nb
  !
  END_DOC

  implicit none
  integer          :: p, q, r, s, i
  double precision :: t1, t2
  double precision :: integral

  print *, ' PROVIDING EFF_TCx_MO_v0 ...'
  call wall_time(t1)

  PROVIDE mo_bi_ortho_tc_two_e_chemist

  integral = mo_bi_ortho_tc_two_e_chemist(1,1,1,1)
  call give_integrals_3_body_bi_ort(1, 1, 1, 1, 1, 1, integral)

  !$OMP PARALLEL                         &
  !$OMP DEFAULT(NONE)                    &
  !$OMP PRIVATE(p, q, r, s, i, integral) &
  !$OMP SHARED(mo_num, elec_beta_num, EFF_TCx_MO_v0, mo_bi_ortho_tc_two_e_chemist)
  !$OMP DO COLLAPSE(3)
  do s = 1, mo_num
    do r = 1, mo_num
      do q = 1, mo_num 
        do p = 1, mo_num
          EFF_TCx_MO_v0(p,q,r,s) = mo_bi_ortho_tc_two_e_chemist(p,r,q,s)
 
          ! Na = Nb
          do i = 1, elec_beta_num
            call give_integrals_3_body_bi_ort(p, q, i, r, s, i, integral)
            EFF_TCx_MO_v0(p,q,r,s) += 2.d0 * (-integral)

            call give_integrals_3_body_bi_ort(p, q, i, r, i, s, integral)
            EFF_TCx_MO_v0(p,q,r,s) -= (-integral)

            call give_integrals_3_body_bi_ort(p, q, i, i, s, r, integral)
            EFF_TCx_MO_v0(p,q,r,s) -= (-integral)
          enddo
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  FREE mo_bi_ortho_tc_two_e_chemist

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING EFF_TCx_MO_v0 (min)', (t2-t1)/60.d0
        
END_PROVIDER 

! ---

double precision function EFF_dTC_spinorb(p, sigma_p, q, sigma_q, r, sigma_r, s, sigma_s)

  BEGIN_DOC
  !
  ! < p q | V(12) | r s > + \sum_i < p q i | L(123)| r s i >
  !
  !          in spin-orbital
  ! 
  END_DOC 

  implicit none
  integer,          intent(in) :: p, q, r, s
  double precision, intent(in) :: sigma_p, sigma_q, sigma_r, sigma_s

  integer                      :: i
  double precision             :: t1, t2
  double precision             :: integral
  logical, external            :: is_same_spin

  PROVIDE mo_bi_ortho_tc_two_e_chemist

  EFF_dTC_spinorb = 0.d0
  if(is_same_spin(sigma_p, sigma_r)) then
    if(is_same_spin(sigma_q, sigma_s)) then

      EFF_dTC_spinorb = mo_bi_ortho_tc_two_e_chemist(p,r,q,s)

      do i = 1, elec_beta_num
        call give_integrals_3_body_bi_ort(p, q, i, r, s, i, integral)
        EFF_dTC_spinorb += (-integral)
      enddo

      do i = 1, elec_alpha_num
        call give_integrals_3_body_bi_ort(p, q, i, r, s, i, integral)
        EFF_dTC_spinorb += (-integral)
      enddo
    endif
  endif

  return

end

! ---

double precision function EFF_TCx_spinorb(p, sigma_p, q, sigma_q, r, sigma_r, s, sigma_s)

  BEGIN_DOC
  !
  !   < p q | V(12) | r s > 
  ! + \sum_i [ < p q i | L(123)| r s i >
  !          - < p q i | L(123)| r i s >
  !          - < p q i | L(123)| i s r > ]
  !
  !          in spin-orbital
  ! 
  END_DOC 

  implicit none
  integer,          intent(in) :: p, q, r, s
  double precision, intent(in) :: sigma_p, sigma_q, sigma_r, sigma_s

  integer                      :: i
  double precision             :: t1, t2
  double precision             :: sigma_i, integral
  logical, external            :: is_same_spin

  PROVIDE mo_bi_ortho_tc_two_e_chemist

  EFF_TCx_spinorb = 0.d0
  if(is_same_spin(sigma_p, sigma_r)) then
    if(is_same_spin(sigma_q, sigma_s)) then

      EFF_TCx_spinorb = mo_bi_ortho_tc_two_e_chemist(p,r,q,s)

      do i = 1, elec_beta_num
        sigma_i = -1.d0

        call give_integrals_3_body_bi_ort(p, q, i, r, s, i, integral)
        EFF_TCx_spinorb += (-integral)

        if(is_same_spin(sigma_q, sigma_i)) then
          call give_integrals_3_body_bi_ort(p, q, i, r, i, s, integral)
          EFF_TCx_spinorb -= (-integral)
        endif

        if(is_same_spin(sigma_p, sigma_i)) then
          call give_integrals_3_body_bi_ort(p, q, i, i, s, r, integral)
          EFF_TCx_spinorb -= (-integral)
        endif
      enddo

      do i = 1, elec_alpha_num
        sigma_i = +1.d0

        call give_integrals_3_body_bi_ort(p, q, i, r, s, i, integral)
        EFF_TCx_spinorb += (-integral)

        if(is_same_spin(sigma_q, sigma_i)) then
          call give_integrals_3_body_bi_ort(p, q, i, r, i, s, integral)
          EFF_TCx_spinorb -= (-integral)
        endif

        if(is_same_spin(sigma_p, sigma_i)) then
          call give_integrals_3_body_bi_ort(p, q, i, i, s, r, integral)
          EFF_TCx_spinorb -= (-integral)
        endif
      enddo
    endif
  endif

  return

end

! ---

BEGIN_PROVIDER [double precision, EFF_dTC_MO, (mo_num, mo_num, mo_num, mo_num)]

  BEGIN_DOC 
  ! 
  ! EFF_dTC_MO(p,q,r,s) = < p q | V(12) | r s > 
  !                     + \sum_i < p q i | L(123) | r s i >
  !
  ! the potential V(12) contains ALL TWO-E CONTRIBUTION OF THE TC-HAMILTONIAN
  !
  ! WE SUPPOSE HERE : Na = Nb
  !
  END_DOC

  implicit none
  integer                       :: p, q, r, s, i, j
  integer                       :: ipoint
  double precision              :: tmp_loc
  double precision, allocatable :: tmp1(:), tmp2(:,:), tmp3(:,:,:,:), tmp4(:,:,:,:)
  double precision              :: t1, t2

  print *, ' PROVIDING EFF_dTC_MO ...'
  call print_memory_usage()
  call wall_time(t1)

  PROVIDE mo_bi_ortho_tc_two_e_chemist
  PROVIDE int2_grad1_u12_bimo_t
  PROVIDE mos_l_in_r_array_transp mos_r_in_r_array_transp

  ! ---
  ! 3e
  ! ---

  allocate(tmp1(n_points_final_grid), tmp2(n_points_final_grid,3))
  allocate(tmp3(n_points_final_grid,3,mo_num,mo_num))
  allocate(tmp4(mo_num,mo_num,mo_num,mo_num))

  tmp1 = 0.d0
  tmp2 = 0.d0
  do i = 1, elec_beta_num
    do ipoint = 1, n_points_final_grid
      tmp1(ipoint)   = tmp1(ipoint)   -        final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * mos_r_in_r_array_transp(ipoint,i)
      tmp2(ipoint,1) = tmp2(ipoint,1) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,1,i,i)
      tmp2(ipoint,2) = tmp2(ipoint,2) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,2,i,i)
      tmp2(ipoint,3) = tmp2(ipoint,3) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,3,i,i)
    enddo
  enddo

  !$OMP PARALLEL                                                  &
  !$OMP DEFAULT(NONE)                                             &
  !$OMP PRIVATE(p, s, i, ipoint, tmp_loc)                         &
  !$OMP SHARED(mo_num, elec_beta_num, n_points_final_grid,        & 
  !$OMP        final_weight_at_r_vector, mos_l_in_r_array_transp, &
  !$OMP        mos_r_in_r_array_transp, int2_grad1_u12_bimo_t,    &
  !$OMP        tmp1, tmp2, tmp3)

  !$OMP DO COLLAPSE(2)
  do s = 1, mo_num
    do p = 1, mo_num

      do ipoint = 1, n_points_final_grid

        tmp_loc = mos_l_in_r_array_transp(ipoint,p) * mos_r_in_r_array_transp(ipoint,s)

        tmp3(ipoint,1,p,s) = tmp_loc * tmp2(ipoint,1) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,1,p,s)
        tmp3(ipoint,2,p,s) = tmp_loc * tmp2(ipoint,2) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,2,p,s)
        tmp3(ipoint,3,p,s) = tmp_loc * tmp2(ipoint,3) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,3,p,s)

      enddo ! ipoint
    enddo ! p
  enddo ! s
  !$OMP END DO
  !$OMP END PARALLEL

  call dgemm( 'T', 'N', mo_num*mo_num, mo_num*mo_num, 3*n_points_final_grid, 1.d0                         &
            , tmp3(1,1,1,1), 3*n_points_final_grid, int2_grad1_u12_bimo_t(1,1,1,1), 3*n_points_final_grid &
            , 0.d0, tmp4(1,1,1,1), mo_num*mo_num)

  deallocate(tmp1, tmp2, tmp3)

  call sum_a_at(tmp4(1,1,1,1), mo_num*mo_num)

  !$OMP PARALLEL       &
  !$OMP DEFAULT (NONE) &
  !$OMP PRIVATE (p, s, q, r) & 
  !$OMP SHARED (EFF_dTC_MO, tmp4, mo_num)
  !$OMP DO 
  do r = 1, mo_num
    do q = 1, mo_num
      do s = 1, mo_num
        do p = 1, mo_num
          EFF_dTC_MO(p,q,r,s) = tmp4(p,r,q,s)
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  deallocate(tmp4)


  ! ---
  ! 2e
  ! ---

  !$OMP PARALLEL            &
  !$OMP DEFAULT(NONE)       &
  !$OMP PRIVATE(p, q, r, s) &
  !$OMP SHARED(mo_num, EFF_dTC_MO, mo_bi_ortho_tc_two_e_chemist)
  !$OMP DO COLLAPSE(3)
  do s = 1, mo_num
    do r = 1, mo_num
      do q = 1, mo_num 
        do p = 1, mo_num
          EFF_dTC_MO(p,q,r,s) = EFF_dTC_MO(p,q,r,s) + mo_bi_ortho_tc_two_e_chemist(p,r,q,s)
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  FREE mo_bi_ortho_tc_two_e_chemist

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING EFF_dTC_MO (min)', (t2-t1)/60.d0
  call print_memory_usage()
        
END_PROVIDER 

! ---

BEGIN_PROVIDER [double precision, EFF_dTC_MO_chem, (mo_num, mo_num, mo_num, mo_num)]

  BEGIN_DOC 
  ! 
  ! EFF_dTC_MO_chem(p,r,q,s) = < p q | V(12) | r s > 
  !                          + \sum_i < p q i | L(123) | r s i >
  !
  ! the potential V(12) contains ALL TWO-E CONTRIBUTION OF THE TC-HAMILTONIAN
  !
  ! WE SUPPOSE HERE : Na = Nb
  !
  END_DOC

  implicit none
  integer                       :: p, q, r, s, i, j, l
  integer                       :: ipoint
  double precision              :: tmp_loc
  double precision              :: t1, t2, t1_3e, t2_3e, t1_2e, t2_2e, tt1, tt2
  double precision, allocatable :: tmp1(:), tmp2(:,:), tmp3(:,:,:,:)
  double precision, allocatable :: a1(:,:,:,:), a2(:,:,:,:)
  double precision, allocatable :: a_jkp(:,:,:), a_kpq(:,:,:), ao_two_e_tc_tot_tmp(:,:,:)

  print *, ' PROVIDING EFF_dTC_MO_chem ...'
  call print_memory_usage()
  call wall_time(t1)

  PROVIDE int2_grad1_u12_bimo_t
  PROVIDE mos_l_in_r_array_transp mos_r_in_r_array_transp

  ! ---
  ! 3e
  ! ---

  print *, ' PROVIDING 3e-part of EFF_dTC_MO_chem ...'
  call wall_time(t1_3e)

  allocate(tmp1(n_points_final_grid), tmp2(n_points_final_grid,3))
  allocate(tmp3(n_points_final_grid,3,mo_num,mo_num))

  tmp1 = 0.d0
  tmp2 = 0.d0
  do i = 1, elec_beta_num
    do ipoint = 1, n_points_final_grid
      tmp1(ipoint)   = tmp1(ipoint)   -        final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * mos_r_in_r_array_transp(ipoint,i)
      tmp2(ipoint,1) = tmp2(ipoint,1) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,1,i,i)
      tmp2(ipoint,2) = tmp2(ipoint,2) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,2,i,i)
      tmp2(ipoint,3) = tmp2(ipoint,3) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,3,i,i)
    enddo
  enddo

  !$OMP PARALLEL                                                  &
  !$OMP DEFAULT(NONE)                                             &
  !$OMP PRIVATE(p, s, i, ipoint, tmp_loc)                         &
  !$OMP SHARED(mo_num, elec_beta_num, n_points_final_grid,        & 
  !$OMP        final_weight_at_r_vector, mos_l_in_r_array_transp, &
  !$OMP        mos_r_in_r_array_transp, int2_grad1_u12_bimo_t,    &
  !$OMP        tmp1, tmp2, tmp3)

  !$OMP DO COLLAPSE(2)
  do s = 1, mo_num
    do p = 1, mo_num

      do ipoint = 1, n_points_final_grid

        tmp_loc = mos_l_in_r_array_transp(ipoint,p) * mos_r_in_r_array_transp(ipoint,s)

        tmp3(ipoint,1,p,s) = tmp_loc * tmp2(ipoint,1) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,1,p,s)
        tmp3(ipoint,2,p,s) = tmp_loc * tmp2(ipoint,2) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,2,p,s)
        tmp3(ipoint,3,p,s) = tmp_loc * tmp2(ipoint,3) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,3,p,s)

      enddo ! ipoint
    enddo ! p
  enddo ! s
  !$OMP END DO
  !$OMP END PARALLEL

  call dgemm( 'T', 'N', mo_num*mo_num, mo_num*mo_num, 3*n_points_final_grid, 1.d0                         &
            , tmp3(1,1,1,1), 3*n_points_final_grid, int2_grad1_u12_bimo_t(1,1,1,1), 3*n_points_final_grid &
            , 0.d0, EFF_dTC_MO_chem(1,1,1,1), mo_num*mo_num)

  deallocate(tmp1, tmp2, tmp3)

  call sum_a_at(EFF_dTC_MO_chem(1,1,1,1), mo_num*mo_num)

  call wall_time(t2_3e)
  print *, ' WALL TIME for 3e-part of EFF_TCx_MO_chem (min)', (t2_3e-t1_3e)/60.d0
  call print_memory_usage()

  ! ---
  ! 2e
  ! ---

  print *, ' PROVIDING 2e-part of EFF_dTC_MO_chem ...'
  call wall_time(t1_2e)

  if(ao_to_mo_tc_n3) then

    print*, ' memory scale of TC ao -> mo: O(N3) '

    if(.not.read_tc_integ) then
       stop 'read_tc_integ needs to be set to true'
    endif

    allocate(a_jkp(ao_num,ao_num,mo_num))
    allocate(a_kpq(ao_num,mo_num,mo_num))
    allocate(ao_two_e_tc_tot_tmp(ao_num,ao_num,ao_num))

    open(unit=11, form="unformatted", file=trim(ezfio_filename)//'/work/ao_two_e_tc_tot', action="read")
      call wall_time(tt1)
      do l = 1, ao_num
        read(11) ao_two_e_tc_tot_tmp(:,:,:)
        do s = 1, mo_num
          call dgemm( 'T', 'N', ao_num*ao_num, mo_num, ao_num, 1.d0              &
                    , ao_two_e_tc_tot_tmp(1,1,1), ao_num, mo_l_coef(1,1), ao_num &
                    , 0.d0, a_jkp(1,1,1), ao_num*ao_num)
          call dgemm( 'T', 'N', ao_num*mo_num, mo_num, ao_num, 1.d0 &
                    , a_jkp(1,1,1), ao_num, mo_r_coef(1,1), ao_num  &
                    , 0.d0, a_kpq(1,1,1), ao_num*mo_num)
          call dgemm( 'T', 'N', mo_num*mo_num, mo_num, ao_num, mo_r_coef(l,s) &
                    , a_kpq(1,1,1), ao_num, mo_l_coef(1,1), ao_num            &
                    , 1.d0, EFF_dTC_MO_chem(1,1,1,s), mo_num*mo_num)
        enddo ! s
        if(l == 2) then
          call wall_time(tt2)
          print*, ' 1 / mo_num done in (min)', (tt2-tt1)/60.d0
          print*, ' estimated time required (min)', dble(mo_num-1)*(tt2-tt1)/60.d0
        elseif(l == 11) then
          call wall_time(tt2)
          print*, ' 10 / mo_num done in (min)', (tt2-tt1)/60.d0
          print*, ' estimated time required (min)', dble(mo_num-10)*(tt2-tt1)/(60.d0*10.d0)
        elseif(l == 101) then
          call wall_time(tt2)
          print*, ' 100 / mo_num done in (min)', (tt2-tt1)/60.d0
          print*, ' estimated time required (min)', dble(mo_num-100)*(tt2-tt1)/(60.d0*100.d0)
        endif
      enddo ! l
    close(11)
    deallocate(a_jkp, a_kpq, ao_two_e_tc_tot_tmp)

  else

    print*, ' memory scale of TC ao -> mo: O(N4) '

    allocate(a2(ao_num,ao_num,ao_num,mo_num))
    call dgemm( 'T', 'N', ao_num*ao_num*ao_num, mo_num, ao_num, 1.d0     &
              , ao_two_e_tc_tot(1,1,1,1), ao_num, mo_l_coef(1,1), ao_num &
              , 0.d0, a2(1,1,1,1), ao_num*ao_num*ao_num)
    FREE ao_two_e_tc_tot

    allocate(a1(ao_num,ao_num,mo_num,mo_num))
    call dgemm( 'T', 'N', ao_num*ao_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_r_coef(1,1), ao_num          &
              , 0.d0, a1(1,1,1,1), ao_num*ao_num*mo_num)
    deallocate(a2)

    allocate(a2(ao_num,mo_num,mo_num,mo_num))
    call dgemm( 'T', 'N', ao_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a1(1,1,1,1), ao_num, mo_l_coef(1,1), ao_num          &
              , 0.d0, a2(1,1,1,1), ao_num*mo_num*mo_num)
    deallocate(a1)

    call dgemm( 'T', 'N', mo_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_r_coef(1,1), ao_num          &
              , 1.d0, EFF_dTC_MO_chem(1,1,1,1), mo_num*mo_num*mo_num)
    deallocate(a2)

  endif

  call wall_time(t2_2e)
  print *, ' WALL TIME for 2e-part of EFF_dTC_MO_chem (min)', (t2_2e-t1_2e)/60.d0

  ! ---

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING EFF_dTC_MO_chem (min)', (t2-t1)/60.d0
  call print_memory_usage()
        
END_PROVIDER 

! ---

BEGIN_PROVIDER [double precision, EFF_TCx_MO, (mo_num, mo_num, mo_num, mo_num)]

  BEGIN_DOC 
  ! 
  ! EFF_TCx_MO(p,q,r,s) = < p q | V(12) | r s > 
  !                     + \sum_i [ < p q i | L(123)| r s i >
  !                              - < p q i | L(123)| r i s >
  !                              - < p q i | L(123)| i s r > ]
  !
  ! the potential V(12) contains ALL TWO-E CONTRIBUTION OF THE TC-HAMILTONIAN
  !
  ! WE SUPPOSE HERE : Na = Nb
  !
  END_DOC

  implicit none
  integer                       :: p, q, r, s, i
  integer                       :: ipoint
  double precision              :: tmp_loc
  double precision              :: t1, t2
  double precision, allocatable :: tmp1(:), tmp2(:,:), tmp3(:,:,:), tmp4(:,:,:)
  double precision, allocatable :: tmp5(:,:,:,:), tmp6(:,:,:,:)
  double precision, allocatable :: tmp7(:,:,:,:)

  PROVIDE mo_bi_ortho_tc_two_e_chemist
  PROVIDE int2_grad1_u12_bimo_t
  PROVIDE mos_l_in_r_array_transp mos_r_in_r_array_transp

  print *, ' PROVIDING EFF_TCx_MO ...'
  call print_memory_usage()

  call wall_time(t1)

  ! ---
  ! 3e
  ! ---

  allocate(tmp1(n_points_final_grid), tmp2(n_points_final_grid,3))
  allocate(tmp3(n_points_final_grid,3,mo_num), tmp4(n_points_final_grid,3,mo_num))
  allocate(tmp5(n_points_final_grid,4,mo_num,mo_num), tmp6(n_points_final_grid,4,mo_num,mo_num))
  allocate(tmp7(mo_num,mo_num,mo_num,mo_num))

  tmp1 = 0.d0
  tmp2 = 0.d0
  do i = 1, elec_beta_num
    do ipoint = 1, n_points_final_grid
      tmp1(ipoint)   = tmp1(ipoint)   -        final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * mos_r_in_r_array_transp(ipoint,i)
      tmp2(ipoint,1) = tmp2(ipoint,1) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,1,i,i)
      tmp2(ipoint,2) = tmp2(ipoint,2) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,2,i,i)
      tmp2(ipoint,3) = tmp2(ipoint,3) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,3,i,i)
    enddo
  enddo

  !$OMP PARALLEL                                                  &
  !$OMP DEFAULT(NONE)                                             &
  !$OMP PRIVATE(p, i, ipoint)                                     &
  !$OMP SHARED(mo_num, elec_beta_num, n_points_final_grid,        & 
  !$OMP        final_weight_at_r_vector, mos_l_in_r_array_transp, &
  !$OMP        mos_r_in_r_array_transp, int2_grad1_u12_bimo_t,    &
  !$OMP        tmp3, tmp4)

  !$OMP DO
  do p = 1, mo_num

    tmp3(:,:,p) = 0.d0
    tmp4(:,:,p) = 0.d0
    do i = 1, elec_beta_num
      do ipoint = 1, n_points_final_grid
        tmp3(ipoint,1,p) = tmp3(ipoint,1,p) + final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,1,p,i)
        tmp3(ipoint,2,p) = tmp3(ipoint,2,p) + final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,2,p,i)
        tmp3(ipoint,3,p) = tmp3(ipoint,3,p) + final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,3,p,i)
        tmp4(ipoint,1,p) = tmp4(ipoint,1,p) + final_weight_at_r_vector(ipoint) * mos_r_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,1,i,p)
        tmp4(ipoint,2,p) = tmp4(ipoint,2,p) + final_weight_at_r_vector(ipoint) * mos_r_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,2,i,p)
        tmp4(ipoint,3,p) = tmp4(ipoint,3,p) + final_weight_at_r_vector(ipoint) * mos_r_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,3,i,p)
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL


  !$OMP PARALLEL                                                  &
  !$OMP DEFAULT(NONE)                                             &
  !$OMP PRIVATE(p, s, i, ipoint, tmp_loc)                         &
  !$OMP SHARED(mo_num, elec_beta_num, n_points_final_grid,        & 
  !$OMP        final_weight_at_r_vector, mos_l_in_r_array_transp, &
  !$OMP        mos_r_in_r_array_transp, int2_grad1_u12_bimo_t,    &
  !$OMP        tmp1, tmp2, tmp3, tmp4, tmp5, tmp6)

  !$OMP DO COLLAPSE(2)
  do s = 1, mo_num
    do p = 1, mo_num

      do ipoint = 1, n_points_final_grid

        tmp_loc = mos_l_in_r_array_transp(ipoint,p) * mos_r_in_r_array_transp(ipoint,s)

        tmp5(ipoint,1,p,s) = tmp_loc * tmp2(ipoint,1) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,1,p,s) + mos_l_in_r_array_transp(ipoint,p) * tmp4(ipoint,1,s) + mos_r_in_r_array_transp(ipoint,s) * tmp3(ipoint,1,p)
        tmp5(ipoint,2,p,s) = tmp_loc * tmp2(ipoint,2) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,2,p,s) + mos_l_in_r_array_transp(ipoint,p) * tmp4(ipoint,2,s) + mos_r_in_r_array_transp(ipoint,s) * tmp3(ipoint,2,p)
        tmp5(ipoint,3,p,s) = tmp_loc * tmp2(ipoint,3) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,3,p,s) + mos_l_in_r_array_transp(ipoint,p) * tmp4(ipoint,3,s) + mos_r_in_r_array_transp(ipoint,s) * tmp3(ipoint,3,p)

        tmp6(ipoint,1,p,s) = int2_grad1_u12_bimo_t(ipoint,1,p,s)
        tmp6(ipoint,2,p,s) = int2_grad1_u12_bimo_t(ipoint,2,p,s)
        tmp6(ipoint,3,p,s) = int2_grad1_u12_bimo_t(ipoint,3,p,s)
        tmp6(ipoint,4,p,s) = final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,p) * mos_r_in_r_array_transp(ipoint,s)

      enddo ! ipoint

      tmp5(:,4,p,s) = 0.d0
      do i = 1, elec_beta_num
        do ipoint = 1, n_points_final_grid
          tmp5(ipoint,4,p,s) = tmp5(ipoint,4,p,s) + int2_grad1_u12_bimo_t(ipoint,1,p,i) * int2_grad1_u12_bimo_t(ipoint,1,i,s) &
                                                  + int2_grad1_u12_bimo_t(ipoint,2,p,i) * int2_grad1_u12_bimo_t(ipoint,2,i,s) &
                                                  + int2_grad1_u12_bimo_t(ipoint,3,p,i) * int2_grad1_u12_bimo_t(ipoint,3,i,s)
        enddo ! ipoint
      enddo ! i

    enddo ! p
  enddo ! s
  !$OMP END DO
  !$OMP END PARALLEL

  call dgemm( 'T', 'N', mo_num*mo_num, mo_num*mo_num, 4*n_points_final_grid, 1.d0        &
            , tmp5(1,1,1,1), 4*n_points_final_grid, tmp6(1,1,1,1), 4*n_points_final_grid &
            , 0.d0, tmp7(1,1,1,1), mo_num*mo_num)

  deallocate(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6)

  call sum_a_at(tmp7(1,1,1,1), mo_num*mo_num)

  !$OMP PARALLEL       &
  !$OMP DEFAULT (NONE) &
  !$OMP PRIVATE (p, s, q, r) & 
  !$OMP SHARED (EFF_TCx_MO, tmp7, mo_num)
  !$OMP DO 
  do r = 1, mo_num
    do q = 1, mo_num
      do s = 1, mo_num
        do p = 1, mo_num
          EFF_TCx_MO(p,q,r,s) = tmp7(p,r,q,s)
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  deallocate(tmp7)


  ! ---
  ! 2e
  ! ---

  !$OMP PARALLEL            &
  !$OMP DEFAULT(NONE)       &
  !$OMP PRIVATE(p, q, r, s) &
  !$OMP SHARED(mo_num, EFF_TCx_MO, mo_bi_ortho_tc_two_e_chemist)
  !$OMP DO COLLAPSE(3)
  do s = 1, mo_num
    do r = 1, mo_num
      do q = 1, mo_num 
        do p = 1, mo_num
          EFF_TCx_MO(p,q,r,s) = EFF_TCx_MO(p,q,r,s) + mo_bi_ortho_tc_two_e_chemist(p,r,q,s)
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  FREE mo_bi_ortho_tc_two_e_chemist

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING EFF_TCx_MO (min)', (t2-t1)/60.d0
  call print_memory_usage()
        
END_PROVIDER 

! ---

BEGIN_PROVIDER [double precision, EFF_TCx_MO_chem, (mo_num, mo_num, mo_num, mo_num)]

  BEGIN_DOC 
  ! 
  ! EFF_TCx_MO_chem(p,r,q,s) = < p q | V(12) | r s > 
  !                          + \sum_i [ < p q i | L(123)| r s i >
  !                                   - < p q i | L(123)| r i s >
  !                                   - < p q i | L(123)| i s r > ]
  !
  ! the potential V(12) contains ALL TWO-E CONTRIBUTION OF THE TC-HAMILTONIAN
  !
  ! WE SUPPOSE HERE : Na = Nb
  !
  END_DOC

  implicit none
  integer                       :: p, q, r, s, i, l
  integer                       :: ipoint
  double precision              :: tmp_loc
  double precision              :: t1, t2, t1_3e, t2_3e, t1_2e, t2_2e, tt1, tt2
  double precision, allocatable :: tmp1(:), tmp2(:,:), tmp3(:,:,:), tmp4(:,:,:)
  double precision, allocatable :: tmp5(:,:,:,:), tmp6(:,:,:,:)
  double precision, allocatable :: a1(:,:,:,:), a2(:,:,:,:)
  double precision, allocatable :: a_jkp(:,:,:), a_kpq(:,:,:), ao_two_e_tc_tot_tmp(:,:,:)

  print *, ' PROVIDING EFF_TCx_MO_chem ...'
  call print_memory_usage()
  call wall_time(t1)

  PROVIDE int2_grad1_u12_bimo_t
  PROVIDE mos_l_in_r_array_transp mos_r_in_r_array_transp

  ! ---
  ! 3e
  ! ---

  print *, ' PROVIDING 3e-part of EFF_TCx_MO_chem ...'
  call wall_time(t1_3e)

  allocate(tmp1(n_points_final_grid), tmp2(n_points_final_grid,3))
  allocate(tmp3(n_points_final_grid,3,mo_num), tmp4(n_points_final_grid,3,mo_num))
  allocate(tmp5(n_points_final_grid,4,mo_num,mo_num), tmp6(n_points_final_grid,4,mo_num,mo_num))

  tmp1 = 0.d0
  tmp2 = 0.d0
  do i = 1, elec_beta_num
    do ipoint = 1, n_points_final_grid
      tmp1(ipoint)   = tmp1(ipoint)   -        final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * mos_r_in_r_array_transp(ipoint,i)
      tmp2(ipoint,1) = tmp2(ipoint,1) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,1,i,i)
      tmp2(ipoint,2) = tmp2(ipoint,2) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,2,i,i)
      tmp2(ipoint,3) = tmp2(ipoint,3) - 2.d0 * final_weight_at_r_vector(ipoint) * int2_grad1_u12_bimo_t(ipoint,3,i,i)
    enddo
  enddo

  !$OMP PARALLEL                                                  &
  !$OMP DEFAULT(NONE)                                             &
  !$OMP PRIVATE(p, i, ipoint)                                     &
  !$OMP SHARED(mo_num, elec_beta_num, n_points_final_grid,        & 
  !$OMP        final_weight_at_r_vector, mos_l_in_r_array_transp, &
  !$OMP        mos_r_in_r_array_transp, int2_grad1_u12_bimo_t,    &
  !$OMP        tmp3, tmp4)

  !$OMP DO
  do p = 1, mo_num

    tmp3(:,:,p) = 0.d0
    tmp4(:,:,p) = 0.d0
    do i = 1, elec_beta_num
      do ipoint = 1, n_points_final_grid
        tmp3(ipoint,1,p) = tmp3(ipoint,1,p) + final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,1,p,i)
        tmp3(ipoint,2,p) = tmp3(ipoint,2,p) + final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,2,p,i)
        tmp3(ipoint,3,p) = tmp3(ipoint,3,p) + final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,3,p,i)
        tmp4(ipoint,1,p) = tmp4(ipoint,1,p) + final_weight_at_r_vector(ipoint) * mos_r_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,1,i,p)
        tmp4(ipoint,2,p) = tmp4(ipoint,2,p) + final_weight_at_r_vector(ipoint) * mos_r_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,2,i,p)
        tmp4(ipoint,3,p) = tmp4(ipoint,3,p) + final_weight_at_r_vector(ipoint) * mos_r_in_r_array_transp(ipoint,i) * int2_grad1_u12_bimo_t(ipoint,3,i,p)
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL


  !$OMP PARALLEL                                                  &
  !$OMP DEFAULT(NONE)                                             &
  !$OMP PRIVATE(p, s, i, ipoint, tmp_loc)                         &
  !$OMP SHARED(mo_num, elec_beta_num, n_points_final_grid,        & 
  !$OMP        final_weight_at_r_vector, mos_l_in_r_array_transp, &
  !$OMP        mos_r_in_r_array_transp, int2_grad1_u12_bimo_t,    &
  !$OMP        tmp1, tmp2, tmp3, tmp4, tmp5, tmp6)

  !$OMP DO COLLAPSE(2)
  do s = 1, mo_num
    do p = 1, mo_num

      do ipoint = 1, n_points_final_grid

        tmp_loc = mos_l_in_r_array_transp(ipoint,p) * mos_r_in_r_array_transp(ipoint,s)

        tmp5(ipoint,1,p,s) = tmp_loc * tmp2(ipoint,1) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,1,p,s) + mos_l_in_r_array_transp(ipoint,p) * tmp4(ipoint,1,s) + mos_r_in_r_array_transp(ipoint,s) * tmp3(ipoint,1,p)
        tmp5(ipoint,2,p,s) = tmp_loc * tmp2(ipoint,2) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,2,p,s) + mos_l_in_r_array_transp(ipoint,p) * tmp4(ipoint,2,s) + mos_r_in_r_array_transp(ipoint,s) * tmp3(ipoint,2,p)
        tmp5(ipoint,3,p,s) = tmp_loc * tmp2(ipoint,3) + tmp1(ipoint) * int2_grad1_u12_bimo_t(ipoint,3,p,s) + mos_l_in_r_array_transp(ipoint,p) * tmp4(ipoint,3,s) + mos_r_in_r_array_transp(ipoint,s) * tmp3(ipoint,3,p)

        tmp6(ipoint,1,p,s) = int2_grad1_u12_bimo_t(ipoint,1,p,s)
        tmp6(ipoint,2,p,s) = int2_grad1_u12_bimo_t(ipoint,2,p,s)
        tmp6(ipoint,3,p,s) = int2_grad1_u12_bimo_t(ipoint,3,p,s)
        tmp6(ipoint,4,p,s) = final_weight_at_r_vector(ipoint) * mos_l_in_r_array_transp(ipoint,p) * mos_r_in_r_array_transp(ipoint,s)

      enddo ! ipoint

      tmp5(:,4,p,s) = 0.d0
      do i = 1, elec_beta_num
        do ipoint = 1, n_points_final_grid
          tmp5(ipoint,4,p,s) = tmp5(ipoint,4,p,s) + int2_grad1_u12_bimo_t(ipoint,1,p,i) * int2_grad1_u12_bimo_t(ipoint,1,i,s) &
                                                  + int2_grad1_u12_bimo_t(ipoint,2,p,i) * int2_grad1_u12_bimo_t(ipoint,2,i,s) &
                                                  + int2_grad1_u12_bimo_t(ipoint,3,p,i) * int2_grad1_u12_bimo_t(ipoint,3,i,s)
        enddo ! ipoint
      enddo ! i

    enddo ! p
  enddo ! s
  !$OMP END DO
  !$OMP END PARALLEL

  call dgemm( 'T', 'N', mo_num*mo_num, mo_num*mo_num, 4*n_points_final_grid, 1.d0        &
            , tmp5(1,1,1,1), 4*n_points_final_grid, tmp6(1,1,1,1), 4*n_points_final_grid &
            , 0.d0, EFF_TCx_MO_chem(1,1,1,1), mo_num*mo_num)

  deallocate(tmp1, tmp2, tmp3, tmp4, tmp5, tmp6)

  call sum_a_at(EFF_TCx_MO_chem(1,1,1,1), mo_num*mo_num)

  call wall_time(t2_3e)
  print *, ' WALL TIME for 3e-part of EFF_TCx_MO_chem (min)', (t2_3e-t1_3e)/60.d0
  call print_memory_usage()

  ! ---
  ! 2e
  ! ---

  print *, ' PROVIDING 2e-part of EFF_TCx_MO_chem ...'
  call wall_time(t1_2e)

  if(ao_to_mo_tc_n3) then

    print*, ' memory scale of TC ao -> mo: O(N3) '

    if(.not.read_tc_integ) then
       stop 'read_tc_integ needs to be set to true'
    endif

    allocate(a_jkp(ao_num,ao_num,mo_num))
    allocate(a_kpq(ao_num,mo_num,mo_num))
    allocate(ao_two_e_tc_tot_tmp(ao_num,ao_num,ao_num))

    open(unit=11, form="unformatted", file=trim(ezfio_filename)//'/work/ao_two_e_tc_tot', action="read")
      call wall_time(tt1)
      do l = 1, ao_num
        read(11) ao_two_e_tc_tot_tmp(:,:,:)
        do s = 1, mo_num
          call dgemm( 'T', 'N', ao_num*ao_num, mo_num, ao_num, 1.d0              &
                    , ao_two_e_tc_tot_tmp(1,1,1), ao_num, mo_l_coef(1,1), ao_num &
                    , 0.d0, a_jkp(1,1,1), ao_num*ao_num)
          call dgemm( 'T', 'N', ao_num*mo_num, mo_num, ao_num, 1.d0 &
                    , a_jkp(1,1,1), ao_num, mo_r_coef(1,1), ao_num  &
                    , 0.d0, a_kpq(1,1,1), ao_num*mo_num)
          call dgemm( 'T', 'N', mo_num*mo_num, mo_num, ao_num, mo_r_coef(l,s) &
                    , a_kpq(1,1,1), ao_num, mo_l_coef(1,1), ao_num            &
                    , 1.d0, EFF_TCx_MO_chem(1,1,1,s), mo_num*mo_num)
        enddo ! s
        if(l == 2) then
          call wall_time(tt2)
          print*, ' 1 / mo_num done in (min)', (tt2-tt1)/60.d0
          print*, ' estimated time required (min)', dble(mo_num-1)*(tt2-tt1)/60.d0
        elseif(l == 11) then
          call wall_time(tt2)
          print*, ' 10 / mo_num done in (min)', (tt2-tt1)/60.d0
          print*, ' estimated time required (min)', dble(mo_num-10)*(tt2-tt1)/(60.d0*10.d0)
        elseif(l == 101) then
          call wall_time(tt2)
          print*, ' 100 / mo_num done in (min)', (tt2-tt1)/60.d0
          print*, ' estimated time required (min)', dble(mo_num-100)*(tt2-tt1)/(60.d0*100.d0)
        endif
      enddo ! l
    close(11)
    deallocate(a_jkp, a_kpq, ao_two_e_tc_tot_tmp)

  else

    print*, ' memory scale of TC ao -> mo: O(N4) '

    allocate(a2(ao_num,ao_num,ao_num,mo_num))
    call dgemm( 'T', 'N', ao_num*ao_num*ao_num, mo_num, ao_num, 1.d0     &
              , ao_two_e_tc_tot(1,1,1,1), ao_num, mo_l_coef(1,1), ao_num &
              , 0.d0, a2(1,1,1,1), ao_num*ao_num*ao_num)
    FREE ao_two_e_tc_tot

    allocate(a1(ao_num,ao_num,mo_num,mo_num))
    call dgemm( 'T', 'N', ao_num*ao_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_r_coef(1,1), ao_num          &
              , 0.d0, a1(1,1,1,1), ao_num*ao_num*mo_num)
    deallocate(a2)

    allocate(a2(ao_num,mo_num,mo_num,mo_num))
    call dgemm( 'T', 'N', ao_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a1(1,1,1,1), ao_num, mo_l_coef(1,1), ao_num          &
              , 0.d0, a2(1,1,1,1), ao_num*mo_num*mo_num)
    deallocate(a1)

    call dgemm( 'T', 'N', mo_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_r_coef(1,1), ao_num          &
              , 1.d0, EFF_TCx_MO_chem(1,1,1,1), mo_num*mo_num*mo_num)
    deallocate(a2)

  endif

  call wall_time(t2_2e)
  print *, ' WALL TIME for 2e-part of EFF_TCx_MO_chem (min)', (t2_2e-t1_2e)/60.d0

  ! ---

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING EFF_TCx_MO_chem (min)', (t2-t1)/60.d0
  call print_memory_usage()
        
END_PROVIDER 

! ---

BEGIN_PROVIDER [double precision, EFF_TC_noL_2e_MO, (mo_num, mo_num, mo_num, mo_num)]

  BEGIN_DOC 
  ! 
  ! EFF_TC_noL_2e_MO(p,q,r,s) = < p q | V(12) | r s > 
  !                           + 2 \sum_i < i p q | L(123) | i r s >
  !                           - \sum_i < i p q | L(123) | r i s >
  !                           - \sum_i < i p q | L(123) | s r i >
  !
  ! WE SUPPOSE HERE : Na = Nb
  !
  END_DOC

  implicit none
  integer          :: p, q, r, s, i
  double precision :: t1, t2
  double precision :: int1, int2, int3

  print *, ' PROVIDING EFF_TC_noL_2e_MO ...'
  call wall_time(t1)

  PROVIDE mo_bi_ortho_tc_two_e_chemist

  int1 = mo_bi_ortho_tc_two_e_chemist(1,1,1,1)
  call give_integrals_3_body_bi_ort(1, 1, 1, 1, 1, 1, int1)

  !$OMP PARALLEL                                 &
  !$OMP DEFAULT(NONE)                            &
  !$OMP PRIVATE(p, q, r, s, i, int1, int2, int3) &
  !$OMP SHARED(mo_num, elec_beta_num, EFF_TC_noL_2e_MO, mo_bi_ortho_tc_two_e_chemist)
  !$OMP DO COLLAPSE(3)
  do s = 1, mo_num
    do r = 1, mo_num
      do q = 1, mo_num 
        do p = 1, mo_num
          EFF_TC_noL_2e_MO(p,q,r,s) = mo_bi_ortho_tc_two_e_chemist(p,r,q,s)
 
          ! Na = Nb
          do i = 1, elec_beta_num
            call give_integrals_3_body_bi_ort(i, p, q, i, r, s, int1)
            call give_integrals_3_body_bi_ort(i, p, q, r, i, s, int2)
            call give_integrals_3_body_bi_ort(i, p, q, s, r, i, int3)
            EFF_TC_noL_2e_MO(p,q,r,s) += (-2.d0*int1+int2+int3)
          enddo
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  FREE mo_bi_ortho_tc_two_e_chemist

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING EFF_TC_noL_2e_MO (min)', (t2-t1)/60.d0
        
END_PROVIDER 

! ---

