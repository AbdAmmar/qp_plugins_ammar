program deb_rpa

  implicit none

  my_grid_becke = .True.
  PROVIDE tc_grid1_a tc_grid1_r
  my_n_pt_r_grid = tc_grid1_r
  my_n_pt_a_grid = tc_grid1_a
  touch my_grid_becke my_n_pt_r_grid my_n_pt_a_grid

  if(tc_integ_type .eq. "numeric") then
    my_extra_grid_becke  = .True.
    PROVIDE tc_grid2_a tc_grid2_r
    my_n_pt_r_extra_grid = tc_grid2_r
    my_n_pt_a_extra_grid = tc_grid2_a
    touch my_extra_grid_becke my_n_pt_r_extra_grid my_n_pt_a_extra_grid

    call write_int(6, my_n_pt_r_extra_grid, 'radial  internal grid over')
    call write_int(6, my_n_pt_a_extra_grid, 'angular internal grid over')
  endif

  !call test_EFF_dTC_MO()
  !call test_EFF_TCx_MO()
  call test_MO_ERI()

end

! ---

subroutine test_EFF_dTC_MO()
  
  implicit none
  integer          :: i, j, k, l
  double precision :: accu, norm, thr
  double precision :: new, ref, contrib

  PROVIDE EFF_dTC_MO_v0
  PROVIDE EFF_dTC_MO_chem

  thr = 1d-10

  accu = 0.d0
  norm = 0.d0
  do i = 1, mo_num
    do j = 1, mo_num
      do k = 1, mo_num
        do l = 1, mo_num

          new = EFF_dTC_MO_chem(l,j,k,i)
          ref = EFF_dTC_MO_v0  (l,k,j,i)
          contrib = dabs(new - ref)
          if(contrib .gt. thr) then
            print*, ' problem in EFF_dTC_MO_chem'
            print*, l, k, j, i
            print*, ref, new, contrib
            stop
          endif

          accu += contrib
          norm += dabs(ref)
        enddo
      enddo
    enddo
  enddo

  print*, ' accu on EFF_dTC_MO_chem (%) = ', 100.d0 * accu / norm

end

! ---

subroutine test_EFF_TCx_MO()
  
  implicit none
  integer          :: i, j, k, l
  double precision :: accu, norm, thr
  double precision :: new, ref, contrib

  PROVIDE EFF_TCx_MO_v0
  PROVIDE EFF_TCx_MO_chem

  thr = 1d-10

  accu = 0.d0
  norm = 0.d0
  do i = 1, mo_num
    do j = 1, mo_num
      do k = 1, mo_num
        do l = 1, mo_num

          new = EFF_TCx_MO_chem(l,j,k,i)
          ref = EFF_TCx_MO_v0  (l,k,j,i)
          contrib = dabs(new - ref)
          if(contrib .gt. thr) then
            print*, ' problem in EFF_TCx_MO_chem'
            print*, l, k, j, i
            print*, ref, new, contrib
            stop
          endif

          accu += contrib
          norm += dabs(ref)
        enddo
      enddo
    enddo
  enddo

  print*, ' accu on EFF_TCx_MO_chem (%) = ', 100.d0 * accu / norm

end

! ---

subroutine test_MO_ERI()
  
  implicit none
  integer                    :: i, j, k, l
  double precision           :: accu, norm, thr
  double precision           :: new, ref, contrib
  double precision, external :: get_two_e_integral

  PROVIDE ERI_MO
  PROVIDE mo_integrals_map

  thr = 1d-10

  accu = 0.d0
  norm = 0.d0
  do i = 1, mo_num
    do j = 1, mo_num
      do k = 1, mo_num
        do l = 1, mo_num

          ! <ij|kl> 
          new = get_two_e_integral(l, k, j, i, mo_integrals_map)
          ref = ERI_MO(l,k,j,i)
          contrib = dabs(new - ref)
          if(contrib .gt. thr) then
            print*, ' problem in test_MO_ERI'
            print*, l, k, j, i
            print*, ref, new, contrib
            stop
          endif

          accu += contrib
          norm += dabs(ref)
        enddo
      enddo
    enddo
  enddo

  print*, ' accu on ERI_MO (%) = ', 100.d0 * accu / norm

end

! ---


