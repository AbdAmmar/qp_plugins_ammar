
BEGIN_PROVIDER [integer, nS_exc]

  implicit none

  PROVIDE spin_adap

  if(spin_adap) then

    nS_exc = elec_beta_num * (mo_num - elec_beta_num)

  else
  
    nS_exc = elec_num * (2*mo_num - elec_num)

  endif

  call write_int(6, nS_exc, 'nS_exc')

END_PROVIDER

! ---


