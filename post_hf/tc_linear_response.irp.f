
! ---

subroutine tc_linear_response(e, Ec, Omega1, Omega2, R1, R2, R3, R4, L1, L2, L3, L4)

  BEGIN_DOC
  !
  ! Solving the RPA problem by diagonalizing:
  !
  !          (+A   +B ) (R1  R2)   (R1  R2) (Omega1  0)
  !          (        ) (      ) = (      ) (         )
  !          (-C  -A.T) (R3  R4)   (R3  R4) (0  Omega2)
  ! and 
  !          (+A.T  -C) (L1  L2)   (L1  L2) (Omega1  0)
  !          (        ) (      ) = (      ) (         )
  !          ( +B   -A) (L3  L4)   (L3  L4) (0  Omega2)
  !
  ! Note that: 
  !            B.T == B
  !            C.T == C
  !            A.T != A
  !            Omega2 = -Omega1
  !
  ! If(TDA) : C = B = 0
  !           ==> R2 = R3 = L2 = L3 = 0
  !               R4 = L1, L4 = R1
  !               Omega2 = - Omega1
  !           
  !           A   R1 = R1 Omega1
  !           A.T L1 = L1 Omega1
  !
  END_DOC

  implicit none

  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: Ec
  double precision, intent(out) :: Omega1(nS_exc), Omega2(nS_exc)
  double precision, intent(out) :: R1(nS_exc,nS_exc), R2(nS_exc,nS_exc), R3(nS_exc,nS_exc), R4(nS_exc,nS_exc)
  double precision, intent(out) :: L1(nS_exc,nS_exc), L2(nS_exc,nS_exc), L3(nS_exc,nS_exc), L4(nS_exc,nS_exc)

  integer                       :: ia, jb
  logical                       :: impose_biorthog
  double precision, allocatable :: A(:,:), B(:,:), C(:,:)
  double precision, allocatable :: R(:,:), L(:,:), Omega(:)
  double precision, allocatable :: tmp(:,:)

  PROVIDE spin_adap
  PROVIDE spin_rpa
  PROVIDE dRPA
  PROVIDE TDA
  PROVIDE thresh_biorthog_diag thresh_biorthog_nondiag

  if(dRPA) then
    print *, ' start a TC-dRPA'
  else
    print *, ' start a TC-RPAx'
  endif
  if(spin_adap) then
    print *, ' spin = ', spin_rpa
  endif


  if(TDA) then

    print *, ' Tamm-Dancoff approximation will be used (TC-CIS)'

    R2 = 0.d0
    R3 = 0.d0
    L2 = 0.d0
    L3 = 0.d0

    call tc_linear_response_Amat(e, R1)

    ! Compute the RPA correlation energy
    Ec = 0.d0
    do ia = 1, nS_exc
      Ec = Ec - 0.5d0 * R1(ia,ia)
    enddo

    call diagonalize_nonsym_matrix(nS_exc, R1, L1, Omega1, thresh_biorthog_diag, thresh_biorthog_nondiag)

    R4 = L1
    L4 = R1
    Omega2 = -Omega1

    ! Compute the TC-RPA correlation energy
    Ec = Ec + 0.5d0 * sum(Omega1)

  else

    allocate(A(nS_exc,nS_exc), B(nS_exc,nS_exc), C(nS_exc,nS_exc))
    call tc_linear_response_Amat(e(1), A(1,1))
    call tc_linear_response_Bmat(B(1,1))
    call tc_linear_response_Cmat(C(1,1))

    ! Compute the RPA correlation energy
    Ec = 0.d0
    do ia = 1, nS_exc
      Ec = Ec - 0.5d0 * A(ia,ia)
    enddo

    allocate(R(2*nS_exc,2*nS_exc), L(2*nS_exc,2*nS_exc), Omega(2*nS_exc))
    do ia = 1, nS_exc
      do jb = 1, nS_exc
        R(       jb,       ia) =  A(jb,ia)
        R(       jb,nS_exc+ia) =  B(ia,jb)
        R(nS_exc+jb,       ia) = -C(ia,jb)
        R(nS_exc+jb,nS_exc+ia) = -A(ia,jb)
      enddo
    enddo
    deallocate(A, B, C)

    !print*, ' TC-RPA matrix:'
    !do ia = 1, 2*nS_exc
    !  write(*,'(1000(F16.10,X))') R(ia,:)
    !enddo

    call diagonalize_nonsym_matrix(2*nS_exc, R(1,1), L(1,1), Omega(1), thresh_biorthog_diag, thresh_biorthog_nondiag)

    do ia = 1, nS_exc

      if(dabs(Omega(ia) + Omega(2*nS_exc-ia+1)) .gt. 1d-12) then
        print*, ' eigenvalues Omega are strange'
        print *, ia, Omega(ia), Omega(2*nS_exc-ia+1)
      endif

      Omega1(ia) = Omega(       ia)
      Omega2(ia) = Omega(nS_exc+ia)

      do jb = 1, nS_exc
        R1(jb,ia) = R(       jb,       ia)
        R2(jb,ia) = R(       jb,nS_exc+ia)
        R3(jb,ia) = R(nS_exc+jb,       ia)
        R4(jb,ia) = R(nS_exc+jb,nS_exc+ia)

        L1(jb,ia) = L(       jb,       ia)
        L2(jb,ia) = L(       jb,nS_exc+ia)
        L3(jb,ia) = L(nS_exc+jb,       ia)
        L4(jb,ia) = L(nS_exc+jb,nS_exc+ia)
      enddo
    enddo

    deallocate(R, L, Omega)

    ! Compute the TC-RPA correlation energy
    ! We use Omega2 instead of Omega1 because eigenvalued are sorted in ascending order
    Ec = Ec + 0.5d0 * sum(Omega2)

  endif

end

! ---

