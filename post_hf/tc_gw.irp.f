program tc_gw

  implicit none

  print*, " --- start a TC-GW calculation ---"

  my_grid_becke = .True.
  PROVIDE tc_grid1_a tc_grid1_r
  my_n_pt_r_grid = tc_grid1_r
  my_n_pt_a_grid = tc_grid1_a
  touch my_grid_becke my_n_pt_r_grid my_n_pt_a_grid

  print *, ' j2e_type = ', j2e_type
  print *, ' j1e_type = ', j1e_type
  print *, ' env_type = ', env_type

  if(tc_integ_type .eq. "numeric") then
    my_extra_grid_becke  = .True.
    PROVIDE tc_grid2_a tc_grid2_r
    my_n_pt_r_extra_grid = tc_grid2_r
    my_n_pt_a_extra_grid = tc_grid2_a
    touch my_extra_grid_becke my_n_pt_r_extra_grid my_n_pt_a_extra_grid

    call write_int(6, my_n_pt_r_extra_grid, 'radial  internal grid over')
    call write_int(6, my_n_pt_a_extra_grid, 'angular internal grid over')
  endif

  call main()

end

! ---

subroutine main()

  implicit none

  integer                       :: ia, p, it
  logical                       :: conv
  double precision              :: Ec_tot
  double precision              :: Eref, E
  double precision              :: eta
  double precision              :: diff
  double precision              :: t0, t1
  double precision, allocatable :: eTCSCF(:)
  double precision, allocatable :: e0(:), sig_c(:), e_QP(:), Z_norm(:)
  double precision, allocatable :: Omega(:), X1(:,:), Y1(:,:), X2(:,:), Y2(:,:)
  double precision, allocatable :: VR_pqn(:,:,:), VL_pqn(:,:,:)

  call wall_time(t0)

  call ezfio_set_post_hf_spin_adap(.True.)
  call ezfio_set_post_hf_spin_rpa(1)
  call ezfio_set_post_hf_drpa(.True.)

  provide TC_HF_energy
  PROVIDE Fock_matrix_tc_diag_mo_tot
  PROVIDE nS_exc

  eta = 1.d-10

  if(elec_alpha_num .ne. elec_beta_num) then
    print*, ' not implemented yet '
    stop
  endif

  allocate(eTCSCF(mo_num))
  allocate(Omega(nS_exc), X1(nS_exc,nS_exc), Y1(nS_exc,nS_exc), X2(nS_exc,nS_exc), Y2(nS_exc,nS_exc))
  allocate(VR_pqn(mo_num,mo_num,nS_exc), VL_pqn(mo_num,mo_num,nS_exc))
  allocate(e0(mo_num), e_QP(mo_num), sig_c(mo_num), Z_norm(mo_num))

  ! TC-SCG starting point
  eTCSCF(1:mo_num) = Fock_matrix_tc_diag_mo_tot(1:mo_num)
  Eref = TC_HF_energy

  it = 0
  e0(1:mo_num) = eTCSCF(1:mo_num)
  do while(it .le. itmax_gw)

    it = it + 1
    print*, " iteration:", it

    call tc_noL_linear_response(e0, Ec_tot, Omega(1), X1(1,1), Y1(1,1), X2(1,1), Y2(1,1))

    print *, ' Omega (singlet):'
    write(*, '(10000(F15.7, 2X))') (Omega(ia), ia = 1, nS_exc)
    print *, ' Eref      = ', Eref
    print *, ' Ec (dRPA) = ', Ec_tot
    print *, ' Etot      = ', Eref + Ec_tot

    call tc_noL_screened_integ(X1(1,1), Y1(1,1), X2(1,1), Y2(1,1), VR_pqn(1,1,1), VL_pqn(1,1,1))
    call tc_noL_sigmac_val_der_diag(e0(1), eta, Omega(1), VR_pqn(1,1,1), VL_pqn(1,1,1), sig_c(1), Z_norm(1))

    print*, ' -------------------------------------------------------- '
    print*, '   Ref energies      TC-GW energies      normaliz factor  '
    print*, ' -------------------------------------------------------- '
    do p = 1, mo_num
      e_QP(p) = e0(p) + Z_norm(p) * (sig_c(p) + eTCSCF(p) - e0(p))
      write(*, '(3(F15.7,4X))') e0(p), e_QP(p), Z_norm(p)
      diff = max(diff, dabs(e_QP(p) - e0(p)))
    enddo
    diff = 0.d0
    !do p = 1, 2*elec_alpha_num
    do p = 1, mo_num
      diff = max(diff, dabs(e_QP(p) - e0(p)))
    enddo
    print*, " diff =", diff

    if(gw_scf .eq. 1) then
      ! evGW
      if(diff .lt. gw_thresh) then
        conv = .True.
        exit
      endif
      e0 = e_QP
    else
      ! 1-shot
      conv = .True.
      exit
    endif

  enddo ! it

  deallocate(eTCSCF)
  deallocate(e0, e_QP, sig_c, Z_norm)
  deallocate(Omega, X1, Y1, X2, Y2)
  deallocate(VR_pqn, VL_pqn)

  if(conv) then
    print*, " TC-GW calculation has converged :)"
  else
    print*, " TC-GW calculation has NOT converged :\" 
  endif

  call wall_time(t1)
  print*, " WT (min) = ", (t1-t0)/60.d0

end

! ---



