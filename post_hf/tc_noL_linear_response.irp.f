
! ---

subroutine tc_noL_linear_response(e, Ec, Omega1, X1, Y1, X2, Y2)

  implicit none

  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: Ec
  double precision, intent(out) :: Omega1(nS_exc)
  double precision, intent(out) :: X1(nS_exc,nS_exc), Y1(nS_exc,nS_exc)
  double precision, intent(out) :: X2(nS_exc,nS_exc), Y2(nS_exc,nS_exc)

  integer                       :: ia, jb, i, j
  logical                       :: impose_biorthog
  double precision              :: accu_d, accu_nd
  double precision, allocatable :: A(:,:), B(:,:), C(:,:)
  double precision, allocatable :: R(:,:), L(:,:), Omega(:)
  double precision, allocatable :: tmp(:,:)

  PROVIDE spin_adap
  PROVIDE spin_rpa
  PROVIDE dRPA
  PROVIDE TDA
  PROVIDE thresh_biorthog_diag thresh_biorthog_nondiag

  if(dRPA) then
    print *, ' start a TC-dRPA'
  else
    print *, ' not implemented yet'
    stop
  endif
  if(spin_adap) then
    print *, ' spin = ', spin_rpa
  else
    print *, ' not implemented yet'
    stop
  endif

  if(TDA) then

    print *, ' Tamm-Dancoff approximation will be used (TC-noL-CIS)'

    Y1 = 0.d0
    X2 = 0.d0

    call tc_noL_linear_response_Amat(e, X1)

    ! Compute the RPA correlation energy
    Ec = 0.d0
    do ia = 1, nS_exc
      Ec = Ec - 0.5d0 * X1(ia,ia)
    enddo

    call diagonalize_nonsym_matrix(nS_exc, X1, Y2, Omega1, thresh_biorthog_diag, thresh_biorthog_nondiag)

    ! Compute the TC-RPA correlation energy
    Ec = Ec + 0.5d0 * sum(Omega1)

  else

    allocate(A(nS_exc,nS_exc), B(nS_exc,nS_exc), C(nS_exc,nS_exc))
    call tc_noL_linear_response_Amat(e(1), A(1,1))
    call tc_noL_linear_response_Bmat(B(1,1))
    call tc_noL_linear_response_Cmat(C(1,1))

    ! Compute the RPA correlation energy
    Ec = 0.d0
    do ia = 1, nS_exc
      Ec = Ec - 0.5d0 * A(ia,ia)
    enddo

    allocate(R(2*nS_exc,2*nS_exc), L(2*nS_exc,2*nS_exc), Omega(2*nS_exc))
    do ia = 1, nS_exc
      do jb = 1, nS_exc
        R(       jb,       ia) =  A(jb,ia)
        R(       jb,nS_exc+ia) =  B(ia,jb)
        R(nS_exc+jb,       ia) = -C(ia,jb)
        R(nS_exc+jb,nS_exc+ia) = -A(ia,jb)
      enddo
    enddo
    deallocate(A, B, C)

    call diagonalize_nonsym_matrix(2*nS_exc, R(1,1), L(1,1), Omega(1), thresh_biorthog_diag, thresh_biorthog_nondiag)

    do ia = 1, nS_exc

      if(dabs(Omega(ia) + Omega(2*nS_exc-ia+1)) .gt. 1d-12) then
        print*, ' eigenvalues Omega are strange'
        print *, ia, Omega(ia), Omega(2*nS_exc-ia+1)
      endif

      Omega1(ia) = Omega(nS_exc+ia)

      do jb = 1, nS_exc
        !X1(jb,ia) = R(       jb,       ia)
        !Y1(jb,ia) = R(nS_exc+jb,       ia)
        !X2(jb,ia) = R(       jb,nS_exc+ia)
        !Y2(jb,ia) = R(nS_exc+jb,nS_exc+ia)

        !X1(jb,ia) =  R(       jb,ia)
        !Y1(jb,ia) =  R(nS_exc+jb,ia)
        !X2(jb,ia) = -L(nS_exc+jb,ia)
        !Y2(jb,ia) =  L(       jb,ia)

        X1(jb,ia) =  L(nS_exc+jb,nS_exc+ia)
        Y1(jb,ia) = -L(       jb,nS_exc+ia)
        X2(jb,ia) =  R(       jb,nS_exc+ia)
        Y2(jb,ia) =  R(nS_exc+jb,nS_exc+ia)
      enddo
    enddo

    deallocate(R, L, Omega)

    ! Compute the TC-RPA correlation energy
    Ec = Ec + 0.5d0 * sum(Omega1)

    ! check normalization
    allocate(tmp(nS_exc,nS_exc))

    call dgemm( "T", "N", nS_exc, nS_exc, nS_exc, 1.d0 &
              , Y2(1,1), nS_exc, X1(1,1), nS_exc       &
              , 0.d0, tmp(1,1), nS_exc)
    call dgemm( "T", "N", nS_exc, nS_exc, nS_exc, 1.d0 &
              , X2(1,1), nS_exc, Y1(1,1), nS_exc       &
              , -1.d0, tmp(1,1), nS_exc)
    do i = 1, nS_exc
      if(dabs(tmp(i,i)+1.d0) .lt. thresh_biorthog_diag) then
        X1(:,i) *= -1.d0
        Y1(:,i) *= -1.d0
      endif
    enddo
    call dgemm( "T", "N", nS_exc, nS_exc, nS_exc, 1.d0 &
              , Y2(1,1), nS_exc, X1(1,1), nS_exc       &
              , 0.d0, tmp(1,1), nS_exc)
    call dgemm( "T", "N", nS_exc, nS_exc, nS_exc, 1.d0 &
              , X2(1,1), nS_exc, Y1(1,1), nS_exc       &
              , -1.d0, tmp(1,1), nS_exc)
    accu_d  = 0.d0
    accu_nd = 0.d0
    do i = 1, nS_exc
      if(dabs(tmp(i,i)-1.d0) .gt. thresh_biorthog_diag) then
        print *, ' problem in diag-normalization of Y2.T X1 - X2.T Y1'
        print *, i, tmp(i,i)
        if(imp_bio) stop
      endif
      do j = 1, nS_exc
        if(i==j) then
          accu_d = accu_d + tmp(i,i)
        else
          accu_nd = accu_nd + tmp(j,i) * tmp(j,i)
        endif
      enddo
    enddo
    accu_d  = accu_d / dble(nS_exc)
    accu_nd = dsqrt(accu_nd) / dble(nS_exc)
    print*, ' Y2.T X1 - X2.T Y1 ', accu_d, accu_nd
    if(accu_nd.gt.thresh_biorthog_nondiag) then
      print *, ' problem in nondiag-normalization of Y2.T X1 - X2.T Y1', accu_nd
      if(imp_bio) stop
    endif

    call dgemm( "T", "N", nS_exc, nS_exc, nS_exc, 1.d0 &
              , Y2(1,1), nS_exc, X2(1,1), nS_exc       &
              , 0.d0, tmp(1,1), nS_exc)
    call dgemm( "T", "N", nS_exc, nS_exc, nS_exc, 1.d0 &
              , X2(1,1), nS_exc, Y2(1,1), nS_exc       &
              , -1.d0, tmp(1,1), nS_exc)
    accu_nd = 0.d0
    do i = 1, nS_exc
      do j = 1, nS_exc
        accu_nd = accu_nd + tmp(j,i) * tmp(j,i)
      enddo
    enddo
    accu_nd = dsqrt(accu_nd) / dble(nS_exc)
    print*, ' Y2.T X2 - X2.T Y2 ', accu_nd
    if(accu_nd.gt.thresh_biorthog_nondiag) then
      print *, ' problem in nondiag-normalization of Y2.T X2 - X2.T Y2', accu_nd
      if(imp_bio) stop
    endif

    call dgemm( "T", "N", nS_exc, nS_exc, nS_exc, 1.d0 &
              , Y1(1,1), nS_exc, X1(1,1), nS_exc       &
              , 0.d0, tmp(1,1), nS_exc)
    call dgemm( "T", "N", nS_exc, nS_exc, nS_exc, 1.d0 &
              , X1(1,1), nS_exc, Y1(1,1), nS_exc       &
              , -1.d0, tmp(1,1), nS_exc)
    accu_nd = 0.d0
    do i = 1, nS_exc
      do j = 1, nS_exc
        accu_nd = accu_nd + tmp(j,i) * tmp(j,i)
      enddo
    enddo
    accu_nd = dsqrt(accu_nd) / dble(nS_exc)
    print*, ' Y1.T X1 - X1.T Y1 ', accu_nd
    if(accu_nd.gt.thresh_biorthog_nondiag) then
      print *, ' problem in nondiag-normalization of Y1.T X1 - X1.T Y1', accu_nd
      if(imp_bio) stop
    endif

    deallocate(tmp)

  endif

end

! ---

