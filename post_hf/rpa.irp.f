program rpa

  BEGIN_DOC
  !
  ! run a dRPA or RPAx calculation
  !
  END_DOC

  implicit none

  !call main()
  call compute_ERPA()

end

! ---

subroutine main()

  implicit none
  integer                       :: ia
  double precision              :: Ec
  double precision, allocatable :: e0(:)
  double precision, allocatable :: Omega_tmp(:), XpY_tmp(:,:), XmY_tmp(:,:)

  PROVIDE nS_exc
  PROVIDE spin_adap
  PROVIDE spin_rpa

  print*, " start RPA of dimension:", nS_exc

  PROVIDE Fock_matrix_diag_mo
  allocate(e0(mo_num))
  e0(1:mo_num) = Fock_matrix_diag_mo(1:mo_num)

  allocate(Omega_tmp(nS_exc), XpY_tmp(nS_exc,nS_exc), XmY_tmp(nS_exc,nS_exc))
  call linear_response(e0, Ec, Omega_tmp, XpY_tmp, XmY_tmp)

  print *, ' RPA correlation energy =', Ec
  print*, ' Omega RPA:'
  do ia = 1, nS_exc
    print*, Omega_tmp(ia)
  enddo

  deallocate(Omega_tmp, XpY_tmp, XmY_tmp)

end

! ---

subroutine compute_ERPA()

  implicit none
  integer                       :: ia
  double precision              :: Ec_sing, Ec_trip, Ec_tot
  double precision              :: Eref, E
  double precision, allocatable :: e0(:)
  double precision, allocatable :: Omega_tmp(:), XpY_tmp(:,:), XmY_tmp(:,:)

  provide SCF_energy
  PROVIDE nS_exc
  PROVIDE spin_adap
  PROVIDE Fock_matrix_diag_mo

  allocate(e0(mo_num))
  e0(1:mo_num) = Fock_matrix_diag_mo(1:mo_num)

  Eref = SCF_energy
 
  if(spin_adap) then

    PROVIDE drpa

    if(drpa) then

      allocate(Omega_tmp(nS_exc), XpY_tmp(nS_exc,nS_exc), XmY_tmp(nS_exc,nS_exc))

      spin_rpa = 1
      TOUCH spin_rpa
      print*, " start dRPA of dimension", nS_exc
      print*, " spin = ", spin_rpa

      call linear_response(e0, Ec_sing, Omega_tmp(1), XpY_tmp(1,1), XmY_tmp(1,1))

      print *, ' Ec_sing = ', Ec_sing
      print*, ' Omega (singlet):'
      write(*, '(10000(F15.7, 2X))') (Omega_tmp(ia), ia = 1, nS_exc)

      ! ---

      spin_rpa = 3
      TOUCH spin_rpa
      print*, " start dRPA of dimension", nS_exc
      print*, " spin = ", spin_rpa

      call linear_response(e0, Ec_trip, Omega_tmp(1), XpY_tmp(1,1), XmY_tmp(1,1))

      print *, ' Ec_trip = ', 3.d0 * Ec_trip
      print*, ' Omega (triplet):'
      write(*, '(10000(F15.7, 2X))') (Omega_tmp(ia), ia = 1, nS_exc)

      deallocate(Omega_tmp, XpY_tmp, XmY_tmp)

      Ec_tot = Ec_sing + 3.0d0 * Ec_trip
 
      print *, ' Eref = ', Eref
      print *, ' Ec   = ', Ec_tot
      print *, ' Etot = ', Eref + Ec_tot

    else

      allocate(Omega_tmp(nS_exc), XpY_tmp(nS_exc,nS_exc), XmY_tmp(nS_exc,nS_exc))

      spin_rpa = 1
      TOUCH spin_rpa
      print*, " start RPAx of dimension", nS_exc
      print*, " spin = ", spin_rpa

      call linear_response(e0, Ec_sing, Omega_tmp(1), XpY_tmp(1,1), XmY_tmp(1,1))

      print *, ' Ec_sing = ', Ec_sing
      print*, ' Omega (singlet):'
      write(*, '(10000(F15.7, 2X))') (Omega_tmp(ia), ia = 1, nS_exc)

      ! ---

      spin_rpa = 3
      TOUCH spin_rpa
      print*, " start RPAx of dimension", nS_exc
      print*, " spin = ", spin_rpa

      call linear_response(e0, Ec_trip, Omega_tmp(1), XpY_tmp(1,1), XmY_tmp(1,1))

      print *, ' Ec_trip = ', 3.d0 * Ec_trip
      print*, ' Omega (triplet):'
      write(*, '(10000(F15.7, 2X))') (Omega_tmp(ia), ia = 1, nS_exc)

      deallocate(Omega_tmp, XpY_tmp, XmY_tmp)

      Ec_tot = Ec_sing + 3.0d0 * Ec_trip
 
      print *, ' Eref = ', Eref
      print *, ' Ec   = ', Ec_tot, 0.5d0*Ec_tot
      print *, ' Etot = ', Eref + Ec_tot, Eref + 0.5d0*Ec_tot

    endif

  else

    print*, " start RPA of dimension:", nS_exc

    allocate(Omega_tmp(nS_exc), XpY_tmp(nS_exc,nS_exc), XmY_tmp(nS_exc,nS_exc))
    call linear_response(e0, Ec_tot, Omega_tmp(1), XpY_tmp(1,1), XmY_tmp(1,1))

    print*, ' Omega:'
    write(*, '(10000(F15.7, 2X))') (Omega_tmp(ia), ia = 1, nS_exc)

    deallocate(Omega_tmp, XpY_tmp, XmY_tmp)

    print *, ' Eref = ', Eref
    if(.not. drpa) then
      print *, ' Ec   = ', Ec_tot, 0.5d0*Ec_tot
      print *, ' Etot = ', Eref + Ec_tot, Eref + 0.5d0*Ec_tot
    else
      print *, ' Ec   = ', Ec_tot
      print *, ' Etot = ', Eref + Ec_tot
    endif

  endif

end

