
! ---

BEGIN_PROVIDER [double precision, ERI_AO, (ao_num, ao_num, ao_num, ao_num)]

  implicit none
  integer                    :: i, j, k, l
  double precision           :: integral
  double precision           :: t1, t2
  double precision, external :: get_ao_two_e_integral

  print *, ' PROVIDING ERI_AO ...'
  call print_memory_usage()
  call wall_time(t1)

  PROVIDE ao_integrals_map
  integral = get_ao_two_e_integral(1, 1, 1, 1, ao_integrals_map)

  !$OMP PARALLEL DEFAULT(NONE)                   &
  !$OMP SHARED(ao_num, ERI_AO, ao_integrals_map) &
  !$OMP PRIVATE(i, j, k, l)
  !$OMP DO
  do j = 1, ao_num
    do i = 1, ao_num
      do l = 1, ao_num
        do k = 1, ao_num

          !  < 1:k, 2:l | 1:i, 2:j > =  < 1:i, 2:j | 1:k, 2:l > 
          ERI_AO(k,l,i,j) = get_ao_two_e_integral(i, j, k, l, ao_integrals_map)
        enddo
      enddo
    enddo
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  call clear_ao_map()

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING ERI_AO (min)', (t2-t1)/60.d0
  call print_memory_usage()

END_PROVIDER

! ---

BEGIN_PROVIDER [double precision, ERI_MO, (mo_num, mo_num, mo_num, mo_num)]

  implicit none
  integer                       :: p, q, r, s, l
  double precision              :: t1, t2, tt1, tt2
  double precision, allocatable :: a1(:,:,:,:), a2(:,:,:,:)
  double precision, allocatable :: a_jkp(:,:,:), a_kpq(:,:,:), a_pqr(:,:,:)

  print *, ' PROVIDING ERI_MO ...'
  call print_memory_usage()
  call wall_time(t1)

  PROVIDE ERI_AO
  PROVIDE mo_coef

  if(ao_to_mo_n3) then

    print*, ' memory scale of ao -> mo: O(N3) '

    allocate(a_jkp(ao_num,ao_num,mo_num))
    allocate(a_kpq(ao_num,mo_num,mo_num))
    allocate(a_pqr(mo_num,mo_num,mo_num))

    call wall_time(tt1)
    do s = 1, mo_num

      ERI_MO(:,:,:,s) = 0.d0
      do l = 1, ao_num

        call dgemm( 'T', 'N', ao_num*ao_num, mo_num, ao_num, 1.d0 &
                  , ERI_AO(1,1,1,l), ao_num, mo_coef(1,1), ao_num &
                  , 0.d0, a_jkp(1,1,1), ao_num*ao_num)

        call dgemm( 'T', 'N', ao_num*mo_num, mo_num, ao_num, 1.d0 &
                  , a_jkp(1,1,1), ao_num, mo_coef(1,1), ao_num    &
                  , 0.d0, a_kpq(1,1,1), ao_num*mo_num)

        call dgemm( 'T', 'N', mo_num*mo_num, mo_num, ao_num, 1.d0 &
                  , a_kpq(1,1,1), ao_num, mo_coef(1,1), ao_num    &
                  , 0.d0, a_pqr(1,1,1), mo_num*mo_num)

        !$OMP PARALLEL         &
        !$OMP DEFAULT(NONE)    &
        !$OMP PRIVATE(p, q, r) &
        !$OMP SHARED(s, l, mo_num, ERI_MO, mo_coef, a_pqr)
        !$OMP DO COLLAPSE(2)
        do p = 1, mo_num
          do q = 1, mo_num
            do r = 1, mo_num
              ERI_MO(p,q,r,s) = ERI_MO(p,q,r,s) + mo_coef(l,s) * a_pqr(p,q,r)
            enddo
          enddo
        enddo
        !$OMP END DO
        !$OMP END PARALLEL

      enddo ! l

      if(s == 2) then
        call wall_time(tt2)
        print*, ' 1 / mo_num done in (min)', (tt2-tt1)/60.d0
        print*, ' estimated time required (min)', dble(mo_num-1)*(tt2-tt1)/60.d0
      elseif(s == 11) then
        call wall_time(tt2)
        print*, ' 10 / mo_num done in (min)', (tt2-tt1)/60.d0
        print*, ' estimated time required (min)', dble(mo_num-10)*(tt2-tt1)/(60.d0*10.d0)
      elseif(s == 26) then
        call wall_time(tt2)
        print*, ' 25 / mo_num done in (min)', (tt2-tt1)/60.d0
        print*, ' estimated time required (min)', dble(mo_num-25)*(tt2-tt1)/(60.d0*25.d0)
      elseif(s == 51) then
        call wall_time(tt2)
        print*, ' 50 / mo_num done in (min)', (tt2-tt1)/60.d0
        print*, ' estimated time required (min)', dble(mo_num-50)*(tt2-tt1)/(60.d0*50.d0)
      elseif(s == 101) then
        call wall_time(tt2)
        print*, ' 100 / mo_num done in (min)', (tt2-tt1)/60.d0
        print*, ' estimated time required (min)', dble(mo_num-100)*(tt2-tt1)/(60.d0*100.d0)
      elseif(s == 201) then
        call wall_time(tt2)
        print*, ' 200 / mo_num done in (min)', (tt2-tt1)/60.d0
        print*, ' estimated time required (min)', dble(mo_num-200)*(tt2-tt1)/(60.d0*200.d0)
      elseif(s == 501) then
        call wall_time(tt2)
        print*, ' 500 / mo_num done in (min)', (tt2-tt1)/60.d0
        print*, ' estimated time required (min)', dble(mo_num-500)*(tt2-tt1)/(60.d0*500.d0)
      endif

    enddo ! s

    deallocate(a_jkp, a_kpq, a_pqr)

  else

    print*, ' memory scale of ao -> mo: O(N4) '

    allocate(a2(ao_num,ao_num,ao_num,mo_num))
  
    call wall_time(tt1)
  
    call dgemm( 'T', 'N', ao_num*ao_num*ao_num, mo_num, ao_num, 1.d0 &
              , ERI_AO(1,1,1,1), ao_num, mo_coef(1,1), ao_num        &
              , 0.d0 , a2(1,1,1,1), ao_num*ao_num*ao_num)
  
    call wall_time(tt2)
    print *, ' 1st transformation done after (min):', (tt2-tt1) / 60.d0
  
    allocate(a1(ao_num,ao_num,mo_num,mo_num))
  
    call wall_time(tt1)
  
    call dgemm( 'T', 'N', ao_num*ao_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_coef(1,1), ao_num            &
              , 0.d0, a1(1,1,1,1), ao_num*ao_num*mo_num)
  
    call wall_time(tt2)
    print *, ' 2nd transformation done after (min):', (tt2-tt1) / 60.d0
  
    deallocate(a2)
    allocate(a2(ao_num,mo_num,mo_num,mo_num))
  
    call wall_time(tt1)
  
    call dgemm( 'T', 'N', ao_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a1(1,1,1,1), ao_num, mo_coef(1,1), ao_num            &
              , 0.d0, a2(1,1,1,1), ao_num*mo_num*mo_num)
  
    call wall_time(tt2)
    print *, ' 3rd transformation done after (min):', (tt2-tt1) / 60.d0
  
    deallocate(a1)
  
    call wall_time(tt1)
  
    call dgemm( 'T', 'N', mo_num*mo_num*mo_num, mo_num, ao_num, 1.d0 &
              , a2(1,1,1,1), ao_num, mo_coef(1,1), ao_num            &
              , 0.d0, ERI_MO(1,1,1,1), mo_num*mo_num*mo_num)
  
    call wall_time(tt2)
    print *, ' 4th transformation done after (min):', (tt2-tt1) / 60.d0
  
    deallocate(a2)

  endif

  call wall_time(t2)
  print *, ' WALL TIME for PROVIDING ERI_MO (min)', (t2-t1)/60.d0
  call print_memory_usage()

END_PROVIDER

! ---

double precision function ERI_spinorb(p, sigma_p, q, sigma_q, r, sigma_r, s, sigma_s)

  BEGIN_DOC
  !
  ! < p q | r s > in spin-orbital
  ! 
  END_DOC 

  implicit none
  integer,          intent(in) :: p, q, r, s
  double precision, intent(in) :: sigma_p, sigma_q, sigma_r, sigma_s

  logical,          external   :: is_same_spin
  double precision, external   :: get_two_e_integral
  
  !PROVIDE ERI_MO
  PROVIDE mo_integrals_map

  ERI_spinorb = 0.d0
  if(is_same_spin(sigma_p, sigma_r)) then
    if(is_same_spin(sigma_q, sigma_s)) then
      !ERI_spinorb = ERI_MO(p,q,r,s)
      ERI_spinorb = get_two_e_integral(p, q, r, s, mo_integrals_map)
    endif
  endif

  return

end

! ---

