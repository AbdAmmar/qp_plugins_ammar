program gw

  implicit none

  print*, " --- start a GW calculation ---"

  call main()

end

! ---

subroutine main()

  implicit none
  integer                       :: ia, p, it
  logical                       :: conv
  double precision              :: Ec_tot
  double precision              :: Eref, E
  double precision              :: eta
  double precision              :: diff
  double precision              :: t0, t1
  double precision, allocatable :: eHF(:)
  double precision, allocatable :: e0(:), sig_c(:), e_QP(:), Z_norm(:)
  double precision, allocatable :: Omega(:), XpY(:,:), XmY(:,:)
  double precision, allocatable :: V_pqn(:,:,:)

  call wall_time(t0)

  call ezfio_set_post_hf_spin_adap(.True.)
  call ezfio_set_post_hf_spin_rpa(1)
  call ezfio_set_post_hf_drpa(.True.)

  provide SCF_energy
  PROVIDE Fock_matrix_diag_mo
  PROVIDE nS_exc

  eta = 1.d-10

  if(elec_alpha_num .ne. elec_beta_num) then
    print*, ' not implemented yet '
    stop
  endif

  allocate(eHF(mo_num))
  allocate(Omega(nS_exc), XpY(nS_exc,nS_exc), XmY(nS_exc,nS_exc))
  allocate(V_pqn(mo_num,mo_num,nS_exc))
  allocate(e0(mo_num), e_QP(mo_num), sig_c(mo_num), Z_norm(mo_num))

  ! HF starting point
  eHF(1:mo_num) = Fock_matrix_diag_mo(1:mo_num)
  Eref = SCF_energy

  it = 0
  e0(1:mo_num) = eHF(1:mo_num)
  do while(it .le. itmax_gw)

    it = it + 1
    print*, " iteration:", it

    call linear_response(e0, Ec_tot, Omega(1), XpY(1,1), XmY(1,1))

    print *, ' Omega (singlet):'
    write(*, '(10000(F15.7, 2X))') (Omega(ia), ia = 1, nS_exc)
    print *, ' Eref      = ', Eref
    print *, ' Ec (dRPA) = ', Ec_tot
    print *, ' Etot      = ', Eref + Ec_tot

    call screened_integ(XpY(1,1), V_pqn(1,1,1))
    call sigmac_val_der_diag(e0(1), eta, Omega(1), V_pqn(1,1,1), sig_c(1), Z_norm(1)) 

    print*, ' -------------------------------------------------------- '
    print*, '   Ref energies        GW energies       normaliz factor  '
    print*, ' -------------------------------------------------------- '
    do p = 1, mo_num
      e_QP(p) = e0(p) + Z_norm(p) * (sig_c(p) + eHF(p) - e0(p))
      write(*, '(3(F15.7,4X))') e0(p), e_QP(p), Z_norm(p)
    enddo
    diff = 0.d0
    !do p = 1, 2*elec_alpha_num
    do p = 1, mo_num
      diff = max(diff, dabs(e_QP(p) - e0(p)))
    enddo
    print*, " diff =", diff

    if(gw_scf .eq. 1) then
      ! evGW
      if(diff .lt. gw_thresh) then
        conv = .True.
        exit
      endif
      e0 = e_QP
    else
      ! 1-shot
      conv = .True.
      exit
    endif

  enddo ! it

  deallocate(eHF)
  deallocate(Omega, XpY, XmY)
  deallocate(V_pqn)
  deallocate(e0, e_QP, sig_c, Z_norm)

  if(conv) then
    print*, " GW calculation has converged :)"
  else
    print*, " GW calculation has NOT converged :\" 
  endif

  call wall_time(t1)
  print*, " WT (min) = ", (t1-t0)/60.d0

end


