
! ---

subroutine sigmac_val_der_diag(e0, eta, Omega, V_pqn, sig_c, Z_norm)

  implicit none
  double precision, intent(in)  :: e0(mo_num), eta
  double precision, intent(in)  :: Omega(nS_exc)
  double precision, intent(in)  :: V_pqn(mo_num,mo_num,nS_exc)
  double precision, intent(out) :: sig_c(mo_num), Z_norm(mo_num)

  integer                       :: i, j, a, b, p, n, ia, jb
  double precision              :: tmp, tmp2
  double precision              :: dif, dif2
  double precision              :: eta2
  double precision              :: tmp_sig, tmp_Z

  eta2 = eta * eta

  !$OMP PARALLEL                                  &
  !$OMP DEFAULT(NONE)                             &
  !$OMP PRIVATE(p, n, i, a, tmp, tmp2, dif, dif2, &
  !$OMP         tmp_sig, tmp_Z)                   &
  !$OMP SHARED(mo_num, nS_exc, elec_alpha_num,    &
  !$OMP        V_pqn, e0, Omega, eta2, sig_c, Z_norm)
  !$OMP DO
  do p = 1, mo_num 

    tmp_sig = 0.d0
    tmp_Z   = 0.d0

    do n = 1, nS_exc

      do i = 1, elec_alpha_num
        !jb = 0
        !tmp = 0.d0
        !do j = 1, elec_alpha_num
        !  do b = elec_alpha_num+1, mo_num
        !    jb = jb + 1
        !    tmp += XpY(jb,n) * get_two_e_integral(p, j, i, b, mo_integrals_map)
        !  enddo
        !enddo
        tmp  = V_pqn(i,p,n)
        tmp2 = tmp * tmp
        dif  = e0(p) + Omega(n) - e0(i)
        dif2 = dif * dif

        tmp_sig += 2.d0 * tmp2 * dif / (dif2 + eta2)
        tmp_Z   -= 2.d0 * tmp2 * (dif2 - eta2) / ((dif2 - eta2)**2 + 4.d0*dif2*eta2)
      enddo ! i

      do a = elec_alpha_num+1, mo_num
        !jb = 0
        !tmp = 0.d0
        !do j = 1, elec_alpha_num
        !  do b = elec_alpha_num+1, mo_num
        !    jb = jb + 1
        !    tmp += XpY(jb,n) * get_two_e_integral(p, j, a, b, mo_integrals_map)
        !  enddo
        !enddo
        tmp  = V_pqn(a,p,n)
        tmp2 = tmp * tmp
        dif  = e0(p) - Omega(n) - e0(a)
        dif2 = dif * dif

        tmp_sig += 2.d0 * tmp2 * dif / (dif2 + eta2)
        tmp_Z   -= 2.d0 * tmp2 * (dif2 - eta2) / ((dif2 - eta2)**2 + 4.d0*dif2*eta2)
      enddo ! a

    enddo ! n

    sig_c (p) = tmp_sig
    Z_norm(p) = 1.d0 / (1.d0 - tmp_Z)
  enddo ! p
  !$OMP END DO
  !$OMP END PARALLEL

  return
end

! ---

subroutine screened_integ(XpY, V_pqn)

  implicit none
  double precision, intent(in)  :: XpY(nS_exc,nS_exc)
  double precision, intent(out) :: V_pqn(mo_num,mo_num,nS_exc)

  integer                       :: p, q, i, a, ia, n
  double precision              :: tmp, integral
  double precision, external    :: get_two_e_integral

  PROVIDE mo_integrals_map
  integral = get_two_e_integral(1, 1, 1, 1, mo_integrals_map)

  !$OMP PARALLEL                               &
  !$OMP DEFAULT(NONE)                          &
  !$OMP PRIVATE(p, q, n, i, a, ia, tmp)        &
  !$OMP SHARED(mo_num, nS_exc, elec_alpha_num, &
  !$OMP        XpY, mo_integrals_map, V_pqn)
  !$OMP DO COLLAPSE(3)
  do p = 1, mo_num
    do q = 1, mo_num
      do n = 1, nS_exc

        ia = 0
        tmp = 0.d0
        do i = 1, elec_alpha_num
          do a = elec_alpha_num+1, mo_num
            ia = ia + 1
            tmp += XpY(ia,n) * get_two_e_integral(p, i, q, a, mo_integrals_map)
          enddo
        enddo

        V_pqn(p,q,n) = tmp
      enddo ! n
    enddo ! q
  enddo ! p
  !$OMP END DO
  !$OMP END PARALLEL

  return
end

! ---

