
! ---

subroutine tc_noL_linear_response_Amat(e, A_lr)

  implicit none
  double precision, intent(in)  :: e(mo_num)
  double precision, intent(out) :: A_lr(nS_exc,nS_exc)

  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2
  double precision, external    :: Kronecker_delta

  PROVIDE nS_exc
  PROVIDE spin_adap

  call wall_time(t1)

  if(.not.spin_adap) then
    print *, ' not implemented yet'
    stop
  endif

  PROVIDE spin_rpa
  PROVIDE dRPA

  if(dRPA) then

    PROVIDE eff_tcx_mo_chem

    ! --- ---
    ! dRPA
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building A matrix for TC-noL-dRPA: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              A_lr(ia,jb) = (e(a) - e(i)) * Kronecker_delta(i, j) * Kronecker_delta(a, b) + 2.d0 * EFF_TCx_MO_chem(a,i,j,b)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print *, ' not implemented yet'
      stop

    endif

    ! --- ---
    ! --- ---

  else

    print *, ' not implemented yet'
    stop

  endif

  call wall_time(t2)
  print *, " WALL TIME for TC-Amat (min)", (t2-t1)/60.d0

  return
end

! ---

subroutine tc_noL_linear_response_Bmat(B_lr)

  implicit none

  double precision, intent(out) :: B_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2

  call wall_time(t1)

  PROVIDE nS_exc
  PROVIDE spin_adap
  if(.not.spin_adap) then
    print *, ' not implemented yet'
    stop
  endif

  PROVIDE spin_rpa
  PROVIDE dRPA

  if(dRPA) then

    PROVIDE EFF_TCx_MO_chem

    ! --- ---
    ! dRPA
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building B matrix for TC-noL-dRPA: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              B_lr(ia,jb) = 2.d0 * EFF_TCx_MO_chem(a,i,b,j)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print *, ' not implemented yet'
      stop

    endif

    ! --- ---
    ! --- ---

  else

    print *, ' not implemented yet'
    stop

  endif

  call wall_time(t2)
  print *, " WALL TIME for TC-Bmat (min)", (t2-t1)/60.d0

  return
end

! ---

subroutine tc_noL_linear_response_Cmat(C_lr)

  implicit none

  double precision, intent(out) :: C_lr(nS_exc,nS_exc)
  
  integer                       :: i, j, a, b, ia, jb
  double precision              :: t1, t2

  call wall_time(t1)

  PROVIDE nS_exc
  PROVIDE spin_adap
  if(.not.spin_adap) then
    print *, ' not implemented yet'
    stop
  endif

  PROVIDE spin_rpa
  PROVIDE dRPA

  if(dRPA) then

    PROVIDE eff_tcx_mo_chem

    ! --- ---
    ! dRPA
    ! --- ---

    if(spin_rpa == 1) then 

      print*, ' Building C matrix for TC-noL-dRPA: singlet manifold'

      ia = 0
      do i = 1, elec_beta_num
        do a = elec_beta_num+1, mo_num
          ia = ia + 1
          jb = 0
          do j = 1, elec_beta_num
            do b = elec_beta_num+1, mo_num
              jb = jb + 1
 
              C_lr(ia,jb) = 2.d0 * eff_tcx_mo_chem(i,a,j,b)
            enddo
          enddo
        enddo
      enddo

    elseif(spin_rpa == 3) then

      print *, ' not implemented yet'
      stop

    endif

    ! --- ---
    ! --- ---

  else

    print *, ' not implemented yet'
    stop

  endif

  call wall_time(t2)
  print *, " WALL TIME for TC-Bmat (min)", (t2-t1)/60.d0

  return
end

! ---

